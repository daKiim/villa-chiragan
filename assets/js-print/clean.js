function clean(content) {

    /* Delete .img-comparaison */
    let imgComparaison = content.querySelectorAll(".img-comparaison");
    for (let i = 0; i < imgComparaison.length; i++) {
        imgComparaison[i].parentNode.removeChild(imgComparaison[i]);
    }

    /* aria-hidden */
    let printElems = content.querySelectorAll(".print");
    for (let p = 0; p < printElems.length; p++) {
        printElems[p].setAttribute('aria-hidden', 'true');
    }


}