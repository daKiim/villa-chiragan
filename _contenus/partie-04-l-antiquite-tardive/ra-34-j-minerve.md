---
title: Minerve
id_inventaire: Ra 34 j
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup>-premier tiers du IV<sup>e</sup> siècle
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: "79"
largeur: "76"
longueur: 
profondeur: "42"
epaisseur: 
id_wikidata: Q27824574
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 280
priority: 3
id_notice: ra-34-j
image_principale: ra-34-j-3
vues: 5
image_imprime: 
redirect_from:
- ra-34-j
- ark:/87276/a_ra_34_j
id_ark: a_ra_34_j
precisions_biblio:
- clef: beckmann_idiom_2020
  note: p. 138, fig. 4
- clef: cazes_musee_1999-1
  note: p. 85
- clef: du_mege_description_1835
  note: n° 156
- clef: du_mege_notice_1828
  note: n° 69
- clef: esperandieu_recueil_1908
  note: n° 11
- clef: rachou_catalogue_1912
  note: n° 34 j
- clef: roschach_catalogue_1892
  note: n° 34 d

---
Déesse de l’artisanat, de la raison, de l’intelligence et de la guerre, Minerve est assimilée à la déesse grecque Athéna qui était sortie toute armée de la tête de son père, Zeus. Elle porte ici un casque basculé vers l’arrière dont la visière est percée de deux trous, à l’emplacement des yeux. Le buste de la déesse est protégé par l’égide, une cuirasse en peau de chèvre bordée de serpents et ornée de la tête de la Gorgone Méduse qui avait le pouvoir de pétrifier ceux qui croisaient sont regard. Souvent figurée sur les cuirasses, elle était considérée comme protectrice.

P. Capus