---
title: Plaque de décor
id_inventaire: 2000.176.1
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: III<sup>e</sup>-IV<sup>e</sup> siècle
materiau: '"Marbre griotte" de Campan'
hauteur: "24"
largeur: "29"
longueur: 
profondeur: "3"
epaisseur: ''
id_wikidata: Q48572497
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 6
order: 410
priority: 3
id_notice: 2000-176-1
image_principale: 2000-176-1-1
vues: 1
image_imprime: 
redirect_from:
- 2000-176-1
- ark:/87276/a_2000_176_1
id_ark: a_2000_176_1
precisions_biblio:
- clef: cazes_musee_1999-1
  note: ''
- clef: joulin_les_1901
  note: p. 83, pl. V, n° 27

---
