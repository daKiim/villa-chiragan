---
title: Esculape
id_inventaire: Ra 34 m
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: "78"
largeur: "75"
longueur: 
profondeur: "38"
epaisseur: 
id_wikidata: Q25212930
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 240
priority: 3
id_notice: ra-34-m
image_principale: ra-34-m-3
vues: 5
image_imprime: 
redirect_from:
- ra-34-m
- ark:/87276/a_ra_34_m
id_ark: a_ra_34_m
precisions_biblio:
- clef: beckmann_idiom_2020
  note: p. 139
- clef: cazes_musee_1999-1
  note: p. 84
- clef: centro_de_exposiciones_arte_canal_roma_2007
  note: p. 150, n° 82
- clef: du_mege_description_1835
  note: n° 152
- clef: du_mege_notice_1828
  note: n° 65
- clef: esperandieu_recueil_1908
  note: p. 33, n° 892, fig. 12
- clef: joulin_les_1901
  note: fig. 56 B
- clef: landes_dieux_1992
  note: n° 3
- clef: long_twelve_1987
  note: p. 10, 285-286
- clef: musee_saint-raymond_essentiel_2011
  note: p. 26-27
- clef: pierron_medaillons_1992
  note: ''
- clef: rachou_catalogue_1912
  note: n° 34 m
- clef: roschach_catalogue_1892
  note: n° 34 a

---
Esculape (l’Asclépios grec) était le dieu de la médecine. Il soulageait la souffrance et ressuscitait les morts. Le serpent sculpté au-dessus de son épaule droite est son attribut ; il fait référence à des forces bénéfiques et sa mue est le symbole de sa renaissance permanente. Parfois enroulé autour d’un bâton, ce symbole est aujourd’hui l’emblème des professions médicales.

Les yeux d’Esculape, saillants et couverts en partie par de larges paupières supérieures tombantes, sont couronnés d'arcades sourcilières à angle aigu. Au-dessus du front, deux grandes boucles sinueuses, parfaitement symétriques, en forme de S, sont disposées de part et d'autre de la division centrale de la chevelure. De longues boucles généreuses couvrent les oreilles ou retombent à l'arrière jusqu'à la nuque. La barbe est encore plus abondante. Divisée verticalement et très profondément, à l'aide du trépan, les enroulements y sont divisés horizontalement au ciseau et s'achèvent à l'avant par deux spirales qui, au niveau de la pomme d'Adam, s'entremêlent. Les autres médaillons figurant des divinités masculines ont reçu un traitement similaire. C’est une caractéristique du travail de l’atelier venu exécuter cette commande de prestige.

Esculape était déjà présent à Chiragan, sous la forme d'une [statuette](/ra-41), antérieure d'au moins deux siècles à la création du groupe des effigies divines sur médaillon. Ainsi les dieux sauveurs, Dionysos, Isis, Sarapis, Hygie ou encore Esculape, garants de la survie, sur terre comme dans l'au-delà, représentent-ils une proportion particulièrement importante au sein de la _villa_.

P. Capus