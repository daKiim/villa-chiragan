---
title: Hercule et les écuries d'Augias
id_inventaire: Ra 28 j
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: "138"
largeur: "85"
longueur: 
profondeur: "20"
epaisseur: 
id_wikidata: Q24662037
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 80
priority: 3
id_notice: ra-28-j
image_principale: ra-28-j-1
vues: 1
image_imprime: 
redirect_from:
- ra-28-j
- ark:/87276/a_ra_28_j
id_ark: a_ra_28_j
precisions_biblio:
- clef: bergmann_chiragan_1999
  note: ''
- clef: cazes_musee_1999-1
  note: p. 90-91
- clef: du_mege_description_1835
  note: n° 173, 178
- clef: du_mege_notice_1828
  note: n° 83, 86
- clef: esperandieu_recueil_1908
  note: n° 899
- clef: joulin_les_1901
  note: n° 101
- clef: massendari_haute-garonne_2006-1
  note: p. 251, fig. 129
- clef: mesple_raccords_1948
  note: p. 157
- clef: rachou_catalogue_1912
  note: n° 28 j
- clef: roschach_catalogue_1865
  note: n° 28 d
- clef: sylloge_nummorum_nodate
  note: ''
- clef: ziegle_hercule_2002
  note: p. 13

---
Cinquième des douze Travaux, l'épisode des écuries se déroule dans le royaume d'Élide, à l'ouest du Péloponnèse. Le roi Augias, fils du Soleil (Hélios), possédait d’immenses troupeaux. Mais il négligeait de faire entretenir ses écuries au point que l’accumulation du fumier rendait les terres environnantes stériles. Eurysthée voulut humilier Héraclès/Hercule en lui confiant cette tâche, habituellement réservée aux esclaves. Pour nettoyer ces écuries, le héros entreprit le détournement de deux cours d'eau, l’Alphée et le Pénée. La convergence de leurs eaux engendra le courant nécessaire à l'assainissement des locaux.

Sur le relief de Chiragan, seul le héros est représenté ; pied droit posé sur un panier renversé, inutilisé, il contemple le courant faire l'humiliant travail à sa place. Encore une fois, son attitude rappelle certaines figurations de dieux grecs, au moins depuis l'époque du sculpteur Lysippe, au IV<sup>e</sup> siècle avant n. è. {% cite rolley_sculpture_1999 -l 333-334 %}, dont l'art romain se souvint. La pose avait été, entre-temps, reprise durant l'époque hellénistique, dans la statuaire comme sur les frappes monétaires, afin de figurer des dieux, tutélaires ou encore fondateurs. On se souviendra également de la célèbre série des tétradrachmes sur lesquels Démétrios Poliorcète, roi de Macédoine, se fit ainsi représenter, reprenant probablement une statue-portrait d'Alexandre le Grand {% cite noauthor_sng_nodate %}. Dans le monde romain, cette même composition, associant nudité, un pied reposant sur un rocher ou un quelconque symbole guerrier ou allégorique et corps penché en avant, fut acclimatée au contexte politique de la fin des guerres civiles et adaptée à l'univers du portrait par l'héritier de César, Octave, figuré de la sorte afin de suggérer sa nouvelle position de maître du monde {% cite sutherland_roman_1984 giard_bibliotheque_1976 -l 256 -L none -l Pl. 13-14 %}. La posture fut également adoptée par des personnalités qui, sans bénéficier d'une telle aura militaire ou politique, recevaient cependant l'hommage sous forme d'un portrait ; ainsi en est-il pour la <a href="/images/comp-ra-28-j-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-28-j-1.jpg" alt="Caius Cartilius Poplicola, musée d'Ostie, Sailko / Wikimedia Commons CC BY"/></span>statue du _duumvir_ Cartilius Poplicola</a>, découverte au niveau de l'escalier du temple d'Hercule d'Ostie, élevée durant l'époque triumvirale, celle des années 40-30 avant n. è..

À l'image de la statue de moyen format, provenant, elle aussi, de la _villa_ de Chiragan, et  représentant [Hercule appuyé sur sa massue](/ra-115) (inv. Ra 115), la main du héros est ramenée dans le dos, soulignant le repos après l’action, en l'occurrence le creusement des dérivations de l'Alphée et du Pénée. Enfin peut-on remarquer le changement de physionomie d'Hercule ; contrairement au visage glabre retenu pour les premiers Travaux, une barbe abondante est ici présente, en tant qu'illustration de la maturité. Au fur et à mesure de ses succès, le fils de Jupiter acquiert la sagesse. Parallèlement à la voie qui le conduira vers la divinisation, débute le changement d'apparence qui lie l'image du héros à celles de dieux gréco-romains : Jupiter, Pluton ou encore Esculape.

P. Capus