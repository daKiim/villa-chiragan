---
title: Hercule et le sanglier d’Érymanthe
id_inventaire: Ra 28 d
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: "144"
largeur: '88,5'
longueur: 
profondeur: "20"
epaisseur: 
id_wikidata: Q24649747
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 70
priority: 3
id_notice: ra-28-d
image_principale: ra-28-d-1
vues: 1
image_imprime: 
redirect_from:
- ra-28-d
- ark:/87276/a_ra_28_d
id_ark: a_ra_28_d
precisions_biblio:
- clef: balty_les_2008
  note: p. 134, fig. 113
- clef: cazes_musee_1999-1
  note: p. 88, 91
- clef: du_mege_description_1835
  note: n° 163
- clef: du_mege_notice_1828
  note: n° 76
- clef: esperandieu_recueil_1908
  note: n° 9
- clef: joulin_les_1901
  note: n° 93 B
- clef: massendari_haute-garonne_2006-1
  note: 'p. 250, fig. 123 '
- clef: rachou_catalogue_1912
  note: n° 28 d
- clef: roschach_catalogue_1865
  note: n° 28 c

---
Quatrième des douze Travaux, la capture du sanglier se situe dans le Péloponnèse, au nord-ouest de l'Arcadie, sur le mont Érymanthe. La bête, gigantesque, y dévastait toute la région. Hercule reçut l'ordre, de la part de son cousin, d'attraper vivant l'animal redoutable. Le héros fit preuve d'ingéniosité en profitant de l’hiver et de la neige pour le poursuivre jusqu'à l'épuiser. Hercule se fait donc chasseur pour la deuxième fois, après la capture du lion de Némée, premier des Travaux. Son visage est encore glabre, caractéristique physique qui peut renvoyer à la tranche d'âge durant laquelle la pratique de la chasse est privilégiée. Car l'activité cynégétique est synonyme de jeunesse et participe de l'éducation et des activités des éphèbes dans la Grèce ancienne. Lion, biche de Cérynie et sanglier sont les proies du chasseur héroïque et rusé ; leur traque et le combat qui en découle relèvent de la _paideia_ grecque, l'éducation d'essence aristocratique qui mène à l'accomplissement de l'individu.

Sur le relief, Hercule tient vigoureusement, contre sa poitrine, le suidé qu’il ramène à Mycènes, chez Eurysthée. Ce dernier, terrorisé à la vue de l’animal monstrueux, s’est réfugié dans un _dolium_, une grande jarre, à demi-enterrée dans le sol. Celui qui contraint Hercule à accomplir des tâches impossibles semble ici ridiculisé, représenté mains levées et poussant un cri ; il semble ainsi  implorer la protection des dieux en même temps qu'il témoigne de sa frayeur. La composition n'est pas nouvelle ; elle prouve la très longue postérité d'une tradition iconographique qui insista, savoureusement, dès le milieu du VI<sup>e</sup> siècle avant n. è., sur la couardise d'Eurysthée. Le retour d'Héraclès/Hercule se transforme en scène de comédie. Sur le col d'une amphore attique de Géla, en Sicile, datée de la fin du VI<sup>e</sup> siècle avant notre ère, Héraclès soulève l'arrière-train du sanglier, le forçant ainsi, cruellement, à marcher sur ses pattes avant, tandis que son cousin, sur la face opposée, se précipite dans la grande jarre, le _pithos_, devant une déesse - probablement Athéna - interloquée qui s'adresse au peureux (Syracuse, musée Archeologico Regionale Paolo Orsi) {% cite noauthor_lexicon_1990 -L none -l V, 2105 %}.

Cependant, l'iconographie du sujet, qu'elle soit vasculaire, monétaire ou sculptée, a toujours privilégié le transportement de l'animal sur l'épaule gauche, à la différence du relief de Chiragan, sur lequel, de manière originale, le sanglier est puissamment maintenu par le flanc contre la poitrine du héros. Par la seule force de son bras droit et de ses mains jointes, Hercule immobilise la proie tout en la maintenant sous son aisselle. C'est bien là le mouvement du lutteur qui, dans le cadre du combat, ceinture le corps de son adversaire de ses bras afin de l'immobiliser.

La composition confirme donc l'impression de familiarité et de redondances que nous pouvons ressentir face à ce cycle. Cet ensemble constitue, indéniablement, un authentique creuset d'images séculaires, retranscrites, consciemment ou non, par des sculpteurs dont la culture demeure intimement liée à l'esthétique diffusée dans l'ensemble du monde grec durant les derniers siècles avant notre ère. Après plus de cinq siècles, ces schémas et ces "manières" d'ateliers ont certes eu le temps de passer par le filtre de la romanité et de sa propre identité mais on peut rester stupéfait devant une telle pérennisation des formes, encore durant l'Antiquité tardive, ici mise au service de l'ostentation du message politique. Encore une fois, si, stylistiquement, la morphologie rappelle les lointains modèles du sculpteur grec Lysippe ainsi que les dieux et géants du <a href="/images/comp-ra-28-d-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-28-d-1.jpg" alt="Grand autel de Pergame Antikensammlung Berlin, Miguel Hermoso Cuesta / Wikimedia Commons CC BY-SA"/></span>grand autel de Pergame</a>, les rapports avec la sculpture tardive orientale de Carie sont évidents. Le pelage de l'animal lui-même, composé de petites touffes effilées, séparées les unes des autres et divisées au ciseau,  trouve un parallèle saisissant, mis en évidence par M. Bergmann, dans la fourrure d'un autre sanglier, représenté sur une frise fragmentaire d'Aphrodisias {% cite bergmann_chiragan_1999 -L none -l p. 36, Pl. 16, 1 %}.

La mise en scène du relief étonne également en raison de la dimension réduite d'Eurysthée qui en fait un personnage secondaire, entaché par la lâcheté. Trop souvent perçu comme une constituante de l'art médiéval, la hiérarchie des figures en fonction de leur importance voire de leurs qualités morales, et toujours au profit du héros, est une ressource expressive souvent retenue dans l'art romain. Bien que très marquée dans ce cycle, elle caractérise de nombreux reliefs, funéraires ou dynastiques.

P. Capus