---
title: Masques de théâtre
id_inventaire: Ra 35
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre
hauteur: 
longueur: 
largeur: 
profondeur: 
epaisseur: 
id_wikidata: Q28444879
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 190
priority: 3
id_notice: ra-35
image_principale: ra-35-1
vues: 1
image_imprime: 
redirect_from:
- ra-35
- ark:/87276/a_ra_35
id_ark: a_ra_35
precisions_biblio:
- clef: beckmann_idiom_2020
  note: p. 144
- clef: cazes_musee_1999-1
  note: p. 82
- clef: esperandieu_recueil_1908
  note: p. 58, n° 944
- clef: joulin_les_1901
  note: pl. VII, fig. 71
- clef: landes_gout_1989
  note: n° 73
- clef: rachou_catalogue_1912
  note: n° 35-36
- clef: soukaras_catalogue_1989
  note: ''

---
Cet ensemble est proche des petits tableaux (_pinakes_) ou des frises en marbre qui évoquaient, dans l'art gréco-romain, plus ou moins directement le dieu du théâtre, Dionysos (le Bacchus des Romains). Le dieu de Thèbes a souvent été figuré, dans le monde grec, sous la forme d’un simple masque barbu accroché à un pilier habillé. Il est donc le dieu-masque et l’une des rares divinités dont le culte promet l’éternité. Ainsi, les masques représentés dans les décors peuvent-ils se lire comme une allégorie de la transformation, celle de l’acteur comme du fidèle qui recherche un au-delà radieux.

Des reproductions de masques de théâtre ne sont pas rares dans le monde grec, en Attique, en Italie méridionale (en particulier sur l'île de Lipari, où les masques de terre cuite retrouvés dans les tombes illustrent remarquablement les différents genres théâtraux) ou encore en Asie Mineure. L'Empire romain a également légué quelques œuvres de même type, en marbre, en terre cuite ou encore en bronze. Ces éléments, qu'ils soient décors ou œuvres votives déposées dans les sanctuaires, rappelaient les véritables masques, en cuir voire en bois, portés par les acteurs (_histriones_) de la tragédie, de la comédie et du drame satyrique.

Le nombre des acteurs présents sur la scène étant réduit, le masque autorisait un même individu à incarner plusieurs rôles dans une même pièce : femme, esclave, vieillard... L'accessoire (désigné en latin par le mot _persona_) identifiait le personnage dès son entrée en scène et servait également de porte-voix. Dans un célèbre recueil du II<sup>e</sup> siècle, l'_Onomastikon_, Pollux de Naucratis établit notamment un catalogue des différents types de masques utilisés lors des représentations théâtrales. Y sont recensés soixante-seize modèles de masques, tragiques, satyriques ou comiques. Cette liste nous permet, plus ou moins, de reconnaître les personnages, bien connus du public et immédiatement identifiables grâce à la couleur de leur peau, de leurs cheveux et à la typologie de leur coiffure (notamment l'_onkos_, amoncellement ostentatoire des cheveux).

L'association des nombreuses sculptures dionysiaques, de petit et moyen formats, et des masques de théâtre composant le décor tardif de la _villa_ est, sans nul doute, loin d'être exceptionnelle. Elle prend cependant à Chiragan une connotation particulière, en raison de l'exceptionnelle survie d'un si grand nombre d'œuvres. Ainsi, la mise en scène des reliefs, des rondes-bosses, et fort probablement des mosaïques (aujourd'hui perdues), relevait-elle certainement d'un discours riche en métaphores. Une telle syntaxe iconographique se retrouve ailleurs, au sein de quelques rares résidences de l'Empire, à l'exemple de la _villa_ tardo-antique de Noheda (Villar de Domingo García, Cuenca) dont les spectaculaires mosaïques, récemment mises au jour, mêlent des thèmes identiques aux décors marmoréens de Chiragan ; à l'image de la résidence des bords de Garonne, mythologie, dionysisme et théâtre y sont en effet intimement liés.

P. Capus