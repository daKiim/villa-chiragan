---
title: Attis
id_inventaire: Ra 34 l
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: "95"
largeur: "79"
longueur: 
profondeur: "43"
epaisseur: 
id_wikidata: Q26705206
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 210
priority: 3
id_notice: ra-34-l
image_principale: ra-34-l-3
vues: 5
image_imprime: 
redirect_from:
- ra-34-l
- ark:/87276/a_ra_34_l
id_ark: a_ra_34_l
precisions_biblio:
- clef: beckmann_idiom_2020
  note: p. 139
- clef: cazes_musee_1999-1
  note: p. 84
- clef: du_mege_description_1835
  note: n° 154
- clef: du_mege_notice_1828
  note: n° 67
- clef: ensoli_aurea_2000
  note: p. 458, n° 55
- clef: esperandieu_recueil_1908
  note: p. 31, n° 892, fig. 4
- clef: joulin_les_1901
  note: n° 58 B
- clef: massendari_haute-garonne_2006-1
  note: p. 252-253, fig. 134
- clef: rachou_catalogue_1912
  note: n° 34 b
- clef: roschach_catalogue_1892
  note: n° 34 e
- clef: smith_late_1990
  note: ''
- clef: ziegle_hercule_2002
  note: ''

---
Attis était, en Phrygie, le gardien du temple de la déesse Cybèle. Cette dernière en était très amoureuse. Dans un accès de jalousie, elle le rendit fou, le poussa à s’émasculer et le transforma ensuite en pin. À Rome, tous les 22 mars, la procession de l’« entrée de l’arbre » célébrait la mort du dieu. Un pin coupé, décoré de bandes de laine rouge et d’instruments de musique, était porté jusqu’au temple de Cybèle.

La tête du jeune anatolien de Chiragan a été comparée, à juste titre, à celle de la reine des Amazones, Hippolytè, représentée dans l’un des reliefs des [Travaux d’Hercule](/ra-28-h) {% cite balty_les_2008 -l 126 %}. Ils se ressemblent beaucoup, en effet : même bonnet phrygien et traitement identique des cheveux bouclés. Il ne fait pas de doute que l'ensemble ait été réalisé par le même atelier de sculpteurs.

Les images de divinités sur bouclier représentent un thème fréquent dans l'art romain. Quatre siècles avant n. è., des dieux étaient ainsi figurés en Italie centrale. Cette association entre des entités divines et la forme circulaire de l'arme défensive trouvait ses fondements dans la célèbre description homérique du bouclier que la déesse Thétis offrit à son fils Achille {% cite homere_iliade_nodate -L none -l XVIII, 478-617 %}. Dans le récit légendaire, l'arme précieuse avait valeur de symbole cosmique par l'évocation de la Terre, au moyen de la forme circulaire, entourée des flots d'Océan et peuplée par les hommes, constructeurs de cités, pacifiées ou impliquées dans de terribles conflits. Les dieux, en tant que maîtres et grands ordonnateurs des destinées humaines, participaient dans ce contexte au labeur des mortels et à leurs activités, en particulier belliqueuses. Symbole d'héroïsation et d'éternité, l'effigie sur bouclier (_imago clipeata_), qu'elle soit sculptée, gravée ou peinte, servira également, à Rome, les aspirations de glorification des empereurs.

Les _tondi_ de Chiragan peuvent être rapprochés de la <a href="/images/comp-ra-34-l-2.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-34-l-2.jpg" alt="Portrait de Pindare de la série de bustes sur boucliers découverte à Aphrodisias (Carie, Asie Mineure), © Feridun F. Alkaya / FlickR"/></span>série</a>, datée de la fin du IV<sup>e</sup> ou du début du V<sup>e</sup> siècle, par R. Smith, découverte à Aphrodisias, en Carie {% cite smith_late_1990 -l 127-155 %}. Peut-être parce qu'il s'agit là de portraits et non de représentations divines, ces œuvres orientales paraissent plus expressives que celles de Chiragan dont les visages sont en revanche stylistiquement très proches de celui de l'<a href="/images/comp-ra-34-l-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-34-l-1.jpg" alt="Hélios/Sol de l’Esquilin, inv. 623, © Ole Haupt, Ny Carlsberg Glyptotek, Copenhague"/></span>Hélios de l'Esquilin</a>, conservé à Copenhague. Plus longilignes, les têtes d'Aphrodisias, aux joues moins pleines, sont également dépourvues des profonds creusements circulaires au trépan des barbes et des chevelures, caractéristiques des têtes de la _villa_ garonnaise. Les mèches, à Chiragan, sont infiniment plus complexes et réunies, pour un certain nombre d'effigies, par de nombreux petits ponts qui se détachent sur les sillons de séparation généreusement creusés au trépan. De nombreux fragments, conservés dans les réserves du musée, témoignent de l'importance de cette série d'_imagines clipeatae_ (représentations sur boucliers) dans le cadre du décor comprenant, outre ces médaillons des dieux, les reliefs des Travaux d'Hercule et de grands pilastres couverts de rinceaux d'acanthe «habités».

P. Capus