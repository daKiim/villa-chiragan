---
title: Tête juvénile de Maxence (?)
id_inventaire: Ra 93 ter
donnees_biographiques1: Fils de Maximien et d'Eutropia, vers 278-312
donnees_biographiques2: Empereur de 306 à 312
type_oeuvre: 
date_creation: Après 293
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: "33"
largeur: "22"
longueur: 
profondeur: "23"
epaisseur: 
id_wikidata: Q26707615
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 1
order: 30
priority: 3
id_notice: ra-93-ter
image_principale: ra-93-ter-1
vues: 8
image_imprime: 
redirect_from:
- ra-93-ter
- ark:/87276/a_ra_93_ter
id_ark: a_ra_93_ter
precisions_biblio:
- clef: balmelle_les_2001
  note: 'p. 230-231, fig. 125 c'
- clef: balty_les_2008
  note: p. 18-19, 76, 79-80, fig. 50, p. 82, fig. 53, p. 84, fig. 55, p. 86, fig 59, p. 89, fig. 65,67, p. 90
- clef: bergmann_chiragan_1999
  note: p. 34, 40-41
- clef: cazes_musee_1999-1
  note: p. 145
- clef: christof_unerkanntes_2003
  note: p. 49-54, ill. tafel 14
- clef: ensoli_aurea_2000
  note: p. 459, n° 58
- clef: joulin_les_1901
  note: p. 340, pl. XXV, n° 328
- clef: massendari_haute-garonne_2006-1
  note: p. 248, fig.114
- clef: musee_saint-raymond_regard_1995
  note: p. 236, n° 172
- clef: rosso_image_2006
  note: p. 489-490, n° 238

---
Les cheveux sont ici organisés comme ceux de la [tête colossale masculine](/ra-34-b) dans laquelle on a reconnu Maximien Hercule. Ces détails, ajoutés à d'autres, permettent de supposer qu'il s'agit ici de l'héritier, Maxence, fils de Maximien, qui régnera lui-même mais sur un territoire limité (au maximum l'Italie et les provinces africaines, ces dernières assurant le ravitaillement de Rome en blé et en huile). Cinq (peut-être six) <a href="/images/comp-ra-93-ter-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-93-ter-1.jpg" alt="Tête de Maxence, musée August Kestner Hanovre, Marcus Cyron / Wikimedia Commons CC BY-SA"/></span>portraits sculptés de Maxence adulte</a> sont aujourd'hui connus. Mais il s'agit ici d'un adolescent. L'œuvre pourrait correspondre à la période de son mariage avec Valeria Maximilla, en 293, ou peu après. Il serait donc âgé d'au moins 13 ou 14 ans.

D'après J.-C. Balty,  _Les portraits romains , La Tétrarchie, 1.5_ (_Sculptures antiques de Chiragan (Martres-Tolosane)_, Toulouse, 2008, p. 75-90.
