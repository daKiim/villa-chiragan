---
title: Fragment d’un pilastre ou d’un jambage de porte
id_inventaire: Ra 23 b
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre
hauteur: "134"
largeur: "65"
longueur: ''
profondeur: "13"
epaisseur: 
id_wikidata: Q48314471
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 6
order: 340
priority: 3
id_notice: ra-23-b
image_principale: ra-23-b-1
vues: 1
image_imprime: 
redirect_from:
- ra-23-b
- ark:/87276/a_ra_23_b
id_ark: a_ra_23_b
precisions_biblio:
- clef: joulin_les_1901
  note: fig. 9 b
- clef: massendari_haute-garonne_2006-1
  note: 'p. 238, fig. 96 '
- clef: rachou_catalogue_1912
  note: n° 23 b
- clef: roschach_catalogue_1892
  note: n° 23 b

---
Ce grand rinceau à feuilles nervées est peuplé d’oiseaux, d’un serpent, d’un escargot et d’un lézard.