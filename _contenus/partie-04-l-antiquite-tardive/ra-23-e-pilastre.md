---
title: Fragment d’un pilastre ou d’un jambage de porte
id_inventaire: Ra 23 e
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre
hauteur: "87"
largeur: '64,5'
longueur: 
profondeur: "11"
epaisseur: 
id_wikidata: Q48315420
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 6
order: 370
priority: 3
id_notice: ra-23-e
image_principale: ra-23-e-1
vues: 1
image_imprime: 
redirect_from:
- ra-23-e
- ark:/87276/a_ra_23_e
id_ark: a_ra_23_e
precisions_biblio:
- clef: du_mege_description_1835
  note: n° 260
- clef: joulin_les_1901
  note: fig. 11 b
- clef: massendari_haute-garonne_2006-1
  note: p. 238, fig. 96
- clef: rachou_catalogue_1912
  note: n° 23 c
- clef: roschach_catalogue_1892
  note: n° 23 e

---
Un grand culot d’acanthe nervée s’étale en couvrant tout le champ limité par de simples listels. En bas à gauche, surgit un serpent, à droite, s’agrippe une grenouille.