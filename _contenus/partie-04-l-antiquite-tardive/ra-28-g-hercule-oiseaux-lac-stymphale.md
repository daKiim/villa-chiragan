---
title: Hercule et les oiseaux du lac Stymphale
id_inventaire: Ra 28 g
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: "130"
largeur: '90,5'
longueur: 
profondeur: "16"
epaisseur: 
id_wikidata: Q24659913
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 90
priority: 3
id_notice: ra-28-g
image_principale: ra-28-g-1
vues: 1
image_imprime: 
redirect_from:
- ra-28-g
- ark:/87276/a_ra_28_g
id_ark: a_ra_28_g
precisions_biblio:
- clef: bergmann_chiragan_1999
  note: ''
- clef: cazes_musee_1999-1
  note: p. 89, 91
- clef: du_mege_description_1835
  note: n° 164
- clef: du_mege_notice_1828
  note: n° 76 bis
- clef: esperandieu_recueil_1908
  note: n° 10
- clef: joulin_les_1901
  note: n° 94 B
- clef: massendari_haute-garonne_2006-1
  note: 'p. 251, fig. 126 '
- clef: rachou_catalogue_1912
  note: n° 28 g
- clef: roschach_catalogue_1865
  note: n° 28 e
- clef: stirling_learned_2005
  note: ''
- clef: ziegle_hercule_2002
  note: p. 13

---
En Arcadie, au nord du Péloponnèse, le lac Stymphale était entouré de fôrets profondes. Sur ses rives, des oiseaux carnassiers terrifiaient les populations. Selon les versions du mythe, les volatiles étaient non seulement dotés de bec et de pattes en bronze mais également de plumes métalliques acérées, lancées comme des flèches contre les ennemis. On comprend ainsi pourquoi ces oiseaux effrayants provoquaient des ravages, détruisaient les récoltes et tuaient les hommes.

Héraclès/Hercule eut pour mission de les combattre ; il s'agissait de son sixième travail. Il se servit, pour l'occasion, de l'arc qui avait appartenu au roi Eurytos, l'un de ses précepteurs. Ce dernier l'avait reçu d'Apollon. Mais Hercule s'arma davantage encore et fit preuve, une nouvelle fois, de ruse : afin de déloger les monstres de leurs arbres, il utilisa, en tant qu'appeaux, des percussions de bronze, des castagnettes (_krotala_) offertes par Athéna/Minerve et Héphaïstos/Vulcain. Diodore, lui, parle d'un «tambour d'airain» {% cite diodore_de_sicile_bibliotheque_nodate -L none -l IV, 13, 2 %}. Quoi qu'il en soit, le son effraya les oiseaux qui s’envolèrent et, ainsi exposés, furent abattus les uns après les autres, par les flèches décochées depuis l'arc divin.

Sur le relief, Hercule, le carquois rempli de flèches dans le dos, regarde les oiseaux tomber. En haut à droite, un oiseau, transpercé d’une flèche, chute d’un arbre ; un second est à terre. De très fortes similitudes ont été remarquées entre les ailes et les griffes impressionnantes des oiseaux de ce panneau et celles d'un aigle mort sur un relief représentant le mythe de Prométhée, à Aphrodisias {% cite bergmann_chiragan_1999 -l 62 %}. Ces détails rendent probants les liens avec les ateliers de Carie. Quant à Hercule lui-même, son corps est ici le plus emprunté du cycle, marqué par une sorte d'engourdissement que l'écartement du bras droit ne fait qu'accentuer. L'hypertrophie musculaire participe, bien évidemment, de cette raideur, par le fort développement de la cage thoracique, des pectoraux, des obliques, du grand droit de l'abdomen, du bourrelet inguinal géométrique et de l'accentuation, au-dessus de la rotule, du vaste interne du quadriceps qui forme un losange et des lignes aiguës. De tels schémas se retrouvent en Carie, et particulièrement à <a href="/images/comp-ra-28-g-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-28-g-1.jpg" alt="Bas-relief de Prométhée et Heraclès provenant du Sébasteion d'Aphrosisias, musée archéologique d'Aphrodisias, Egisto Sani CC BY-NC-SA, Flickr, https://www.flickr.com/photos/69716881@N02/42344930332"/></span>Aphrodisias</a>, où les ateliers impriment aux corps une même emphase {% cite stirling_learned_2005 -l 57 %}.

P. Capus