---
title: Hercule et les bœufs de Géryon
id_inventaire: Ra 28 l
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: "155"
largeur: "103"
longueur: 
profondeur: "23"
epaisseur: 
id_wikidata: Q24659883
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 100
priority: 3
id_notice: ra-28-l
image_principale: ra-28-l-1
vues: 1
image_imprime: 
redirect_from:
- ra-28-l
- ark:/87276/a_ra_28_l
id_ark: a_ra_28_l
precisions_biblio:
- clef: bader_les_1992
  note: ''
- clef: balty_les_2008
  note: p. 129, fig. 109, 136
- clef: beckmann_idiom_2020
  note: p. 141, fig. 8
- clef: bergmann_chiragan_1999
  note: p. 26-43
- clef: bergmann_ensemble_1995
  note: p. 197-205
- clef: bernard_nec_2018
  note: ''
- clef: cazes_musee_1999-1
  note: p. 96-98
- clef: du_mege_description_1835
  note: n° 171
- clef: du_mege_notice_1828
  note: n° 81
- clef: ensoli_aurea_2000
  note: p. 457, n° 52
- clef: eppinger_hercules_2015
  note: ''
- clef: esperandieu_recueil_1908
  note: p. 38, n° 899 (8)
- clef: gricourt_taranis_1991
  note: ''
- clef: joulin_les_1901
  note: n° 110
- clef: jourdain-annequin_heracles_1992
  note: ''
- clef: jourdain-annequin_heracles_1998
  note: ''
- clef: knapp_cambridge_2014
  note: ''
- clef: massendari_haute-garonne_2006-1
  note: p. 252, fig. 130
- clef: rachou_catalogue_1912
  note: p. 29, n° 28 l
- clef: roschach_catalogue_1865
  note: n° 28 a
- clef: ziegle_hercule_2002
  note: p. 15

---
Unanimement placé en dixième position dans la succession des Travaux par les différents auteurs antiques, le relief montrant Hercule terrassant Géryon précède cependant aujourd'hui, au sein du musée, les épreuves des chevaux de Diomède et du combat contre la reine des Amazones. C'est en effet son format qui conditionna ce choix. Supérieur d'une quinzaine de centimètres, tant en hauteur qu'en largeur, par rapport aux autres panneaux en marbre du même cycle, on peut supposer que cette différence de taille incombait moins à un choix esthétique qu'à une volonté politique ou idéologique.                                Thème ici privilégié, il est permis d'imaginer que la mise à mort du géant à trois têtes, demeurant en Hispanie, devenait, au cœur de la _villa_, une œuvre pivot. Probable point de focalisation du regard mais également clé d'un récit rapporté et conté lors de visites d'hôtes prestigieux qui devaient, quoiqu'il en soit, percevoir le message que le commanditaire avait voulu subtilement proposer.

Le défi qui oppose Hercule à Géryon nous transporte dans l'Extrême-Occident, au sud-est de la péninsule ibérique, dans l'île d'Érythie, l'île «Rouge». En ce lieu, sera fondée la cité de Gadès, l'actuelle Cadix, au sud de l'embouchure du fleuve Tartessos (le Guadalquivir) {% cite strabon_geographie_nodate -L none -l III, 2, 1 %}. Depuis cette lointaine contrée, le héros dut ramener les bœufs, dont la couleur rouge faisait écho à l'astre solaire lorsqu'il disparaît dans les flots. Les troupeaux, qui paissaient dans des prés humides et toujours verts, appartenaient donc à Géryon, fils de Chrysaor et petit-fils de Poséidon/Neptune. Monstre à trois têtes et à trois corps, ou encore, selon les sources, à un corps et trois têtes, celui-ci vivait sur cette terre entourée par «les eaux du fleuve Océan» {% cite hesiode_theogonie_nodate -l 287 %}. Sur la route qui le menait chez Géryon, Hercule dressa deux piliers de part et d'autre du passage maritime qu'il réalisa entre Europe et Afrique. Appelés « Colonnes d’Hercule », actuel détroit de Gibraltar, ces signaux prévenaient, selon Diodore, de l'invasion de la Méditerranée par les monstres de l'Océan {% cite diodore_de_sicile_bibliotheque_nodate -L none -l IV, 18 %}. De la région, ainsi bornée par Hercule, dépend la ville de Gadès/Cadix, fondée par les Phéniciens. Le lieu représente toujours, sous l'Empire romain, la limite occidentale du monde connu, les confins des terres habitées {% cite bernard_nec_2018 -l 27-41 %}. Il s'agit bien là du seuil du monde nocturne, étrange et effrayant, dominé par un être non moins terrifiant. Hercule y parvient en naviguant, depuis la côte, sur la coupe d'or, char du Soleil, métaphore de la course de celui-ci lors du retour quotidien vers son point de départ {% cite bader_les_1992 -l 36 %}. Suite à son débarquement, et avant de pouvoir s'emparer des bovins, but de cette épopée, le héros doit se débarrasser du berger Eurytion et du chien à deux têtes, Orthros (l'«aube»), qu'il tue de sa massue. Géryon s'arme alors. Que son nom renvoie, selon les auteurs, à une seule créature triforme ou encore aux trois fils de Chrysaor {% cite diodore_de_sicile_bibliotheque_nodate -L IV, 5, 182 %} , unis par une sorte d'union sacrée et fusionnelle ne formant qu'un seul corps symbolique, il est important de noter le puissant contraste, permanent dans l'iconographie du combat, entre un Hercule rendu invincible par sa seule léonté et la créature de l'île occidentale, vainement caparaçonnée et armée. Ainsi Géryon est-il représenté depuis au moins la seconde moitié du VII<sup>e</sup> siècle avant n. è., comme le démontre le plastron votif, en bronze, de l'Héraion de Samos (musée de Vathy) {% cite knapp_cambridge_2014 jourdain-annequin_heracles_1998 -L none -l p. 286, Pl. XXXIV, 2 -L none -l fig. 16-7 %}.

La représentation du monstre tricéphale du relief toulousain s'inscrit dans une volonté d'actualiser le mythe dans l'histoire militaire romaine, au moyen d'un uniforme de général du Haut-Empire, caractérisé par la cuirasse musculaire (_lorica musculata_) d'ascendance grecque, en métal voire en cuir bouilli. Un grand manteau de général (_paludamentum_) couvre les épaules. Cousues au _subarmalis_ (gilets en cuir ou en grosse toile sous la cuirasse), les ptéryges (lambrequins), en tissu plutôt que de cuir, protègent les cuisses, en retombant depuis le bassin, tandis que d'autres protègent le haut des bras, depuis les épaules ; elles témoignaient du haut statut du militaire qui les portait. Depuis la cuirasse, une double rangée de ptéryges métalliques, que l'on ne trouve guère que sur des cuirasses de tribun puis majoritairement impériales, est décorée de têtes de loups, de griffons, de lions et de méduses. Géryon tente de se défendre avec un glaive, malheureusement brisé, tenu dans sa main droite, et se protège au moyen d'un bouclier maintenu, de la main gauche, par le manipule (poignée) dont les extrémités sont en forme d'ancre. Quant au profil biseauté du bouclier, il semblerait trahir une forme hexagonale qui pourrait tout autant correspondre à une forme germanique que romaine. Enfin, soulignons que si l'attirail militaire de Géryon entre parfaitement dans la catégorie de l'armement romain de prestige, son couvre-chef, un bonnet phrygien, dépend d'un tout autre contexte. Il est effet le symbole par excellence du monde sauvage, barbare, une allusion à l'étranger, l'Oriental en particulier .

Alors, pourquoi aurait-on prêté une telle attention pour cette épreuve lors de l'exécution de ce cycle ? Deux arguments, complémentaires, ont pu être avancés. C'est en premier lieu le contexte géographique de la _villa_, proche des Pyrénées et des différentes routes qui menaient vers la péninsule ibérique, qui aurait ainsi pu favoriser l'épisode {% cite cazes_musee_1999-1 %}. Par ailleurs, à la fin du III<sup>e</sup> siècle, c’est-à-dire, sans doute, à l’époque de la création du cycle, l’armée de l’empereur Maximien traversa probablement le territoire afin de se rendre en Hispanie, avant de gagner l'Afrique {% cite balty_les_2008 -l 129-132 %}. Maximien devenant lui-même, durant cette même période, une figure herculéenne, l’ensemble des Travaux fut donc peut-être l'occasion de célébrer les victoires impériales de manière allégorique… à plus forte raison l’épreuve de Géryon, qui renvoie directement à l’expédition espagnole.

L'éloge du rhéteur gaulois Mamertin, panégyriste officiel de Maximien, prononcé en 289, célèbre la victoire de l'empereur sur les populations rurales qui, en Gaule, s'étaient rebellées contre le pouvoir et qui furent matées par Maximien. Le texte les nomme «monstre biforme» et les assimile aux Géants de la mythologie, combattus par Zeus/Jupiter. Parallèlement, des émissions monétaires en or (_aurei_), frappées pour Dioclétien, Maximien et Constance Chlore, révèlent sur leur revers Jupiter levant le foudre, au-dessus d'un monstre à double queue de poisson {% cite gricourt_taranis_1991 -l 361 %}. Un anguipède donc, dont la nature ainsi que l'attitude rappellent fortement la sculpture du Géant découverte à Silahtaraḡa (Turquie) {% cite chaisemartin_les_1984 -L none -l Pl. 100 %}. Ne retouve-t-on pas, en l'occurrence, dans le Géryon de Chiragan, une même composition, inscrite dans un contexte belliqueux similaire ? Et ce combat d'Hercule contre Géryon n'est-il pas, à l'image des Gigantomachies de l'époque grecque, l'allégorie de la barbarie éliminée de la cité et donc du monde civilisé ? On peut bien entendu y voir également le «modèle de l'acculturation des Barbares» {% cite bonnet_heracles_1992 -L none -l p. 312-313 et 394 %}. Le relief découvert dans la _villa_ de Chiragan ne montre aucun boeuf et seul compte le combat entre les deux antagonismes symbolisés par ces deux figures, le monstre et le héros.

Mamertin, poursuivant son ton courtisan et sa flagornerie - c'est bien là l'essence même d'un panégyrique -, fait d'Hercule un protagoniste essentiel du combat contre les ennemis des Olympiens, référence à l'épithète de l'Auguste, qui renvoyait au fils de Zeus/Jupiter {% cite mamertin_panegyrique_nodate -L none -l X (2), 4, 2 %}. Le panégyrique évoque d'autre part, indirectement, les combats contre l'usurpateur Carausius qui, dans la province de _Britannia_ (actuelle Grande-Bretagne), avait fait sécession ; la localisation de son territoire, dans la partie occidentale de l'Empire, justifiait de comparer le traître à Géryon {% cite eppinger_hercules_2015 -l 187 %}. Si le laudateur impérial comparait Dioclétien à Jupiter, souverain des cieux, Maximien devenait, sans conteste aucun, et tout particulièrement après ses victoires océaniques et gauloises, l'image même d'Hercule, pacificateur de l'univers {% cite mamertin_panegyrique_nodate -L none -l 11, 6 %}. Le texte apologétique en l'honneur du tétrarque précise par ailleurs combien la victoire sur «le monstre hideux» surpasse-t-elle celle d'Hercule sur le «grossier bouvier à trois têtes» {% cite mamertin_panegyrique_nodate -L none -l 2, 1 %}. Serait-ce la révolte populaire rurale des bagaudes, réprimée par Maximien, qui devrait être entendue derrière la métaphore ?

P. Capus