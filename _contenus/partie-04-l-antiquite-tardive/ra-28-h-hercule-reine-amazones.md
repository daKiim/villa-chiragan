---
title: Hercule et la reine des Amazones
id_inventaire: Ra 28 h
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: "146"
largeur: "66"
longueur: 
profondeur: "19"
epaisseur: 
id_wikidata: Q24659835
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 120
priority: 3
id_notice: ra-28-h
image_principale: ra-28-h-1
vues: 1
image_imprime: 
redirect_from:
- ra-28-h
- ark:/87276/a_ra_28_h
id_ark: a_ra_28_h
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 94-95
- clef: du_mege_description_1835
  note: n° 167
- clef: du_mege_notice_1828
  note: n° 79
- clef: esperandieu_recueil_1908
  note: p. 37, n° 899, fig. 5
- clef: joulin_les_1901
  note: n° 96 B, D
- clef: massendari_haute-garonne_2006-1
  note: p. 251, fig. 127
- clef: rachou_catalogue_1912
  note: n° 28 h
- clef: roschach_catalogue_1865
  note: n° 28 f

---
Neuvième des douze Travaux, cette épreuve oblige Hercule/Héraclès à partir vers l'Asie Mineure, sur les terres d'Hippolyté, reine des Amazones. C'est en Cappadoce que celle-ci guidait le légendaire peuple de guerrières cavalières. Le mythe fait écho à l'inversion des rôles de la guerre et du pouvoir, traditionnellement garantis par les hommes mais singulièrement endossés par des femmes dans ces contrées ; une inversion synonyme du grand chaos qui régnait en Orient, selon les Grecs {% cite jourdain-annequin_heracles_1992 -l 173 %}.

Le voyage d'Hercule vers l'embouchure du fleuve Thermodon (Cappadoce) trouve son fondement dans la ceinture en or que possédait Hippolyté. Convoitée par la fille d’Eurysthée, le héros reçut l'ordre de récupérer le précieux accessoire. Ainsi fut-il dans l'obligation de neutraliser l'hégémonie de ce peuple de femmes, tuant en combat singulier les douze Amazones les plus éminentes, avant de se voir remettre la ceinture. Selon Apollodore, Héra, déesse jalouse et furieuse, aurait fait courir le bruit qu’Hercule voulait enlever la souveraine. Furieuses, les Amazones auraient alors engagé le combat contre le héros, une bataille qui s'acheva par la mort d'Hippolyté {% cite pseudo-apollodore_bibliotheque_nodate -L none -l II, 5, 9 %}.

Sur le relief, d'Hercule ne reste qu'un pied. La cavalière, que celle-ci incarne la reine ou bien, plus génériquement, une Amazone, est quant à elle bien conservée. La guerrière orientale est coiffée du bonnet scythe, en peau de renard, l'_alopekis_. Elle brandit de la main droite une bipenne, hache à deux tranchants, tandis qu'on reconnaît, en bas à droite, la _pelta_, bouclier distinctif des Amazones, léger et maniable, en forme de croissant de lune, caractéristique de l'armement des peuples orientaux. Sa tête ressemble beaucoup à celle d’[Attis](/ra-34-l), sculptée sur l'un des médaillons des dieux qui furent peut-être associés à ce cycle.

Conformément aux autres reliefs des douze Travaux, le dynamisme de la scène est exprimé par un débordement des personnages et des objets sur le cadre. La nervosité des mouvements de la lutte est également démontrée, pour cette scène en particulier, par le raccourci du cheval dont l'arrière-train déborde en saillie dans l'espace, depuis l’arrière, tandis que la partie antérieure de l'équidé semble pénétrer le champ du panneau, en très bas-relief. Cette technique, bien connue dans le domaine de la sculpture des maîtres de la Renaissance italienne, définie par un terme, _schiacciato_ («écrasement»), qui en traduit bien l'intention, permet, avec une grande subtilité, d'appréhender la profondeur de champ. La peinture antique avait, de son côté, adapté depuis longtemps ce rendu illusionniste à l'aide d'une perspective atmosphérique monochrome. C'est bien, en l'occurrence, une impression picturale qui se dégage du beau travail en relief de Chiragan.

P. Capus