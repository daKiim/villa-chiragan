---
title: Fragment d’un pilastre ou d’un jambage de porte
id_inventaire: Ra 23 a
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre
hauteur: "197"
largeur: "64"
longueur: 
profondeur: "12"
epaisseur: 
id_wikidata: Q48327507
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 6
order: 320
priority: 3
id_notice: ra-23-a
image_principale: ra-23-a-1
vues: 1
image_imprime: 
redirect_from:
- ra-23-a
- ark:/87276/a_ra_23_a
id_ark: a_ra_23_a
precisions_biblio:
- clef: bulletin_1936
  note: p. 587
- clef: cazes_musee_1999-1
  note: p. 80
- clef: du_mege_description_1835
  note: n° 258
- clef: du_mege_notice_1828
  note: n° 110
- clef: joulin_les_1901
  note: fig. 2 b
- clef: massendari_haute-garonne_2006-1
  note: p. 238, fig. 96
- clef: nodier_voyages_1833
  note: pl. 31 bis
- clef: rachou_catalogue_1912
  note: n° 23 a
- clef: roschach_catalogue_1892
  note: n° 23 a

---
La doucine de l’encadrement de ce pilastre est ornée de palmettes. Un grand rinceau feuillu et fleuri naît d’un splendide culot d’acanthe. Il est peuplé d’oiseaux, de papillons, d’un lézard, d’un serpent, d’un escargot et d’un criquet.

Les rinceaux jaillissant d'un culot d'acanthe et formant un candélabre ou encore, à l'image de ces exemplaires de Chiragan, une superposition de volutes donnant naissance à des fleurs, représentent un motif récurrent depuis l'époque grecque. G. Sauron a souligné le rôle symbolique de l'acanthe, si généreusement présente sur les parois de l'enclos de marbre de l'Autel de la Paix d'Auguste (_Ara Pacis Augustae_), inauguré en 13 avant n. è. ; l'acanthe y formait «les plus vastes rinceaux de surface conservés de l'antiquité grecque et romaine» {% cite sauron_histoire_2000 -l 30 %}. Car le monument voulu par Auguste manifestait le retour de l'âge d'or, après les guerres civiles auxquelles avait su mettre fin l'héritier de César.  Une plante, donc, et aussi des animaux, au service d'un programme politique, sous l'égide d'Apollon et vigoureusement étayé par la poésie de Virgile {% cite sauron_histoire_2000 -L none -l p. 76-80 et 115-116 %}. La composition fera florès.

Il est possible que les grands pilastres de Chiragan aient servi d'éléments de séparation des reliefs d'Hercule. Si l'on admet l'insertion des médaillons des dieux à ce grand décor de marbre, l'ensemble composait alors une sorte de vaste poème allégorique. Ainsi la victoire et le renouveau étaient-ils placés sous l'autorité divine, vecteurs de la bonne fortune à laquelle participait également la luxuriance des acanthes, peuplées de créatures.

P. Capus