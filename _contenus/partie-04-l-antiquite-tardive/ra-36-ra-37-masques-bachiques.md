---
title: Masques bachiques
id_inventaire: Ra 36 - Ra 37
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: 
materiau: Marbre
hauteur: 
longueur: 
largeur: 
profondeur: 
epaisseur: 
id_wikidata: Q63758870
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 200
priority: 3
id_notice: ra-36-ra-37
image_principale: ra-36-ra-37-1
vues: 6
image_imprime: 
redirect_from:
- ra-36-ra-37
- ark:/87276/a_ra_36_ra_37
id_ark: a_ra_36_ra_37
precisions_biblio:
- clef: beckmann_idiom_2020
  note: p. 133-160
- clef: bergmann_chiragan_1999
  note: p. 26-43
- clef: bergmann_ensemble_1995
  note: p. 197-205
- clef: cazes_musee_1999-1
  note: p. 82
- clef: du_mege_description_1835
  note: n° 267
- clef: du_mege_monumens_1814
  note: p. 262-263, pl. III, n° 1
- clef: du_mege_notice_1828
  note: n° 64
- clef: ensoli_aurea_2000
  note: ''
- clef: esperandieu_recueil_1908
  note: p. 48, n° 922
- clef: guillevic_toulouse_1989
  note: n° 2
- clef: joulin_les_1901
  note: p. 8 et 92-93, pl. VII, 63 A à 70 A
- clef: martene_voyage_1717
  note: 2e partie, p. 34-35
- clef: rachou_catalogue_1912
  note: n° 37
- clef: roschach_catalogue_1892
  note: n° 37

---
Ces têtes, évidées à l'arrière, et destinées à une paroi, doivent être associées, sans aucun doute, à l'ensemble du décor mis en place, dans la demeure, durant l'Antiquité tardive. Plusieurs détails trahissent, en effet, une exécution par le même atelier : les chevelures, en larges mèches, séparées d'un sillon profond exécuté au ciseau ; la terminaison en boucle, profondément forée au trépan et composant un petit cercle parfait ; la similitude de composition de la coiffure au toupet du satyre (7) avec celle d'Eurysthée, dans le grand relief représentant [Hercule et le sanglier d'Érymanthe](/ra-28-d) ; les arcades sourcilières arquées, formant un angle aigu ; les pupilles rondes, creusées au foret ; l'évidement pratiqué entre les lèvres, par conséquent entrouvertes, générant un canal sinueux. Il suffit de comparer minutieusement certaines de ces physionomies avec les représentations d'Hercule (la [tête isolée nue](/ra-28-a) du héros, celle visible sur le relief de l'[hydre de Lerne](/ra-28-b), celle encore du panneau au [sanglier d'Érymanthe](/ra-28-d)) pour constater la cohérence formelle et propre à cet atelier, proche des formules mises en œuvre dans certains centres d'Asie Mineure. Il faut enfin évoquer le matériau qui fut employé pour cette série originale ; il fut récemment confirmé par les analyses de la majorité des sculptures découvertes dans la _villa_. À l'instar des Travaux d'Hercule, des [masques de théâtre](/ra-35), des [médaillons des dieux](/ra-34-l), des portraits de [Maximien](/ra-34-b) et des siens et, enfin, d'une importante suite de représentations mythologiques de petit et moyen format, ces têtes dites «bachiques» ont été taillées dans du marbre de Saint-Béat.

On y reconnaît un silène \[1\], deux _Horae_ (Saisons) ou peut-être ménades \[2, 4\], un Bacchus enfant (?) \[3\], un Pan \[5\] et deux satyres \[6, 7\]. Ces têtes de marbre sont parfaitement imaginables sur le mur d’une salle de banquet. Une mosaïque de Volubilis, notamment, décorant le sol du _triclinium_ de la maison «des Travaux d'Hercule», réunit, entre autres, les Saisons aux épreuves du héros gréco-romain {% cite noauthor_lexicon_1990 -L none -l Horai/Horae 168 %}. Doit-on imaginer un tel rapprochement — marmoréen cette fois-ci — dans un même espace à Chiragan ? L’association de ces demi-dieux est une référence à l’univers de Bacchus (le Dionysos grec). La succession des Saisons symbolise l’éternité et le renouveau permanent, ce que promet également le dieu. Ces _Horae_ pourraient être, ici, l’été, couronné d’épis, et le printemps, couronné de boutons de fleur. Ces filles de Zeus et de Thémis (déesse de la justice) ne sont donc en rien contradictoires avec l'univers dionysiaque (ou bachique), au contraire, puisque la mythologie a, par ailleurs, fait de ces gardiennes de l'Olympe, les nourrices du petit Bacchus/Dionysos {% cite  nonnos_de_panopolis_dionysiaques_450 -L none -l IX, 11 %}.

P. Capus