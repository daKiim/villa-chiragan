---
title: Hercule et Cerbère
id_inventaire: Ra 28 e
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: "143"
largeur: '85,5'
longueur: ''
profondeur: "19"
epaisseur: 
id_wikidata: Q24662338
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 140
priority: 3
id_notice: ra-28-e
image_principale: ra-28-e-1
vues: 1
image_imprime: 
redirect_from:
- ra-28-e
- ark:/87276/a_ra_28_e
id_ark: a_ra_28_e
precisions_biblio:
- clef: esperandieu_recueil_1908
  note: p. 37, n° 899 (6)
- clef: joulin_les_1901
  note: pl. VIII, n° 92 B, 103 B, 117 B, 106 B
- clef: jourdain-annequin_heracles_1989
  note: ''
- clef: massendari_haute-garonne_2006-1
  note: p. 250, fig. 124
- clef: rachou_catalogue_1912
  note: p. 2c
- clef: roschach_catalogue_1865
  note: n° 28 h

---
Le voyage dans les Enfers, dont le chien à trois têtes, Cerbère, était le gardien, correspond à l'ultime mission demandée à Héraclès/Hercule. L'épreuve est déjà présente chez Homère {% cite homere_iliade_nodate -L none -l chant 8, vers 362-369 %} {% cite homere_odyssee_nodate -L none -l chant 2, vers 623-626 %}. Le fils d'Alcmène doit ramener le monstre vivant du monde des morts, dont, bien entendu, nul humain ne pouvait revenir. Hadès/Pluton, maître du royaume infernal, y autorise Hercule à capturer Cerbère à la condition que la mission soit effectuée sans armes. Mordu par le dragon qui formait la queue du gardien des Enfers, Hercule empoigne alors le chien par le cou. Le souffle ainsi coupé, Cerbère se laisse dominer {% cite pseudo-apollodore_bibliotheque_nodate -L none -l II, 5, 12 %}. Le tribut est apporté à Eurysthée qui, terrifié, ordonne alors à son cousin de le ramener là où il l’avait trouvé.

Sur le relief, survit le seul corps d'Hercule. Cependant, le mouvement de la léonté, qui flotte dans son dos, dénote une grande agitation. Il faut sans doute imaginer Cerbère, traîné hors des Enfers, à droite du héros, ce dernier tournant peut-être la tête vers le terrifiant gardien tricéphale. L'identification de l'épreuve est ici rendue possible grâce à l'attitude d'Hercule, semblable à celle qui fut souvent adoptée par les artistes, dans la ronde-bosse, sur des sarcophages ou des mosaïques, afin d'illustrer ce thème {% cite lexicon_1990 -L none -l 2649, 2656, 2661, 2662, 2664 %}.

La victoire sur la nuit éternelle résonne bien entendu à travers ce voyage infernal. On sait que le héros a préalablement effleuré, sinon côtoyé, la mort lors de son passage dans l'île du soleil couchant, Érythie, chez le monstre tricéphale Géryon, ou chez les Hespérides, filles de la nuit, autre lieu de frontière. La descente dans le royaume des morts, ou catabase, n'est pas spécifique au mythe d'Hercule, loin s'en faut. Il s'agit là d'une initiation qui rend possible le sauvetage de l'âme et son apothéose. Afin d'accomplir une telle épreuve, Hercule doit être préalablement initié aux mystères de la déesse Déméter, à Éleusis, non loin d’Athènes. Cette instruction conditionne la communication vers les Enfers, une meilleure protection dans les ténèbres, une meilleure connaissance du royaume des ombres. Alors peut s'accomplir l'appropriation du chien d’Hadès, condition pour une autre conquête : l’immortalité, accordée par Zeus, sur le mont Oeta {% cite jourdain-annequin_heracles_1989 -l 511, 563-564 %}.

Il est difficile de ne pas songer au contexte des épreuves du temps, auxquelles était confronté l'empereur Maximien, depuis les soulèvements populaires des bagaudes en Gaule jusqu'aux mouvements sécessionnistes berbères en Afrique. Le récit herculéen ne pouvait que résonner à travers ces combats victorieux, véritable catalyseur pour une apothéose impériale, ardemment désirée, à n'en pas douter.

P. Capus