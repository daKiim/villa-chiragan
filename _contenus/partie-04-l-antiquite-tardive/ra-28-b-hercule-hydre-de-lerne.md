---
title: Hercule et l’Hydre de Lerne
id_inventaire: Ra 28 b
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: "141"
largeur: "88"
longueur: 
profondeur: "22"
epaisseur: 
id_wikidata: Q24649576
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 60
priority: 3
id_notice: ra-28-b
image_principale: ra-28-b-1
vues: 1
image_imprime: 
redirect_from:
- ra-28-b
- ark:/87276/a_ra_28_b
id_ark: a_ra_28_b
precisions_biblio:
- clef: balty_les_2008
  note: p. 134, fig. 111
- clef: cazes_musee_1999-1
  note: p. 86-87
- clef: du_mege_description_1835
  note: n° 162
- clef: du_mege_notice_1828
  note: n° 75
- clef: esperandieu_recueil_1908
  note: n° 7
- clef: hannestad_skulpturenausstattung_2006
  note: ''
- clef: ig_1890
  note: ''
- clef: joulin_les_1901
  note: n° 90 B
- clef: lexicon_1994
  note: ''
- clef: massendari_haute-garonne_2006-1
  note: p. 250, fig. 121
- clef: rachou_catalogue_1912
  note: n° 28 b
- clef: roschach_catalogue_1865
  note: n° 28 b
- clef: ziegle_hercule_2002
  note: p. 11

---
Le haut-relief s'inscrit dans la série qui retraçait, au sein de la _villa_, les douze Travaux d'Hercule, l'Héraclès grec. Fils d'Alcmène, reine de Tyrinthe, et de Zeus, maître de l'Olympe, celui-ci naquit sous le nom d'Alcée, ou Alcide, nom dérivé du substantif grec _alkè_ (ἀλχή) qui renvoie à la force. Il devint _Hêras cléos_, «la gloire d'Héra», car selon Diodore de Sicile, c'est de la déesse que lui venait la gloire {% cite diodore_de_sicile_bibliotheque_nodate -L none -l IV, 10, 1 %}. Selon le mythe grec, Héra, sœur et épouse de Zeus, ulcérée par la relation adultérine de son concupiscent conjoint, ambitionna de faire disparaître le fruit de cette union illégitime par tous les moyens. Elle hâta en premier lieu la naissance d'Eurysthée, fils de Sthénélos, roi de Mycènes et de Tirynthe, octroyant ainsi au nouveau-né la préséance sur le fils de Zeus pour l'accession au trône de cette dernière cité. Héra poursuivit sa vengeance, envoyant deux serpents dans le berceau d'Alcide qui, à la surprise de tous, les saisit par le cou et les étrangla. Une fois adulte, celui qui n'était pas encore devenu Héraclès eut à souffrir bien pire : Héra lui infligea une démence temporaire. Inconscient de ses actes, Alcide assassina son épouse, Mégare, ainsi que tous ses enfants, qu'il confondit, dans son délire, avec ceux d'Eurysthée {% cite euripide_folie_416bc -L none -l 965 et suiv. %}. Reprenant ses esprits, le forcené, désemparé face à la gravité de ses actes, consulta alors l'oracle de Delphes, en Phocide, et n'eut d'autre choix que de se placer sous l'autorité de la déesse persécutrice, de prendre le nom d'Héraclès et, enfin, de s'aliéner à Eurysthée qui lui commanda une mission périlleuse par an, une décennie durant. Ces douze épreuves répondent à la nécessité, pour le demi-dieu, d'expier ses fautes. Victorieux, Héraclès fut récompensé : il bénéficia d'une apothéose, le jour de sa mort, et obtint donc l'immortalité.

Les douze Travaux avaient été mis en scène, autour de 460 avant notre ère, sur les métopes du temple de Zeus, à Olympie {% cite lexicon_1990 -L none -l V, 1705 %}. Il s'agit pour nous du premier témoignage iconographique relatif à ces épreuves (_athla_), conformes par leur nombre et leur contenu aux sources écrites. La présence d'un tel cycle sculpté dans le grand sanctuaire de cette cité de l'Élide n'est pas fortuit puisque, selon Pindare {% cite pindare_olympiques_nodate -L none -l III, 14-18 %} et Pausanias {% cite pausanias_periegese_nodate -L none -l V, 7, 7 %}, Héraclès y aurait notamment fondé les célèbres jeux et planté les oliviers dont les branches étaient à l'origine des couronnes destinées aux vainqueurs. Par ailleurs, c'est en Élide que le héros accomplira l'exploit de nettoyer les écuries du roi Augias.

Longtemps marqué par la variabilité du nombre comme de la nature des exploits à intégrer, le récit des Travaux ne forme un cycle canonique qu'à partir de la fin de l'époque hellénistique, époque de cristallisation des épreuves chez Diodore de Sicile ou sur le panneau à relief d'époque romaine, connu sous le nom de _Tabula Albana_ {% cite ig_1890 -L none -l XIV, n°1293 A %}. Cependant, les exploits d'Héraclès s’affirmaient déjà sur les céramiques attiques à figures noires du VI<sup>e</sup> siècle avant notre ère. Par la suite, le thème ne présente aucun signe de tarissement et sa présence demeure régulière dans des domaines aussi divers que le relief architectural, la mosaïque, les sarcophages, la ronde-bosse, l'orfèvrerie, la glyptique et la numismatique. Le même récit régit encore, durant l'Antiquité tardive, les grands panneaux de marbre de la _villa_ de Chiragan.

La manière de traiter le corps d’Hercule, muscles surdéveloppés et position des jambes, correspond à des schémas mis en place plusieurs siècles auparavant en Grèce orientale (Turquie actuelle), durant l'époque hellénistique, et sans cesse réutilisés dans l’art romain. La pose du héros, torse de face, prenant appui sur une jambe fléchie et vue de profil, tandis que l'autre est tendue, rappelle plusieurs reliefs de ce cycle. C'est notamment la posture de Zeus combattant Porphyrion sur la frise orientale du Grand Autel de Pergame, tout autant que celle d'Athéna, dans son affrontement avec le Géant Alcyonée, sur le même monument. À cette gesticulation théâtrale s'ajoute l'hypertrophie musculaire, autre trait constitutif du style qui caractérise, pour l'époque hellénistique, le monument de Pergame et sa grande frise de la lutte des dieux et des géants, conçue durant la première moitié du II<sup>e</sup> siècle avant notre ère. Il s'agit bien là de lointains modèles dont l'héritage et la réception s'expriment encore dans les hauts-reliefs de Chiragan.

La confrontation, opérée par Marianne Bergmann, de ces reliefs avec l'art des ateliers d'Aphrodisias, en Carie, dans le sud-est de l'actuelle Turquie, est particulièrement convaincante. Tout en admettant l'origine hellénistique d'un tel style au sein de ces ateliers orientaux, la chercheuse s'est notamment posé la question de la période durant laquelle une telle conception, marquée par l'éclectisme des formules, aurait pu singulariser la sculpture de cette région de l'Empire. Il semble bien qu'elle couvre un large champ chronologique et qu'il soit raisonnable de faire remonter le début de cette production à l'époque julio-claudienne, dans le courant du I<sup>er</sup> siècle de notre ère. Une fois ce style mis en place, les ateliers purent ainsi trouver, précocement, leurs marques avant de réitérer longtemps ces formules, poursuivies jusqu'à la fin de l'Antiquité {% cite bergmann_chiragan_1999 -l 61-62 %}. Les analogies avec la sculpture romaine d'Asie Mineure, et plus particulièrement les ateliers de Carie, ne manquent pas dans les hauts-reliefs consacrés au héros. À Chiragan, le torse d'Hercule figuré encore jeune est par exemple proche du géant anguipède (la partie inférieure du corps prenant la forme d'une queue de serpent), retrouvé à Silahtaraǧa {% cite chaisemartin_les_1984 -L none -l p. 44-45, Pl. 31-32 %}.

Il est difficile de situer l'implantation d'un tel décor au sein de la résidence des bords de Garonne. Compte tenu des dimensions des reliefs, l'hypothèse selon laquelle ils auraient pu décorer le portique de 45 m de long, qui longe la cour du côté ouest, est séduisante {% cite cazes_musee_1999-1 -l 85 %}. D'autres espaces pourraient également offrir une ampleur convenable à ce cycle qu'il ne faut probablement pas séparer des [grands pilastres](/ra-23-a) à rinceaux «habités», peuplés de divers animaux et insectes, eux aussi taillés dans un marbre de Saint-Béat. Il est probable que les [médaillons des dieux](/ra-34-l), conçus dans le même matériau, aient été ajoutés à l'ensemble, dès le projet initial voire plus tardivement ; leur style trahit en effet une conception tardive. Le cycle dut faire partie de l'une des pièces principales de la _villa_, "salle d'audience" selon N. Hannestad {% cite hannestad_skulpturenausstattung_2006 -l 204 %} lieu effectivement idoine pour un tel décor.

Au III<sup>e</sup> siècle, à l'époque des empereurs-soldats comme sous la Tétrarchie, la monnaie, précieuse source d'information et de diffusion de l'idéologie impériale, permet de comprendre le rôle important qu'a pu jouer Hercule. En 268, l'atelier II (probablement à Cologne) frappa une série extraordinaire de monnaies d'or, pour Postume, empereur usurpateur gaulois (260-269), à l'occasion des dix années de son règne (_decennalia_) {% cite bastien_les_1958 -l 61 %}. Destinées à être offertes aux membres de l'élite politique et militaire, ces émissions représentent les douze Travaux dans leur intégralité ainsi qu’Hercule dans sa lutte contre le géant Antée. Le héros, qui avait été également privilégié sur les émissions de Gordien III, le fut encore sous Gallien, empereur légitime (260-268), contemporain de Postume, et, bien entendu, des différents représentants de la Tétrarchie des années 293-305. Maximien, en particulier, fit du fils de Jupiter et d'Alcmène sa divinité tutélaire et s'assimila à lui dans le cadre de sa «propagande» par l'image. Jean-Charles Balty a démontré, de manière convaincante, les possibles associations entre le groupe dynastique, centré sur cet Auguste Hercule, et le héros auquel le tétrarque a pu s'identifier {% cite balty_les_2008 -l 133-137 %}. Le co-empereur, en charge de l'Occident, ne devait-il pas combattre Burgondes, Alamans et Hérules dans la région rhénane, bagaudes en Gaule, pirates en Espagne et Berbères en Maurétanie, et ainsi conduire chaque épreuve vers une issue victorieuse qui prédisposait l'empereur à une apothéose ? Au demeurant, concordance de style et manière d'atelier permettent de constater combien les points communs, d'un point de vue physique, sont remarquables, dans ce grand ensemble marmoréen de Chiragan, entre l'Auguste et son héros tutélaire. Comme ce dernier, l'empereur tétrarque combat et élimine les ennemis et les créatures néfastes, comme lui encore, il apporte, par ses déplacements et la mainmise sur les territoires, paix et civilisation.

Parallèlement aux différences morphologiques et à la plus ou moins grande élégance des attitudes, on ne peut passer outre, dans cette suite de grands panneaux conçus à plusieurs mains, l'évolution de la pilosité d'Hercule. On ne saurait y voir un simple choix esthétique de la part d'un sculpteur par rapport à un autre. Et si l'absence ou la présence de la barbe induit une évolution chronologique et par conséquent le poids du temps qui s'écoule, depuis les années de jeune adulte à celles de la maturité, elles illustrent parallèlement l'expression de l'évolution psychologique et intellectuelle d'un héros ; par le raisonnement, la connaissance, l'application mais également le contrôle, les Travaux sont autant d'étapes comparables à l'évolution d'un être soumis à l'expérience sensible qui progressivement appréhende les vérités de l'âme, fondement de la plénitude. À l'image de tout humain, Hercule, qui n'est pas encore un dieu, est en quête de l’œuvre nécessaire à cet accomplissement. Puisqu'à la fin du long parcours, l'apothéose lui sera donc bien accordée, il demeurera encore un modèle, moral en premier lieu, pour l'aristocratie de l'Antiquité tardive, dans sa quête d'absolu et d'éternité, après les étapes qui jonchent le chemin de leur vie terrestre. La réutilisation de son image dans l'iconographie "mixte" des premiers christianismes témoigne de cette perception.

### L'hydre de Lerne, deuxième travail d'Hercule

La mise à mort de l'hydre, demandée au héros par son cousin Eurysthée, qui agissait sous l'autorité de Héra, est considérée comme la deuxième épreuve, selon l'ordre chronologique convenu, notamment dans les écrits de Diodore {% cite diodore_de_sicile_bibliotheque_nodate -L none -l IV, 5 %} ou Hygin {% cite hygin_fables_nodate -L none -l XXX, 30 %}. L'exploit se déroule dans les marais de Lerne, près d'Argos, dans le Péloponnèse. L'hydre, dont le nom renvoie à l'eau (ὕδωρ/_hudor_), y vivait dans une tanière, près d'une source du nom d'Amymonée. Elle avait été élevée par Junon (l’Héra grecque) pour servir d’épreuve à Hercule (Héraclès) et possédait plusieurs têtes, jusqu'à neuf selon les auteurs, dont celle du milieu était immortelle (elle prend la forme d'une tête humaine sur plusieurs sarcophages de l'époque romaine impériale). Ses gueules dégageaient un souffle mortel, comparable aux pestilences des marais. Même coupées, elles renaissaient sans cesse. Au fur et à mesure qu’Hercule les décapitait, son neveu Iolaos, engagé dans cet épisode, conformément aux sources écrites, cautérisait les plaies avec des tisons ardents pour éviter leur repousse.

Le relief établit une hiérarchie intéressante, peu classique, entre les trois protagonistes. Hercule occupe ici l'essentiel de l'espace et déborde largement sur le cadre qui ne conditionne jamais totalement l'action sur l'ensemble du cycle. Les sculpteurs semblent même s'amuser à s'affranchir de cette limite. L'hydre, qui s'enroule autour de la cheville gauche d'Hercule, n'a pas l'ampleur que l'on a généralement pu lui attribuer dans l'iconographie grecque et romaine ; mais ce format permet de consacrer l'espace supérieur droit à Iolaos.

Du carquois d'Hercule, il ne reste rien ; celui-ci se trouvait au second plan, entre les deux membres inférieurs du héros, comme en témoignent les plumes de l'empennage des flèches, visibles derrière la cuisse droite. Ces mêmes flèches seront trempées, par Hercule, dans le sang envenimé du monstre vaincu. Derrière la cuisse droite du demi-dieu, la tête de griffon renvoie de toute évidence à l’une des extrémités, ici décorées, de l’arc offert par Apollon. Toujours dans le domaine des armes, il est intéressant de noter que la massue est utilisée par Hercule ; objet insolite s'il en est, lorsqu'il s'agit de trancher les têtes. Il faut remonter au VI<sup>e</sup> siècle avant notre ère pour le voir, sur certains vases à figures noires d'Attique et d'Érétrie, utiliser une épée, tandis que Iolaos manie la _harpè_, arme blanche au tranchant courbe. L'emploi de la massue obéit en effet à une tradition littéraire dont le Pseudo-Apollodore se fait encore l'écho vers la fin du II<sup>e</sup> siècle avant notre ère : Héraclès percute et écrase les têtes, de son bâton taillé dans du bois d'olivier, et ne tranche pas. En l'occurrence, la représentation de la massue en toute circonstance appartient à la sémantique iconographique de ce héros et en permet une reconnaissance immédiate.

La présence du jeune Iolaos, fréquente sur ces céramiques, s'estompe quelque peu à l'époque romaine. Éduqué selon la tradition grecque par celui qui fut perçu comme son tuteur et partenaire (éromène), Iolaos joue le rôle de l'assistant, ou _parastatès_, selon la terminologie employée par Euripide, une fonction qui énonce, implicitement, une relation amoureuse {% cite jourdain-annequin_heracles_1986 -l 293 %}. Sur le relief de Chiragan, son corps apparaît à moitié, à l'arrière d'un rocher, vêtu d'une nébride, peau de daim nouée au niveau de l'épaule gauche. Si le visage a malheureusement été brisé, on distingue bien la coiffure, longue et ondulée, ainsi que le mouvement de la tête, tournée vers Hercule, auquel le jeune garçon apporte son soutien. Abaissant une massue en direction des têtes de l'hydre il en empêche leur repousse grâce à la chaleur du bois incandescent. C'est bien parce que Iolaos participa à cet exploit qu'Eurysthée refusa d'entériner celui-ci, estimant que seul Hercule devait accomplir l'ensemble des Travaux qui lui étaient demandés {% cite pseudo-apollodore_bibliotheque_nodate -L none -l II, 5, 2 %}.

P. Capus