---
title: Corniche
id_inventaire: 2000.173.5
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: III<sup>e</sup>-IV<sup>e</sup> siècle
materiau: Marbre des Pyrénées
hauteur: "8"
largeur: "42"
longueur: 
profondeur: '5,5'
epaisseur: 
id_wikidata: Q47603031
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 6
order: 390
priority: 3
id_notice: 2000-173-5
image_principale: 2000-173-5-1
vues: 1
image_imprime: 
redirect_from:
- 2000-173-5
- ark:/87276/a_2000_173_5
id_ark: a_2000_173_5
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 79-82
- clef: joulin_les_1901
  note: p. 83, pl. V, n° 13

---
