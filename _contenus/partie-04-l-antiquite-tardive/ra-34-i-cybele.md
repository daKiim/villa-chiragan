---
title: Cybèle
id_inventaire: Ra 34 i
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: "91"
largeur: "72"
longueur: 
profondeur: "40"
epaisseur: 
id_wikidata: Q26720955
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 220
priority: 3
id_notice: ra-34-i
image_principale: ra-34-i-3
vues: 4
image_imprime: 
redirect_from:
- ra-34-i
- ark:/87276/a_ra_34_i
id_ark: a_ra_34_i
precisions_biblio:
- clef: aupert_attis_1995
  note: ''
- clef: beckmann_idiom_2020
  note: p. 138-139, fig. 3
- clef: cazes_musee_1999-1
  note: p. 84
- clef: clarac_musee_1841
  note: p. 585
- clef: esperandieu_recueil_1908
  note: n° 892
- clef: rachou_catalogue_1912
  note: n° 34 i
- clef: roschach_catalogue_1892
  note: n° 34 c
- clef: sartre_orient_1991
  note: ''

---
Souvent nommée « Grande Mère des dieux », Cybèle était la patronne de la nature sauvage et de la fertilité. Son culte trouve ses racines en Phrygie (Anatolie) et embrassa peu à peu l'ensemble du bassin méditerranéen. En 204 avant n. è., Rome, menacée par les Carthaginois, dirigés par Hannibal, fait chercher la pierre noire (un probable fragment de météorite), depuis le sanctuaire de la déesse, à Pessinonte, en Phrygie. La _Mater deum magna_ est alors introduite dans l'enceinte sacrée et un temple dédié sur le Palatin, colline des origines de Rome. Le culte, dit métroaque, devient progressivement synonyme de religion universelle et Cybèle une déesse salvatrice pour toutes les couches de la société {% cite sartre_orient_1991 -l 479 %}. Attis,  parèdre de la Mère universelle, bénéficie lui aussi apparemment, tout autant que cette dernière, d'un fort engouement sous l'Empire, dans les provinces occidentales. Leur culte prit, notamment, à Lectoure (Gers) une tournure officielle ; pas moins de vingt-deux autels y témoignent  de la "régénération des forces de l'Empire", de l'époque des Antonins aux années chaotiques des empereurs militaires du III<sup>e</sup> siècle. Il en fut probablement de même, toujours au III<sup>e</sup> siècle, à _Lugdunum Convenarum_ (Saint-Bertrand-de-Comminges) {% cite aupert_attis_1995 -l 189-192 %}.

La couronne endommagée de la Grande Mère, à Chiragan, prend la forme d'un rempart, muni de tours et recouvert d'un voile. Ce couvre-chef renvoie à la protection de la déesse en période de conflit. Certes, les épisodes belliqueux ne manquent pas, en l'occurrence, en Occident, durant le III<sup>e</sup> siècle et encore au début du siècle suivant. Cependant, la présence de la déesse dans la _villa_ témoigne surtout de son intégration à l'assemblée des dieux du panthéon traditionnel. Certainement associés au plus prestigieux des espaces de la _villa_, tous veillaient, depuis leurs supports en forme de bouclier. On peut concevoir la scénographie dont ils pouvaient dépendre : accrochés sur les parties supérieures des parois qui devaient accueillir le cycle riche en références politiques d'Hercule, ces dieux représentaient un lien puissant qui unissait Occident et Orient.

P. Capus