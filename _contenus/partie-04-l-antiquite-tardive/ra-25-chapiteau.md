---
title: Chapiteau de pilastre
id_inventaire: Ra 25
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: IV<sup>e</sup> siècle
materiau: Marbre
hauteur: '35,8'
largeur: "44"
longueur: 
profondeur: '9,3'
epaisseur: 
id_wikidata: Q47546786
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 6
order: 305
priority: 3
id_notice: ra-25
image_principale: ra-25-1
vues: 1
image_imprime: 
redirect_from:
- ra-25
- ark:/87276/a_ra_25
id_ark: a_ra_25
precisions_biblio:
- clef: balmelle_les_2001
  note: p. 223-224, fig. 113
- clef: cazes_musee_1999-1
  note: p. 81
- clef: herrmann_products_2009
  note: p. 63
- clef: joulin_les_1901
  note: pl. V, n° 29 b
- clef: kramer_korinthische_1994
  note: p. 126-127, n° 7-22, pl. 1-3

---
Ce chapiteau fut découvert en 1842, dans le contexte des bains de la _villa_, un ensemble thermal mis au jour entre 1840 et 1843 par la Société archéologique du Midi de la France {% cite cazes_musee_1999-1 joulin_les_1901 -L none -l p. 75 et fig. p. 81 -L none -l p. 80, pl. V, 29 B %}. Deux rangées de feuilles d'acanthe, creusées au trépan, décorent le _calathos_ (corbeille). Trois caulicoles (tiges) à rainure centrale rythment verticalement l’ensemble. Au-dessus, un troisième registre, en faible relief, reprend le motif des hélices et des volutes, chères au système décoratif corinthien dont dépend ce couronnement.

Cependant, il s’agit là d’un ordre corinthien profondément renouvelé par les sculpteurs de l’Antiquité tardive. On trouve des chapiteaux « corinthianisants », très proches de celui-ci, en Asie Mineure, à l’époque tétrarchique. J. Kramer y voyait la production d’un atelier qui travaillait les marbres des carrières de _Docimium_ (Phrygie) {% cite balmelle_les_2001 -L none -l p. 223 et p. 224, fig 113 %} {% cite kramer_korinthische_1994 -L none -l p. 126-127, n° 7 à 22, Pl. 1-3 %}. Peut-être en raison du déplacement de cet atelier, le style a été exporté et des chapiteaux de ce type ont été découverts en Syrie, en Grèce et en Italie {% cite herrmann_products_2009 -l 63 %}.

Une information particulièrement édifiante, concernant l’œuvre de Chiragan, provient des analyses des marbres, engagées depuis 2012. En effet, cet exemplaire, exceptionnel en Gaule, a été taillé dans un marbre pyrénéen de Saint-Béat. Ainsi, provenance locale du matériau employé et style, raisonnablement attribuable au IV<sup>e</sup> siècle, sont autant d’indices qui rattachent ce chapiteau à la grande entreprise de restructuration de la _villa_ qui vit la mise en place d’un somptueux décor, attribuable à un atelier sinon oriental, au moins formé selon des caractères d’origine micrasiatique.

P. Capus