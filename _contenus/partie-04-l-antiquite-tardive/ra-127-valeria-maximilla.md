---
title: Portrait de Valeria Maximilla (?)
id_inventaire: Ra 127
donnees_biographiques1: Fille de l'empereur Galère (César puis Auguste), épouse de
  l'empereur Maxence
donnees_biographiques2: Vers 280 – après 312
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: '32,5'
largeur: '26,5'
longueur: 
profondeur: '24,5'
epaisseur: 
id_wikidata: Q26707632
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 1
order: 40
priority: 3
id_notice: ra-127
image_principale: ra-127-1
vues: 7
image_imprime: 
redirect_from:
- ra-127
- ark:/87276/a_ra_127
id_ark: a_ra_127
precisions_biblio:
- clef: balmelle_les_2001
  note: p. 230-231, fig. 125 d
- clef: balty_les_2008
  note: p. 21-22, 112, 115-119, 121, fig. 100 et 4e de couv.
- clef: bergmann_chiragan_1999
  note: p. 34, 40-41
- clef: cazes_musee_1999-1
  note: p. 144
- clef: ensoli_aurea_2000
  note: p. 459, n° 57
- clef: esperandieu_recueil_1908
  note: p. 92-93, n° 1004
- clef: hannestad_tradition_1994
  note: p. 133
- clef: joulin_les_1901
  note: p. 338-339, pl. XXIII et n° 301 D
- clef: massendari_haute-garonne_2006-1
  note: p. 248, fig. 115
- clef: musee_saint-raymond_regard_1995
  note: p. 238, n° 174
- clef: rachou_catalogue_1912
  note: p. 61, n° 127
- clef: rosso_image_2006
  note: p. 490-491, n° 239

---
Ce portrait pourrait bien être celui de la fille de Galère (César de Dioclétien), qui épousa Maxence en 293. On ne connaît aucun autre exemple de ce type de coiffure dans le portrait antique (curieusement, d'anciennes photographies nous montrent des exemples tout à fait similaires dans la tradition folklorique de Croatie). Si la mode des coiffures « à côtes de melon » remonte au II<sup>e</sup> siècle (avec l'impératrice Faustine la Jeune), jamais celle-ci n'atteignit une aussi grande complexité qu'ici. Coiffeurs et perruquiers romains, on le sait, se surpassaient dans le cadre de leur art.

Cette œuvre, par sa facture et ses proportions, renvoie aux portraits qui l'entourent. Cette série forme un ensemble familial exceptionnel sous la forme de sculptures monumentales, en pied, dont, malheureusement, seules les têtes subsistent.

D'après J.-C. Balty,  _Les portraits romains , La Tétrarchie, 1.5_ (_Sculptures antiques de Chiragan (Martres-Tolosane)_, Toulouse, 2008, p. 111-122.