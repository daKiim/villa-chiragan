---
title: Fragment d’un pilastre ou d’un jambage de porte
id_inventaire: Ra 23 c
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre
hauteur: "105"
largeur: "42"
longueur: 
profondeur: '7,5'
epaisseur: 
id_wikidata: Q48327041
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 6
order: 380
priority: 3
id_notice: ra-23-c
image_principale: ra-23-c-1
vues: 1
image_imprime: 
redirect_from:
- ra-23-c
- ark:/87276/a_ra_23_c
id_ark: a_ra_23_c
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 81
- clef: du_mege_notice_1828
  note: n° 111
- clef: joulin_les_1901
  note: fig. 12 b
- clef: massendari_haute-garonne_2006-1
  note: 'p. 238, fig. 96 '
- clef: rachou_catalogue_1912
  note: n° 23 c
- clef: roschach_catalogue_1892
  note: n° 23 c

---
De grandes feuilles nervées, aplaties et se recouvrant, naissent d’un grand culot d’acanthe.