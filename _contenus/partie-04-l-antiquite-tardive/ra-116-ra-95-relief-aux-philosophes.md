---
title: Assemblée de philosophes (?)
id_inventaire: Ra 116-Ra 95
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: Fin III<sup>e</sup>-IV<sup>e</sup> siècle
date_creation: 
materiau: Marbre
hauteur: 70 (Ra 116) 26 (Ra 95)
largeur: 30 (Ra 116) 35 (Ra 95)
longueur: 
profondeur: 17,5 (Ra 116) 14 (Ra 95)
epaisseur: 
id_wikidata: Q28732591
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 205
priority: 3
id_notice: ra-116-ra-95
image_principale: ra-116-ra-95-1
vues: 1
image_imprime: 
redirect_from:
- ra-116-ra-95
- ark:/87276/a_ra_116_ra_95
id_ark: a_ra_116_ra_95
precisions_biblio:
- clef: beckmann_idiom_2020
  note: p. 142-144, fig. 10
- clef: bergmann_chiragan_1999
  note: p. 33, 36, 69, pl. 1, 3-5, 10
- clef: bergmann_ensemble_1995
  note: p. 197-205
- clef: bulletin_1936
  note: p. 831
- clef: cazes_musee_1999-1
  note: p. 117
- clef: dillon_ancient_2006
  note: ''
- clef: dontas_eikones_1960
  note: ''
- clef: du_mege_description_1835
  note: p. 95, n° 182
- clef: ensoli_aurea_2000
  note: p. 457-458, n° 53
- clef: esperandieu_recueil_1908
  note: p. 59, n° 946
- clef: hannestad_tradition_1994
  note: p. 128
- clef: hermary_socrate_1996
  note: p. 26-30
- clef: joulin_les_1901
  note: fig. 193 B et D
- clef: lebegue_notice_1891
  note: p. 416, pl. 27, 2
- clef: lippold_griechische_1912
  note: p. 54
- clef: massendari_haute-garonne_2006-1
  note: p. 258, fig. 146
- clef: rachou_catalogue_1912
  note: n° 116
- clef: richter_portraits_1965
  note: p. 118
- clef: roschach_catalogue_1865
  note: p. 24-25, n° 31
- clef: scheibler_sokrates_1989
  note: p. 80

---
Deux fragments, réunis à la fin du XIX<sup>e</sup> siècle, composent ce qu'il reste d'un grand tableau de marbre, très mutilé. La partie inférieure, découverte lors des investigations d'Albert Lebègue, en 1890-1891, montre un homme assis, au premier plan, au pied d'un hermès dont la partie supérieure avait été mise au jour dès 1828-1830, à l'occasion des fouilles d'Alexandre Du Mège.

L'hermès est une sculpture composite, probablement née à Athènes au VI<sup>e</sup> siècle avant n. è.  Souvent inscrits de sentences moralisatrices, ces piliers dits hermaïques pouvaient servir de marqueurs de tombes, de matérialisation des limites spatiales et des carrefours, comme de marqueurs de distances sur le territoire, à partir du cœur de la cité. Le type connut une grande fortune dans le monde romain. Il présentait, à l'image de celui qui est évoqué sur le relief de Chiragan, une tête sculptée barbue, en marbre ou en bronze, insérée dans une gaine quadrangulaire. Le visage est ici tourné vers la droite. Bien que le sculpteur ait, conventionnellement, privilégié le profil, le visage formant saillie par rapport au fond laisse voir, lorsqu'on tente de l'observer de face, l'intégralité du nez, le départ de l'arcade sourcilière gauche, une bouche quasi complète ainsi qu'une division des mèches, au-dessus du front. Un large ruban ceint le crâne horizontalement et couvre la partie supérieure du front ; il est maintenu par une bande, plus mince, qui, depuis la nuque, entoure le crâne au niveau du vertex. Une barbe abondante et longue est composée d'enroulements superposés, séparés par un profond sillon horizontal formant deux rangées verticales, elles-mêmes divisées par un creusement au foret. C'est ce même outil qui participe à la création des boucles, s'enroulant autour de trous circulaires profonds, au niveau des terminaisons des mèches temporales comme à l'arrière du cou. Associé aux pointes des mèches, bifides ou trifides, ce rendu de la barbe et de la coiffure équivaut à une véritable signature d'atelier qui se retrouve tant sur les reliefs des Travaux d'Hercule que sur les médaillons des dieux.

Si Léon Joulin faisait de la noble tête de cet hermès un « Jupiter, tourné vers des personnages perdus » {% cite joulin_les_1901 -l 105 %}, A. Hermary, prenant pour preuve le bandeau ceignant le crâne de cette représentation masculine, a proposé d'y reconnaître Pythagore {% cite hermary_socrate_1996 -l 29-30 %}. La <a href="/images/comp-ra-116-95-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-116-95-1.jpg" alt="Buste de Pythagore en hermès musées du Capitole (inv. MC0594), Szilas / Wikimedia Commons CC0"/></span>tête en hermès des Musées du Capitole</a>, devenue canonique et conventionnellement identifiée comme un portrait du philosophe, pourrait fournir quelques points communs avec notre œuvre, tant par le type de support que par la longue barbe effilée. Cependant, le bandeau devient ici turban tandis que la chevelure semble loin de la sophistication des longues boucles, retombant sur les épaules, caractéristiques de la figure engainée de Chiragan. Sur ce dernier point, <a href="/images/comp-ra-116-95-2.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-116-95-2.jpg" alt="Portrait de Socrate de la série de bustes sur boucliers découverts à Aphrodisias, Musée d'Aphrodisias, Carole Raddato / Wikimedia Commons CC BY"/></span>un autre portrait</a>, sans être conforme au relief de Toulouse, pourrait peut-être s'en approcher davantage. Cet exemplaire dépend d'une suite de bustes sur boucliers (_imagines clipeatae_), figures d'immortels, découvertes à Aphrodisias (Carie, Asie Mineure) et conçues entre la fin du IV<sup>e</sup> et le milieu du siècle suivant. La scénographie marmoréenne du bâtiment, interprété comme une école philosophique {% cite smith_late_1990 -L none -l p. 127-128 et 130 %}, intègre une évocation des grande écoles de pensée à travers leurs principaux représentants. Elle comprend donc la tête de Pythagore, que nous indiquons ici à titre de comparaison. Découverte dans les comblements du théâtre en 1968, elle fut associée bien plus tard à son buste originel, sur bouclier (_clipeus_), celui-ci ayant été mis au jour dans un tout autre contexte, en 1981. Il porte le nom, gravé, du grand intellectuel {% cite smith_new_1991 -L none -l p. 142, Pl. XI, 6p. %}. Le portrait d'Aphrodisias, par la composition très symétrique des mèches de la coiffure, comme de la barbe, le visage aux traits impersonnels, qui ne relèvent pas de l'influence d'un quelconque portrait connu de Pythagore, et le drapé plat et synthétique de l'_himation_ (manteau) est proche de l'esprit des médaillons des dieux de Chiragan. Le bandeau, bien que plus simple, semble davantage conforme à celui du site haut-garonnais, et sans rapport aucun avec l'imposante étoffe du turban de l'hermès du Capitole, devenu le portait canonique du mathématicien.

En avant du pilier hermaïque, on reconnaît Socrate. Le maître de Platon demeure aisément identifiable grâce à son nez camus, ses petits yeux très enfoncés, sous des arcades sourcilières saillantes et sa calvitie. Platon {% cite platon_banquet_380 -L none -l 215b-d, 216d, 221d-222a %} et Xénophon {% cite platon_banquet_380 -L none -l V, 7 %} avaient tour à tour comparé les traits du philosophe à ceux d'un silène. Dans le discours amoureux qu'il prononce, Alcibiade insiste bien sur cette particularité : Socrate « ressemble tout à fait à ces Silènes placés dans les ateliers des sculpteurs et représentés par eux avec la syrinx et la flûte...Je soutiens ensuite qu'il ressemble au satyre Marsyas », précisant bien vite que « la seule différence qu'il y ait entre Marsyas et toi, Socrate, c'est que sans instruments, par de simples paroles, tu produis en nous les mêmes effets ». Dans la sculpture, cette physionomie, si caractéristique, sert à restituer le visage du penseur athénien à partir du modèle, qui fut le plus retenu à Rome pour représenter celui-ci, le "type B", conçu vers la fin du IV<sup>e</sup> siècle avant notre ère, qui tempère nettement les traits siléniques caricaturaux {% cite ahbel-rappe_picturing_2006 -l 112-113 %}. Sur le relief de Chiragan, le manteau, simplement posé sur les épaules, dévoile des pectoraux tombant. L'« accoucheur des âmes » est assis, son expression et sa tête en appui sur la main gauche, disparue, suggèrent l'impression d'un état mélancolique et méditatif à la fois.

Un fragment de relief, montrant une tête d'homme barbu, mutilée, a été rapproché du tableau de marbre au Socrate. Le sujet déborde du champ du relief et empiète sur un cadre mouluré qui semble prolonger celui de la scène aux philosophes {% cite cazes_musee_1999-1 -l 117 %}. L'hermès "pythagoricien" y débordait de manière similaire, à l'image par ailleurs du cycle d'Hercule dans lesquels les figures s'affranchissaient des bordures. Il s'agit bien là d'un trait qui, sans être unique dans le cadre de la sculpture antique en relief, distingue cependant, à Chiragan, les compositions de cet atelier, engagé dans la fastueuse décoration de la _villa_ durant l'Antiquité tardive.

La présence de philosophes au sein des villas est un phénomène connu. Socrate, père fondateur de la philosophie, au même titre qu'Aristote, apparaissait dans le cadre privé ou semi-public des résidences romaines au moins dès le début de l'Empire {% cite zanker_maske_1995 sauron_les_2009 -L none -l p. 44, fig. 23 -L none -l p. 272, fig. 229 et p. 275 %}. Maintenir leur image à la fin de l'Antiquité ne doit pas nous étonner, malgré la rareté des exemplaires découverts. Comme nous l'avons mentionné plus haut à propos de Pythagore, plusieurs figures sur boucliers à Aphrodisias, au sein d'un édifice qui semble être un lieu d'enseignement destiné à une élite, représentaient de grands penseurs de l'Antiquité classique mais également tardive {% cite smith_late_1990 -l 127-155 %}. C'est également sous forme d'hermès que l'on présentait les portraits des philosophes, dans les jardins ou les péristyles, encore des siècles plus tard, comme le prouve le relief de Chiragan. Le portrait en hermès entretenait un lien avec le dieu dont il prit le nom, ce voyageur et messager, fils de Zeus et de Maïa, en particulier en raison de sa fonction de diffuseur de la parole (Hermès _Logios_) {% cite dillon_ancient_2006 -l 31 %}. Autre demeure et nouvel exemple, la <a href="/images/comp-ra-116-95-3.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-116-95-3.jpg" alt="Série de bustes de la villa de Welschbillig (Rhénanie-Palatinat) Rheinisches Landemuuseum, Kleon3 / Wikimedia Commons CC BY-SA"/></span>villa de Welschbillig (Rhénanie-Palatinat)</a>, a également livré une preuve de l'importance accordée, durant l'Antiquité tardive, aux antiques écoles philosophiques. Cette luxueuse demeure de Gaule belgique, datée du IV<sup>e</sup> siècle, accueillit une exceptionnelle série de bustes en hermès qui circonscrivait un très grand bassin. Lettrés et grands militaires formaient un ensemble composé de 68 portraits, dont la diversité et les références érudites viennent combler quelque peu la médiocre qualité de la sculpture. On y distingue Socrate {% cite wrede_spatantike_1972 -l 46-54 %}.

Les mosaïques des riches demeures dévoilent également des thèmes iconographiques semblables, privilégiés par leurs propriétaires. En Allemagne, le tapis de pierre, daté de la fin de la période sévérienne, qui fut découvert au niveau de la cathédrale de Cologne, représente une série de pentagones incorporant des philosophes, accompagnés de leurs noms, écrits en grec. Diogène, au centre, y côtoie notamment Socrate ; cependant, et à l'inverse de Chiragan, seule l'inscription, et non des spécificités physiques, permet ici de reconnaître le maître, représenté comme un lettré de convention, sous forme stéréotypée {% cite lucciano_les_2012 -L none -l p. 11-12, ill. 1 %}.

En définitive, si le relief "aux philosophes" a pu être rattaché au Haut-Empire, la seconde moitié du II<sup>e</sup> siècle {% cite hermary_socrate_1996 -l 28 %} ou, plus largement, entre les II<sup>e</sup> et III<sup>e</sup> siècle {% cite cazes_musee_1999-1 -l 117 %}, il faut, bien au contraire, en abaisser la date et associer ce panneau à une production tardive. Elle rejoint ainsi l'essentiel de la sculpture en marbre de Saint-Béat qui caractérise la villa de la fin du III<sup>e</sup> sinon du premiers tiers du IV<sup>e</sup> siècle.

P. Capus