---
title: Diane (?)
id_inventaire: Ra 34 h
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: "86"
largeur: "74"
longueur: 
profondeur: "38"
epaisseur: 
id_wikidata: Q48235114
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 230
priority: 3
id_notice: ra-34-h
image_principale: ra-34-h-3
vues: 5
image_imprime: 
redirect_from:
- ra-34-h
- ark:/87276/a_ra_34_h
id_ark: a_ra_34_h
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 85
- clef: du_mege_description_1835
  note: n° 155
- clef: du_mege_notice_1828
  note: n° 68
- clef: esperandieu_recueil_1908
  note: n° 892.2
- clef: rachou_catalogue_1912
  note: n° 34 k
- clef: roschach_catalogue_1892
  note: n° 34 f, g, h

---
Il est difficile d’identifier la déesse figurée par cette sculpture. D. Cazes suggérait que le diadème lisse pouvait renvoyer à la déesse de la chasse et de la nature sauvage : Diane (l'Artémis grecque). La fille de Jupiter et de Latone, dont le principal sanctuaire latin était celui d'Aricie (Latium), semble avoir été très populaire dans les contextes domestiques des III<sup>e</sup> et IV<sup>e</sup> siècles où on la retrouve sous forme de statuettes. Très souvent, son image accompagne celles d’Esculape, d’Hercule et de Pan. C'est bien le cas à Chiragan, malgré  l'absence de témoignage du dernier de ces dieux pour cette période.

P. Capus