---
title: Vénus (?)
id_inventaire: Ra 34 f
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: "45"
largeur: "32"
longueur: 
profondeur: "31"
epaisseur: 
id_wikidata: Q28672675
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 290
priority: 3
id_notice: ra-34-f
image_principale: ra-34-f-1
vues: 8
image_imprime: 
redirect_from:
- ra-34-f
- ark:/87276/a_ra_34_f
id_ark: a_ra_34_f
precisions_biblio:
- clef: du_mege_description_1835
  note: n° 157
- clef: du_mege_notice_1828
  note: n° 70
- clef: esperandieu_recueil_1908
  note: p. 32, n° 892, fig. 5
- clef: joulin_les_1901
  note: fig. 52 B
- clef: massendari_haute-garonne_2006-1
  note: p. 252-253, fig. 135
- clef: rachou_catalogue_1912
  note: n° 34 f

---
Cette tête féminine pourrait représenter Vénus (l'Aphrodite grecque), déesse du charme, de l’envoûtement et de l’amour. L'attachement particulier qu'entretint Rome à son encontre est intimement lié à son statut de mère d'Enée, ancêtre mythique du peuple romain.

Elle porte un diadème lisse, comme [Diane](/ra-34-h), représentée sur l'un des médaillons des dieux ; une série à laquelle elle était à l'origine intégrée, comme le prouve les dimensions de cette tête ainsi que l'arrachement situé à l'arrière.

P. Capus