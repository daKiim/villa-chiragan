---
title: Minerve/Athéna
id_inventaire: Ra 30
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: "42"
largeur: "40"
longueur: 
profondeur: '12,5'
epaisseur: 
id_wikidata: Q26719033
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 180
priority: 3
id_notice: ra-30
image_principale: ra-30-1
vues: 1
image_imprime: 
redirect_from:
- ra-30
- ark:/87276/a_ra_30
id_ark: a_ra_30
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 99
- clef: du_mege_description_1835
  note: p. 587
- clef: du_mege_notice_1828
  note: n° 93
- clef: esperandieu_recueil_1908
  note: n° 916
- clef: joulin_les_1901
  note: pl. IX, n° 113 B
- clef: massendari_haute-garonne_2006-1
  note: p. 252, fig. 131
- clef: rachou_catalogue_1912
  note: n° 30
- clef: roschach_catalogue_1892
  note: n° 30

---
Cette déesse a aidé Hercule pour certains de ses travaux. La mouluration, sur laquelle déborde le bouclier de la figure, indique que ce fragment devait se trouver sur la bordure droite de l’un des reliefs d’Hercule. Il pourrait s’agir de la scène représentant le fils de Zeus et d'Alcmène étouffant le lion de Némée, dont seule la [tête](/ra-28-a) fut retrouvée, nue par conséquent, car encore dépourvue de la léonté.

P. Capus