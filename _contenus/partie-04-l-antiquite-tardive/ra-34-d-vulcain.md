---
title: Vulcain
id_inventaire: Ra 34 d
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup>-premier tiers du IV<sup>e</sup> siècle
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: "47"
largeur: "31"
longueur: 
profondeur: "30"
epaisseur: 
id_wikidata: Q28536081
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 270
priority: 3
id_notice: ra-34-d
image_principale: ra-34-d-1
vues: 8
image_imprime: 
redirect_from:
- ra-34-d
- ark:/87276/a_ra_34_d
id_ark: a_ra_34_d
precisions_biblio:
- clef: balty_les_2008
  note: p. 127, fig. 105
- clef: beckmann_idiom_2020
  note: p. 138-139, fig. 5
- clef: cazes_musee_1999-1
  note: p. 83
- clef: du_mege_description_1835
  note: n° 159
- clef: du_mege_notice_1828
  note: n° 72
- clef: esperandieu_recueil_1908
  note: n° 892, fig. 3
- clef: joulin_les_1901
  note: fig. 55 b
- clef: massendari_haute-garonne_2006-1
  note: p. 252-253, fig. 132
- clef: rachou_catalogue_1912
  note: n° 34 d
- clef: roschach_catalogue_1892
  note: n° 34 b

---
La tête dépendait à l'origine de la série des médaillons des dieux. Il s'agissait donc de l'une des figures sur bouclier (_imagines clipeatae_), comme le laisse penser la similitude de hauteur ainsi que l'arrachement visible à l'arrière.

Vulcain (l'Héphaïstos grec) était le dieu du feu, des forges et des volcans. Habile artisan, il fut le concepteur de nombreuses armes pour les héros. Il est ici représenté sous les traits d’un homme d’âge mûr, coiffé d’un bonnet conique en cuir épais, caractéristique des artisans. La force et l'impression de puissance de cette tête proviennent en particulier de l'ampleur de la barbe foisonnante. Les boucles épaisses y sont, en effet, enrichies de profondes cavités, creusées au trépan, sorte de foret dont la mèche permettait de créer ces zones d'ombre si caractéristiques et chères aux sculpteurs de cet atelier, fortement imprégné de l'esthétique orientale.

P. Capus