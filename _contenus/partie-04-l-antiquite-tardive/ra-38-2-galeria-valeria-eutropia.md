---
title: Portrait de Galeria Valeria Eutropia (?)
id_inventaire: Ra 38 (2)
donnees_biographiques1: Épouse de Maximien Hercule, mère de Maxence et de Fausta.
donnees_biographiques2: Années cinquante du III<sup>e</sup> siècle – 330
type_oeuvre: 
date_creation: Fin III<sup>e</sup>, premières années du IV<sup>e</sup> siècle
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: "31"
largeur: "28"
longueur: 
profondeur: "25"
epaisseur: 
id_wikidata: Q26707714
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 1
order: 20
priority: 3
id_notice: ra-38-2
image_principale: ra-38-2-1
vues: 8
image_imprime: 
redirect_from:
- ra-38-2
- ark:/87276/a_ra_38_2
id_ark: a_ra_38_2
precisions_biblio:
- clef: balmelle_les_2001
  note: p. 230-231, fig. 125 b
- clef: balty_les_2008
  note: p. 16-17, 96, 98 fig. 74, 76, p. 99 fig. 78, p. 102-104, 106 fig. 86
- clef: bergmann_chiragan_1999
  note: p. 34, 40, pl. 11, 1-4
- clef: cazes_musee_1999-1
  note: p. 143
- clef: ensoli_aurea_2000
  note: p. 458-459, n° 56
- clef: esperandieu_recueil_1908
  note: p. 50-51, n° 927
- clef: joulin_les_1901
  note: p. 319-320, pl. XIII
- clef: massendari_haute-garonne_2006-1
  note: p. 248, fig. 113
- clef: musee_saint-raymond_regard_1995
  note: p. 237, n° 173
- clef: rachou_catalogue_1912
  note: n° 38
- clef: rosso_image_2006
  note: p. 488-489, n° 237

---
La coiffure rappelle celle d'Hélène, mère de l'empereur Constantin. Cette mode est apparue à la fin du III<sup>e</sup> siècle. Sur certains sarcophages de la première moitié du IV<sup>e</sup> siècle, des femmes de très haut rang sont également coiffées de la sorte. Un rouleau de cheveux encadre le visage, une grande tresse forme un « turban » qui, depuis l'arrière, remonte en bandeaux aplatis se glissant sous les enroulements du dessus et s'achevant au milieu du front en huit mèches frisées au fer. Ce portrait peut être relié aux trois autres têtes présentées ici. Il s'agit donc d'un très rare groupe dynastique.

D'après J.-C. Balty,  _Les portraits romains , La Tétrarchie, 1.5_ (_Sculptures antiques de Chiragan (Martres-Tolosane)_, Toulouse, 2008, p. 95-109.