---
title: Hercule et le roi Diomède
id_inventaire: Ra 28 i
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: 38 (jument) 105 (Hercule) 31 (Diomède)
largeur: 49 (jument) 54 (Hercule) 30 (Diomède)
longueur: 
profondeur: 13 (jument) 15 (Hercule) 16 (Diomède)
epaisseur: 
id_wikidata: Q24649999
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 110
priority: 3
id_notice: ra-28-i
image_principale: ra-28-i-1
vues: 1
image_imprime: 
redirect_from:
- ra-28-i
- ark:/87276/a_ra_28_i
id_ark: a_ra_28_i
precisions_biblio:
- clef: balty_les_2008
  note: p. 47, 128, fig. 107
- clef: bergmann_chiragan_1999
  note: p. 26-43, pl. 4.2, 20.4
- clef: cazes_musee_1999-1
  note: p. 91-93
- clef: du_mege_description_1835
  note: n° 166
- clef: du_mege_notice_1828
  note: n° 78
- clef: ensoli_aurea_2000
  note: p. 457, n° 51
- clef: esperandieu_recueil_1908
  note: p. 36-37, n° 899 (3)
- clef: joulin_les_1901
  note: p. 90, pl. IX, n° 104 b
- clef: massendari_haute-garonne_2006-1
  note: p. 251, fig. 128
- clef: musee_archeologique_henri_prades_cirque_1990
  note: p. 383, n° 106, p. 388, fig. 106
- clef: musee_saint-raymond_cirque_1990-1
  note: p. 76, n° 25
- clef: rachou_catalogue_1912
  note: p. 29, n° 28 i
- clef: roschach_catalogue_1865
  note: n° 28 g
- clef: ziegle_hercule_2002
  note: p. 15

---
Huitième des douze Travaux, l'affrontement avec le roi Diomède, fils du dieu guerrier Arès/Mars, dirige Héraclès/Hercule vers la Thrace. Le souverain y possédait quatre juments, portant les noms de _Podargos_ («Aux pieds légers»)_, Lampros_ («Brillant»)_, Xanthos_ («Roux») et _Deinos_ («Terrible»). Tout étranger pénétrant dans le royaume de Diomède était mis à mort par l'autocrate et son corps utilisé comme nourriture pour les chevaux royaux. Hercule, chargé par Eurysthée de capturer l'élevage, tua le souverain et livra le corps en pâture aux animaux carnivores. Ainsi, une fois leur maître dévoré, le héros pût-il imposer le mors aux quatre juments {% cite euripide_folie_416bc -l 380-385 %}, les rendre dociles et les emmener chez son cousin, à Mycènes.

En Grèce, l'iconographie de ce travail ne serait apparemment pas antérieure au dernier quart du VI<sup>e</sup> siècle avant n. è. Le monde romain lui-même ne le retint que très peu, hors les sarcophages des II<sup>e</sup> et III<sup>e</sup> siècles relatant les douze Travaux, ainsi que quelques émissions monétaires provinciales, à l'image d'un bronze de Thrace, sur lequel la mise en scène rappelle celle de Chiragan, mais inversée {% cite vollkommer_herakles_2013 jongste_twelve_1992 noauthor_rpc_nodate -l 91 -L none -l p. 19-20 et n° F3 -L none -l IV.1, 10483 %}.

Des juments, représentées à l'origine sur le relief de Chiragan, seule une tête a été retrouvée. En dépit des importantes lacunes, la puissance expressive de la scène demeure pourtant. L'attitude du héros détermine amplement l'impression de grandeur du drame qui se joue. De dos, buste de trois quarts et tête tournée vers la gauche, Hercule maintient fermement Diomède par la chevelure. Le roi saisit en même temps son agresseur par la cuisse. Le traitement des poils de la léonté, dont la crinière, telle une imposante chevelure, se répand sur la tête et les épaules du héros, est similaire à celle des cheveux du [portrait de l’empereur Maximien](/ra-34-b), découvert dans la _villa_. La statue du tétrarque, conçue de toute évidence au même moment, par le même atelier, devait s'élever théâtralement non loin des Travaux du fils de Jupiter, un cycle qui devenait la métaphore des actions impériales.

P. Capus