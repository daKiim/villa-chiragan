---
title: Hercule et les Pommes d’or des Hespérides
id_inventaire: Ra 28 f
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: "148"
largeur: "63"
longueur: 
profondeur: "21"
epaisseur: 
id_wikidata: Q24662568
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 130
priority: 3
id_notice: ra-28-f
image_principale: ra-28-f-1
vues: 1
image_imprime: 
redirect_from:
- ra-28-f
- ark:/87276/a_ra_28_f
id_ark: a_ra_28_f
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 98
- clef: du_mege_description_1835
  note: n° 172
- clef: du_mege_notice_1828
  note: n° 82
- clef: esperandieu_recueil_1908
  note: p. 36-37, n° 899 (4)
- clef: joulin_les_1901
  note: n° 91 B, 119 A, 105 B, 120 BDE
- clef: massendari_haute-garonne_2006-1
  note: 'p. 251, fig. 125 '
- clef: rachou_catalogue_1912
  note: p. 29, n° 28 k
- clef: roschach_catalogue_1865
  note: n° 28 i

---
Onzième des douze travaux, l'épreuve du jardin des Hespérides se situe sur les marges du monde connu, au niveau du Mont Atlas. Pour Hésiode, Atlas est situé «par delà l'illustre Océan, vers l'empire de la Nuit, dans ces lointaines contrées, où demeurent les Hespérides à la voix sonore» {% cite hesiode_theogonie_nodate -l 275 %}, et Apollodore voit derrière ce toponyme le pays des Hyperboréens, contrée septentrionale mystérieuse, où résidait Apollon tous les hivers {% cite pseudo-apollodore_bibliotheque_nodate bonnet_heracles_1992 -L none -l II, 5, 11 -l 289 %}. Mais c'est majoritairement au niveau de _Lixos_, comptoir phénicien fondé sur la côte atlantique marocaine, que fut situé l'épisode. Junon (l’Héra grecque) avait reçu en cadeau de mariage de Gaïa (déesse de la terre) des pommes d’or. Elle les fit garder, là où le soleil se couche, par un serpent immortel et trois "nymphes du soir" : les Hespérides (d'_Hesperos_, «le soir»). L'or, métal inaltérable, incorruptible, qui force depuis toujours l'admiration des hommes, devenait ici le symbole même de l'éternité des dieux et partant, de l'immortalité ; celle qui sera, en l'occurrence, accordée par Zeus à Hercule, après ses douze Travaux.

Pour cueillir ces pommes d’or, le héros demanda l’aide du géant Atlas. Mais selon d’autres versions, Hercule tua le serpent d’une flèche et cueillit lui-même les pommes. Eurysthée, ne sachant que faire des fruits d’or, les rendit à Hercule qui préféra les offrir à une déesse, celle qui le soutint dans toutes ses épreuves : Minerve (l’Athéna grecque). Si celle-ci les rapporta dans le jardin (_képos_), car la loi divine interdisait de les extraire du jardin sacré, l'acte de l'offrande, de la part d'Hercule, prend un sens allégorique qui renvoie à la raison et à la piété à l'égard des dieux. Le héros devient celui qui combat les passions terrestres, à l'origine du désordre et du chaos {% cite pensabene_villa_2010 -l 42 %}.

Le relief exprime le travail achevé. Hercule tient les fruits dans sa main gauche, la lanière du carquois en travers du corps. En haut à droite, parmi les feuilles, on remarque la hampe d’une flèche : celle qui a tué le serpent. La composition de ce relief est très similaire à celle qui représente la mort des[ oiseaux du lac Stymphale.](/ra-28-g)

P. Capus