---
title: Tête nue d'Hercule
id_inventaire: Ra 28 a
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: "31"
largeur: "22"
longueur: 
profondeur: "20"
epaisseur: 
id_wikidata: Q24649089
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 150
priority: 3
id_notice: ra-28-a
image_principale: ra-28-a-1
vues: 3
image_imprime: 
redirect_from:
- ra-28-a
- ark:/87276/a_ra_28_a
id_ark: a_ra_28_a
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 86
- clef: du_mege_description_1835
  note: n° 176
- clef: du_mege_notice_1828
  note: n° 84
- clef: esperandieu_recueil_1908
  note: p. 36, n° 899, fig. 2
- clef: joulin_les_1901
  note: fig. 111 b
- clef: massendari_haute-garonne_2006-1
  note: 'p. 250, fig. 120 '
- clef: rachou_catalogue_1912
  note: n° 28 a

---
Cette tête juvénile dépendait sans aucun doute du cycle des Travaux d'Hercule. Il s'agit, très probablement, de la première épreuve : l'étouffement du lion, à Némée, dans le Péloponnèse {% cite cazes_musee_1999-1 -l 86 %}. Aucune arme ne pouvant pénétrer la peau du félin, Hercule l'étouffa donc. Il se servit ensuite des griffes du fauve pour le dépecer. Cette léonté devint la cuirasse qui, à l'avenir, le protégerait.

Ainsi, le visage glabre et l'absence de la dépouille du fauve en guise de couvre-chef, que le héros devait donc endosser dans tous les autres Travaux de la série, sont autant d'indices décisifs pour la reconnaissance de cet épisode qui prenait la forme d'une épreuve initiatique. Le mythe du héros combattant le lion trouve des parallèles dans les civilisations les plus anciennes, en particulier au Proche-Orient avec notamment le récit légendaire du roi Gilgamesh. Le lion représentait toutes les forces de la nature que seul un héros, autrement dit un être personnifiant la suprématie humaine, pouvait contrarier avant de se les accaparer. Comparable aux préceptes d'une éducation aristocratique à la grecque (_paideia_), la lutte contre un être fabuleux, ou réputé invulnérable, autorise l'élévation et le passage à un niveau d'action supérieur {% cite presicce_eracle_2013 -l 141-142 %}. Hercule, encore jeune, sera donc autorisé à poursuivre sa mission, héroïque, bien plus que valeureuse.

P. Capus