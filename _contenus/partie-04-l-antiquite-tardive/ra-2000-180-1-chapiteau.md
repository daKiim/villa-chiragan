---
title: Chapiteau de pilastre
id_inventaire: 2000.180.1
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre
hauteur: "20"
largeur: "30"
longueur: 
profondeur: '8,5'
epaisseur: 
id_wikidata: Q47532029
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 6
order: 310
priority: 3
id_notice: 2000-180-1
image_principale: 2000-180-1-1
vues: 1
image_imprime: 
redirect_from:
- 2000-180-1
- ark:/87276/a_2000_180_1
id_ark: a_2000_180_1
precisions_biblio:
- clef: balmelle_les_2001
  note: ''
- clef: cazes_musee_1999-1
  note: p. 79-82
- clef: joulin_les_1901
  note: p. 84, pl. V, n° 18 B
- clef: kramer_korinthische_1994
  note: ''

---
Si le style de ce chapiteau doit être rattaché au corinthien, caractérisé par des rangées de feuilles superposées, le sculpteur se libère ici des conventions de l'époque classique. Les feuilles, grasses et plates, se distinguent les unes des autres au moyen de percements en forme de goutte, réalisés au trépan (sorte de foret). Il s'agit là d'une pratique qui ressort davantage de l'esthétique orientale. C'est bien vers l'Asie Mineure, et notamment Constantinople, qu'il faut se tourner pour constater une similarité de composition et de technique de taille. Ainsi, cet élément de couronnement renvoie-t-il, une fois encore, à l'engagement, au sein de la _villa_ de Chiragan, d'ateliers d'origine orientale, au moins soumis aux courants esthétiques favorisés en Asie Mineure.

P. Capus