---
title: Hercule et le taureau de Crète
id_inventaire: Ra 28 c
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: "49"
largeur: "48"
longueur: 
profondeur: "20"
epaisseur: 
id_wikidata: Q24649914
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 160
priority: 3
id_notice: ra-28-c
image_principale: ra-28-c-1
vues: 1
image_imprime: 
redirect_from:
- ra-28-c
- ark:/87276/a_ra_28_c
id_ark: a_ra_28_c
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 91
- clef: du_mege_description_1835
  note: n° 165
- clef: du_mege_notice_1828
  note: n° 77
- clef: joulin_les_1901
  note: n° 117 B
- clef: massendari_haute-garonne_2006-1
  note: 'p. 250, fig. 122 '
- clef: rachou_catalogue_1912
  note: n° 28 c
- clef: roschach_catalogue_1865
  note: n° 28 h

---
Le septième des douze Travaux prend pour cadre l'île de Crète. Un taureau furieux y répandait la terreur. Hercule parvint à le maîtriser en l'empoignant par les cornes et à le ramener à son cousin Eurysthée.

S'il ne subsiste, du grand relief originel, qu’un seul fragment identifiable, celui-ci est de très grande qualité. Tandis que d'une main, Hercule saisit l’une des cornes, il enserre de son bras le cou de l'animal dont la langue pendante exprime bien l'agonie.

P. Capus