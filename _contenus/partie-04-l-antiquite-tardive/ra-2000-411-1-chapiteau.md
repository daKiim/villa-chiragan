---
title: Chapiteau corinthien à tête de feuillage
id_inventaire: 2000.411.1
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du III<sup>e</sup> siècle
materiau: Marbre
hauteur: '49,5'
longueur: 
largeur: "76"
profondeur: '17,2'
epaisseur: 
id_wikidata: Q47531958
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 6
order: 300
priority: 3
id_notice: 2000-411-1
image_principale: 2000-411-1-1
vues: 1
image_imprime: 
redirect_from:
- 2000-411-1
- ark:/87276/a_2000_411_1
id_ark: a_2000_411_1
precisions_biblio:
- clef: agusta-boularot_les_2016
  note: ''
- clef: beckmann_idiom_2020
  note: p. 145-146, fig. 11
- clef: cazes_musee_1999-1
  note: p. 81
- clef: hirschland_head-capitals_1967
  note: p. 14
- clef: joulin_les_1901
  note: pl. IV, n° 6 B
- clef: massendari_haute-garonne_2006-1
  note: p. 240, fig. 98
- clef: mercklin_antike_1962
  note: ''
- clef: musee_saint-raymond_palladia_1988
  note: ''
- clef: picard_acroteres_1963
  note: ''

---
Une série de chapiteaux de ce type étaient, à Chiragan, prévus pour des pilastres. Un autre chapiteau, seulement dégrossi, est conservé dans les réserves du musée. Une tête entourée de feuillage surmonte ici deux rangs de feuilles d’acanthe. La tête prend l'apparence d'un masque. Elle est située au niveau de l'abaque, au-dessus de l'acanthe centrale du _calathos_ (corbeille) et vient donc ici remplacer la rosace de l'ordre corinthien canonique.

Le motif de la tête associée au chapiteau était déjà connu dans l'Égypte pharaonique ainsi qu'en témoigne le célèbre type dit «hatorique» {% cite bernhauer_hathorsaulen_2005 %}. Une composition différente, et conforme aux sensibilités gréco-romaines, réunit à nouveau les deux éléments en Occident, durant l'Antiquité classique. Cette thématique ornementale récurrente se retrouve en Étrurie, à Vulci ou à Sovana, et dans le contexte culturel grec de l'Italie méridionale, en Campanie ou en Apulie, à Tarente, Arpi et Canosa, qui fournissent de nombreux exemples {% cite pensabene_il_1990 -L none -l 283-285 et 302-306, fig. 13, 18, Pl. CXV-CXIX %}. Au I<sup>er</sup> siècle avant n. è., ce sont des chapiteaux et des acrotères à figures à _Glanum_ (Saint-Rémy-de-Provence), Vernègues, Nîmes, Toulouse ou, plus récemment, Narbonne (la Nautique), entre autres, qui témoignent de cette association de figures à un élément de couronnement {% cite mercklin_antike_1962 picard_acroteres_1963 musee_saint-raymond_palladia_1988 agusta-boularot_les_2016 -L none -l 109, no299-300 et fig. 534-535 et 538, 112, no313 et fig. 555-556 -L none -l p. 168-174, fig. 46, 48 et 49 -L none -l n° 256, daté du I<sup>er</sup> siècle de n. è. -L none -l p. 283, fig. 10-12, p. 286-288, fig. 17 et 18 %}. C'est apparemment l'Asie Mineure qui aurait produit, à partir de la fin du II<sup>e</sup> siècle, et durant le siècle suivant, des chapiteaux associant notamment des masques aux feuillages {% cite hirschland_head-capitals_1967 -l 14 %}. Au style et aux schémas de ces productions orientales auraient été formées des officines de sculpteurs qui exercèrent en Occident, et à Rome en premier lieu, durant l'Antiquité tardive. L'atelier qui fut à l'origine du grand décor de Chiragan en fait partie.

P. Capus