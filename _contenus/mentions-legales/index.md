---
title: Mentions légales
layout: page
def: mentions_legales
type: statique

---
Le contenu multimédia du catalogue des sculptures de la _villa_ de Chiragan est la propriété de la mairie de Toulouse.

Les [données d'inventaire](https://data.toulouse-metropole.fr/explore/dataset/inventaire-principal-musee-saint-raymond/information/?q=chiragan), les images des sculptures du musée Saint-Raymond, les textes et les plans sont publiés sous la [Licence Ouverte Etalab](https://www.etalab.gouv.fr/wp-content/uploads/2017/04/ETALAB-Licence-Ouverte-v2.0.pdf) en accord avec la [politique d'_open data_ de Toulouse Métropole](https://data.toulouse-metropole.fr/page/modedemploi/).

Les images réutilisées seront accompagnées de la mention suivante : Titre de la sculpture, n° d'inventaire, musée Saint-Raymond, musée d'Archéologie de Toulouse, photo : Daniel Martin.

Toutes les autres images sont reproduites avec l'autorisation des titulaires de droits mentionnés dans les légendes et sont expressément exclues des licences libres couvrant le reste de cette publication. Sauf mention contraire, ces images ne peuvent être reproduites, copiées, transmises ou manipulées sans le consentement des propriétaires.

Les logos de la mairie de Toulouse et du musée Saint-Raymond sont protégés par le droit de la propriété intellectuelle. Ainsi, toute reproduction ou modification (totale ou partielle) de ces éléments graphiques sans autorisation préalable et écrite est interdite.

Fichiers sources du catalogue: [https://gitlab.com/musee-saint-raymond/villa-chiragan](https://gitlab.com/musee-saint-raymond/villa-chiragan)