---
title: L'enlèvement de Perséphone
id_inventaire: Ra 152
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: I<sup>er</sup> siècle
materiau: Marbre
hauteur: "43"
largeur: "46"
longueur: ''
profondeur: '4,5'
epaisseur: 
id_wikidata: Q25216237
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe: 
order: 110
priority: 3
id_notice: ra-152
image_principale: ra-152-1
vues: 1
image_imprime: 
redirect_from:
- ra-152
- ark:/87276/a_ra_152
id_ark: a_ra_152
precisions_biblio:
- clef: bulletin_1936
  note: p. 835, n° 152, p. 837
- clef: cazes_musee_1999-1
  note: p. 74, 116-117
- clef: esperandieu_recueil_1908
  note: p. 35-36, n° 898
- clef: joulin_les_1901
  note: p. 97-98, pl. XI, n° 134 E
- clef: labrousse_art_1956
  note: p. 31, n° 34
- clef: massendari_haute-garonne_2006-1
  note: p. 257, fig. 144
- clef: musee_petrarque_triomphe_2004
  note: p. 103, n° 58
- clef: musee_saint-raymond_cirque_1990-1
  note: n° 96
- clef: musee_saint-raymond_essentiel_2011
  note: p. 34-35
- clef: rachou_catalogue_1912
  note: p. 66, n° 152

---
Les tableaux de marbre sont très rares. Ils s’inspirent des œuvres peintes à fresque ou à l'encaustique, sur support de bois, et transportables : les _pinakes_ grecs, que l'on nomme _tabulae_ en latin, si fréquents dans les belles demeures. Entre 1897 et 1899, Léon Joulin eut le bonheur de découvrir celui-ci, remarquable petit relief néo-attique, brisé en plusieurs morceaux. Daté du Haut-Empire, il met en scène le rapt de Perséphone, ou Korè, par Hadès, dieu des Enfers (Proserpine et Pluton à Rome). Hermès, messager des dieux, indique la route à emprunter. Athéna, armée, et une des compagnes de la captive tentent d’arrêter le char.

Il s'agit de l'un des grands mythes grecs, sur lequel se fondaient les fameux mystères d'Éleusis, dont les liturgies étaient encore bien vivantes sous l'Empire romain. La jeune Perséphone, fille de Déméter et de Zeus, cueillait un narcisse dans la campagne sicilienne lorsque d'une grande crevasse surgit un quadrige mené à vive allure par Hadès. Épris de Perséphone, le dieu des Enfers s'empare d'elle. Informée de ce malheur, désespérée, Déméter part à la recherche de sa fille. Elle apprend du soleil, seul témoin du drame, que le maître des puissances souterraines est le ravisseur. Aphrodite lui a inspiré cet amour coupable, Zeus a donné son consentement et Hermès lui-même a apporté son aide. Afin de se venger des quatre dieux complices, Déméter abandonne l'Olympe et empêche toute plante de pousser, rendant ainsi la terre stérile. Afin que le sol retrouve sa fertilité, Perséphone est contrainte de partager son existence entre Hadès et Déméter : elle doit résider la première partie de l'année aux Enfers et la suivante sur Terre. Le retour annuel de la jeune fille se traduit par la renaissance de la végétation, tout particulièrement la germination des céréales, semées par Triptolème, héros d'Éleusis, protégé de Déméter. Si la prospérité des champs garantit la bonne alimentation et la vie, l'enlèvement de la jeune fille et son séjour aux Enfers sont, au contraire, une allégorie de la mort.

Le tableau de Chiragan montre le quadrige conduit par un Amour ailé qui, responsable des sentiments d'Hadès, l'est aussi de toute l'action qui en découle. Deux autres Amours volent dans les airs, l'un d'eux tenant une couronne de victoire au-dessus d'Hadès. Ce dernier, se retournant brusquement, tient d'une main l'Amour cocher et porte Perséphone sur ses épaules. Celle-ci se débat vigoureusement, vêtement et chevelure au vent, poussant un grand cri qui alerte l'univers. L'une de ses compagnes, peut-être sa demi-sœur Artémis, est surprise, accroupie devant un vase. Son autre demi-sœur, Athéna, portant casque, lance et bouclier, affronte Hermès, qui indique la route des Enfers. Une troisième femme, nymphe, déesse ou simple amie de Perséphone, tente de trancher de son glaive les rênes de l'attelage. À ce moment précis, la course est perturbée par l'attaque d'un grand serpent, symbole chtonien, autrement dit des forces matricielles du sous-sol. Le reptile effraie et fait se cabrer les noirs et immortels chevaux. À travers cette image, aux multiples actions simultanées, les forces de la nature et les dieux sont en conflit. Le mouvement des draperies, tantôt moulées sur le corps, tantôt flottantes ainsi que les rythmes saccadés concourent à l'animation extrême d'un relief dont les détails accentuent aussi la qualité.

Un second tableau, moins complet que celui-ci, fut également retrouvé dans les vestiges de la _villa_ ([_inv_. Ra 32](/ra-32)). On y a longtemps reconnu des Faunes, petits génies champêtres qui évoquent la nature sauvage, au même titre que les satyres, les nymphes ou les sylvains.

P. Capus