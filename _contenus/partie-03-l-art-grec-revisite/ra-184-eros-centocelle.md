---
title: Statuette du type de l’Éros de Centocelle
id_inventaire: Ra 184
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: II<sup>e</sup> siècle (?)
materiau: Marbre
hauteur: "30"
largeur: '15,5'
longueur: ''
profondeur: "12"
epaisseur: 
id_wikidata: Q41583653
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe: 
order: 90
priority: 3
id_notice: ra-184
image_principale: ra-184-1
vues: 3
image_imprime: 
redirect_from:
- ra-184
- ark:/87276/a_ra_184
id_ark: a_ra_184
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 114
- clef: joulin_les_1901
  note: fig. 188 E
- clef: pasquier_praxite._2007
  note: p. 358, n° 96
- clef: rachou_catalogue_1912
  note: n° 184

---
Il s'agit de l'une des trente répliques antiques connues de ce type, <a href="/images/comp-ra-184-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-184-1.jpg" alt="Eros Farnèse du type de Centocelle, musée archéologique national de Naples, Marie-Lan Nguyen / Wikimedia Commons CC BY"/></span> dit «de Centocelle»</a>, en raison de l'exemplaire découvert au XVIII<sup>e</sup> siècle, dans ce lieu-dit, situé aux portes de Rome. L'œuvre entra dans les collections pontificales du Vatican. Un carquois aux pieds, un arc dans la main gauche et une flèche dans la droite, l'Éros ailé regardait vers la droite avec langueur. Bien qu'il ait été longtemps considéré comme une réplique d’un chef-d’œuvre de Praxitèle ou de son école, ce type iconographique, par les surprenants mélanges dont il fait preuve, pourrait être perçu comme une œuvre « classicisante ». Certes, Éros reprend les grandes lignes de la sculpture grecque classique, comme la position déhanchée ou la douceur du modelé, mais  certains éléments plus tardifs, telle l'abondante chevelure bouclée, viennent perturber la datation et le style. Il est alors probable que cet Éros soit une création composite romaine faisant écho à des procédés artistiques, classique et hellénistique, très connus {% cite pasquier_praxite._2007 -L none -l p. 356 et 358, n° 96 %}.

Cette création dut jouir d'une grande popularité chez les élites romaines car elle fut reproduite et diffusée dans tout le bassin méditerranéen, comme en témoigne la version, de petite taille, retrouvée dans la _villa_ de Chiragan. Ces pastiches caractérisent ainsi un type statuaire créé à l'époque romaine dont la seule finalité est ornementale.

P. Capus