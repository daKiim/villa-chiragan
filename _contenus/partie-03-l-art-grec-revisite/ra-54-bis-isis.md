---
title: Tête de divinité (Isis ?)
id_inventaire: Ra 54 bis
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: II<sup>e</sup>-III<sup>e</sup> siècle
materiau: Marbre
hauteur: "45"
largeur: "28"
longueur: ''
profondeur: "25"
epaisseur: 
id_wikidata: Q27145780
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe: 
order: 30
priority: 3
id_notice: ra-54-bis
image_principale: ra-54-bis-1
vues: 8
image_imprime: 
redirect_from:
- ra-54-bis
- ark:/87276/a_ra_54_bis
id_ark: a_ra_54_bis
author: Daniel Cazes
author_id: Cazes D.
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 100-101
- clef: centro_de_exposiciones_arte_canal_roma_2007
  note: 'p. 154, n° 85 '
- clef: esperandieu_recueil_1908
  note: n° 926
- clef: joulin_les_1901
  note: n° 180
- clef: massendari_haute-garonne_2006-1
  note: p. 256, fig. 142
- clef: rachou_catalogue_1912
  note: n° 54 bis

---
Cette belle tête féminine provient-elle véritablement de Chiragan ? On a reconnu ici Ariane, compagne de Thésée puis de Dionysos, ou encore Isis. Les représentations de la déesse égyptienne montrent une coiffure très proche, avec ses "anglaises" caractéristiques, tombant jusqu'aux épaules. Ces mèches torsadées sont ici maintenues par un double ruban, au-dessus du front et sur les côtés du crâne. La partie sommitale des trois mèches, réunies au-dessus du front, brisée, retaillée ou laissée inachevée servait-elle de support à un croissant de lune ? À des cornes ? À un disque ? Si tel était le cas, nous serions bien en présence d’une tête d’Isis.

D. Cazes