---
title: Tête de Minerve
id_inventaire: Ra 154
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: II<sup>e</sup> siècle
materiau: Marbre de Göktepe (Turquie)
hauteur: "27"
longueur: 
largeur: '10,7'
profondeur: '13,8'
epaisseur: 
id_wikidata: Q28872036
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe: 
order: 150
priority: 3
id_notice: ra-154
image_principale: ra-154-1
vues: 5
image_imprime: 
redirect_from:
- ra-154
- ark:/87276/a_ra_154
id_ark: a_ra_154
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 114
- clef: darblade_audoin_nouvel_2006-1
  note: p. 17, n° 024, pl. 20-024
- clef: esperandieu_recueil_1908
  note: p. 42, n° 906
- clef: joulin_les_1901
  note: fig. 130 E
- clef: pasquier_praxite._2007
  note: ''
- clef: rachou_catalogue_1912
  note: n° 154

---
Ainsi que pour de nombreuses autres sculptures mythologiques de petit et moyen format, cette évocation de Minerve devait décorer un espace privilégié du secteur le plus privé de la résidence. La déesse archaïque latine Minerve, qui fut progressivement parée des attributs de la grecque Athéna, fille de Zeus, était la divinité de la lumière, diurne et nocturne, de l'intelligence, de la guerre, de l’artisanat et des techniques en général.

L’œuvre du musée pourrait dépendre d'un courant rétrospectif, tendance récurrente dans l'histoire de l'esthétique contemporaine mais à laquelle l'Antiquité ne dérogeait pas. Concernant l'époque romaine, ce recours à un style du passé est surtout connu pour la période augustéenne, à la fin du I<sup>er</sup> siècle avant n. è. et au début du suivant, ainsi que pour l'époque antonine, en particulier sous le règne d'Hadrien, durant la première moitié du II<sup>e</sup> siècle {% cite darblade_audoin_nouvel_2006 -L none -l p. 17, n° 024, pl. 20-024 %}. La faveur accordée à certains prototypes grecs ancestraux ne semble en vérité ne s'être jamais tarie. Au-delà de l'intérêt que ces modèles suscitent toujours au IV<sup>e</sup> siècle, et de toute évidence encore au V<sup>e</sup> siècle, on constate un respect indéniable pour les œuvres rassemblées durant le Haut-Empire. Ces collections anciennes furent enrichies durant l'Antiquité tardive par de nouvelles figurations mythologiques, en accord avec l'émulation pour les sculptures de petit format, alors abondamment produites et destinées à des espaces privilégiés, notamment les vestibules et les thermes {% cite stirling_shifting_2016 -l 274-275 %}. La tête de Minerve, dont les caractères généraux puisent leurs fondements dans quelques prototypes célèbres (on pensera aux types «Velletri» ou encore «Giustiniani»), participe de cette mode, à l'image de dizaines d'autres sculptures découvertes à Chiragan, dont un certain nombre d'exemplaires dans le secteur balnéaire.

L'œuvre demeure cependant unique, non pas tant en raison de la chouette qui dominait le casque de la déesse, comme en témoignent les serres des deux pattes encore visibles, que par l'évocation très subtile de ce même oiseau de nuit à l'arrière du couvre-chef. Minerve d'un côté, chouette de l'autre, la tête se prête donc au jeu de l'ambivalence. Le sens de l'équivoque que l'on distingue ici et qui propose de découvrir une œuvre qui en cache une autre, devient également redondance iconographique par le dédoublement de l'attribut de la déesse, entièrement confondue avec son symbole et réciproquement. Ce casque zoomorphe représente donc une réelle originalité car il est, en effet, bien difficile de trouver, dans la production statuaire antique, un quelconque parallèle. La chouette avait été assimilée à la déesse tutélaire d’Athènes et devint son emblème, au moins à partir du début du VI<sup>e</sup> siècle avant n. è. L'oiseau, qui préfigurait la mort, était également associé à la vigilance, dont témoignaient ses yeux perçants. Ces derniers, à l’image de ceux de la déesse Athéna, ont toujours fasciné les hommes. Le regard du rapace, capable d’éloigner les forces négatives, explique également sa fonction de porte-bonheur dans le monde hellénique.

Le modèle même du casque paraît différent du type corinthien auquel nous ont habitués les sculpteurs, peintres et modeleurs de l'Antiquité. Ainsi la forme rappelle-t-elle davantage un type chalcidien, avec sa calotte ronde. Quant aux paragnathides (protège-joues), celles-ci devaient bien exister et c'est davantage en bronze qu'il faut les imaginer, peut-être solidaires d'un cerclage, lui-même métallique, qui pouvait ceindre la partie inférieure concave du casque. Les deux grosses mortaises témoignent dans tous les cas d'un système de fixation.

P. Capus