---
title: Athéna
id_inventaire: Ra 112
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: II<sup>e</sup> siècle
materiau: Marbre
hauteur: '14,5'
largeur: "52"
longueur: ''
profondeur: "32"
epaisseur: 
id_wikidata: Q25114990
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe: 
order: 20
priority: 3
id_notice: ra-112
image_principale: ra-112-1
image_imprime: 
vues: 3
redirect_from:
- ra-112
- ark:/87276/a_ra_112
id_ark: a_ra_112
precisions_biblio:
- clef: bulletin_1936
  note: p. 830
- clef: cazes_musee_1999-1
  note: p. 107-108
- clef: daltrop_athena_1983
  note: ''
- clef: daltrop_il_1980
  note: ''
- clef: esperandieu_recueil_1908
  note: n° 911
- clef: jamot_lathena_1930
  note: p. 59-70, 1 pl.
- clef: joulin_les_1901
  note: n° 148 D
- clef: lechat_notes_1910
  note: p. 137 ss
- clef: lechat_notes_1913
  note: p. 130 ss
- clef: michon_seance_1908
  note: ''
- clef: musee_paul_valery_paul_1995
  note: p. 120
- clef: musee_saint-raymond_essentiel_2011
  note: p. 38-39
- clef: pollak_athena_1909
  note: ''
- clef: rachou_catalogue_1912
  note: n° 112
- clef: reinach_courrier_1910
  note: p. 78
- clef: reinach_repertoire_1897
  note: t. II, vol. II, p. 674, fig. 2
- clef: sauer_athena-marsyasgruppe_1907
  note: ''
- clef: sauer_marsyasgruppe_1908
  note: pl. III-IV
- clef: slavazzi_italia_1996
  note: p. 30-31, fig. 25, p. 182-183

---
La grande sculpture d’Athéna découverte dans la _villa_ de Chiragan est la reprise de l’une des œuvres connues, au moins par les sources écrites, de Myron d’Éleuthères, artiste actif dans les années 450/440 avant n. è. La déesse, originellement en bronze, était accompagnée d’une autre sculpture, conçue dans le même alliage, représentant un silène, du nom de Marsyas, compagnon du cortège de Dionysos, {% cite boardman_greek_1985 junker_athena-marsyas-gruppe_2002 -L none -l fig. 61-64 -L none -l p. 130-131, Pl. 1-6 et p. 138-139, Pl. 11-14 %}. Mi-homme, mi-animal, cet être hirsute, venu de Phrygie, région située au cœur de l’Asie Mineure, a été largement représenté dans l’art grec. Le groupe qu’il formait avec Athéna avait été conçu pour l'Acropole, site le plus hautement religieux d’Athènes. La mise en scène de ces deux êtres mythologiques trouvait peut-être son origine dans la représentation d’une œuvre théâtrale lyrique dédiée à Dionysos, un dithyrambe, écrit par un poète du V<sup>e</sup> siècle avant n. è., Mélanippidès. Pausanias, écrivain grec du II<sup>e</sup> siècle, décrit, six siècles après sa création, ce duo en bronze dont la notoriété fut telle que de nombreuses variantes furent conçues pour les plus beaux domaines de l’Empire à son époque {% cite pausanias_periegese_nodate -L none -l I, 24, 1 %}.

Cette pétrification dans le marbre, par des ateliers spécialisés dans la copie, reprenait donc avec plus ou moins d'exactitude le moment de l'histoire auquel se référait initialement le bronze de Myron. L'épisode retenu par le sculpteur grec était celui du rejet de l'_aulos,_ la double-flûte, par la sage et lumineuse Athéna qui venait de constater à quel point elle pouvait être défigurée lorsqu'elle soufflait dans cet instrument {% cite plutarque_cohibenda_nodate -L none -l 6, 456 b %}. Marsyas surgissait alors, s’apprêtant à ramasser l'instrument, mais avec circonspection, en raison de l’injonction proférée par la déesse, qui interdisait à quiconque de l'utiliser. Les satyres, c’est bien connu, sont amateurs de musique et Marsyas est lui-même intimement associé aux instruments à vent, _aulos_ ou _syrinx_ (flûte de Pan). Les exercices musicaux nombreux et les efforts que cette créature déploya pour apprivoiser l'objet délaissé par la déesse, conduiront ce compagnon de Dionysos à provoquer Apollon, dieu musicien par excellence, intimement associé à une autre famille d’instruments, celle des cordes. Marsyas en paiera le prix ; à l'issu du concours (_agôn_) qu'il avait initié, il fut vaincu par la divinité olympienne, protecteur de la poésie, qui le fit pendre à un arbre avant de l'écorcher vif. Ainsi la lyre s’imposa-t-elle sur l’_aulos_ {% cite plutarque_symposiaques_nodate -L none -l VII8. 713b %}.

Le silène du groupe en bronze de Myron est surtout connu par la belle et puissante réplique conservée au <a href="/images/comp-ra-112-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-112-1.jpg" alt="Athéna et Marsyas, musées du Vatican, Sailko / Wikimedia Commons CC BY"/></span>Museo gregoriano profano du Vatican</a>. Les variantes du <a href="/images/comp-ra-112-2.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-112-2.jpg" alt="Athéna de Myron, inv. 195 musée Liebieghaus, Francfort, Carole Raddato / Wikimedia Commons CC BY-SA"/></span>Liebieghaus de Francfort</a> mais aussi celles du Louvre, du Prado, ou de la collection Lancelotti à Rome montrent une sculpture de type classique mais dont les gestes dynamiques et les attitudes gracieuses rappelant une chorégraphie semblent distinguer Myron de certains de ses contemporains, en premier lieu Phidias. Aucun des nombreux fragments conservés au musée Saint-Raymond ne prouve, cependant, la présence de Marsyas à côté d'Athéna dans la _villa_ de Chiragan.

Si l’on compare la tunique (le _péplos_) de la déesse à d'autres répliques romaines, on constate une plus grande variété dans le rendu des plis sur l'exemplaire toulousain. Quant à la structure générale de l'œuvre, celle-ci demeure conforme aux autres ouvrages connus et mentionnés précédemment. Athéna maintenait contre sa jambe droite, sur laquelle elle prend appui, la lance, dont il reste un fragment. Son recul, au moment où surgit l’hirsute Marsyas, est souligné par une légère flexion et un pivotement de la jambe gauche. La déesse ordonnait au silène, de son bras gauche tendu, de s'éloigner, lui interdisant de toucher aux flûtes restées à terre. Certes, la statue est aujourd’hui acéphale, mais la tension perceptible au niveau du cou laisse cependant deviner le pivotement de la tête. On distingue enfin, à l'arrière de la nuque, les traces d’un renfort que l'on aurait tort de négliger. Ces étais de marbre pourraient en effet caractériser certains ateliers actifs dans les villes côtières de Pamphylie (Asie Mineure) {% cite slavazzi_italia_1996 -l 140 %}. La statue de Chiragan proviendrait-elle donc de cette région, féconde dans le domaine de la sculpture ?

P. Capus