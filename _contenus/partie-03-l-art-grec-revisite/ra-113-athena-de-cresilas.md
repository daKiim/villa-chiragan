---
title: 'Statuette d''Athéna '
id_inventaire: Ra 113
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: "«Velletri»"
date_creation: Milieu du II<sup>e</sup> siècle (?), d'après un original créé vers
  430 avant n. è.
materiau: Marbre de Göktepe (Turquie)
hauteur: "61"
largeur: "23"
longueur: ''
profondeur: "16"
epaisseur: 
id_wikidata: Q27096521
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe: 
order: 70
priority: 3
id_notice: ra-113
image_principale: ra-113-1
vues: 8
image_imprime: 
redirect_from:
- ra-113
- ark:/87276/a_ra_113
id_ark: a_ra_113
precisions_biblio:
- clef: bulletin_1936
  note: p. 830
- clef: cazes_musee_1999-1
  note: p. 109-110
- clef: esperandieu_recueil_1908
  note: n° 907
- clef: joulin_les_1901
  note: n° 136 D
- clef: rachou_catalogue_1912
  note: n° 113
- clef: reinach_repertoire_1897
  note: n° 292
- clef: slavazzi_italia_1996
  note: p. 40-41, 185-186

---
On reconnaît l’égide sur la poitrine de la déesse, peau de chèvre ourlée de serpents et timbrée de la tête de la gorgone Méduse. Cette cuirasse, tout autant défensive qu’offensive, appartient, avec la lance et le bouclier, aux attributs guerriers de la fille de Zeus.

C’est une statue en bronze, attribuée à Crésilas, sculpteur d’origine crétoise, qui la conçut vers 430 avant n. è., qui aurait servi de modèle au sculpteur romain.  Le bras droit était levé, une lance en appui sur le sol, probablement tenue dans la main. Le bras gauche, légèrement plié, devait être dirigé vers le bas, la main reposant probablement sur le bouclier.

Le musée du Louvre en conserve une réplique colossale en marbre, de plus de trois mètres de hauteur, découverte à Velletri, près de Rome. Dimensions et attitude doivent être conformes à la statue d’origine, les bras relevant quant à eux d’une restauration moderne, due à Vincenzo Pacetti (1746-1820) {% cite nocca_dalla_1997 %}. Cette <a href="/images/comp-ra-113-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-113-1.jpg" alt="Pallas Velletri, musée du Louvre, Kimberly Vardeman / Wikimedia Commons CC BY"/></span>« _Pallas_ Velletri »</a> fut mise en relation avec des moulages fragmentaires en plâtre, découverts lors de la fouille d'un atelier de copistes de l’époque romaine, à Baïes (_Baiae_), près de Pouzzoles (Campanie). Les empreintes avaient été prises directement sur des originaux grecs voire sur des copies d'originaux, témoignant ainsi de la pratique du surmoulage. Ces négatifs  sont d'une importance considérable pour l'histoire de la réplique des œuvres. Devenus propriété d’ateliers plus ou moins achalandés, ils autorisaient par conséquent la conception de copies et de déclinaisons destinées au marché de l’art italien, dont le renom explique l'exploitation commerciale.

Si la statuette découverte dans les vestiges de la _villa_ de Chiragan relève par conséquent du type même de la grande statue du Louvre, on ne manquera pas de constater quelques différences. L’égide et le drapé présentent des plis particulièrement profonds, à l’origine d’un jeu d’ombres et de lumières plus raffiné que dans l’œuvre monumentale. D’autres détails permettent encore de dissocier les deux répliques, à l’image de la ceinture du _chiton_ ou de la longueur du manteau. Ainsi, les deux sculptures ne peuvent-elles être perçues comme une stricte citation du type grec.

P. Capus