---
title: Tête de Vénus
id_inventaire: Ra 52
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: I<sup>er</sup>-II<sup>e</sup> siècle
materiau: Marbre lychnites (île de Paros)
hauteur: "40"
largeur: "24"
longueur: ''
profondeur: "31"
epaisseur: 
id_wikidata: Q24628970
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe: 
order: 10
priority: 3
id_notice: ra-52
image_principale: ra-52-1
vues: 8
image_imprime: 
redirect_from:
- ra-52
- ark:/87276/a_ra_52
id_ark: a_ra_52
precisions_biblio:
- clef: bassal_venus_1996
  note: ''
- clef: bulletin_1936
  note: pl. p. 605
- clef: cazes_musee_1999-1
  note: p. 104-107
- clef: clarac_musee_1841
  note: p. 588
- clef: du_mege_description_1835
  note: n° 140
- clef: du_mege_notice_1828
  note: n° 60
- clef: esperandieu_recueil_1908
  note: n° 902
- clef: gachon_histoire_1926
  note: pl. V, p. 72-73
- clef: godechot_visages_1949
  note: p. 70
- clef: joulin_les_1901
  note: fig. 121 B
- clef: lebegue_ecole_1889
  note: p. 9
- clef: massendari_haute-garonne_2006-1
  note: 'p. 255, fig. 141 '
- clef: musee_saint-raymond_essentiel_2011
  note: p. 32-33
- clef: pages_venus_1867
  note: ''
- clef: pasquier_praxite._2007
  note: p. 182-183, n° 38
- clef: pasquier_venus_1985-1
  note: ''
- clef: praviel_toulouse_1935
  note: p. 247
- clef: pugliese_carratelli_enciclopedia_1997
  note: ''
- clef: rachou_catalogue_1912
  note: n° 52
- clef: ramet_histoire_1935
  note: p. 26-27
- clef: roschach_catalogue_1865
  note: n° 52
- clef: roschach_histoire_1904
  note: p. 211
- clef: slavazzi_italia_1996
  note: p. 41-45 et fig. 28

---
Les fouilles de 1826 exhumèrent cette remarquable tête d'Aphrodite-Vénus. La limite inférieure s'arrête en haut du sternum, au niveau du manubrium, et comprend le départ de l'épaule gauche. Malgré le nettoyage, souvent trop radical, effectué au XIX<sup>e</sup> siècle, quelques concrétions calcaires subsistent sur le côté gauche, surtout dans la chevelure, conformément à ce qu'avait décrit le comte de Clarac, conservateur des Antiquités du Louvre, qui avait vu la tête peu après sa mise au jour {% cite clarac_musee_1841 -l 588 %}. La partie postérieure forme un renflement piqueté, caractéristique d'un bouchon d'encastrement qui permet de supposer l'insertion de la tête dans le corps d'une statue. Aucun fragment de celle-ci n'a pu, néanmoins, être identifié dans les réserves du musée, malgré la mention, par Alexandre Du Mège, de "portions de bras qui paraissent avoir fait partie de cette statue" {% cite du_mege_description_1835 -L none -l p. 79, n° 140 %}. On peut également penser à un encastrement sur un buste voire une association de la tête à un hermès (pilier).

La renommée de l'œuvre fut telle qu'elle bénéficia d'une dénomination, "Vénus de Martres", qui la distinguait donc et témoignait du prestige qui lui avait été accordé. Le comte de Clarac, de son œil expert et avisé, en fit une Vénus « des plus belles qui existent, si même elle ne leur est pas supérieure », la comparant aux <a href="/images/comp-ra-52-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-52-1.jpg" alt="Vénus Médicis, Galerie des Offices Florence, Sailko / Wikimedia Commons CC BY"/></span>Vénus Médicis</a>, <a href="/images/comp-ra-52-2.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-52-2.jpg" alt="Vénus d'Arles, musée du Louvre, Alain Darles / Wikimedia Commons CC BY"/></span>d'Arles</a> et <a href="/images/comp-ra-52-3.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-52-3.jpg" alt="Vénus de Milo, musée du Louvre, Kimberly Vardeman / Wikimedia Commons CC BY"/></span>de Milo</a>, références suprêmes en cette première moitié du XIX<sup>e</sup> siècle néo-classique. Exposée à Paris en 1867, elle fut attribuée au grec Praxitèle {% cite pages_venus_1867 %} avant de devenir, plus raisonnablement, l'une des répliques de l'Aphrodite de Cnide, ville côtière de Carie (sud-ouest de l'actuelle Turquie). Si l'œuvre originale, créée vers 360 avant n. è., a disparu, elle fut amplement copiée. Sa réputation doit beaucoup au témoignage de Pline l'Ancien, qui écrit, au sujet du sculpteur : « Sa Vénus est à la tête, je ne dis pas seulement de toute sa production, mais de celle de tous les artistes du monde » {% cite pline_lancien_histoire_nodate -L none -l XXXVI, 20 %}. Le même Pline rapporte que s'il avait été donné à Praxitèle la possibilité de privilégier l'une de ses œuvres, l’artiste aurait sans hésitation porté son choix sur celles qui avaient été peintes par Nicias. Ce dernier était effectivement passé maître dans l’application de couleurs sur le marbre poli des sculptures, en premier lieu celles de son confrère Praxitèle.

Les études comparatives qui ont porté sur les dizaines de répliques répertoriées de la statue, considérée comme l'un des sommets de la sculpture du deuxième classicisme grec, ont à la fois semé le doute sur l'aspect réel du chef-d’œuvre de Praxitèle et affiné l'appréciation de ses différentes copies ou variantes. Ainsi, informations descriptives, rapportées par quelques auteurs de l'Antiquité, et reproduction de la sculpture sur les monnaies de Cnide doivent-elles être comparées et évaluées en rapport avec toutes les répliques, d'époque hellénistique ou romaine, susceptibles de nous en donner une copie plus ou moins fidèle. 

On sait que le corps nu de la déesse avait surpris, sinon choqué, dans un premier temps, à tel point que les gens de Cos, voisins et rivaux de ceux de Cnide, refusèrent la statue, dont ils avaient passé commande pour leur temple. Ils la cédèrent aux Cnidiens, peut-être plus audacieux, mais qui, surtout, semblent l'avoir substituée à une effigie cultuelle déjà nue, de tradition orientale, où dominait le concept de fécondité. Du reste, ce corps pétrifié était d'autant moins caché qu'il fut installé dans une chapelle largement ouverte, petit temple circulaire élevé dans une enceinte sacrée plantée de myrtes, de cyprès, de platanes et de vigne. De son bras droit, Aphrodite ramenait sa main en direction de son pubis, un geste qui fut interprété de pudeur, tel un être surpris dans son intimité, mais qui permettait, à la déesse de la fécondité, de désigner l'appareil génital reproducteur. La main gauche maintenait quant à elle une étoffe, au-dessus d'une hydrie en bronze. La présence de ce récipient indique sans ambiguïté la toilette d'Aphrodite, dont le buste, légèrement penché, se reflétait peut-être dans une vasque. La composition, qui dépassait une simple scène de genre montrant Aphrodite surprise au bain, renvoyait probablement au caractère rituel des ablutions.

Une part du mystère de ce visage, de ce corps et de cette attitude imprègne encore les plus belles répliques parvenues jusqu'à nous. La tête de Martres, projetée vers l'avant, vient nous rappeler la douce inclinaison de la Vénus cnidienne. Son haut chignon dégage la gracieuse ligne arrière d'un cou que poursuivait la rondeur du dos. De longues mèches ondulées sont divisées par une raie médiane et maintenues par une double bandelette lisse qui entoure le crâne. Cette bandelette fait-elle allusion au _kestos himas_ (« ceinture enroulée »), brodé, d’Aphrodite ? Ce _cestus veneris_ rendait la déesse irrésistible et objet de l’amour de quiconque s’en approchait. Les traits du visage sont réguliers hormis la légère dissymétrie des yeux. Mais au contraire de la froideur de certaines répliques, à l'exemple de la <a href="/images/comp-ra-52-4.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-52-4.jpg" alt="Tête Borghèse, musée du Louvre, Marie-Lan Nguyen / Wikimedia Commons CC BY"/></span>tête Borghèse du Louvre</a> ou de la <a href="/images/comp-ra-52-5.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-52-5.jpg" alt="Vénus Colonna, musées du Vatican, Wikimedia Commons Public Domain"/></span>Vénus Colonna du Vatican</a>, pourtant jugées par la plupart des historiens de l'art plus proches du travail de Praxitèle, les traits de la Vénus de Martres ou ceux de la <a href="/images/comp-ra-52-6.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-52-6.jpg" alt="Tête Kaufmann, musée du Louvre, Marie-Lan Nguyen / Wikimedia Commons CC BY"/></span>tête Kaufmann du Louvre</a>, paraissent moins froids et plus subtilement estompés. Un épiderme animé et des lèvres charnues la rendent plus sensuelle. Elle paraît plus humaine, mais ne l'est point tout à fait : le sacré est toujours présent, même si l'expression distante glisse vers la rêverie et la tendresse. 

Si la tête Kaufmann, découverte à Tralles, en Turquie, datée du II<sup>e</sup> siècle avant n. è., pourrait être considérée comme une réinterprétation de l'époque hellénistique {% cite pasquier_venus_1985-1 -l 58-59 %}, la Vénus de Martres ne peut être datée avec autant de précision. Son marbre, analysé en 2011, est bien grec ; il provient de Paros. On ne saurait en faire un argument pour dater l’œuvre, les marbres de l'île égéenne ayant été employés, encore, tout au long du Haut-Empire romain. Tout au plus pourrait-on avancer une datation avant la seconde moitié du II<sup>e</sup> siècle de n. è.. Le milieu du I<sup>er</sup> siècle de n. è. et l'attribution à un atelier oriental ont, en l’occurrence, été proposés {% cite slavazzi_italia_1996 -l 186-187 %}. On sait combien, en effet, les grands classiques de l'art grec connurent, durant les deux premiers siècles de n. è., un nouvel engouement dans le milieu impérial comme dans quelques luxueux domaines, italiens ou provinciaux. Chiragan ne dérogea apparemment pas à cette mode ; en témoignent notamment les tableautins de marbre, connexes au courant artistique dit néo-attique, les deux figures d'Athéna mais également toute une série de petits formats qui sont autant de répliques, ou bien de variantes, d'originaux grecs.

P. Capus