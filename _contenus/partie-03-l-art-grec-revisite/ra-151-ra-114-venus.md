---
title: Vénus
id_inventaire: Ra 151-Ra 114
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: IV<sup>e</sup> siècle ?
materiau: Marbre
hauteur: '77,5'
largeur: '28,5'
longueur: 
profondeur: "20"
epaisseur: 
id_wikidata: Q28523491
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe: 
order: 160
priority: 3
id_notice: ra-151-ra-114
image_principale: ra-151-ra-114-1
vues: 8
image_imprime: 
redirect_from:
- ra-151-ra-114
- ark:/87276/a_ra_151_ra_114
id_ark: a_ra_151_ra_114
precisions_biblio:
- clef: bergmann_chiragan_1999
  note: ''
- clef: besnard_age_2019-1
  note: repr. p. 225
- clef: capus_minceur_2020
  note: ''
- clef: cazes_martres-tolosane_2005
  note: ''
- clef: esperandieu_recueil_1908
  note: n° 903, 905
- clef: joulin_les_1901
  note: p. 95-96, pl. X, n° 123 D, n° 128 E
- clef: mesple_raccords_1948
  note: p. 156
- clef: musee_saint-raymond_dieux_2010-1
  note: p. 49
- clef: musee_saint-raymond_wisigoths_2020
  note: p. 169-179 (notice P. Capus)
- clef: perrot_rapport_1891
  note: p. 64
- clef: rachou_catalogue_1912
  note: n° 114, n° 151
- clef: stirling_gods_1996
  note: ''
- clef: stirling_learned_2005
  note: ''

---
Si nombre des œuvres venues des fouilles de la _villa_ ont immédiatement fait l’objet d'une mise en valeur particulière et d’une exposition immédiate, certaines d’entre elles  furent reléguées dans les réserves. Ce bannissement des espaces d'exposition s'explique. Il faut, en premier lieu, mentionner les incertitudes et questionnements relatifs à la provenance d'un certain nombre de ces sculptures ; l’imprécision des inventaires, comme des sources du XIX<sup>e</sup> siècle, ont souvent conduit les conservateurs à demeurer prudents et réservés quant à l’attribution d’un certain nombre de marbres à la _villa_. On ne peut ensuite ignorer  les partis pris esthétiques, qui ont conduit à exclure certaines œuvres ; celles-ci ont, de toute évidence, pâti des _a priori_ relevant de l’histoire des formes. Attribuées à quelque atelier moins compétent, trop souvent comparées à une statuaire d’essence plus classique, ces oeuvres furent condamnées à l'exil. Une condamnation entérinée, pour chaque conservateur confronté au manque d’espace des surfaces d’exposition, par l’obligation d'opérer une sélection drastique. De manière exemplaire, cette statue de Vénus fut victime de ces raisons. Elle incarne, mieux que tout autre, un type d'œuvre plus aisément mis en retrait .

Couronnée d’un diadème, la figure se distingue par une minceur et des proportions inhabituelles qui attirent immédiatement l'attention. L'élongation de ses formes, associée à la platitude de son fessier comme de son torse, rendent assurément l'œuvre singulière. Lorsque les deux fragments qui la composent furent découverts, en 1890, la _villa_ de Chiragan connaissait sa troisième grande campagne officielle de fouilles, dirigée par Albert Lebègue, professeur à la Faculté des lettres de Toulouse {% cite perrot_rapport_1891 -l 415 %}. Le bris se situait au-dessus de la région ombilicale. Les  deux parties furent réunies par un collage. La main droite, tenant une mèche de cheveu, aurait été simultanément mise au jour {% cite esperandieu_recueil_1908 -L none -l n° 903 %}. La tête, probablement découverte durant la première moitié du siècle et déjà conservée dans les collections du musée de Toulouse, ne fut mise en rapport avec le corps qu'en 1948, par Paul Mesplé {% cite mesple_raccords_1948 -l 156 %}. Émile Espérandieu attribua erronément sa découverte aux fouilles de 1897-1899, menées par Léon Joulin {% cite esperandieu_recueil_1908 -L none -l n° 905 %}. Peut-être était-il fait mention de cette tête dans le premier feuillet, malheureusement disparu, du cahier manuscrit correspondant aux découvertes des 19 et 20 septembre 1826 {% cite balty_les_2005 -l 35 %}.

#### Description

Vénus est entièrement nue. Le corps prend appui sur la jambe gauche. Elle tourne la tête vers la gauche et se courbe légèrement vers l’avant. Le buste est particulièrement étroit, les seins petits, tandis que le bassin, large mais très peu profond, contraste avec la partie supérieure du corps. Le cou, enfin, long et fin lorsqu’on l’observe de face, gagne en épaisseur vu depuis la gauche, prenant davantage l’aspect d’un cylindre dont la jonction avec le visage (fin et à l’ovale bien marqué) devient plus artificielle. La tête est coiffée d’un diadème lisse. Deux mèches s’échappaient librement d’un chignon pour revenir sur l’épaule gauche et sur le bras droit. Elles furent probablement dégradées et restaurées, en stuc, comme le prouveraient les marques exécutées à la pointe, en haut du dos, qui pourraient correspondre à autant de points d'accroche du matériau de substitution. Cinq points de contact, quatre en haut de la cuisse droite et un autre dans la zone inguinale, témoignent de la position de la main gauche, ramenée en avant du pubis. Sur la cuisse gauche, au niveau du muscle vaste latéral, un tenon signale l’élément qui, à l’origine, accompagnait Vénus et permettait une bonne stabilité à la statue. Trois points de contact, sur la hanche, correspondent, sans doute, à l’extrémité de la nageoire caudale d’un dauphin, pourvue, selon la tradition iconographique antique afférente à cet animal, de plusieurs appendices. La tête du cétacé reposait probablement directement sur une base moulurée, commune à la déesse et à son attribut marin.

#### Un modèle célèbre...

On a pu voir dans l’œuvre un dérivé, très tardif, de l’Aphrodite cnidienne, illustre et récurrent modèle, né dans l’atelier de Praxitèle vers 360 avant n. è. {% cite pasquier_praxite._2007 -l 172-195 %}. Ode à la beauté de la bien-aimée Phryné, dont l’éclat des formes ne pouvait que convenir à la déesse de Chypre, l’œuvre originale, conçue dans le marbre, est aujourd’hui perdue. La version romaine des Musées du Vatican, dite “Vénus du Belvédère” (inv. 4260), en respecterait le schéma. Mais ce corps dont l’intégrale et éclatante nudité ne réclamait pas d’atours superflus, renvoyait à une beauté corporelle jusque-là réservée au seul corps masculin. Il généra une production infinie de copies et dérivés de toute sorte. En témoignent les déclinaisons attribuées à des ateliers orientaux. Dynamisés par un marché toujours croissant de collectionneurs, les sculpteurs de Rhodes ou d’Alexandrie conçoivent en effet, entre la fin du II<sup>e</sup> siècle avant n. è. et le cours du siècle suivant, une statuaire de taille moyenne, souvent prolixe en références et clins d’œil aux chefs-d’œuvre de l’époque classique. C'est notamment le cas d'une Aphrodite anadyomène rhodienne, dont le corps, lorsqu'il est vu depuis les côtés, se distingue par la faible épaisseur {% cite queyrel_sculpture_2016 palaggia_atticism_1998 -L none -l p. 95-96, fig. 77 -l 94-99 %}.

On sait combien l’industrie de la copie, et les débouchés commerciaux occidentaux, serviront de passage de témoin fondamental entre la vieille civilisation hellénique et les riches acheteurs, avides de références mythologiques dans le déploiement de leurs pièces d’habitation, voire de leurs locaux associatifs et commerciaux. La statuaire décorative qui leur est destinée n’est pas exempte de sensualité voire de détails humoristiques et de références anecdotiques savoureuses (Le groupe de _Pan et Aphrodite_, de l’association des commerçants beyrouthins de Délos, conservé au musée national d'Athènes, en est un bon exemple).

La Vénus de Chiragan est l’héritière, quelque cinq cents ans plus tard, de ces compositions. La main gauche ramenée en avant du pubis, geste de pudeur aussi bien que d’ostentation de la puissance fécondante de la fille d’Ouranos, entretient un lien direct avec le chef-d’œuvre de Praxitèle ; il rappelle bien la sculpture acquise par l'île de Cnide, à laquelle fait également référence la position de la tête et le regard tourné vers la gauche {% cite corso_art_2007 -l 9-22 %}. On constate cependant que la main ramenée au-devant des organes génitaux de notre Vénus est ici la gauche. Cette inversion s’explique par les variations esthétiques, bien minimes il est vrai si l’on tient compte du nombre de siècles écoulés, qui sont autant de filtres déposés entre l’œuvre originelle et les créations de l’époque impériale. Le plus célèbre de ces dérivés, caractéristique de cette inversion du geste, est représenté par le type _pudica_, dit aussi Vénus Médicis. Il faut également noter la position du bras droit, certes brisé lui aussi, mais encore agrémenté d’une mèche retombant de la longue chevelure. Vénus relevait vigoureusement ce membre. Il s’agit là, davantage, d’une reprise de la gestuelle de l’Aphrodite dite «_anadyomène»_, création du peintre Apelle, s’il faut en croire Athénée {% cite athenee_deipnosophistes_nodate -L none -l XIII, 59 %}. Elle aurait été, elle aussi, inspirée par l’envoûtante courtisane Phryné {% cite athenee_deipnosophistes_nodate -L none -l XIII, 59 %}.

Exporté vers Rome, suite aux conquêtes romaines de la fin de la République, et exposé sur ordre d’Auguste dans le temple de César, d’après Pline, le tableau (_pynax_) d’Apelle subit une forte dégradation, avant d’être reproduit sous le règne de Néron {% cite pline_lancien_histoire_nodate -L none -l 36.28 %} ; si l’on a pu imaginer que la Vénus de Praxitèle s’apprêtait à entrer au bain {% cite corso_art_2007 -l 14 %}, l’_anadyomène_ en sortirait, empoignant sa chevelure de ses deux mains, de part et d'autre de sa tête, afin de l’essorer. Le geste rappelait celui de l’athlète ceignant son bandeau, célèbre marbre de Polyclète, connu sous le nom de _Diadumène_ {% cite rolley_sculpture_1999 -l 35-36 %}. La ronde-bosse de l’_anadyomène_, transposition féminine de l’œuvre attribuée à Polyclète, fut probablement créée dans le contexte fécond des ateliers alexandrins du I<sup>er</sup> siècle avant n. è. Il développe et diffuse la _charis_ (la grâce) grecque de ce corps féminin dominée par la courbe, les lignes pleines, les hanches larges et le sein menu et ferme. La chevelure devint symbole, au même titre que le pubis, de la puissance féminine fécondante mais également de la séduction et du besoin impérieux de plaire {% cite bodiou_chevelure_2006 -l 187 %}. Tordre cet attribut renverrait également, sous une autre forme, à la compression des seins, un geste symbolique issu d’une longue tradition en Orient {% cite de_matteis_sullafrodite_2004 -l 129 %}.

#### ...mais réadapté

Si le système de proportion adopté pour les Vénus de l’époque classique dépendait donc d’un canon faisant la part belle aux formes rondes et charpentées, rien de tel dans notre sculpture. La relégation de l’œuvre dans les réserves et l’indifférence, bien injustifiée, à son égard, pourraient s'expliquer, nous l'avons dit, par des discordances esthétiques vis-à-vis du prototype : des proportions ingrates, l’affinement peu conventionnel de son thorax, la platitude de l’ensemble du corps, caractérisée par l’écrasement du fessier, qui prolonge sans variation un dos lui-même sans relief et, enfin, le cou, semblable à un long cylindre de face et trop large vu depuis le côté. Depuis la gauche, le raccord sans transition, très abrupt, avec le visage heurte la vue.

Si le corps de Vénus paraît particulièrement plat et fin vu de l’arrière, plus particulièrement de trois quarts, il se pourrait bien que cette élongation et ces déformations répondent à un goût particulier et, de toute évidence, à un style, celui qui fut mis en place au sein de certains ateliers orientaux durant les derniers siècles de l'Antiquité {% cite stirling_gods_1996 stirling_divinities_1996 -l 215 -l 103-143 %}. On se référera à un groupe statuaire du musée de Cyrène {% cite noauthor_lexicon_1990 -L none -l II, 1, 157, 69 ; II, 2, 161, fig. 69 %}, néanmoins probablement plus précoce, mais également à une <a href="/images/comp-ra-151-ra-114-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-151-ra-114-1.jpg" alt="Artemis du type Rospigliosi, Staatlichen Kunstsammlungen Dresde" data-link="https://skd-online-collection.skd.museum/Details/Index/166375" data-link-title="skd.museum"/></span>série de statues de moyen format, conservées à Dresde</a> (Elle procède du type « Rospigliosi-Latran » {% cite noauthor_lexicon_1990 -L none -l II, 1, 646, 251 - 283 %}), dont les torses très affinés, s’opposant à des membres inférieurs massifs, confinent à l’étrange et permettent d’abonder dans le sens d'une corporalité plus graphique que réaliste {% cite bergmann_chiragan_1999 -L none -l pl. 56-57 %}. Certains autres éléments trahissent cette esthétique, notamment la boucle en forme de S sur le front, les transitions un peu abruptes des plans de taille au niveau du visage, les arcades sourcilières formant un angle aigu, les sillons profonds créant une division synthétique des mèches ondulées. Ces caractéristiques permettent de relier entre elles une série d’œuvres, dont la datation tardive a été pertinemment justifiée par Marianne Bergmann : Hygie de Rhodes {% cite bergmann_chiragan_1999 -L none -l 51, pl. 49-1 %}, têtes féminines de Dijon {% cite bergmann_chiragan_1999 -L none -l p. 45, p. 47, pl. 55-2, 56-2 %} et Chiragan {% cite bergmann_chiragan_1999 -L none -l p. 54, pl. 55-3.4, 56-3 %}, d’Apollon à Saint-Georges-de-Montagne {% cite bergmann_chiragan_1999 -L none -l p. 54, pl. 55-1, 56-4 %} ou encore un grand portrait féminin des musées du Capitole {% cite bergmann_chiragan_1999 erim_aphrodisias_1986 floriani_squarciapino_scuola_1943 -l 61-63 -L p. 59, pl. 19-20 %}.

Vénus représente apparemment, de loin, la figure mythologique la plus fréquente au sein des _villae_ en activité durant cette période, après les représentations de satyres, qui se faisaient l'écho de l'autre dieu dominant : Dionysos {% cite videbech_private_2015 -l 473 %}. Caractérisée par sa gestuelle hybride, la sculpture de Chiragan correspond donc à une époque tardive durant laquelle fleurit cette petite et moyenne statuaire, distribuée  dans les entrées, les salles à manger et les thermes, sans oublier les jardins. Des espaces voués à un usage privé pour certains, semi-public, voire public, pour d’autres.

P. Capus