---
title: Esculape
id_inventaire: Ra 41
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: 'II<sup>e</sup> siècle '
materiau: Marbre d’Afyon (Turquie)
hauteur: "69"
largeur: "43"
longueur: ''
profondeur: '19,5'
epaisseur: 
id_wikidata: Q28464617
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe: 
order: 100
priority: 3
id_notice: ra-41
image_principale: ra-41-1
vues: 2
image_imprime: 
redirect_from:
- ra-41
- ark:/87276/a_ra_41
id_ark: a_ra_41
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 110-111
- clef: du_mege_description_1835
  note: n° 116
- clef: du_mege_notice_1828
  note: n° 53
- clef: esperandieu_recueil_1908
  note: n° 912
- clef: joulin_les_1901
  note: n° 144
- clef: landes_dieux_1992
  note: n° 10
- clef: rachou_catalogue_1912
  note: n° 41
- clef: roschach_catalogue_1865
  note: n° 41
- clef: thiollier-alexandrowicz_itineraires_1996
  note: p. 175

---
Esculape (l'Asclépios grec), dieu de la médecine, fut confié, très jeune, par son père, Apollon, au centaure Chiron qui lui apprit l’art de la guérison. La discipline est symbolisée, entre autres, par le serpent, dont on aperçoit encore la queue, sur la base, à gauche, qui s'enroulait autour du bâton noueux que tenait la divinité d'Épidaure.

Vue dans un premier temps comme la <a href="/images/comp-ra-41-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-41-1.jpg" alt="Esculape de Bulla Regia, musée du Bardo (Tunisie), Elcèd77 / Wikimedia Commons CC BY-SA"/></span>variante d'un original grec</a> du V<sup>e</sup> siècle avant n. è., cette œuvre est aujourd'hui classée dans la catégorie des pastiches. Si on suppose que le type aurait été créé à l’époque impériale, il n’en garde pas moins l’essence de l’art grec classique {% cite koppel_petite_2006 %}. Le prototype connut un grand succès, comme semble en  attester, en l'occurrence, <a href="/images/comp-ra-41-3.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-41-3.jpg" alt="Esculape, musée national archéologique de Tarragone, Ramon Cornadó, musée de Tarragone"/></span>une autre réplique</a>, de même format, découverte dans la salle froide (_frigidarium_) des thermes inférieurs de la _villa_ d’Els Munts, à Altafulla (Catalogne) {% cite koppel_decoracion_1995 -l 42 %}. La statue de moyen format, mise au jour à Chiragan dès le début des fouilles de Du Mège, en 1826, proviendrait-elle également de l'un des deux ensembles balnéaires de la _villa_ ? Les liens profonds qui unissent les eaux au dieu guérisseur ne contrediraient pas cette hypothèse.

P. Capus