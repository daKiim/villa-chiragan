---
title: Harpocrate
id_inventaire: Ra 40
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: III<sup>e</sup>-premier tiers du IV<sup>e</sup> siècle (?)
materiau: Marbre
hauteur: '111,5'
largeur: '43,8'
longueur: ''
profondeur: "28"
epaisseur: 
id_wikidata: Q26720973
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe: 
order: 60
priority: 3
id_notice: ra-40
image_principale: ra-40-1
vues: 4
image_imprime: 
redirect_from:
- ra-40
- ark:/87276/a_ra_40
id_ark: a_ra_40
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 102
- clef: clarac_musee_1841
  note: p. 585
- clef: du_mege_description_1835
  note: n° 126
- clef: du_mege_notice_1828
  note: n° 56
- clef: esperandieu_recueil_1908
  note: p. 49, n° 923
- clef: hostein_roman_2016
  note: vol. IX, n° 282
- clef: joulin_les_1901
  note: n° 179 B
- clef: lafore_culte_nodate
  note: p. 10
- clef: lebegue_ecole_1889
  note: p. 10-11
- clef: massendari_haute-garonne_2006-1
  note: p. 254, fig. 140
- clef: rachou_catalogue_1912
  note: n° 40
- clef: reinach_repertoire_1897
  note: t. II, vol. II, p. 430, fig. 8
- clef: roschach_catalogue_1865
  note: n° 40

---
Cette statue est la plus grande représentation d’Harpocrate découverte en France. Le dieu porte une corne d’abondance, symbole de fertilité et de fécondité. Le cou et les jambes ont été reconstitués. Le bras droit n’a pas été complété car la position exacte de la main n'est pas assurée. L'index devait cependant pointer devant la bouche, symbolisant ainsi, pour les Romains, l'appel au silence et évoquant donc la restriction aux seuls initiés des rites liés à Isis {% cite matthey_chut_2011 -l 545-546 %}. Si cette invitation au secret est mentionnée par des sources écrites au I<sup>er</sup> siècle de notre ère, le geste était cependant montré, à l’origine, par un hiéroglyphe signifiant « enfant (ou celui qui ne parle pas) ».

La dynastie des Ptolémées, d’origine macédonienne, à la tête de l’Égypte depuis la fin du IV<sup>e</sup> siècle avant n. è., a notamment institué à Alexandrie, sa capitale, une triade divine constituée d’Isis, de Sarapis et d’Harpocrate. Sous une forme désormais hellénisée, ce dernier renvoyait à Horus, dieu-faucon ancestral égyptien, fils posthume d’Osiris, enfanté par Isis. Horus prenait le nom, dans l’Égypte pharaonique, d’_Hor-pakhered_, lorsqu’il était représenté et vénéré sous la forme d’un enfant, désignation à l'origine du nom grec de la divinité. C’est donc bien cet aspect juvénile qui est conservé à l’époque hellénistique, et encore durant la période romaine. La corne d’abondance sous-tend, dans ce contexte, la notion de l’enfantement. Le dieu était étroitement lié au soleil naissant. Ainsi, dans le monde grec oriental, comme dans la Rome contemporaine des derniers temps de la République, le rapport d’Harpocrate au soleil fut renforcé par son assimilation à Apollon.

Assez peu mentionné dans les inscriptions, le fils d'Isis fut au contraire abondamment représenté sur les lampes, les pierres dures, dans le bronze ou la terre cuite, à partir du II<sup>e</sup> siècle. Harpocrate devient alors une divinité protectrice des demeures, particulièrement populaire au sein des couches modestes. Posture, gestuelle et attributs de la sculpture de Chiragan se retrouvent sur certaines <a href="/images/comp-ra-40-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-40-1.jpg" alt="Tétradrachme de Trajan, © University of Oxford Roman Provincial Coinage Online"/></span>monnaies provinciales, frappées à Alexandrie, sous Trajan</a>. On distingue les mêmes caractéristiques sur des émissions d'Asie Mineure, au III<sup>e</sup> siècle, frappées dans la province de Lycie-Pamphilie, pour Maximin césar, fils de l'empereur Maximin le Thrace (235-238), ou encore dans celle de Bithynie, à l'effigie de Trajan Dèce et même <a href="/images/comp-ra-40-2.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-40-2.jpg" alt="Monnaie d'Etruscilla, atelier de Nicée (Bithynie), Bibliothèque nationale de France, Domaine public" data-link="https://gallica.bnf.fr/ark:/12148/btv1b8555621g/f2.item.zoom" data-link-title="gallica.bnf.fr"/></span>de sa femme, Etruscilla</a>, au milieu du siècle. L'impératrice y est associée à une figure d'Harpocrate qui renvoyait sans doute aux enfants du couple et, par conséquent, au devenir de la Maison impériale {% cite amandry_roman_2015 -L none -l 4183 %}  {% cite hostein_roman_2016 -L none -l n° 282 %}  . Si un [portrait d'Etruscilla](/ra-74) fut découvert à Chiragan (il compte parmi les plus imposantes images féminines connues pour toute la période romaine), aucun indice ne nous permet cependant de le rattacher aux sculptures isiaques de la _villa_.

P. Capus