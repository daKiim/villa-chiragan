---
title: Hercule au repos
id_inventaire: Ra 115
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: II<sup>e</sup>-III<sup>e</sup> siècle
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: "62"
largeur: "45"
longueur: ''
profondeur: "21"
epaisseur: 
id_wikidata: Q26159966
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe: 
order: 80
priority: 3
id_notice: ra-115
image_principale: ra-115-1
vues: 4
image_imprime: 
redirect_from:
- ra-115
- ark:/87276/a_ra_115
id_ark: a_ra_115
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 110-111
- clef: centro_de_exposiciones_arte_canal_roma_2007
  note: p. 171, n° 102
- clef: esperandieu_recueil_1908
  note: n° 893
- clef: joulin_les_1901
  note: fig. 138 D, 147 E
- clef: rachou_catalogue_1912
  note: n° 115
- clef: slavazzi_italia_1996
  note: fig. 30

---
Au IV<sup>e</sup> siècle avant n. è., le sculpteur grec Lysippe conçut une sculpture en bronze représentant _Hercule au repos_. Le prototype connut un immense succès, comme en témoignent les nombreuses copies et dérivés des époques hellénistique et romaine.

Le héros se repose donc, après avoir accompli tous les Travaux qui lui furent commandés par son cousin et rival, Eurysthée. Il s’appuie sur la massue taillée dans un bois d'olivier, recouverte de la peau du lion de Némée. Il tenait dans sa main droite, ramenée dans le dos, les pommes d’or du Jardin des Hespérides, offertes à Héra en l’honneur de son mariage avec Zeus et qu’Hercule dut ramener depuis l'extrême Occident, royaume du soleil couchant et de la mort, sur la façade Atlantique du Maroc.

Le type statuaire est  bien connu, tout d’abord en raison de la <a href="/images/comp-ra-115-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-115-1.jpg" alt="Hercule Farnèse, musée archéologique national de Naples, Inv. 6001, Marie-Lan Nguyen / Wikimedia Commons CC BY"/></span>sculpture monumentale, de près de trois mètres de hauteur, de l’ancienne collection Farnèse</a>, découverte à Rome, en 1546, dans les Thermes de Caracalla, et envoyée à Naples en 1787, comme l’ensemble des prestigieuses découvertes. C’est en particulier au III<sup>e</sup> siècle de notre ère, que l’œuvre de Lysippe fut reproduite sur des revers de monnaies de Caracalla et de Gordien III.

Les variantes romaines, en raison de quelques modifications sensibles, brouillent un peu la vision de ce que dut être, réellement, l’original grec. La version découverte dans la _villa_ de Chiragan, pourrait bien correspondre au deuxième des trois types distingués par P. Moreno {% cite moreno_lisippo_1995 -l 242-250 %} : la peau de lion prend peut-être ici plus d’importance, l’inclinaison de la tête, malheureusement disparue dans le cas de notre exemplaire, serait plus prononcée que sur d’autres types et la masse musculaire moins outrancière qu’à Naples ou Florence (Palais Pitti) où, à la différence de la variante de Chiragan, la jambe gauche vient en avant de la droite, les pieds s'alignant ainsi. La seule position de la jambe droite, écartée, du marbre toulousain, rapproche donc bien plus ce dernier de l’_Héraclès au repos_, découvert à Foligno et conservé au musée du Louvre.

La main droite est ramenée dans le dos. Aujourd’hui disparue, elle avait peut-être été refaite en stuc dès l’Antiquité. Cette sculpture n’a pas été importée à Chiragan mais conçue sur place, comme le prouve la provenance du marbre pyrénéen.

P. Capus