---
title: Isis
id_inventaire: Ra 38 (1)
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: 'III<sup>e</sup>- premier tiers du IV<sup>e</sup> siècle (?) '
materiau: Marbre de Saint-Béat (Haute-Garonne)
hauteur: "186"
largeur: "74"
longueur: ''
profondeur: "40"
epaisseur: 
id_wikidata: Q26721217
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe: 
order: 40
priority: 3
id_notice: ra-38-1
image_principale: ra-38-1-1
vues: 1
image_imprime: 
redirect_from:
- ra-38-1
- ark:/87276/a_ra_38_1
id_ark: a_ra_38_1
precisions_biblio:
- clef: balty_les_2008
  note: p. 16
- clef: cazes_musee_1999-1
  note: p. 100
- clef: clarac_musee_1841
  note: p. 586
- clef: du_mege_description_1835
  note: n° 109
- clef: du_mege_notice_1828
  note: n° 46
- clef: esperandieu_recueil_1908
  note: n° 927
- clef: joulin_les_1901
  note: n° 177 B
- clef: massendari_haute-garonne_2006-1
  note: p. 254, fig. 139
- clef: rachou_catalogue_1912
  note: n° 38
- clef: roschach_catalogue_1865
  note: n° 38

---
L’Égypte et sa culture étaient à la mode dans l’Empire romain. Si la figure d’Isis est souvent présente sur certains objets du quotidien, elle s’impose néanmoins à Chiragan sous une forme bien plus spectaculaire. Cette sculpture fragmentaire est effectivement la plus grande parmi l’impressionnante moisson de marbres découverts dans la _villa_. Privée de sa tête, l'œuvre n'en atteint pas moins presque deux mètres de hauteur. Le marbre noir veiné, qui fut extrait dans un filon pyrénéen de Saint-Béat (Haute-Garonne), traduit la couleur symbolique de la déesse. Le manteau sacré bordé d'une frange est un autre signe distinctif {% cite malaise_a_2011 -l 471-472 %}. Surtout, plusieurs pans de celui-ci, ramenés au centre du buste et entre les seins, sont maintenus par un nœud, dit isiaque, rappel du hiéroglyphe _ânkh_, symbole de vie. Des cavités avaient été réservées afin de loger <a href="/images/comp-ra-38-1-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-38-1-1.jpg" alt="Isis, Kunsthistorisches Museum Vienne, Gryffindor / Wikimedia Commons CC BY"/></span>« les pieds, les bras et la tête, probablement taillés dans un marbre blanc, disparus eux aussi »</a>.

Les Ptolémées, maîtres de l’Égypte depuis la fin du IV<sup>e</sup> siècle avant n. è., donnent à la grande déesse égyptienne une apparence hellénique, suite à l’institution, à Alexandrie, de la triade divine qu'elle forme avec Sarapis et Harpocrate. Isis, associée aux origines de l’Univers et incarnation de la fertilité, des activités humaines, de la justice et des préceptes moraux, est également guérisseuse.

En raison des liens entretenus par les tyrans de Syracuse avec les Ptolémées, son culte est implanté en Sicile grecque orientale, au moins à partir de la fin du III<sup>e</sup> siècle avant n. è. Ce sont, au contraire, des initiatives individuelles, notamment dans le cadre de voyages commerciaux, qui sont à l’origine de la diffusion de la religion isiaque en Méditerranée et dans le monde occidental. En Campanie, à Rome, et probablement ailleurs dans la péninsule italique, la religion d’Isis est établie au II<sup>e</sup> siècle avant n. è. Plus encore que la Sicile, c’est probablement Délos, en mer Egée, qui doit être considérée comme le point majeur de cette importation. L’île de la naissance légendaire du dieu Apollon devient alors l’épicentre du commerce des esclaves en Méditerranée orientale et le lieu où s’enrichissent considérablement nombre d’Italiens, hommes d’affaires, banquiers et même artisans, en relation avec ces cultes originaires d’Alexandrie {% cite bricault_isis_2007 bricault_isis_2004 -l 265-267 %}. Déjà au I<sup>er</sup> siècle avant n. è., les cultes isiaques avaient pénétré les autres régions du monde occidental. Vivement combattus par Auguste et Tibère, ils trouvent au contraire en la personne de Caligula un zélé protecteur. Mais on retient en particulier le soutien d’Hadrien, au II<sup>e</sup> siècle, dont l’enthousiasme pour l’atmosphère spirituelle égyptisante se confond avec son histoire personnelle et la mort de son amant, Antinoüs.

Dans le domaine numismatique, de très rares émissions représentant les divinités dites isiaques sont frappées à partir de la fin du I<sup>er</sup> siècle de notre ère. Mais les dieux du Delta apparaissent bien plus fréquemment sous les Sévères, durant le premier tiers du III<sup>e</sup> siècle. On constate ensuite, à partir de l’époque tétrarchique et l'empereur Dioclétien, que les modestes frappes en laiton à légende VOTA PUBLICA (_Voeux publics_), distribuées le 3 janvier, sont, pour la première fois, associées aux dieux isiaques. La prodigalité impériale, exprimée par la distribution de ces petits modules aux couches populaires, renvoie aux vœux de prospérité adressés à l’Empire et à ses sujets et intègre donc la déesse démiurge Isis, Sarapis et, à un moindre degré, Harpocrate. Paradoxalement poursuivie durant l’époque des empereurs chrétiens, au IV<sup>e</sup> siècle, la frappe de ces « monnaies » pourrait en fait témoigner des revendications du courant conservateur et d'une classe aristocratique anti-chrétienne qui, à Rome, davantage qu’un attachement spécifique aux dieux alexandrins, aspirait à une fidélité à l'égard la religion polythéiste ancestrale {% cite bricault_isis_2011 -l 16 %}.

L’importance accordée aux divinités isiaques de la part des augustes Dioclétien et Maximien Hercule est notamment démontrée par les monnaies de cuivre des _Vota publica_. Le phénomène pourrait ne pas être étranger à la présence des statues isiaques monumentales de la _villa_ de Chiragan. Le format de ces sculptures, exceptionnel dans un cadre privé, laisse en effet songeur. Si l'on ne peut que difficilement conjecturer l'existence d'un sanctuaire dans l'espace de la demeure, au moins la présence de cette triade renvoie-t-elle sans aucun doute à un attachement particulier, pour l'un des propriétaires du lieu, aux fonctions salvatrice et fécondante de ces dieux alexandrins.

P. Capus