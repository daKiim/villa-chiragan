---
title: Sarapis-Pluton
id_inventaire: Ra 29
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: III<sup>e</sup> siècle – premier tiers du IV<sup>e</sup> siècle (?)
materiau: Marbre
hauteur: "142"
largeur: "42"
longueur: ''
profondeur: "17"
epaisseur: 
id_wikidata: Q26720688
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe: 
order: 50
priority: 3
id_notice: ra-29
image_principale: ra-29-1
vues: 1
image_imprime: 
redirect_from:
- ra-29
- ark:/87276/a_ra_29
id_ark: a_ra_29
precisions_biblio:
- clef: beckmann_idiom_2020
  note: p. 142-143, fig. 9
- clef: cazes_musee_1999-1
  note: p. 102-103
- clef: du_mege_description_1835
  note: n° 161
- clef: du_mege_notice_1828
  note: n° 74
- clef: esperandieu_recueil_1908
  note: n° 891
- clef: galerie_dart_municipale_luxembourg_les_1989
  note: n° 96
- clef: kater-_sibbes_preliminary_1973
  note: p. 153, n° 810
- clef: massendari_haute-garonne_2006-1
  note: 'p. 256, fig. 143 '
- clef: rachou_catalogue_1912
  note: n° 29
- clef: reinach_repertoire_1897
  note: t. II/1, p. 18, fig. 9 ; t. II/2, p. 698, fig. 2 ; t. III, 8, n° 3
- clef: roschach_catalogue_1865
  note: n° 29
- clef: stirling_shifting_2016
  note: ''

---
À Alexandrie, la dynastie macédonienne des Ptolémées serait à l’origine de la création du dieu Sarapis. Probablement, l’une des principales fonctions originelles de cette entité divine consistait-elle à fédérer les diverses communautés qui se côtoyaient dans le delta du Nil et au-delà. Le nom lui-même, Sarapis, particulièrement consensuel, était né, dans cette Égypte de l’époque hellénistique, de la contraction de deux dénominations divines, _OSOR-HAPI_ ou « Osiris-Apis ». Osiris avait été assimilé, dans l'Égypte pharaonique, au dieu-taureau Apis, symbole de fécondité. Sarapis, à son tour, se substituant au traditionnel Osiris, devint l’époux de la déesse noire Isis et fut vénéré pour ses capacités de guérison. La représentation de ce dieu sauveur à Chiragan l’assimilerait à Pluton (l’Hadès grec), dieu des forces souterraines, si l’on en croit la présence de Cerbère, chien à trois têtes, gardien du royaume des morts (les Enfers). Si aucun fragment ne permet de relier avec certitude l’animal au corps de la divinité, un même support lisse et mouluré semble unir les deux figures. À la gauche du chien tricéphale se raccorde un morceau qui porte la suite de sa queue et un pied droit chaussé d'une sandale à lanières, identique à un pied gauche, associé lui aussi à cet ensemble, malgré l’absence de liaison par un quelconque fragment. Le haut-relief du dieu guérisseur de la _villa_ de Chiragan peut être rapproché d’une sculpture en ronde-bosse représentant <a href="/images/comp-ra-29-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-29-1.jpg" alt="Hadès trônant, théâtre de Hierapolis, Hierapolis Archeological Museum, © The Tourism Directorate of Denizli Directorate of Pamukkale Tourist Information"/></span>Hadès trônant, mise au jour dans le théâtre de _Hierapolis_</a> (Pammukale, Turquie), ainsi que du Pluton, lui aussi coiffé du _modius_, découvert dans le théâtre antique d’_Augusta Emerita_ (Mérida, Estrémadure). Sarapis est coiffé d’un _calathos_ (ou _modius_ latin), corbeille et plus exactement, mesure de capacité qui, en tant qu’attribut, trouve son fondement dans le contexte agricole des fêtes d'Osiris concluant la crue du Nil, source d’approvisionnement en blé. Ce symbole est accompagné d’un autre, au premier plan : le _hedjet_, mitre triangulaire surmontée d’un bulbe. _Hedjet_ blanche de la Haute-Égypte et _deshret_ rouge de la Basse-Égypte, formaient la double-couronne, le _pshent_, porté par les pharaons.

Les statues de Sarapis, Isis et Harpocrate étaient-elles, à Chiragan, des statues de culte, comme le supposait, au moins pour certaines périodes, L. Stirling {% cite stirling_shifting_2016 -l 60 %} ? En l’absence de témoignage archéologique convaincant, il est impossible d'avancer quoi que ce soit à ce sujet. D’un autre côté, réduire ces grandes effigies à un simple décor ne semble, il est vrai, que peu convaincant. Une réaction conservatrice, à l'égard de ces dieux étrangers, si étroitement intégrés cependant par l'élite à partir d'une certaine période, nous semble parfaitement plausible et quelque propriétaire nostalgique et ayant entretenu des liens avec l'Égypte aurait parfaitement pu décider de l'implantation d'un espace isiaque au sein de la _villa_. Car si ces dieux étrangers avaient connu de nombreuses oppositions, parfois violentes, au sein de l’aristocratie romaine - on se souviendra ainsi de la destruction, à Rome, au milieu du I<sup>er</sup> siècle avant n. è., sur ordre du Sénat, des lieux de culte des divinités isiaques, élevés avec des fonds privés {% cite dion_cassius_histoire_nodate -L none -l 40, 47 %} – on assiste à un retour en grâce des dieux du Delta, sous les empereurs Flaviens, à la fin du I<sup>er</sup> siècle de n. è. C’est ensuite sous le règne de Commode, dernier empereur de la dynastie antonine, qu’Isis et Sarapis sont honorés, notamment par des séries d’émissions monétaires. À la suite des empereurs Sévères, ce sont les Tétrarques de la fin du III<sup>e</sup> siècle, dont les deux Augustes, Dioclétien et Maximien Hercule, qui élèveront bien haut dans le panthéon les dieux sauveurs et protecteurs.

Le grand haut-relief de Sarapis, découvert à Chiragan, pourrait bien s’inspirer d’un modèle particulièrement connu durant l’Antiquité et dont on retrouve notamment un écho sur des monnaies d’Alexandrie, frappées sous les règnes de Trajan et d’Hadrien. Le dieu, debout, tient un sceptre dans la main gauche et tend la droite au-dessus de la <a href="/images/comp-ra-29-2.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-29-2.jpg" alt="Perséphone (en Isis), Cerbère, et Pluton (en Serapis). Provenance Gortys, Crète, Musée archéologique d'Héraklion Jebulon CC0 Wikimedia Commons"/></span>tête de Cerbère</a> {% cite amandry_roman_2015 -L none -l III, 4187, 4533, 6032 %}. C’est à ce modèle qu’il faut associer l'œuvre, aujourd'hui au musée Saint-Raymond, et non au type statuaire créé par le sculpteur grec-oriental Bryaxis, création chatoyante, conçue à l’aide de matériaux luxueux. Cette dernière aurait en réalité représentée Hadès et son identité détournée, au moins 50 ans après sa création, au profit du nouveau dieu des Ptolémées, dont le grand temple venait d’être construit à Alexandrie.

De Sicile et de l’île de Délos, où la religion de Sarapis s’était solidement implantée, furent importés les dieux du Delta. Commerçants et esclaves égyptiens ne furent pas étrangers à la diffusion de ces cultes en terre italienne. Sous l’Empire, Sarapis _calathophoros_ (porteur de la corbeille), guérisseur et accompagnateur des morts conserve bien son rôle de parèdre d’Isis, la déesse noire qui promettait une vie éternelle aux initiés. En Gaule Narbonnaise, ainsi que le signale L. Bricault, les image de Sarapis sont particulièrement nombreuses dans la vallée du Rhône, les premières pentes des Alpes et du Massif Central et les grands centre urbains, de Marseille à Vienne, en passant par Nîmes, Arles ou Glanum {% cite bricault_atlas_2001 %}. Sa présence est plus diffuse dans la haute vallée de la Garonne et à Toulouse où se distinguent de beaux exemplaires : bustes de bronze à Montmaurin et en marbre à Saint-Michel-du-Touch (disparu).

P. Capus