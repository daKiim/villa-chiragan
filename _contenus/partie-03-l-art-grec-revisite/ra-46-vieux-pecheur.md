---
title: Le vieux pêcheur
id_inventaire: Ra 46
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: III<sup>e</sup>-premier tiers du IV<sup>e</sup> siècle (?)
materiau: Marbre de Göktepe (Turquie)
hauteur: "52"
largeur: "27"
longueur: ''
profondeur: "16"
epaisseur: 
id_wikidata: Q27666104
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe: 
order: 140
priority: 3
id_notice: ra-46
image_principale: ra-46-1
vues: 8
image_imprime: 
redirect_from:
- ra-46
- ark:/87276/a_ra_46
id_ark: a_ra_46
precisions_biblio:
- clef: baratte_exotisme_2001
  note: p. 71
- clef: bergmann_chiragan_1999
  note: p. 55
- clef: cazes_musee_1999-1
  note: p. 111
- clef: clarac_musee_1841
  note: p. 586
- clef: du_mege_description_1835
  note: n° 133
- clef: du_mege_notice_1828
  note: n° 149
- clef: esperandieu_recueil_1908
  note: p. 62, n° 952
- clef: hannestad_tradition_1994
  note: p.135
- clef: joulin_les_1901
  note: fig. 200B, 212 E
- clef: lebegue_ecole_1889
  note: p. 10
- clef: rachou_catalogue_1912
  note: n° 46
- clef: rey-delque_vieux_1975
  note: p. 89 à 95
- clef: roschach_catalogue_1892
  note: n° 46
- clef: slavazzi_italia_1996
  note: fig. 29

---
La sculpture de l'époque hellénistique montra ostensiblement son intérêt pour les individus de basse condition, âgés et souffrants. Cette vérité sans concession fut particulièrement sensible entre le III<sup>e</sup> et le I<sup>er</sup> siècle avant n. è. dans le monde grec. Cependant, la crudité de la vision portée sur ceux qui, de manière générale, demeurent invisibles, est tempérée par l'utilisation d'un matériau luxueux, réservé à un cercle restreint. Souffrance, humilité et activités de la rue contrastent donc étrangement avec les marbres les plus rares et les plus onéreux.

Plusieurs autres répliques du prototype du pêcheur sont aujourd'hui connues. La plus célèbre demeure la version du Louvre, dite «Sénèque mourant», provenant de l'ancienne collection Borghèse. Mais dans le cadre d'une statuaire de moyen format comparable à la statue de la _villa_ de Chiragan, une œuvre en particulier, mise au jour à Aphrodisias (Turquie), offre de troublantes similitudes avec notre exemplaire. Quant à la réplique aujourd'hui conservée à l'Altes Museum de Berlin, remarquable par sa qualité et le poli de son marbre, la couleur blanche de ce dernier atténue l'effet de surprise et de fascination que procure la roche noire, à Chiragan comme à Aphrodisias {% cite hannestad_tradition_1994 bergmann_chiragan_1999 -L none -l p. 135, 138-39 -l 55 %}.

Selon Léon Joulin, Du Mège aurait trouvé un bras tenant un fragment du filet. Ce document n'a pas été retrouvé parmi les centaines de vestiges relevant de la petite et moyenne statuaire de Chiragan. En revanche, un pied appuyé contre la base d'un arbre, lui aussi en marbre noir, est toujours conservé dans les réserves du musée et ses dimensions ne semblent pas contrarier l'appartenance à cette statue. Ce qu'il reste d'un support, à l'arrière du cou, permet de supposer qu'un tronc d'arbre devait être associé à la figure. Il s'agirait là d'une caractéristique de la petite et moyenne statuaire orientale tardive, bien représentée dans les ateliers d'Aphrodisias. Ce rapprochement avec les ateliers micrasiatiques, officiant durant l'Antiquité tardive, serait également à envisager si l'on prend en compte le fort contraste esthétique entre l'arrière de la sculpture, où les grandes masses, dont la colonne vertébrale particulièrement saillante, ont été dégrossies et non polies, et la partie antérieure de ce corps, où l'anatomie, dessinée mais relativement simplifiée, est caractérisée par le lustre accordé au matériau {% cite hannestad_tradition_1994 -l 135 %}. Les liens avec Aphrodisias et les ateliers orientaux sont par conséquent tout aussi convaincants que pour les médaillons des dieux ou encore les reliefs d'Hercule.

Connu par vingt-cinq répliques d'époque impériale, l'original, daté du III<sup>e</sup> siècle avant n. è., devait être un bronze dont l'aspect scintillant est rappelé par le marbre noir luisant. Le vieillard, seulement vêtu d'un pagne enroulé autour des reins, présentait de modestes objets aux dieux {% cite queyrel_sculpture_2016 -l 315 %}. La réplique du musée Saint-Raymond, réduite en taille, montre un drapé décalé, dont un pan tombe avec raideur et dissimule les parties génitales du personnage. Sur certaines autres répliques, le sexe du vieillard est exhibé, conformément au pathos en vogue à l'époque hellénistique : celle de Chiragan est par conséquent édulcorée, moins brutale, plus décorative. Par cette simple modification formelle, la sculpture prend donc un sens différent et s'adapte au lieu pour lequel elle a été commandée à un moment donné. Un même modèle iconographique grec peut ainsi subir une série d'évolutions qui transforme sa signification, au gré des différents ateliers.

P. Capus