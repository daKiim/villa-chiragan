---
title: Bacchus
id_inventaire: Ra 134-Ra 137
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: III<sup>e</sup>- IV<sup>e</sup> siècle
materiau: Marbre de Göktepe (Turquie)
hauteur: "49"
largeur: "30"
longueur: ''
profondeur: "21"
epaisseur: 
id_wikidata: Q25114454
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe: 
order: 130
priority: 3
id_notice: ra-134-ra-137
image_principale: ra-134-ra-137-1
vues: 3
image_imprime: 
redirect_from:
- ra-134-ra-137
- ark:/87276/a_ra_134_ra_137
id_ark: a_ra_134_ra_137
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 111-112
- clef: esperandieu_recueil_1908
  note: p. 47 n° 919, p. 54 n° 935
- clef: joulin_les_1901
  note: fig. 155E, 160E
- clef: massendari_haute-garonne_2006-1
  note: p. 253, fig. 136
- clef: mesple_raccords_1948
  note: p. 156, fig. 1-2
- clef: musee_saint-raymond_essentiel_2011
  note: p. 36-37
- clef: musee_saint-raymond_odyssee_2013
  note: p. 32
- clef: pasquier_praxite._2007
  note: p. 340-341, n° 87
- clef: rachou_catalogue_1912
  note: n° 134, 137
- clef: stirling_learned_2005
  note: ''

---
Le dieu Bacchus (le Dionysos grec) est intimement associé à la nature non civilisée et aux puissances du monde sauvage. Dieu du vin et du théâtre, il est ici représenté couronné et plein de langueur, appuyé contre un cep de vigne, auquel il est également relié par de nombreux étais et ponts. Ses cheveux bouclés sont caractérisés par des sillons, exécutés au ciseau droit, et des creusements, conçus au foret (ou trépan), une technique commune à de nombreuses autres figures de la _villa_, qu'elles soient de petit ou moyen format. La figure devait être accompagnée au moins d'une autre, peut-être <a href="/images/comp-Ra134-137-3.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-Ra134-137-3.jpg" alt="Groupe de Bacchus du temple de Mithra, Museum of London, inv. 18496, Ethan Doyle White /Wikimedia Commons CC BY-SA"/></span>une panthère ou encore un satyre</a>. Son corps est celui d'un pré-adolescent et relève d'une esthétique de type éphébique, appréciée dans le monde grec au IV<sup>e</sup> siècle avant n. è. Pondération du corps, sinuosité des formes et androgynie semblent conformes à celles de l'<a href="/images/comp-ra-134-ra-137-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-134-ra-137-1.jpg" alt="Apollon Sauroctone, musée du Louvre, Paris, inv. MR 78, Ma 44, Marie-Lan Nguyen /Wikimedia Commons CC BY"/></span>_Apollon Sauroctone_</a> et plus généralement à l'art du sculpteur grec Praxitèle, ici « pastiché » {% cite pasquier_praxite._2007 -l 340 %}.

Ces caractères, propres au second classicisme grec, sembleraient être encore revisités huit siècles plus tard. Il apparaît, quoi qu'il en soit, que des sculptures relevant de l'art classique envahissent toujours le cadre domestique des élites, en quête de signes esthétiques identitaires. Le Bacchus de Chiragan pourrait en témoigner. On y perçoit  une sensibilité esthétique proche d'une série de sculptures produites par des ateliers orientaux. Ainsi, poli du marbre, division au ciseau et emplacement des coups de trépan dans les longues boucles sinueuses, schématisme des arcades sourcilières et creusement des pupilles rapprochent par exemple notre dieu juvénile d'une célèbre statuette de Carthage datée du V<sup>e</sup> siècle, représentant Ganymède et l'aigle. Si la base du Bacchus a malheureusement disparu, celle de la figure mythologique carthaginoise, formée de deux bandeaux séparés par une gorge, renvoie typologiquement à certains supports de figures mythologiques provenant de Chiragan comme, également, à la base de la statue de Diane, découverte dans la _villa_ de Saint-Georges-de-Montagne (Gironde). Autant d'œuvres qui peuvent s'apparenter à une série de figures plus ou moins fragmentaires, dont une statue de Sol (Apollon), mises au jour dans une <a href="/images/comp-ra-134-ra-137-2.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-134-ra-137-2.jpg" alt="Ensemble de la _villa_ de Silahtarağa à Constantinople, musée d'Istanbul, Carole Raddato CC BY-SA"/></span>_villa_ de Silahtarağa</a>, à Constantinople {% cite chaisemartin_les_1984 -L none -l Pl. 4-5 %}.

Dionysos/Bacchus et son cercle constituent le groupe le plus important dans la _villa_ de Chiragan, environ la moitié de la statuaire indépendante : figures de l'entourage du dieu, à l'image des satyres, des silènes et des ménades ou encore Ariane. Mais la popularité pour ces divinités est, il faut bien l'avouer, commune à l'ensemble des grands domaines et luxueuses demeures à travers tout l'Empire. Dans le contexte des _villae_ tardo-antiques du Sud-Ouest des Gaules ou de la péninsule Ibérique, l'évident succès de Bacchus pourrait être la conséquence du renforcement de son rôle de sauveur tout autant que de soutien à la bonne fortune des hommes {% cite stirling_learned_2005 koppel_skulpturenausstattung_1993 arasa_i_gil_decoracion_2004-1 -l 87 -l 201 -l 233 %}.

En dépit de ces préoccupations spirituelles, la simple fonction décorative ne prendrait-elle pas le pas, ici, sur la fonction symbolique ? On a notamment vu dans cette séduisante figure, et la composition dont elle devait dépendre, un simple, mais indéniablement somptueux, pied de table {% cite pasquier_praxite._2007 -l 340 %}. Aucun élément ne peut cependant prouver une telle utilisation. Cep de vigne, branches et frondaisons, malheureusement lacunaires, qui encadraient et servaient d'arrière-plan à cette figure de type éphébique, renverraient au contraire au type de composition qui caractérise la Diane de Saint-Georges-de-Montagne, sertie dans une véritable couronne végétale. De nombreux fragments, dans les réserves du musée, entre autres une tête « barbare », prouvent la récurrence d'une telle mise en scène. Il s'agirait bien là, en l'occurrence, de formules attribuées à des ateliers orientaux qui, encore tardivement, produisirent une surprenante quantité de sculptures en ronde-bosse, destinées aux niches des grandes demeures aristocratiques à travers tout l'Empire.

P. Capus