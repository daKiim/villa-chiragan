---
title: Piédouche d'un buste disparu
id_inventaire: Ra 178
donnees_biographiques1: 
donnees_biographiques2: 
date_creation: III<sup>e</sup> siècle
materiau: Marbre lychnites (île de Paros)
hauteur: '19,5'
largeur: "23"
longueur: 
profondeur: "20"
epaisseur: 
id_wikidata: Q48322083
bibliographie: oui
layout: notice
type: notice
partie: 1
sous_groupe: 
order: 10
priority: 3
id_notice: ra-178
image_principale: ra-178-1
image_imprime: 
vues: 1
redirect_from:
- ra-178
- ark:/87276/a_ra_178
id_ark: a_ra_178
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 75
- clef: ensoli_aurea_2000
  note: p. 172-173
- clef: graillot_villa_1908
  note: p. 12-15
- clef: hirschfeld_cil_1899
  note: n°11007
- clef: joulin_les_1901
  note: p. 293-294, 341, pl. XXIV, n° 307 E
- clef: massendari_haute-garonne_2006-1
  note: p. 237, fig. 95
- clef: musee_saint-raymond_regard_1995
  note: p. 211

---
De même que pour l'essentiel des _villae_, le problème de l'identité des maîtres de Chiragan semble inextricable. En outre, on ne peut réduire un tel domaine, dont l'activité s'échelonne sur plusieurs siècles, à un seul type de propriétaire. Davantage que les remaniements, agrandissements et restructurations architecturales, c'est surtout l'impressionnante quantité de portraits, dont une série notable de probables hauts fonctionnaires, qui a permis d'avancer l'hypothèse d'une résidence de gouverneur de province. En outre, et en raison même de la somptueuse parure de marbre qui fut associée à cette architecture imposante, une fonction de résidence impériale a toujours été plus ou moins soupçonnée.

À qui appartenait cette extraordinaire collection de sculptures ? Les liens avec l'Italie sont bien entendu très puissamment inscrits dans ce rassemblement, en particulier les portraits. La majorité, impériaux comme anonymes, peuvent être directement reliés à des ateliers romains. Aujourd'hui mêlée aux effigies impériales, l'importante série des anonymes remonte pour sa part aux époques antonine et sévérienne. Elle concerne très probablement des personnages qui dépendaient de l'ordre équestre et représentaient d'importants acteurs de la vie fiscale et administrative romaine. Parmi ces inconnus, certains faciès sembleraient avoir été reproduits ailleurs, dans l'Occident romain. La présence simultanée de portraits similaires en différents points de l'Empire pourrait ainsi témoigner des charges de procurateur endossées par un même individu au service de plusieurs provinces {% cite bergmann_chiragan_1999 balty_les_2012 -L none -l p. 30, 42-43 -l 268-269 %}.

Bien malheureusement, nous devons nous contenter, pour la _villa_ de Chiragan, d'une seule inscription, celle qui fut gravée sur ce piédouche (base), aujourd'hui privé de son buste, découvert à l'ouest de la grande cour méridionale, au  niveau d'une série de pièces, bordées par un cryptoportique. On y lit GENIO C. ACONI TAURI VET. ainsi traduit : «Au génie d'_Aconius Taurus_» {% cite hirschfeld_cil_1899 -L none -l 11007 %}. Le nom propre d'_Aconius_ (gentilice) est placé, comme il se doit, entre le prénom (_praenomen_) _Gaius_, abrégé en C., et le surnom (_cognomen_), _Taurus_ {% cite graillot_villa_1908 -l 14-15 %}. Si ce document épigraphique est loin de répondre à toutes les interrogations relatives aux propriétaires ou aux gestionnaires du domaine foncier, il représente cependant un expédient non négligeable en ouvrant la voie à quelques hypothèses. L'inscription, incomplète, dont la forme des lettres ne serait pas antérieure au II<sup>e</sup> siècle {% cite eck_sugli_2000 bergmann_chiragan_1999 -l 172 -l 43 %}, est donc dédiée au _genius_ (le double surnaturel) de _Gaius Aconius Taurus_. On connaît deux familles différentes d'_Aconii_, à la fin du III<sup>e</sup> siècle, en Italie. L'une d'entre elles était installée à Pérouse, où fut retrouvé un portrait privé, rapproché de l'une des effigies de Chiragan {% cite stirling_learned_2005 -l 63 %}. Un sénateur nommé _Aco Catullinus_ est également attesté à Rome au début du IV<sup>e</sup> siècle.

Le nom d'Aconius réapparaît à Rome à la fin du IV<sup>e</sup>-début du V<sup>e</sup> siècle ; l'un des membres parmi les plus connus de la famille est _Aconia_, femme de _Vettius Agorius Praetextatus_. L'attachement des époux au paganisme était notoire, comme leur fidélité à de nombreux cultes à mystères {% cite stirling_learned_2005 -l 62 %}. Mais les preuves seraient insuffisantes pour relier une dédicace de statue de Gaule méridionale, peut-être datable du II<sup>e</sup> siècle de n. è., à des noms, certes identiques, mais répertoriés bien plus tardivement, à Rome et en Italie {% cite eck_sugli_2000 -l 172-173 %}. On ne doit cependant pas oublier qu'au XVII<sup>e</sup> siècle, le site des ruines de Chiragan était localement appelé _Angonia_, du nom de la _villa_ _Aconiaca_, éventuel souvenir de l'un des propriétaires du domaine. Ce portrait, dont ne subsiste que la base inscrite, était-il donc celui de l'un de ces hauts fonctionnaires, propriétaires ou gestionnaires du domaine ?

Pascal Capus