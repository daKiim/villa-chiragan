---
title: Buste d’une inconnue
id_inventaire: Ra 77
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Vers 125 - 135
materiau: Marbre de Göktepe 3 (Turquie)
hauteur: "47"
largeur: "36"
longueur: ''
profondeur: "32"
epaisseur: 
id_wikidata: Q24490548
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 110
priority: 3
id_notice: ra-77
image_principale: ra-77-1
vues: 8
image_imprime: 
redirect_from:
- ra-77
- ark:/87276/a_ra_77
id_ark: a_ra_77
precisions_biblio:
- clef: balty_les_2012
  note: p. 47-48, fig. 46-48, p. 172, 174-176, 178, 181-182, fig. 101-104, 107, 110-112
- clef: cazes_musee_1999-1
  note: p. 133
- clef: du_mege_description_1835
  note: n° 204
- clef: du_mege_notice_1828
  note: n° 142
- clef: esperandieu_recueil_1908
  note: n° 989
- clef: joulin_les_1901
  note: n° 289
- clef: massendari_haute-garonne_2006-1
  note: p. 249, fig. 119
- clef: musee_saint-raymond_essentiel_2011
  note: p. 42-43
- clef: musee_saint-raymond_image_2011
  note: p. 84-85
- clef: rachou_catalogue_1912
  note: n° 77
- clef: roschach_catalogue_1892
  note: n° 77

---
Cet élégant buste, d’une finesse de facture et de style peu commune, figure une jeune femme vêtue d’une tunique plissée, boutonnée sur l’épaule droite et recouverte d’un manteau qui vient s’enrouler autour des épaules. La découpe suit la ligne inférieure des seins avant de s’évaser obliquement vers les épaules où une cassure, du côté gauche, a endommagé le drapé. La tête est nettement tournée vers la gauche, le cou est fin et long, le port de tête gracieux. Les yeux, aux globes oculaires larges, sont soulignés par de fins sourcils étirés vers les tempes. La bouche, charnue et délicatement dessinée, présente une lèvre supérieure légèrement retroussée.

La pureté des lignes de ce visage lisse, à l’ovale classicisant, aux yeux et aux sourcils non incisés, fait écho à la finesse avec laquelle les masses capillaires sont traitées. Sur le front, les cheveux, peignés de part et d’autre d’une raie centrale, dégagent complètement les oreilles et sont tirés vers le haut, où les mèches sont rassemblées en trois nattes plates adhérant au crâne. Ces tresses constituent ensuite un épais turban, formé d’un quadruple enroulement, qui vient enserrer l’ensemble de la calotte crânienne. Enfin, deux élégantes boucles en accroche-cœur ornent les tempes et quelques mèches folles ondulent sur la nuque. La facture est remarquable par son emploi quasi exclusif du ciseau droit dans le traitement des yeux comme de la chevelure : le détail de chaque cheveu est obtenu au moyen de très fines incisions, avec une minutie et une sensibilité virtuoses.

La simplicité de cette coiffure se distingue des assemblages aussi variés que raffinés qui caractérisent les portraits féminins de la première moitié du II<sup>e</sup> siècle : elle ne comporte ni les cheveux agencés en forme de diadème bouclé de l’époque trajanienne, ni les édifices capillaires ornés d’ondulations frisées au fer popularisés par les effigies de la pleine période antonine. Le portrait officiel des dames de la Maison impériale constitue, on le sait, un référent obligé pour l’encadrement chronologique des nombreux visages anonymes qui nous sont parvenus ; le point de confrontation le plus probant est, en l’occurrence, constitué  par certains portraits de l’impératrice Sabine ou très proches de ces derniers – notamment une statue en <a href="/images/comp-ra-77-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-77-1.jpg" alt="Sabine en Vénus Genetrix, inv. 24, musée d'Ostie, Lalupa / Wikimedia Commons CC BY-SA"/></span>Vénus _Genetrix_ du collège des Augustales d’Ostie</a>, d’identification controversée.

Dans le domaine du portrait privé, l’effigie de Chiragan appartient à une série cohérente d’œuvres datables entre le règne d’Hadrien et le début de l’époque antonine : les variantes concernent la position plus ou moins haute du bandeau tressé sur le front ou autour de la tête. En réalité, la comparaison avec les exemplaires issus de l’_Urbs_ ou de ses environs permet de mesurer l’extraordinaire qualité du buste de Chiragan, indéniablement issu d’un atelier de haut niveau. Il n’est pas exclu qu’il figure un personnage éminent de l’aristocratie romaine du temps.

De fragiles indices permettent d’en préciser quelque peu la datation : en raison de l’absence de creusement de la pupille et de l’iris, elle ne saurait être repoussée bien au-delà de 130, quoique le critère ne soit pas infaillible ; quant à la physionomie, empreinte d’une grâce non dénuée d’une certaine gravité, elle achève de rattacher cette œuvre à cet horizon chronologique et fait d’elle un très bel exemple de « visage d’époque » hadrianique, proche de celui des princesses divinisées de la famille des _Ulpii_, de Plotine à Marciana, respectivement épouse et sœur de l'empereur Trajan.

D'après E. Rosso 2012, _Les portraits romains , 1 : Le siècle des Antonins, 1.2_ (_Sculptures antiques de Chiragan (Martres-Tolosane)_, Toulouse, p. 171-183.