---
title: Tête de Lucius Verus
id_inventaire: Ra 63
donnees_biographiques1: 130 - 169
donnees_biographiques2: Co-empereur avec Marc Aurèle de 161 à 169
type_oeuvre: IV (« Samtherrschaftstypus »,  type du règne conjoint avec Marc Aurèle)
date_creation: Entre 161 et 169
materiau: Marbre de Göktepe 3 (Turquie)
hauteur: '35,5'
largeur: '26,5'
longueur: 
profondeur: "27"
epaisseur: 
id_wikidata: Q24476631
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 170
priority: 3
id_notice: ra-63
image_principale: ra-63-1
vues: 8
image_imprime: 
redirect_from:
- ra-63
- ark:/87276/a_ra_63
id_ark: a_ra_63
precisions_biblio:
- clef: balty_les_2012
  note: p. 38, 39 ; fig. 34-37
- clef: bernoulli_romische_1882
  note: p. 210, n° 41
- clef: braemer_les_1952
  note: p. 145
- clef: bulletin_1936
  note: p. 651
- clef: cazes_musee_1999-1
  note: p. 125
- clef: du_mege_description_1835
  note: p. 116-117, n° 207
- clef: du_mege_description_1844
  note: n° 369
- clef: du_mege_notice_1828
  note: p. 66-67, n° 128
- clef: esperandieu_recueil_1908
  note: p. 83, n° 987
- clef: musee_saint-raymond_image_2011
  note: p. 56
- clef: musee_saint-raymond_regard_1995
  note: p. 168
- clef: rachou_catalogue_1912
  note: p. 44, n° 63
- clef: roschach_catalogue_1865
  note: n° 63
- clef: roschach_catalogue_1892
  note: p. 34, n° 63
- clef: rosso_image_2006
  note: p. 463-465, n° 222
- clef: wegner_herrscherbildnisse_1939
  note: p. 247
- clef: wegner_verzeichnis_1979
  note: p. 62

---
Le jeune Lucius Ceionius Commodus fut Adopté par Antonin le Pieux, à la demande d’Hadrien, le 25 février 138, en même temps que le futur Marc Aurèle. Il portera dès lors le nom de Lucius Aelius Aurelius Commodus mais sera davantage connu sous celui de Lucius Verus. Il était entré, dès l'âge sept ans, dans la _gens Aurelia_, la famille d’Antonin ; aussi conserve-t-on, comme pour son frère adoptif _Marcus_, des portraits d’enfant et d’adolescent du jeune prince (trois types successifs, dont deux correspondant à ceux de _Marcus_).

Quoique faisant partie de la _domus Augusta_, Verus « vécut longtemps dans la condition d’un simple citoyen, sans jouir de ces marques d’honneur dont était entouré _Marcus_ », écrit l’_Histoire Auguste_ (_Vie de Verus_, 3, 4) ; mais il fut consul en 154 et 161. À la mort d’Antonin, le 7 mars 161, Marc Aurèle l’investit de la dignité impériale, lui conféra tous les honneurs (il reçut la puissance tribunicienne annuelle et fut à nouveau consul en 167) et partagea le pouvoir avec lui.

C’est de ce moment que datent les nombreux portraits (on en connaît aujourd’hui une centaine) qui le représentent donc à l’âge mûr ; il avait 31 ans. Durant les huit années de ce règne conjoint avec Marc Aurèle, cette effigie ne connut aucune modification ; elle correspond très exactement au type III de Marc Aurèle qui, lui aussi, demeura inchangé de 161 à la mort de Verus (janvier-février 169) : même durée, même diffusion dans tout l’Empire. Les seules différences que l’on puisse observer d’un exemplaire à l’autre tiennent à des pratiques d’ateliers différents : ainsi, l’intense utilisation du trépan qui caractérise le portrait de Chiragan, comme la plupart de ceux parvenus jusqu’à nous, ne se retrouve pas sur certains exemplaires provenant d’autres officines, exemplaires où les cheveux sont presque exclusivement travaillés au ciseau droit (dont celui de Tarragone, découvert dans le secteur du forum de la colonie). Maîtrisant parfaitement le maniement du foret, le praticien qui réalisa l’œuvre de Toulouse a rendu avec virtuosité et une extrême précision le modelé et le dessin très particulier des mèches de l’« Urbild » (prototype créé par les ateliers impériaux du palais) et ce, jusqu’à l’arrière et dans la nuque où elles ont un aspect presque végétal. Cette abondante chevelure bouclée qui retombe assez bas sur le front a souvent été imitée dans le portrait privé de ces mêmes années, les dandys de l’époque s’évertuant à reproduire en cela la coiffure de l’empereur (on sait le soin qu’il apportait à ses cheveux blonds d'après l'_Histoire Auguste, Vie de Verus_, 10, 7) et créant une véritable mode dont témoignent encore bien des portraits de nos musées, souvent pris à tort pour des portraits de Verus.

D'après J.-C. Balty 2012, _Les portraits romains , 1 : Le siècle des Antonins, 1.2_ (_Sculptures antiques de Chiragan (Martres-Tolosane)_, Toulouse, p. 233-242.