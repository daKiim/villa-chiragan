---
title: Tête de jeune garçon (C. Fulvius Plautus Hortensianus ?)
id_inventaire: Ra 68 (1)
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Entre 202 et 205
materiau: Marbre
hauteur: "21"
largeur: "18"
longueur: 
profondeur: "21"
epaisseur: 
id_wikidata: Q31271612
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 260
priority: 3
id_notice: ra-68-1
image_principale: ra-68-1-1
vues: 8
image_imprime: 
redirect_from:
- ra-68-1
- ark:/87276/a_ra_68_1
id_ark: a_ra_68_1
precisions_biblio:
- clef: balty_les_2021
  note: 'p. 17-20, 161-168, fig. 9, 10 '
- clef: cazes_musee_1999-1
  note: p. 132
- clef: clarac_musee_1841
  note: p. 581
- clef: du_mege_description_1835
  note: n° 216
- clef: du_mege_notice_1828
  note: n° 134
- clef: esperandieu_recueil_1908
  note: n° 1002
- clef: fittschen_katalog_1985
  note: p. 90, n° 79, pl. 95-6
- clef: fittschen_prinzenbildnisse_1999
  note: p. 75-77, pl.
- clef: fittschen_review_1978
  note: p. 147, n° 5
- clef: hausmann_zur_1981
  note: p. 147, n° 5
- clef: joulin_les_1901
  note: n° 284
- clef: rachou_catalogue_1912
  note: n° 68
- clef: roschach_catalogue_1865
  note: n° 68
- clef: roschach_catalogue_1892
  note: n° 68
- clef: rosso_image_2006
  note: p. 467-468, n° 224
- clef: wiggers_caracalla_1971
  note: p. 110

---
Le traitement de ce beau portrait marqué par la mélancolie témoigne de la grande dextérité du sculpteur dans le rendu de la chevelure comme des yeux en amande et de  leurs cernes. Le menton est en retrait, la bouche semble boudeuse, les cheveux, ramenés depuis le haut du crâne jusqu'aux oreilles et vers le front, forment une coiffure assez plate, composée de mèches ondoyantes bien individualisées, en particulier latéralement. Si cet indice capillaire peut encore évoquer les coiffures antonines, on le retrouve cependant sur les portraits de Septime Sévère et du jeune Caracalla.

Une <a href="/images/comp-ra-68-1-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-68-1-1.jpg" alt="Musées du Capitole, Rome / Institut archéologique allemand, Rome"/></span>seconde tête</a>, de même type, est conservée à Rome, dans les musées du Capitole. Ces deux répliques d'un même type iconographique ayant été découvertes en deux endroits différents de l'Empire, on préféra y voir non pas le fils d'un riche citoyen mais bien un prince. Puisque les premières effigies des princes Caracalla et Géta sont connues, seul Pertinax César s'est longtemps posé comme un candidat crédible {% cite fittschen_katalog_1985 fittschen_prinzenbildnisse_1999 -L none -l n° 79, p. 90, pl. 95-96 -l 75-76 %}. Fils de Pertinax, lui-même successeur de Commode, Pertinax fils a tenu, en 193, le rôle de César et de _princeps iuventutis_ moins de cent jours, avant que son père ne soit assassiné, après un règne de seulement trois mois. Il subit lui-même les foudres du colérique Caracalla qui le fit mettre à mort en 212 ou 213. Pertinax fils et sa mère, Flavia Titiana, ne reçurent ni honneur ni titre et ne furent pas représentés sur les monnaies de Rome mais sur certaines frappes d'Alexandrie.

Voir aujourd'hui, dans la belle tête de Chiragan, Pertinax fils paraît moins certain. Un autre candidat semble plausible: C. Fulvius Plautus Hortensianus, le fils de Plautien, le préfet du prétoire, ami de Septime Sévère. Ces deux derniers ont en commun une même origine, l'Afrique (l'un et l'autre étaient vraisemblablement nés tous les deux à Lepcis Magna) voire un éventuel rapport de parenté (?). Ils fiancèrent et marièrent leurs enfants, Caracalla et Plautilla, en 202, tous deux alors agés de quatorze ans. Les années 202-205, durant lesquelles le mariage aurait scellé les deux familles, pourraient permettre de dater la conception de l’effigie du jeune C. Fulvius Plautus. Une inscription de Timgad, qui ne saurait dater que de cette période également, le qualifie de _clarissimus puer_, ce qui renvoie indiscutablement à un adolescent de moins de 17 ans et qui n’a pas encore pris la toge virile. Exilé en 205, avec sa soeur, aux îles Lipari (ou en Sicile, selon les sources), il y fut mis à mort par Caracalla en 212.

L'œuvre occupe une place importante dans la série des portraits du début du III<sup>e</sup> siècle ; elle introduit des caractéristiques stylistiques propres à l'époque sévérienne. Très proches des images de Caracalla et de Géta et inspirés, comme elles, par celles des jeunes princes de la dynastie antonine à laquelle Sévère prétendait se rattacher, ces portraits d’adolescents du « type Capitole – Toulouse » sont aussi ceux de toute une génération.

D'après J.-C. Balty 2020, _Les Portraits Romains : L’époque des Sévères_, Toulouse, p. 161-168.