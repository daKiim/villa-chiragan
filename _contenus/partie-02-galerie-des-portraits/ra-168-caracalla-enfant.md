---
title: Portrait de Caracalla enfant
id_inventaire: Ra 168
donnees_biographiques1: 188 - 217
donnees_biographiques2: Fils de Septime Sévère et Julia Domna, frère de Géta. Empereur
  de 211 à 217
type_oeuvre: "« type arc des Argentiers», ou « premier type de la succession » ; «
  Typus Argentarierbogen » ou « 1. Thronfolgertypus »"
date_creation: 200 - 205
materiau: Marbre de Göktepe 3 (Turquie)
hauteur: "25"
largeur: '17,5'
longueur: 
profondeur: "19"
epaisseur: 
id_wikidata: Q26707731
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 241
priority: 3
id_notice: ra-168
image_principale: ra-168-1
vues: 8
image_imprime: 
redirect_from:
- ra-168
- ark:/87276/a_ra_168
id_ark: a_ra_168
precisions_biblio:
- clef: balty_les_2012
  note: p. 267, fig. 200
- clef: braemer_les_1952
  note: p. 143-148
- clef: cazes_musee_1999-1
  note: p. 130
- clef: esperandieu_recueil_1908
  note: n° 999
- clef: fittschen_bemerkungen_1969
  note: p. 197 à 236
- clef: fittschen_katalog_1985
  note: n° 86 p. 99
- clef: ghisellini_museo_1988
  note: p. 336-338
- clef: heintze_studien_1966
  note: p. 199
- clef: joulin_les_1901
  note: n° 304 E
- clef: rachou_catalogue_1912
  note: n° 168
- clef: rosso_image_2006
  note: p. 475-477, n° 230
- clef: wiggers_caracalla_1971
  note: p. 46, 87

---
Cette tête de Chiragan appartenait peut-être à une statue en pied. À l'instar de l'autre portrait de ce prince (_inv_. [Ra 119](/ra-119-ra-58-c)), cette œuvre est proche de l'effigie visible sur l'arc des Argentiers de Rome. Cependant, le portrait de Chiragan est plus fin, davantage caractéristique de l'adolescence. La chevelure est moins volumineuse et se distingue, notamment, par la mèche effilée située au-dessus de l'œil droit. Les caractéristiques capillaires ainsi que la forme du visage sont comparables au <a href="/images/comp-ra-168-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-168-1.jpg" alt="Buste de Caracalla enfant provenant de Markouna, Algérie, inv. Ma 1173 (MNB 782), musée du Louvre, Marie-Lan Nguyen / Wikimedia Commons CC BY"/></span>portrait du Louvre provenant de Markouna, en Algérie</a>. Ainsi, si le type demeure toujours celui dit de l'« arc des _Argentarii_ », auquel se rattache donc également l'autre portrait de Caracalla enfant découvert à Chiragan (_inv_. [Ra 119](/ra-119-ra-58-c)), il faudrait cependant en abaisser légèrement ici la date de conception et privilégier les années 200-205.

Un troisième portrait est conservé dans les réserves (_inv._ [2000.32.1](/2000-32-1)). Fragmentaire, il montre le fils de Septime Sévère à un âge plus avancé. La découverte de plusieurs représentations de Caracalla sur un même site est tout à fait exceptionnelle.

D'après J.-C. Balty 2020, _Les Portraits Romains : L’époque des Sévères_, Toulouse, p. 140-144.