---
title: Buste de jeune homme
id_inventaire: Ra 73 a
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Vers 120
materiau: Marbre de Göktepe 4 (Turquie)
hauteur: "63"
largeur: "43"
longueur: 
profondeur: "26"
epaisseur: 
id_wikidata: Q24477793
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 80
priority: 3
id_notice: ra-73-a
image_principale: ra-73-a-1
vues: 8
image_imprime: 
redirect_from:
- ra-73-a
- ark:/87276/a_ra_73_a
id_ark: a_ra_73_a
precisions_biblio:
- clef: balty_les_2005
  note: p. 44, fig. 15
- clef: balty_les_2012
  note: p.13-14, fig. 1-3, p.121-127,129-130, fig.46-58
- clef: cazes_musee_1999-1
  note: p. 123
- clef: daltrop_stadtromischen_1958
  note: p. 129
- clef: esperandieu_recueil_1908
  note: p. 89, n° 998
- clef: joulin_les_1901
  note: p. 332, pl. XVII, n° 258
- clef: massendari_haute-garonne_2006-1
  note: p. 243, fig 105
- clef: musee_saint-raymond_image_2011
  note: p. 78-79
- clef: musee_saint-raymond_regard_1995
  note: n° 157

---
Ce buste est l'un des plus beaux portraits d'inconnus de la collection du musée Saint-Raymond. Le type de coiffure s’inspire de celui des représentations de Trajan ; mais les cheveux ont plus de volume et leurs mèches un mouvement plus ondoyant et animé, qui annonce déjà les portraits d’Hadrien.

Quelques éléments permettent d’affiner la datation. Au nombre de ceux-ci, l'absence d’indication de l’iris sur le globe de l’œil, comme de la pupille, précision à l'origine apportée par la polychromie, aujourd'hui disparue. Ce n'est qu'à partir de 130 environ que l'on incisera et que l'on forera ces détails oculaires. Ici, les yeux demeurent lisses. On ne peut donc descendre la date de l’œuvre au-delà de cette limite. Un critère supplémentaire est celui de la forme même du buste : toute représentation du haut de l’abdomen est absente, comme sur un buste d’<a href="/images/comp-ra-73-a-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-73-a-1.jpg" alt="Buste d'Hadrien, British Museum, Carole Raddato / Wikimedia Commons CC BY-SA"/></span>Hadrien du British Museum</a>, datable d’entre 118 et 121, voire de 119 très exactement (troisième consulat de l’empereur). Par conséquent, une date proche de 120 semble bien s’imposer.

On ne peut rattacher un nom à ce portrait de qualité ; cependant, une tête, malheureusement très dégradée, du Palais du Quirinal, à Rome, pourrait représenter le même personnage. Ce jeune garçon n'était donc pas n’importe qui. Bien entendu ce type de portrait héroïsé (car son torse est dénudé) et l'âge de l’individu ne permettent guère d’y voir un haut fonctionnaire impérial, comme ce sera le cas à la fin du II<sup>e</sup> et au III<sup>e</sup> siècle pour plusieurs portraits « privés » de Chiragan. Cependant l’existence même d’une réplique à Rome montre bien les relations étroites existant entre la _villa_ et le « centre du pouvoir ».

D'après J.-C. Balty 2012, _Les portraits romains , 1 : Le siècle des Antonins, 1.2_ (_Sculptures antiques de Chiragan (Martres-Tolosane)_, Toulouse, p. 119-130.