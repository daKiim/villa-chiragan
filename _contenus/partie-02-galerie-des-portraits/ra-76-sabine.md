---
title: Buste de Sabine
id_inventaire: Ra 76
donnees_biographiques1: 85/87 ? - 136/137
donnees_biographiques2: 
type_oeuvre: principal ou « Vatican, Busti 359 »
date_creation: Entre 128 et 137
materiau: Marbre lychnites (île de Paros)
hauteur: "44"
largeur: '34,5'
longueur: ''
profondeur: '40,5'
epaisseur: 
id_wikidata: Q24068193
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 100
priority: 3
id_notice: ra-76
image_principale: ra-76-1
vues: 8
image_imprime: 
redirect_from:
- ra-76
- ark:/87276/a_ra_76
id_ark: a_ra_76
precisions_biblio:
- clef: alexandridis_frauen_2004
  note: p. 184, n° 182 et fig. 3
- clef: balty_les_2012
  note: p. 51, fig. 50, p. 147-149, 152, 158-159, fig. 78-79, 81, 83, 86, 88-89
- clef: braemer_les_1952
  note: p. 145
- clef: carandini_vibia_1969
  note: p. 185, n° 49, pl. XCIV.221
- clef: cazes_musee_1999-1
  note: p. 124
- clef: du_mege_description_1835
  note: p. 113, n° 199
- clef: esperandieu_recueil_1907
  note: p. 86, n° 992
- clef: fittschen_katalog_1985
  note: p. 11 réplique n° 21
- clef: joulin_les_1901
  note: p. 335, n° 278
- clef: musee_saint-raymond_image_2011
  note: p. 124
- clef: musee_saint-raymond_regard_1995
  note: p. 42, n° 4
- clef: musees_departementaux_de_loire-atlantique_tresor_1987
  note: n° 96
- clef: rosso_image_2006
  note: p. 456-457, n° 218
- clef: wegner_hadrian_1956
  note: p. 90 et 130
- clef: wegner_verzeichnis_1984
  note: p. 155

---
Aucune source ne permet d'avancer que ce buste provient de Chiragan. Sa provenance demeure donc, encore pour nous aujourd'hui, absolument inconnue. L'intégrer à ce catalogue est une démarche qui relève davantage d'une tradition intrinsèque au musée, où cette œuvre fut toujours exposée parmi la série des portraits de la _villa_. Probablement l'importance de l'effigie ainsi que l'absence frappante d'image féminine dans cet ensemble prestigieux participèrent-elles à cette intégration de la part des conservateurs successifs.

Brisée dans sa partie inférieure, l'œuvre représente une femme couronnée, vêtue d’un manteau qui recouvrait probablement une tunique, aujourd’hui disparue. Malgré les nombreuses altérations, on reconnaît bien Sabine, l’épouse d’Hadrien, selon le type le plus répandu de son iconographie. Les traits du visage sont réguliers, les lèvres délicatement ourlées et les yeux en amande soulignés par des arcades sourcilières à la ligne pure. La tête est surmontée d’un diadème dont les extrémités s’enfoncent dans la masse ondulée de la chevelure. L’ensemble donne à ce portrait une tonalité atemporelle, proche des représentations des déesses de la Grèce classique. Cette image idéale introduit donc une forte rupture dans l’iconographie des dames de la maison impériale. La coiffure ne présente plus les nattes (postiches ou non), les boucles ou les ondulations reprises au fer que l'on connaissait pour les dames de la famille de Trajan, les _Ulpii_. Au contraire, simplicité et naturel sont ici privilégiés. Le chignon haut en forme de corbeille s'éloigne des coiffures en «nid» (_Nestfrisur_) des portraits officiels de dames de l'époque de Trajan car il n'est plus composé de tresses. Les longues mèches ondulées et torsadées forment un chignon haut, en forme de corbeille. Le buste appartient donc bien au dernier type reconnu pour les portraits de l'impératrice et en représente même, malgré son mauvais état, une très bonne réplique. On connaît aujourd'hui près de trente copies relevant de ce type ; ainsi s'explique le terme allemand d'«Haupttypus» («type principal»), désignant cette série.

Aucun de ces symboles ne livre le sens précis de la dernière image officielle créée du vivant de Sabine. Cependant, ils font de l'impératrice une figure idéale voire sacrée. Ces attributs renvoient aussi à un style rétrospectif et profondément marqué par la Grèce, en pleine conformité avec les orientations de la fin du règne d’Hadrien.

D'après E. Rosso 2012, _Les portraits romains , 1 : Le siècle des Antonins, 1.2_ (_Sculptures antiques de Chiragan (Martres-Tolosane)_, Toulouse, p. 145-161.