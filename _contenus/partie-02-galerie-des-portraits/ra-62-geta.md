---
title: Buste de Géta enfant
id_inventaire: Ra 62
donnees_biographiques1: 198 - 211
donnees_biographiques2: Fils de Septime Sévère et Julia Domna, frère de Caracalla
type_oeuvre: "«Munich-Toulouse»"
date_creation: Entre 202 et 205
materiau: Marbre de Göktepe 3 (Turquie) pour la tête et  marbre d'Altintaş (Turquie)
  pour le buste
hauteur: "58"
largeur: "45"
longueur: ''
profondeur: "27"
epaisseur: 
id_wikidata: Q27096193
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 230
priority: 3
id_notice: ra-62
image_principale: ra-62-1
vues: 8
image_imprime: 
redirect_from:
- ra-62
- ark:/87276/a_ra_62
id_ark: a_ra_62
precisions_biblio:
- clef: balty_les_2012
  note: p. 40-41, fig. 38-40, p. 266, fig.198
- clef: balty_les_2021
  note: p. 16-20, 66-67, 152-159, fig. 8
- clef: balty_style_1983
  note: p. 306, fig. 10, p. 308
- clef: braemer_les_1952
  note: p. 143-147
- clef: budde_jugendbildnisse_1951
  note: pl. 20
- clef: cazes_musee_1999-1
  note: p. 131
- clef: du_mege_description_1835
  note: n° 208
- clef: du_mege_notice_1828
  note: n° 129
- clef: esperandieu_recueil_1908
  note: n° 1011
- clef: fittschen_bemerkungen_1969
  note: p. 197-236
- clef: heintze_studien_1966
  note: p. 195-6, 199
- clef: joulin_les_1901
  note: n° 291 B
- clef: rachou_catalogue_1912
  note: n° 62
- clef: roschach_catalogue_1865
  note: n° 62
- clef: roschach_catalogue_1892
  note: n° 62
- clef: rosso_image_2006
  note: p. 477-478, n° 231
- clef: wiggers_caracalla_1971
  note: p. 97, 112, pl. 26 c-d

---
Cette belle tête enfantine appartient à une série de onze portraits remontant à un original commun («Urbild») qui fut réalisé à Rome en 198 et que l’on désigne comme  « type Munich –Toulouse », ces deux villes en conservant chacune un exemplaire précis et de qualité. La conception de ce type correspond au moment de l’attribution à Géta du titre de _Caesar._ L'enfant avait alors à peine dix ans. Ce portrait sera reproduit par les officines chargées de la distribution des effigies impériales jusqu’en 204 ; on distingue par conséquent quelques légères différences d'une œuvre à une autre, qui visent à préciser l’âge du jeune prince durant ces quelques années où sera diffusé le type iconographique.

Tête et buste n’appartenaient évidemment pas, ici, à la même œuvre : la tête paraît trop grande pour la taille du buste et, d’autre part, le type du piédouche est nettement antérieur à la date du portrait.

Les portraits de Septime Sévère et de ses deux fils, Caracalla et Géta, découverts dans la villa de Chiragan, d’une réelle qualité, sont datables d'une même période, les années 198-204. Mais les divergences sont considérables entre les portraits des deux frères, le modelé des mèches du Géta est assurément plus souple, plus «léché», sans ce vigoureux refouillement du volume qui caractérise justement les effigies de Sévère et de Caracalla ; le dessin même de ces mèches est tout autre aussi et annonce celui des _penne_ (extrémité en forme de petite plume) des portraits du premier tiers du siècle.

D'après J.-C. Balty 2020, _Les Portraits Romains : L’époque des Sévères_, Toulouse, p. 151-159.