---
title: Buste de Trajan
id_inventaire: Ra 117
donnees_biographiques1: 53 - 117
donnees_biographiques2: Empereur de 98 à 117
type_oeuvre: dit « du sacrifice »
date_creation: Entre 108 et 113
materiau: Marbre de Carrare
hauteur: "56"
largeur: "36"
longueur: 
profondeur: "25"
epaisseur: 
id_wikidata: Q24436503
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 70
priority: 3
id_notice: ra-117
image_principale: ra-117-1
vues: 8
image_imprime: 
redirect_from:
- ra-117
- ark:/87276/a_ra_117
id_ark: a_ra_117
precisions_biblio:
- clef: balty_les_2012
  note: p. 65, fig. 60. p. 104-111, fig. 28-39. p. 261, fig.192
- clef: balty_nouveau_1977
  note: n° 108 p. 60
- clef: braemer_les_1952
  note: p. 145, Trajan II
- clef: cazes_musee_1999-1
  note: p. 122
- clef: centro_de_exposiciones_arte_canal_roma_2007
  note: p. 92-93, n° 17
- clef: esperandieu_recueil_1908
  note: p. 64, n° 956
- clef: fittschen_katalog_1985
  note: p. 43, n° 44, réplique 5
- clef: gross_bildnisse_1940
  note: n° 64
- clef: joulin_les_1901
  note: p. 115, pl. XVII, n° 261 d
- clef: mansuelli_galleria_1958
  note: p. 82
- clef: musee_saint-raymond_image_2011
  note: p. 24
- clef: musee_saint-raymond_regard_1995
  note: p. 215
- clef: pugliese_carratelli_enciclopedia_1997
  note: p. 962-965
- clef: rachou_catalogue_1912
  note: p. 59, n° 117
- clef: rosso_image_2006
  note: p. 453-455 n° 216
- clef: sanchez_montes_civilizacion._2006
  note: p. 210

---
Le torse nu était peut-être, à l’origine, en partie revêtu de l’égide (l'armure de la déesse Athéna/Minerve, donnée par Zeus/Jupiter), comme le montrent deux autres bustes, conservés à <a href="/images/comp-ra-117-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-117-1.jpg" alt="Buste de Trajan des thermes de Caracalla à Rome, Ny Carlsberg Glyptotek, Copenhague, Carole Raddato / Wikimedia Commons CC BY-SA"/></span>Copenhague</a> et à Munich. À cette cuirasse, le buste danois ajoute un calice d'acanthe, à sa base. Ces éléments permettraient de comprendre la large lacune arrondie, au niveau de l'épaule gauche, du buste de Chiragan, ainsi que le vide triangulaire très particulier séparant les deux pectoraux.

L’égide apparaît pour la première fois sur les portraits d’Alexandre le Grand et rappelle la filiation divine du souverain. Elle est relativement fréquente dans le monnayage de Trajan, et ce dès les premières émissions monétaires. Elle constitue un témoignage particulièrement éclairant de cette « théologie jovienne (se rapportant à Jupiter) du pouvoir impérial » qui caractérise le règne de l’_Optimus Princeps_ (le meilleur des empereurs), doctrine dont l’écrivain Pline le Jeune se fait l’écho dans son _Panégyrique de Trajan_. Au moment où l’Empire a atteint son extension territoriale maximale, on ne pouvait mieux représenter cette domination universelle de l’empereur.

La chevelure, coupée « au bol » et peignée de l’arrière vers l’avant sur les portraits les plus anciens du _Princeps_, est ici plus complexe et annonce celle des effigies de l’époque d’Hadrien, son successeur. La disposition des mèches situées aux niveaux de la frange et des tempes suit un schéma très spécifique, qui apparaît sur des scènes de la Colonne de Trajan, à Rome, où l'empereur accomplit un sacrifice. C'est l'origine du nom donné à ce nouveau type iconographique. Sans doute est-il un des derniers, sinon le dernier du règne. Attesté, nous l'avons dit, sur la Colonne Trajane, il est donc antérieur à celle-ci, dont la dédicace date du 12 mai 113.

D'après J.-C. Balty 2012, _Les portraits romains , 1 : Le siècle des Antonins, 1.2_ (_Sculptures antiques de Chiragan (Martres-Tolosane)_, Toulouse, p. 102-112.