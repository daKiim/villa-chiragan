---
title: Buste de Trajan
id_inventaire: Ra 58 b
donnees_biographiques1: 53 - 117
donnees_biographiques2: Empereur de 98 à 117
type_oeuvre: dit « des Decennalia »
date_creation: Vers 108
materiau: Marbre de Göktepe 3 (Turquie)
hauteur: "41"
largeur: '26,5'
longueur: ''
profondeur: "24"
epaisseur: 
id_wikidata: Q24302048
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 60
priority: 3
id_notice: ra-58-b
image_principale: ra-58-b-1
vues: 8
image_imprime: 
redirect_from:
- ra-58-b
- ark:/87276/a_ra_58_b
id_ark: a_ra_58_b
precisions_biblio:
- clef: balty_les_2012
  note: p. 22-23, fig.10-11, p. 92-98, 100-101, fig. 12-13, 15-16, 19-21, 24-26, p. 260, fig. 191
- clef: balty_nouveau_1977
  note: n° 109 p. 60
- clef: braemer_les_1952
  note: p. 143-148
- clef: centre_culturel_abbaye_de_daoulas_rome_1993
  note: n° 36.01
- clef: du_mege_description_1835
  note: p. 111, n° 195
- clef: du_mege_description_1844
  note: n° 359-361
- clef: du_mege_notice_1828
  note: p. 63, n° 122
- clef: esperandieu_recueil_1908
  note: p. 64-65, n° 958
- clef: joulin_les_1901
  note: p. 115 et pl. XVII, n° 262 b
- clef: massendari_haute-garonne_2006-1
  note: p. 243, fig. 103
- clef: musee_saint-raymond_cirque_1990-1
  note: n° 11
- clef: musee_saint-raymond_image_2011
  note: p. 20
- clef: musees_departementaux_de_loire-atlantique_tresor_1987
  note: p. 75, n° 94
- clef: rachou_catalogue_1912
  note: p. 42, n° 58 b
- clef: roschach_catalogue_1865
  note: n° 58 b
- clef: roschach_catalogue_1892
  note: p. 32, n° 58 b
- clef: rosso_image_2006
  note: p. 451-453, n° 215

---
Après l’assassinat de l'empereur Domitien, le 18 septembre 96, et donc la fin des Flaviens, le règne de Nerva (96-98) représenta une courte mais nécessaire transition dans l'histoire du Principat. Il inaugura le temps d'une dynastie, celle des Antonins, du nom d’Antonin le Pieux qui, mieux que tout autre, symbolise la stabilité de l’Empire. La nouvelle maison impériale privilégiera le système de l'adoption du meilleur, et non les liens du sang, avant que Marc Aurèle, avant-dernier représentant de cette lignée artificielle, ne revienne, bien malheureusement, sur ce principe. Après Nerva, le pouvoir fut concentré entre les mains de six empereurs : Trajan, Hadrien, Antonin le Pieux, Marc Aurèle (associé un temps à Lucius Verus) et Commode.

Trajan est issu d'une famille de colons italiens installés dans la province de Bétique (actuelle Andalousie) à _Italica_ (actuelle Santiponce, près de Séville). Adopté par Nerva, il épouse Plotine, dont la famille était originaire de Nîmes. L’évolution de son image sculptée est dictée par les grands événements de son règne. Quatre types de portraits sont aujourd'hui reconnus, qui se différencient bien souvent par la seule disposition des mèches de cheveux.

Cet exemplaire est l’œuvre de l'une des meilleures officines de Rome. En témoignent les mèches de la chevelure sur le front et les tempes, rendues avec souplesse, et le modelé sensible du pavillon et du lobe de l’oreille. La large zone piquetée à la base du cou indique qu'à ce buste était associé, à l’origine, un _paludamentum_ (manteau militaire des généraux et des empereurs). La tête, vigoureusement tournée vers la droite, témoigne de l’énergie et de la force morale de l’empereur.

La série dans laquelle s'inscrit cette effigie, indique, pour la première fois, le départ des bras, très clairement marqués par une incision ou une légère dépression qui les distinguent du modelé général du torse. Cette étape décisive vers un rendu plus naturaliste sera poursuivie par l’accentuation des muscles de la poitrine. Ce type iconographique, si novateur, apparaît sur les reliefs de la Colonne Trajane, à Rome, et doit donc être antérieur à 113. On l'a longtemps mis en correspondance avec la célébration des _decennalia_ (dix ans de règne), en 108. Certains ont toutefois préféré relier sa réalisation au second triomphe sur les Daces, en 107.

D'après J.-C. Balty 2012, _Les portraits romains , 1 : Le siècle des Antonins, 1.2_ (_Sculptures antiques de Chiragan (Martres-Tolosane)_, Toulouse, p. 92-101.
