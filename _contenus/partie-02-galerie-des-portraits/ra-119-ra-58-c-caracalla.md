---
title: Buste cuirassé de Caracalla enfant
id_inventaire: Ra 119 - Ra 58 c
donnees_biographiques1: 188 - 217
donnees_biographiques2: Fils de Septime Sévère et Julia Domna, frère de Géta. Empereur
  de 211 à 217
type_oeuvre: "« de l'arc des Argentarii » ou « premier type de la succession » ; «
  Typus Argentarierbogen » ou « 1. Thronfolgertypus »"
date_creation: Entre 195/196 et 205
materiau: Marbre de Göktepe 3 (Turquie)
hauteur: "48"
largeur: "49"
longueur: ''
profondeur: "26"
epaisseur: 
id_wikidata: Q27106354
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 240
priority: 3
id_notice: ra-119-ra-58-c
image_principale: ra-119-ra-58-c-1
vues: 8
image_imprime: 
redirect_from:
- ra-119-ra-58-c
- ark:/87276/a_ra_119_ra_58_c
id_ark: a_ra_119_ra_58_c
precisions_biblio:
- clef: balty_les_2012
  note: p. 266, fig.199
- clef: balty_les_2021
  note: p. 62-64, 125-138
- clef: balty_style_1983
  note: p. 301-314, pl. p. 307-308
- clef: braemer_les_1952
  note: p. 143-148
- clef: bruneau_sculpture_1991
  note: p. 168
- clef: budde_jugendbildnisse_1951
  note: pl. 10, 12
- clef: cazes_musee_1999-1
  note: p. 130
- clef: du_mege_notice_1828
  note: p.110, n°194
- clef: esperandieu_recueil_1908
  note: n° 996, p.62-63, n°954
- clef: fittschen_bemerkungen_1969
  note: p. 198-236, pl. 220
- clef: fittschen_katalog_1985
  note: p. 99, n° 86
- clef: giuliano_museo_1979
  note: p. 332-333
- clef: joulin_les_1901
  note: n° 280 D, pl. XVI, n°260 B
- clef: lebegue_notice_1892
  note: p.32, n°58 c
- clef: massendari_haute-garonne_2006-1
  note: p. 246, fig. 109
- clef: rachou_catalogue_1912
  note: n° 119, p.42, n° 58 c
- clef: roschach_catalogue_1892
  note: p.32, n°58 c
- clef: rosso_image_2006
  note: p. 474-475, n° 229
- clef: wiggers_caracalla_1971
  note: p. 19, 22, 46, 87, 97, pl. 3b

---
Le buste, qui fut découvert lors des fouilles de 1826, n'a été que récemment rapproché de la tête, mise au jour plus de soixante plus tard, en 1890. Les deux parties furent solidarisées en 2015. La différence de couleur trahit un contexte d'enfouissement différent. Comme tous les portraits mis au jour au cours des dernières campagnes de fouilles (1890-1891 et 1897), tête et haut du buste, en excellent état de conservation, n’ont pas été restaurés. Visage et cou ont ainsi gardé le poli qui leur avait été donné dans l’atelier à la fin du travail.

L’extraordinaire poli du visage, contrastant avec la masse des cheveux bouclés refendue de longs canaux de trépan, crée des effets plastiques de clair obscur. L’épiderme du marbre atteint ici un brillant de porcelaine, que n’altèrent que de légères griffures dessinant les sourcils.

Lucius Septimius Bassianus, dit Caracalla, fils de l'Africain Septime Sévère et de la syrienne Julia Domna, est né à Lyon en 188. Son portrait est ici très proche de celui de l'arc des Argentiers, à Rome, un monument érigé en bordure du Tibre, à l'emplacement du débouché du _vicus Jugarius_ sur la place du _Forum Boarium_. L'arc avait été offert à la famille impériale par les banquiers et les marchands de bestiaux de cette importante aire de transactions commerciales.

On voit encore aujourd'hui, sur le panneau interne de cette construction, du côté oriental, le couple impérial sacrifiant. Septime Sévère et son épouse Julia Domna étaient à l'origine accompagnés de Géta, élevé au rang de César, mais son effigie fut bûchée, sur ordre de son frère, Caracalla, qui l'assassina (fin de l'année 211) et tenta d'en faire disparaître le souvenir (_damnatio memoriae_). Faisant face à ce relief, <a href="/images/comp-ra-119-ra-58-c-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-119-ra-58-c-1.jpg" alt="Représentation de Caracalla sur l'arc des Argentiers à Rome, Diletta Menghinello / Wikimedia Commons CC BY"/></span>un second panneau</a>, sur la pile occidentale, montrait à l'origine Caracalla accompagné de son épouse, Plautille, et de son beau-père, Plautien. Brutalement éliminés, sur ordre du fils de Septime, il en fut de même de leurs portraits.

Il est possible de faire remonter la date de création du type dit «des Argentiers» à 198, alors que Caracalla avait été élevé au rang d'Auguste. Il avait 8 ans. On connaît aujourd'hui une cinquantaine d'exemplaires similaires. Ces nombreuses images du jeune prince ont été conçues sur une très courte période, probablement à l’occasion de l’adoption auto-proclamée de leur père, Septime Sévère, comme fils de Marc Aurèle et frère de Commode, en 195, et du passage fictif de son fils aîné Bassianus dans la _gens_ Aurelia sous le nom de M. Aurelius Antoninus, en 196. En 205, apparaitra une nouvelle image qui commémorera le consulat commun des deux frères. La diffusion de ce premier type va donc de pair avec celle des deux premiers types (« Adoption » et « Sérapis») de Septime Sévère, signe caractéristique de la volonté d’affirmer l’apparition d’une nouvelle dynastie. Parmi les exemplaires les plus proches de ce portrait de Caracalla de Chiragan, on distingue celui qui fut découvert à Rome dans la Maison des Vestales, aujourd'hui conservé au <a href="/images/comp-ra-119-ra-58-c-2.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-119-ra-58-c-2.jpg" alt="Buste de Caracalla enfant provenant de la maison des Vestales du forum romain/Jastrow Wikimedia Commons Public Domain"/></span>Musée des Thermes</a> ou bien encore ceux du Musée national de Naples et de Mantoue.

La cuirasse « musclée » (« Muskelpanzer »), aux épaulières non décorées, comporte à la taille une large ceinture nouée (_cingulum_). Cet élément, que l’on retrouve sur nombre de statues cuirassées et ce dès le règne de Trajan, figure assez rarement et beaucoup plus tardivement sur les bustes, ceux-ci ne descendant pas assez bas, jusqu’à la fin du II<sup>e</sup> siècle, pour qu’on pût l’y représenter. La cuirasse présente, par ailleurs, à l’échancrure du bras droit, une série de courtes languettes arrondies, au rebord souligné par une fine incision, rappelant celles du buste de Marc Aurèle de Chiragan (inv. [Ra 61 b](/ra-61-b)). Cet élément particulier de la cuirasse n’apparaît qu’assez rarement et tardivement sur les bustes d’époque romaine. On ne le retrouve que très rarement aussi, et tout aussi tardivement, sur les statues et sur les reliefs. Cet élément supplémentaire semble réservé aux cadres supérieurs de l’armée, aux officiers et à l’empereur, comme le montrent différents documents et monuments funéraires des provinces du Norique et de Pannonie. Les statues les plus tardives ne laissent aucun doute sur l’identification et la fonction de cette partie de la cuirasse, que l’on imaginera faite de cuir souple et parfois renforcée de plaques métalliques ornées.

D'après J.-C. Balty 2020, _Les Portraits Romains : L’époque des Sévères_, Toulouse, p. 125-138.