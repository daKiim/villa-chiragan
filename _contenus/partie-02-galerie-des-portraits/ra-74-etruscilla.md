---
title: Portrait d'Etruscilla (?)
id_inventaire: Ra 74
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Milieu du III<sup>e</sup> siècle
materiau: Marbre de Göktepe 4 (Turquie)
hauteur: '64,5'
largeur: "64"
longueur: 
profondeur: "29"
epaisseur: 
id_wikidata: Q26703756
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 4
order: 400
priority: 3
id_notice: ra-74
image_principale: ra-74-1
vues: 8
image_imprime: 
redirect_from:
- ra-74
- ark:/87276/a_ra_74
id_ark: a_ra_74
precisions_biblio:
- clef: braemer_culte_1999
  note: p. 53
- clef: cazes_musee_1999-1
  note: p. 142
- clef: du_mege_notice_1828
  note: p. 69, n° 133
- clef: esperandieu_recueil_1908
  note: p. 86-87, n° 993
- clef: felletti_maj_iconografia_1958
  note: n° 328, p. 240
- clef: joulin_les_1901
  note: p. 339-340, pl. XXV, n° 314
- clef: massendari_haute-garonne_2006-1
  note: p. 246, fig 110
- clef: musee_saint-raymond_regard_1995
  note: p. 155, n° 110
- clef: rosso_image_2006
  note: p. 484-486, n° 235
- clef: saletti_ritratti_1967
  note: p. 39, n° 29
- clef: wegner_verzeichnis_1979
  note: p. 134
- clef: wiggers_caracalla_1971
  note: p. 129

---
Il s'agit ici de l'un des plus grands portraits féminins connus de l'art romain. La main droite retient un pan du grand manteau qui passe sur l'épaule. À l'arrière de la tête, une natte plate remonte vers le sommet du crâne mais la partie basse de ce chignon était sculptée à part et fixée par collage. Le type est nommé, selon la terminologie allemande, «Halbkörperbüste» soit «buste demi-corps», par conséquent coupé au niveau de la taille. Ce procédé demeure rare dans la sculpture romaine et n'apparaît pas avant le règne de Gordien III (238-244), empereur dont une effigie du musée du Louvre présente le même type de découpe.

La tête pourrait être rapprochée d'un portrait conservé à Rome, au palais des Conservateurs (_inv_. 2689), qui semble représenter le même personnage, sans aucun doute membre de la famille impériale. Coiffure et profil se retrouvent sur les <a href="/images/comp-ra-74-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-74-1.jpg" alt="Sesterce d'Etruscilla, Rome 250, inv. 2000.23.508 musée Saint-Raymond Toulouse, Daniel Martin CC BY-SA"/></span>monnaies de l'impératrice Etruscilla</a>, femme de Trajan Dèce (249-251), candidate aujourd'hui privilégiée pour ce portrait, au détriment de Salonine, épouse de Gallien, ou de Plautille, mariée à Caracalla, antérieurement proposées.

D'après E. Rosso 2006, _L'image de l'empereur en Gaule romaine, portraits et inscriptions_, p. 484-486.