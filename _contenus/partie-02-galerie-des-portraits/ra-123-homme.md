---
title: Buste d'homme héroïsé
id_inventaire: Ra 123
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Vers 230 - 240
materiau: Marbre de Göktepe 3 (Turquie)
hauteur: '61,5'
largeur: "53"
longueur: 
profondeur: "29"
epaisseur: 
id_wikidata: Q27106491
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 4
order: 311
priority: 3
id_notice: ra-123
image_principale: ra-123-1
vues: 8
image_imprime: 
redirect_from:
- ra-123
- ark:/87276/a_ra_123
id_ark: a_ra_123
precisions_biblio:
- clef: balty_les_2012
  note: p. 66, fig. 61, p. 269, fig. 205
- clef: cazes_musee_1999-1
  note: p. 138
- clef: de_lachenal_museo_1986
  note: p. 48-53
- clef: esperandieu_recueil_1908
  note: n° 972
- clef: joulin_les_1901
  note: n° 269 D
- clef: massendari_haute-garonne_2006-1
  note: p. 249, fig. 117
- clef: musee_saint-raymond_regard_1995
  note: p. 159, n° 114
- clef: perrot_rapport_1891
  note: ''
- clef: rachou_catalogue_1912
  note: n° 123

---
Après l'assassinat de Sévère Alexandre, le III<sup>e</sup> siècle sera marqué par une guerre civile presque continue. Des généraux sont alors élevés à la tête de l'Empire par les armées. Aucun de ces empereurs ne règne longtemps et finit par être assassiné par son successeur. Cette période fut nommée « Anarchie militaire ».

Cette œuvre rappelle le style adopté pour les portraits sculptés des empereurs militaires Maximin le Thrace (235-238) et Gordien I (238). La chevelure aux mèches à la fois très courtes et souples caractérise elle aussi l'art du portrait de cette époque. Le visage est calme et retenu. La poitrine dénudée occupe une grande place, les plis du manteau sont profondément creusés et le regard est expressif grâce à l'iris gravé et aux pupilles marquées par un point double.

On distingue, au-dessus et à l'arrière de l'oreille gauche ainsi que sur le côté droit du crâne des perforations. On remarque également les étranges configurations de l'hélix, de l'anthélix et de l'orifice, plus encore au niveau de l'organe de droite où une étrange cavité oblongue apparaît, que dans celui de gauche. Autant d'anomalies qui témoignent des hésitations voire d'une retaille, au moins en partie, du portrait. 

P. Capus