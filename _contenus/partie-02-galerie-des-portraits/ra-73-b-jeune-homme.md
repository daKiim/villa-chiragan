---
title: Buste de jeune homme
id_inventaire: Ra 73 b
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Entre 120 et 130
materiau: Marbre de Göktepe 4 (Turquie)
hauteur: "66"
largeur: "45"
longueur: ''
profondeur: "28"
epaisseur: 
id_wikidata: Q24478254
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 85
priority: 3
id_notice: ra-73-b
image_principale: ra-73-b-1
vues: 8
image_imprime: 
redirect_from:
- ra-73-b
- ark:/87276/a_ra_73_b
id_ark: a_ra_73_b
precisions_biblio:
- clef: balty_les_2005
  note: p. 44, fig. 15
- clef: balty_les_2012
  note: p. 15, fig.4-5, p.133-136, 139, 141, fig. 59-63, 69-71
- clef: cazes_musee_1999-1
  note: p. 123
- clef: du_mege_description_1835
  note: n° 189 ou 190
- clef: du_mege_notice_1828
  note: n° 118
- clef: joulin_les_1901
  note: n° 265
- clef: massendari_haute-garonne_2006-1
  note: p. 243, fig. 104
- clef: musee_saint-raymond_image_2011
  note: p. 80
- clef: rachou_catalogue_1912
  note: n° 73 b
- clef: roschach_catalogue_1892
  note: n° 73 a-b

---
Le visage et la coiffure de ce jeune homme témoignent d’un certain « mimétisme » avec les premiers portraits d’Hadrien. Du reste, c'est le buste dans son ensemble qui correspond chronologiquement au début du règne d’Hadrien. Le piédouche (base) est dorénavant orné de volutes. Une mode apparaît : le port de la barbe. Le jeune homme la porte en collier, comme l’empereur Hadrien, dès son premier type iconographique. Ainsi peut-on rapprocher ce visage de l'un des portraits visibles sur l'arc de Bénévent, en Campanie, au niveau des parties hautes du monument, sur l’attique, où l’on reconnaît généralement Hadrien (les reliefs auraient été achevés après la mort de Trajan, au tout début du règne de son successeur).

Le buste est nu, tel un héros de la mythologie, à l'image de plusieurs autres œuvres contemporaines figurant des jeunes gens. La chevelure est plus volumineuse que celle du buste masculin [_inv_. Ra 73a](/ra-73-a), sans atteindre pour autant l’épaisseur de celles des différents types iconographiques du successeur de Trajan, ni avoir leur mouvement si caractéristique de vagues de l’arrière vers l’avant et leur enroulement « baroque ». Nous sommes donc là à un moment charnière du début du règne d'Hadrien :  nouvelle forme de buste et de piédouche, nouvelle façon de donner vie et éclat au regard des personnages portraiturés et nouvelle pilosité faciale.

D'après J.-C. Balty 2012, _Les portraits romains , 1 : Le siècle des Antonins, 1.2_ (_Sculptures antiques de Chiragan (Martres-Tolosane)_, Toulouse, p. 131-143.