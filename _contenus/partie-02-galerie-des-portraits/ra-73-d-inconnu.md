---
title: Portrait d'un inconnu
id_inventaire: Ra 73 d
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Vers 215 - 220
materiau: Marbre de Dokimium (Turquie)
hauteur: "34"
largeur: "22"
longueur: 
profondeur: "24"
epaisseur: 
id_wikidata: Q28797775
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 301
priority: 3
id_notice: ra-73-d
image_principale: ra-73-d-1
vues: 8
image_imprime: 
redirect_from:
- ra-73-d
- ark:/87276/a_ra_73_d
id_ark: a_ra_73_d
precisions_biblio:
- clef: balty_les_2021
  note: p. 27-33, 233-237, fig. 19, 22
- clef: cazes_musee_1999-1
  note: p. 139
- clef: esperandieu_recueil_1908
  note: n° 1021
- clef: rachou_catalogue_1912
  note: n° 73 d

---
Ce très beau portrait d’un homme relativement jeune, dans la pleine force de l’âge, rejoint ici trois autres têtes (inv. [Ra 73 g](/ra-73-g), [Ra 73 f](/ra-73-f) et Ra 160) dont les traits et le caractère tranchent sur les têtes et bustes de personnes nettement plus âgées, sans que l’on puisse malheureusement déterminer la fonction qui était la leur et/ou la raison d’être de leur image dans les salles de représentation du domaine de Chiragan. Ils appartenaient à la même génération que les fils de Septime Sévère et furent en fonction durant les toutes dernières années de ce règne et durant le règne de Caracalla, répondant selon toute vraisemblance à ce renouvellement des cadres de l’administration impériale qui est une des caractéristiques essentielles de ce moment.

La découpe du bouchon d’encastrement au départ de l’épaule gauche paraît indiquer que cette tête appartenait à une statue cuirassée ; à moins qu’il ne s’agisse d’un buste dont le torse aurait été réalisé dans un marbre différent. On songerait alors volontiers à une autre œuvre de Chiragan: le magnifique buste en pavonazzetto, erronément associé à une tête conçue dans un marbre différent (inv. [Ra 121](/ra-121)).

Les mèches de la chevelure, relativement courtes et de peu d’épaisseur, mais légèrement ondulées et « bifides » ou « trifides » à la pointe — annonciatrices, en cela, de la coiffure « _a penna_ » des portraits de Sévère Alexandre —, sont peignées de l’arrière et du vertex vers l’avant ; elles viennent mourir en une courte frange, assez rectiligne, qui dégage largement le front.

D'après J.-C. Balty 2020, _Les Portraits Romains : L’époque des Sévères_, Toulouse, p. 233-237.