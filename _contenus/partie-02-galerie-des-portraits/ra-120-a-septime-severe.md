---
title: Tête de Septime Sévère
id_inventaire: Ra 120 a
donnees_biographiques1: 145 - 211
donnees_biographiques2: Empereur de 193 à 211
type_oeuvre: dit « de l'avènement »
date_creation: Entre 193 et 195
materiau: Marbre de Göktepe 3 (Turquie)
hauteur: "34"
largeur: "24"
longueur: 
profondeur: "24"
epaisseur: 
id_wikidata: Q26707669
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 200
priority: 3
id_notice: ra-120-a
image_principale: ra-120-a-1
vues: 8
image_imprime: 
redirect_from:
- ra-120-a
- ark:/87276/a_ra_120_a
id_ark: a_ra_120_a
precisions_biblio:
- clef: balty_essai_1966
  note: p. 38, n° 9
- clef: balty_les_1964
  note: p. 60-61, n° 7
- clef: balty_les_2012
  note: p. 66, fig. 62, p. 264, fig.195
- clef: balty_les_2021
  note: p. 61, 85-93
- clef: braemer_les_1952
  note: p. 145
- clef: bulletin_1936
  note: p. 831
- clef: cazes_musee_1999-1
  note: p. 128
- clef: esperandieu_recueil_1907
  note: p. 68, n° 963
- clef: felletti_maj_museo_1953
  note: p. 128
- clef: heintze_studien_1966
  note: p. 198, n° 42, n° 3
- clef: joulin_les_1901
  note: p. 337, pl. XXII, n° 294 D
- clef: mccann_portraits_1968
  note: p. 135-136, n° 14, pl. XXXIII a-b
- clef: musee_saint-raymond_regard_1995
  note: p. 173
- clef: poulsen_portratstudien_1928
  note: p. 27, n° 1
- clef: rachou_catalogue_1912
  note: p. 60, n° 120
- clef: rosso_image_2006
  note: p. 468-470, n° 225
- clef: saastamoinen_afrikan_2009
  note: p. 38
- clef: soechting_portrats_1972
  note: p. 146, n° 23, pl. 3 c-d

---
Sénateur d'origine africaine, né à Leptis Magna (en Tripolitaine, actuelle Libye), Septime Sévère devient gouverneur de la province de Pannonie supérieure, au cœur de l'Europe. Suite à l'assassinat de l'empereur Commode, puis de celui de Pertinax, il est fait empereur par les légions du Danube. Il s'appuie sur ses armées et entreprend une marche sur Rome afin d'être légitimé et d'éliminer ses principaux rivaux.

Cette tête de grande qualité, conçue dans un atelier officiel de Rome, date de cette accession au pouvoir, en 193. Cette année fut riche en rebondissements d'ordre politique. Le 28 mars, Pertinax fut assassiné, après seulement quatre-vingt-sept jours de règne. Cet événement ouvre une période de troubles, analogue à celle que l’Empire avait connue en 69, après la liquidation de Néron. À Rome même, le jour de la mort de Pertinax, Didius Julianus fut proclamé empereur par les prétoriens et par le Sénat ; mais, dès le 9 avril, les légions de Pannonie supérieure élevèrent le gouverneur de la province, Septime Sévère, à la dignité impériale et, deux mois plus tard, ce dernier était à Rome où le Sénat avait fini par le reconnaître et où Didius Julianus venait d’être assassiné. En Orient, le gouverneur de Syrie, Pescennius Niger, avait, lui aussi, reçu de ses troupes le titre d’_Augustus_, dès avril 193, et s’était mis en route vers Rome. Sévère se porta à sa rencontre, le défit près de Cyzique (sur la côte méridionale de la mer de Marmara, ancienne Propontide) puis revint à Rome où l’attendait l’annonce d’une nouvelle rébellion, celle de Clodius Albinus, le gouverneur de Bretagne, qu’il avait cependant désigné comme Caesar en avril 193 et avec qui il avait partagé le consulat en 194. Déclaré ennemi public (_hostis_) en décembre 195, Albinus fut battu à Lyon en février 197 et tué dans sa fuite.

Ce portrait de Chiragan correspond au «type I» des portraits de l'empereur. Septime Sévère avait alors 38 ans : Coiffure et barbe courtes, trois mèches rondes sur le front, regard dirigé vers le haut et rotation de la tête vers la droite. La tête, de grande qualité, appartient à un groupe de cinq portraits trahissant, jusque dans les moindres détails, l’existence d’un original commun et respectivement conservés à Toronto (Royal Ontario Museum, inv. 933.27.4), Toulouse (Musée Saint-Raymond, inv. Ra 120 a), Saint-Pétersbourg (Musée de l’Ermitage, A 3184), Londres, Bonhams Antiquities (vente du 3 avril 2014) et Melbourne (National Gallery of Victoria, inv. 1490/56). L’exemplaire de <a href="/images/comp-ra-120-a-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-120-a-1.jpg" alt="Septime Sévère, Courtesy of the Royal Ontario Museum, Toronto, Brian Boyle"/></span>Toronto, provenant d’Ostie</a>, est le plus complètement conservé. À ce groupe de base de cinq portraits, il convient sans doute d’associer une tête colossale d’Istanbul (Musée archéologique, inv. 46), couronnée de chêne, ainsi qu’une tête de Tyr, aujourd’hui au <a href="/images/comp-ra-120-a-2.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-120-a-2.jpg" alt="Portrait de Septime Sévère, musée de Beyrouth"/></span>musée de Beyrouth (Musée national, inv. DGA 13204)</a> couronnée de laurier . Les différences observées sur ces deux derniers portraits s’expliquent par les pratiques différentes des ateliers orientaux mais aussi par les difficultés que dut rencontrer la diffusion de cette première image premier de l’empereur dans les provinces.

Sur ces portraits du premier type iconographique de Septime Sévère, les cheveux sont assez nettement plus courts et plus près du crâne que sur les deux types suivants, où de longues mèches bouffantes sur les tempes, un front plus lisse et des arcades sourcilières moins marquées donnent un caractère très différent au personnage ; les oreilles sont découvertes. C’est bien l’empereur soldat, en lutte contre ses concurrents à l’Empire, qui est ici figuré ; les types suivants auront une tout autre signification. Par rapport à eux également, on notera une direction et une acuité différentes du regard, qui en font davantage un homme d’action : porté vers la droite sur ces effigies de l’avènement, il n’est pas encore tourné vers le haut comme sur certains exemplaires des « type de l’adoption » et « type Sérapis » où se perçoit cette affirmation d’un contact maintenu avec les dieux.

D'après J.-C. Balty 2020, _Les Portraits Romains : L’époque des Sévères_, Toulouse, p. 85-93.