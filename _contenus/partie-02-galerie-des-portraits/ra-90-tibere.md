---
title: Tête de Tibère couronné de chêne
id_inventaire: Ra 90
donnees_biographiques1: 42 avant n. è. - 37
donnees_biographiques2: Empereur de 14 à 37
type_oeuvre: Inversion (des mèches de la frange) du type « Berlin-Naples-Sorrente »
date_creation: Dernière décennie du règne (26/27-31 de n. è.) ?
materiau: Marbre lychnites (île de Paros)
hauteur: "34"
largeur: "18"
longueur: ''
profondeur: '22,2'
epaisseur: 
id_wikidata: Q26707653
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 1
order: 40
priority: 3
id_notice: ra-90
image_principale: ra-90-1
vues: 8
image_imprime: 
redirect_from:
- ra-90
- ark:/87276/a_ra_90
id_ark: a_ra_90
precisions_biblio:
- clef: balty_les_2005
  note: p. 164-179, n° 6
- clef: cazes_musee_1999-1
  note: p. 118, fig. 120
- clef: centro_de_exposiciones_arte_canal_roma_2007
  note: p. 100-101, n° 21
- clef: esperandieu_recueil_1908
  note: p. 95-96, n° 1012
- clef: felletti_maj_claudio_1959
  note: p. 706
- clef: fittschen_katalog_1985
  note: p. 15, n° 13
- clef: gascou_presence_1996
  note: p. 47-49, fig. 28-30
- clef: hausmann_bemerkungen_1985
  note: 'p. 222 exemplaire "u" '
- clef: hausmann_redeat_1988
  note: 'p. 332 exemplaire "q" '
- clef: leon_alonso_retratos_2000
  note: ''
- clef: rachou_catalogue_1912
  note: p. 53, n° 90
- clef: rosso_image_2006
  note: p. 443-445, n° 211
- clef: salviat_a_1980
  note: p. 28, 30, fig. p. 27
- clef: terrer_nouvel_2003
  note: p. 72 (à propos du n° 143)

---
Mentionnée dans un catalogue de 1912, où elle fut associée aux œuvres issues des fouilles de Chiragan des années 1826 et 1828, cette tête n'apparaissait nulle part auparavant ; Léon Joulin lui-même n'en fit pas mention, ni ne l'illustra, dans son ouvrage, publié en 1901. Par conséquent, et dans l'attente d'un témoignage probant, on ne peut que rester prudent quant au lien de ce portrait de Tibère avec la _villa_.

Né en 42 avant n. è., de l'union de Livie et Tiberius Claudius Nero, Tibère appartient à la _gens Claudia_, famille au lignage prestigieux. Il a trois ans lorsque sa mère se remarie à Octave, héritier de Jules César et membre du second _Triumvirat_, aux côtés de Marc Antoine et Lépide. Suite aux décès de tous ses successeurs présomptifs, celui qui, désormais, porte le nom d'Auguste, se voit contraint d'adopter le fils de Livie, associant donc le nom des _Claudii_ à la _gens_ des _Iulii_.

Les caractéristiques retenues pour représenter cet héritier, monté sur le trône à l'âge de 55 ans, semblent entrer en concordance avec ce qui pouvait, psychologiquement et politiquement, l'opposer à son beau-père. Les portraits restent en effet dominés par une description sans concession, objective et fort éloignée de l'idéal grec des effigies de son beau-père. Cependant, l'agencement des mèches frontales, dérivé du type «Prima Porta» d'Auguste, trahit le besoin de s'approprier certains détails des images du règne précédent, preuve du loyalisme du souverain envers ce régime et témoignage de la perpétuité dynastique. La tête toulousaine manifeste une réelle originalité en ce sens : si les mèches y reprennent exactement celles des principales effigies du type dit «Berlin-Naples-Sorrente», elles sont inversées sans que l'on ne puisse donner une réelle explication à cette disposition.

Le personnage, on le sait, est une personnalité complexe. Fuyant l'ambiance fielleuse de la capitale en 27, il s'exile, jusqu'à sa mort, sur l'île de Capri. La tête de Chiragan dépend d'un prototype créé très tardivement ; Tibère avait alors entre 65 et 75 ans et ne vivait donc plus à Rome depuis longtemps. Cependant, il ne s'était encore jamais fait représenter ainsi, couronné de chêne. Le prince aurait refusé que l'on accroche cette distinction dans le vestibule de son palais. L'emblème, qui évoque la clémence de l’empereur et le rapproche de Jupiter, est également associé au titre de «Père de la Patrie» (_Pater Patriae_), un honneur récusé par Tibère lui-même {% cite tacite_annales_nodate suetone_tibere_nodate dion_cassius_histoire_nodate -L none -l 2, 87 -L none -l 26,4 et 67,2 -L none -l 57, 8, 1 %}.

Une partie de la couronne de la tête du musée Saint-Raymond fut probablement brisée dès l'origine, peut-être lors de l'exécution des feuilles de chêne, opération très délicate pour le sculpteur. Ainsi s'explique la grande mortaise, située sur le côté droit, qui correspond à une réparation et à l'insertion d'une prothèse. C'est un pareil accident qui explique le creusement du <a href="/images/comp-ra-90-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-90-1.jpg" alt="Portrait de Tibère, Musée archéologique national de Tarragone, Francis Raher / Wikimedia Commons CC BY"/></span>portrait de l'empereur découvert à Tarragone</a> {% cite leon_alonso_retratos_2000 -l 81-83 %}.

D'après J.-C. Balty 2005, _Les portraits romains , 1 : Époque julio-claudienne, 1.1_ (_Sculptures antiques de Chiragan (Martres-Tolosane)_, Toulouse, p. 163-179.