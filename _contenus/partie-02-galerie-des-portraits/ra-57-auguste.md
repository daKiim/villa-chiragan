---
title: Buste d'Auguste couronné de chêne
id_inventaire: Ra 57
donnees_biographiques1: 63 avant n. è. - 14 de n. è.
donnees_biographiques2: Empereur de 27 avant n. è. à 14 de n. è.
type_oeuvre: " dit « Prima Porta »"
date_creation: Première moitié du I<sup>er</sup> siècle
materiau: Marbre lychnites (île de Paros)
hauteur: "51"
largeur: "34"
longueur: ''
profondeur: "25"
epaisseur: 
id_wikidata: Q24580187
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 1
order: 10
priority: 3
id_notice: ra-57
image_principale: ra-57-1
vues: 8
image_imprime: 
redirect_from:
- ra-57
- ark:/87276/a_ra_57
id_ark: a_ra_57
precisions_biblio:
- clef: salviat_a_1980
  note: p. 22-25
- clef: rosso_image_2006
  note: 'p. 448-450, n° 213 '
- clef: rachou_catalogue_1912
  note: p. 41, n° 57
- clef: musee_saint-raymond_essentiel_2011
  note: p. 40-41
- clef: musee_saint-raymond_regard_1995
  note: p. 39, n° 1
- clef: musée_saint-raymond_cirque_1990-1
  note: p. 52, n° 9
- clef: musée_archeologique_henri-prades_cirque_1990
  note: p. 364, n° 96
- clef: montini_il_1938
  note: p. 51-52, p. 93, n° 118
- clef: massendari_haute-garonne_2006-1
  note: 'ill. de couv.,  p. 242, fig. 100'
- clef: joulin_les_1901
  note: p. 115, pl. XVIII, n° 255 b, 256 b
- clef: johansen_portrait_1976
  note: p. 57, n° 23
- clef: hertel_untersuchungen_1982
  note: p. 66, n° 127, p. 271
- clef: hausmann_zur_1981
  note: p. 583-584, n° 261
- clef: gonse_les_1904
  note: p. 330 et fig.
- clef: frigerio_augusto_1938
  note: p. 49, fig. 25
- clef: esperandieu_recueil_1908
  note: p. 60, n° 948
- clef: curtius_ikonographische_1935
  note: p. 281
- clef: cazes_musee_1999-1
  note: p. 118 et fig.
- clef: braemer_culte_1999
  note: p. 54, fig. 3
- clef: bramer_les_1952
  note: p. 143-148
- clef: boschung_gens_2002
  note: n° 45.1, p. 128, 131, pl. 95.1
- clef: boschung_bildnisse_1993
  note: p. 44, 86, 102, n° 61-62, 138, 142, 158, 163, 168, 494, cat. n° 199 p. 190, pl. 166.1-4, 223.2
- clef: bernoulli_romische_1882
  note: 'II : Die Bildnisse der römischen Kaiser. 3 : Von Pertinax bis Theodosius, p. 39, n° 61'
- clef: bergmann_chiragan_1999
  note: p. 28, n° 142
- clef: benoit_sanctuaire_1952
  note: p. 38
- clef: balty_les_2005
  note: p. 45-47, fig. 16-17, p. 75-98, fig. p. 74, 76, 78, 81 n° 7, 83 n° 10, 86 n° 16, 89 n° 21, 90, 92-93, 96 n° 26
- clef: du_mege_notice_1828
  note: p. 60-61, nº120
- clef: du_mege_notice_1830
  note: p. 396
- clef: du_mege_description_1835
  note: p. 98-99, nº 188
- clef: roschach_catalogue_1865
  note: p. 32, nº 57

---
Petit-neveu de Jules César, Octave est le sauveur de la République et le fondateur de la dynastie des Julio-Claudiens. Le titre d'Auguste (épithète renvoyant à la sphère religieuse et qui l'élève au-dessus des autres hommes) lui fut décerné le 16 janvier 27 avant n. è. par le Sénat. _Augustus_ devint rapidement son prénom et celui de tous ses successeurs.

Le schéma des mèches frontales est identique à celui de la tête, associée à un corps cuirassé et aux pieds nus, de la <a href="/images/comp-ra-57-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-57-1.jpg" alt="Auguste de Prima Porta, Musées du Vatican, Michal Osmenda / Wikimedia Commons CC BY"/></span>statue découverte à Prima Porta</a>, aux portes de Rome, dans la _villa_ de Livie, épouse d'Auguste. Cette œuvre, aujourd'hui conservée dans les Musées du Vatican, montre un visage qui, loin de la physionomie de l'empereur, s'inspirait de celui du Doryphore (« Porteur de lance »), sculpture en bronze, connue par quelques répliques romaines en marbre, exécutée vers 440 avant n. è. par Polyclète, l'un des grands maîtres de l'époque classique. L'art de la Grèce du V<sup>e</sup> siècle avant n. è. est en effet une référence constante dans l'art augustéen. L'idéal grec permet notamment de donner une image immuable de l'empereur, éternellement jeune, reflet de la stabilité de l’Empire. Le type iconographique de la tête de la statue de Prima Porta est associé à un type statuaire dont la portée symbolique était lourde de sens : des pieds nus, qui renvoyaient sans doute à un contexte religieux, ou encore d'héroïsation, et un décor sur la cuirasse montrant la restitution, en 20 avant n. è., des enseignes prises par les Parthes aux légions romaines de Crassus, lors de la bataille de Carrhes (actuelle Turquie), en 53 avant n. è. Un seul type de portrait, récurrent et stable, pouvait, on le sait, être associé à des types de statues différents : cuirassée, en toge (tête nue ou voilée), torse nu avec manteau enroulé à la taille, équestre ou encore assise. Le portrait d'Auguste, identique, donc, à celui de la statue de Prima Porta conservée au  Vatican, montre une organisation des mèches, en particulier au-dessus du front, qui rompt avec le schéma qui avait été adopté pour les représentations antérieures de l'Empereur. On y distingue un agencement, strictement suivi durant tout le règne, déterminé par de grandes mèches qui, depuis la droite, dessinent une fourche et deux « pinces de crabe ». La coiffure s'est aplatie et s'éloigne du gonflement caractéristique visible dans les portraits de jeunesse, conçus durant la guerre civile. Désormais, le visage, marqué du sceau de l'équilibre et de la plénitude de l'art grec classique, répond à la chevelure disciplinée, l'ensemble formant une métaphore de la pacification de Rome par le vengeur de la mort de César. Le type, appelé à devenir immuable, permet d'identifier sans réelle ambigüité le _primus inter pares_ (premier parmi ses égaux).
C'est à partir de l'attribution du titre d'_Augustus_ et de sa prise de pouvoir sur la République (sans qu'il ne soit réellement question d'un régime de type monarchique), en 27 avant n. è., que ce portrait de l'_imperator_ fut créé et put s'imposer. Partout dans l'Empire, jusqu'à ses confins, cette image idéale fut rapidement diffusée.

Le portrait de Chiragan arbore la _corona civica_ (« couronne civique »), composée de feuilles de chêne, symbole de Jupiter. Cet emblème honorifique fut décerné par le Sénat, en 27 avant n. è., lors de la cérémonie d'investiture de celui qui avait mis fin aux guerres civiles, rétabli la paix et assurait préserver les valeurs de la République oligarchique. Deux autres portraits, de même type et couronnés de chêne, conservés au <a href="/images/comp-ra-57-2.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-57-2.jpg" alt="Portrait d'Auguste du type de Prima Porta, Musée du Louvre Ma 1247 (MR 426), Marie-Lan Nguyen / Wikimedia Commons CC BY"/></span>musée du Louvre</a> et à la <a href="/images/comp-ra-57-3.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-57-3.jpg" alt="« Auguste Bevilacqua », Glyptothèque de Munich, Carole Raddato / Wikimedia Commons CC BY-SA"/></span>Glyptothèque de Munich</a>, ont été rapprochés de l'effigie de Chiragan. On notera, notamment, sur ces trois œuvres, comme sur bien d'autres, le crâne aplati à l'arrière ; il semble bien que ce trait récurrent sur les effigies en marbre de l'empereur représente l'une de ses caractéristiques physiques, à l'image, peut-être, de la bosse nasale, conforme à ce que rapporte Suétone {% cite suetone_auguste_nodate %} dans sa description détaillée d'Auguste.

D'après J.-C. Balty 2005, _Les portraits romains , 1 : Époque julio-claudienne, 1.1_ (_Sculptures antiques de Chiragan (Martres-Tolosane)_, Toulouse, p. 73-98.
