---
title: Buste cuirassé de Septime Sévère
id_inventaire: Ra 66 b
donnees_biographiques1: 145 - 211
donnees_biographiques2: Empereur de 193 à 211
type_oeuvre: "« Sérapis »"
date_creation: Entre 203 et 211 ; peut-être déjà dès 199/200
materiau: Marbre de Dokimium (Turquie)
hauteur: "78"
largeur: "64"
longueur: ''
profondeur: "33"
epaisseur: 
id_wikidata: Q25221272
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 220
priority: 3
id_notice: ra-66-b
image_principale: ra-66-b-1
vues: 8
image_imprime: 
redirect_from:
- ra-66-b
- ark:/87276/a_ra_66_b
id_ark: a_ra_66_b
precisions_biblio:
- clef: attanasio_marbles_2019
  note: ''
- clef: balty_les_2012
  note: p. 43, fig. 41, p. 265, 268, fig. 197, 203
- clef: balty_les_2021
  note: 'p. 12-14, 106-116, fig.3-5 '
- clef: bergmann_kaiserzeitlichen_2007
  note: p. 332
- clef: bernoulli_romische_1882
  note: p. 26, n° 58
- clef: braemer_les_1952
  note: p. 146
- clef: cazes_musee_1999-1
  note: p. 128
- clef: du_mege_description_1835
  note: p. 120, n° 213
- clef: du_mege_description_1844
  note: n° 373
- clef: esperandieu_recueil_1908
  note: p. 75, n° 976
- clef: heintze_studien_1966
  note: p. 198, n° 42, n° 2
- clef: joulin_les_1901
  note: p. 337, n° 293, pl. XXII
- clef: lexicon_1990
  note: p. 692
- clef: massendari_haute-garonne_2006-1
  note: 'p. 245, fig. 108 '
- clef: mccann_portraits_1968
  note: p. 172-173, n° 84, pl. LXXVa
- clef: musee_saint-raymond_essentiel_2011
  note: p. 46-47
- clef: musee_saint-raymond_regard_1995
  note: p. 177
- clef: rachou_catalogue_1912
  note: p. 45, n° 66 b
- clef: raeder_bildnis_2019
  note: ''
- clef: roschach_catalogue_1865
  note: n° 66 b
- clef: roschach_catalogue_1892
  note: p. 35, n° 66 b
- clef: rosso_image_2006
  note: p. 473-474, n° 228
- clef: soechting_portrats_1972
  note: p. 213, n° 118

---
Le portrait a été retaillé dans une œuvre qui représentait initialement un tout autre empereur. Mariane Bergmann a noté la disproportion existant entre les dimensions de la couronne civique (d’ailleurs rare, voire inhabituelle, sur les effigies conçues par les ateliers de Rome) et celles du visage {% cite bergmann_kaiserzeitlichen_2007 -L none -l p. 332 %}. On remarquera également la grandeur anormale des oreilles qui, comme la couronne civique, appartiennent à un premier état du buste ; de profil, l’anomalie est particulièrement frappante, la partie antérieure de la couronne dépassant nettement la ligne de silhouette du visage. La retaille est également distincte, sur le devant, à la limite inférieure de la couronne. Le cou est très gros par rapport au volume d’ensemble de la tête ; de profil, il a, en revanche, été retaillé à l’arrière pour supprimer le ressaut dorsal de la cuirasse.

Plusieurs portraits d’Antonin le Pieux, quatre exactement, émanant d’officines de Rome, portent ce type de couronne. Dans la nuque, du côté gauche, les deux ou trois mèches superposées peignées vers la gauche et faisant suite à une grosse mèche en S (quasiment dans l’axe du cou) rappellent celles que l'on distingue sur des portraits d’Antonin. C’est donc bien dans un buste de cet empereur que paraît avoir été retaillé le portrait toulousain. Il ne s’agissait nullement de la « réappropriation» de l’image d’un empereur ayant subi la «condamnation de mémoire» (_damnatio memoriae_), mais d’un cas de « recyclage » d’un buste produit en surnombre lors d’un règne précédent et demeuré dans les stocks de l’officine romaine qui l’avait réalisé près d’un demi-siècle plus tôt. Compte tenu de la qualité du portrait et des parallèles qu’il suggère, on ne peut imaginer que la transformation ait été opérée en Narbonnaise. D’autres portraits de «bons empereurs», qui n’ont donc pas connu de _damnatio memoriae_, ont apparemment connu le même sort, même si à une date bien plus tardive et pour de tout autres raisons. La rapide succession des règnes, tout au long du III<sup>e</sup> siècle, a certainement favorisé la récupération de portraits que la proclamation inattendue d’un nouvel empereur empêchait de diffuser dans les différentes provinces. Reste cependant à expliquer ce que faisait encore un buste d’Antonin le Pieux, dans une officine romaine, dans les toutes premières années du III<sup>e</sup> siècle. Ce n’est que par la multiplication d’exemples analogues qu’apparaîtra la meilleure réponse à cette question.

Le type de ce troisième portrait de Septime Sévère provenant de Chiragan appartient à celui des effigies les plus caractéristiques et les plus nombreuses de l’empereur ; on en compte, à ce jour, près de 90 exemplaires. Facilement reconnaissable aux quatre mèches en tire-bouchon qui retombent sur le front, il a été rapproché des images du dieu Sérapis, nées à Alexandrie au tout début de l’époque hellénistique et généralement attribuées au sculpteur Bryaxis. On le nomme donc « type Sérapis ».

On ne doutera pas de la volonté de Sévère de s’assimiler en quelque sorte à ce dieu : selon le biographe de l’_Histoire Auguste_, Septime Sévère n’aurait cessé, en effet, après le séjour qu’il fit à Alexandrie au retour de son expédition parthique (fin 199 – printemps 200), « de rappeler le plaisir que lui avait procuré ce voyage, qui lui avait permis de connaître le culte de Sérapis, de contempler des monuments antiques et de voir des animaux et des lieux nouveaux »  {% cite __noauthor_histoire_nodate -L none -l v. Sev. 17, 4 %}.                                                                                                 Sérapis est un dieu universel, _cosmocrator_ ; dieu dynastique des souverains Lagides, « garant de la prospérité du règne et de la victoire des armes » {% cite __noauthor_lexicon_1994 -L none -l p. 692 %}, il le fut aussi de certains empereurs. Au retour d’une visite au _Serapeum_ d’Alexandrie, Septime Sévère ne pouvait mieux choisir sa divinité protectrice. Celle-ci ne lui était d’ailleurs certainement pas étrangère. À Leptis Magna, dans sa ville natale, comme à Sabratha, s’élevaient d’importants sanctuaires du dieu d’Alexandrie, dont le culte avait gagné ces _emporia_ de Tripolitaine dès l’époque hellénistique.

Iconographiquement préparé par le « type de l’adoption» qui reliait Sévère à Marc Aurèle, l’empereur philosophe, ce « type Sérapis » franchit une étape décisive dans la représentation du souverain. Il marque notamment l’avènement, à la tête de l’Empire, d’hommes désormais originaires des provinces africaines ou orientales et de leurs dieux. Sérapis ne se substituait pas à Jupiter, mais en redoublait la valence. Les deux épithètes de _cosmocrator_ et d’« invincible » justifient d’ailleurs pleinement l’assimilation ("Angleichung") de Sévère à la divinité d'Alexandrie, lui qui venait de se débarrasser de ses compétiteurs à l’Empire et régnait désormais sur l’ensemble des provinces, fier de la concorde qui scellait à ses yeux l’avenir d’une nouvelle dynastie. Précisons cependant que cette assimilation ne fait pas pour autant de l’empereur un dieu.

En dépit de l’important travail de retaille qui a concerné l’ensemble du visage, le portrait est un des meilleurs de tous ceux qui nous ont été conservés de l’empereur ; il n’a subi, de surcroît, aucune restauration moderne (si ce n'est le bout du nez, aujourd’hui enlevé mais où se voit toujours le lissage de la surface pratiqué pour faire adhérer la prothèse).

D'après J.-C. Balty 2020, _Les Portraits Romains : L’époque des Sévères_, Toulouse, p. 106-116.