---
title: Buste de jeune garçon
id_inventaire: Ra 125
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Deuxième tiers du III<sup>e</sup> siècle
materiau: Marbre de Dokimium (Turquie)
hauteur: '36,5'
largeur: "25"
longueur: 
profondeur: "17"
epaisseur: 
id_wikidata: Q28764831
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 4
order: 360
priority: 3
id_notice: ra-125
image_principale: ra-125-1
vues: 8
image_imprime: 
redirect_from:
- ra-125
- ark:/87276/a_ra_125
id_ark: a_ra_125
precisions_biblio:
- clef: backe-dahmen_welt_2008
  note: ''
- clef: baratte_deux_1993
  note: fasc. 1, p. 101-110
- clef: cazes_musee_1999-1
  note: p. 141
- clef: derriks_tete_1998
  note: p. 91-103
- clef: esperandieu_recueil_1907
  note: p. 93, n° 1006
- clef: goette_romische_1989
  note: p. 203-217
- clef: gonzenbach_untersuchungen_1957
  note: ''
- clef: joulin_les_1901
  note: p. 336, pl. XVIII, n° 272 D
- clef: musee_saint-raymond_regard_1995
  note: p. 53, n° 13
- clef: rachou_catalogue_1912
  note: n° 125
- clef: vonderstein_cirrus_2011
  note: p. 161-176

---
Selon le rhéteur du IIe siècle de notre ère Julius Pollux, originaire de Naucratis, on aurait laissé pousser en partie la chevelure de certains enfants. Ainsi pouvait-on offrir ces longues mèches à la divinité sous la protection de laquelle ils étaient placés. L'offrande devait concourir à les protéger des risques qu'ils encouraient durant leur jeune âge {% cite pollux_grammaticus_onomasticon_nodate  -L none -l II, 30 %}. L'intriguante touffe de cheveux située à l'arrière du crâne de ce buste, dont les cheveux sont pour l'essentiel coupés très court, permit d'avancer l'hypothèse que l'enfant lui-même aurait été consacré à une divinité. C'est à la déesse égyptienne Isis que l'on pensa, par assimilation de ces longues boucles à un indice capillaire connu sous le nom de «mèche d'Horus».

La «mèche d'Horus», héritée de l'ancienne Egypte, se maintint durant l'époque impériale romaine comme le démontrent l'iconographie, en particulier au IIIe siècle. Quant aux longues mèches réservées à l'arrière du crâne, interprétées à leur tour comme le signe distinctif de la consécration de l'enfant à Isis, elles ne singularisent pas plus de quatorze portraits d'enfants  {% cite goette_romische_1989 -l 208 %} ; bien peu, donc, par rapport aux jeunes individus qui arborent une «mèche d'Horus» davantage respectueuse de la tradition, autrement dit portée sur le côté droit (plus marginalement sur le côté gauche) et retombant sur l'épaule. Si celle-ci représenta, dans l'Egypte ancienne, un marqueur social et de statut, elle put, au contraire, dans le monde romain, renvoyer plus directement à Isis {% cite backe-dahmen_roman_2018 -l 538 %}.

Assurément bien présentes à Chiragan, sous la forme de [statues de grand format](/ra-38-1), les divinités nilotiques ne peuvent cependant prouver la consécration de cet enfant à la grande déesse du Delta. Ainsi la coiffure pourrait-elle ici, plus modestement, renvoyer à un enfant, fragile par excellence, placé sous la protection de la déesse, garante de bonne santé et de postérité.

P. Capus