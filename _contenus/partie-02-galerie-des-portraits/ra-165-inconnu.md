---
title: Buste d'un inconnu
id_inventaire: Ra 165
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Premier quart du I<sup>er</sup> siècle
materiau: Marbre lychnites (île de Paros)
hauteur: '43,5'
largeur: "25"
longueur: ''
profondeur: "22"
epaisseur: 
id_wikidata: Q28735813
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 1
order: 20
priority: 3
id_notice: ra-165
image_principale: ra-165-1
vues: 8
image_imprime: 
redirect_from:
- ra-165
- ark:/87276/a_ra_165
id_ark: a_ra_165
precisions_biblio:
- clef: balty_les_2005
  note: p. 63-67, fig. 38-41, p. 129-142, fig. p. 128-132, 134, 138, 140, 143-144
- clef: bergmann_chiragan_1999
  note: p. 28-30, n° 147
- clef: braemer_art_1963
  note: p. 173, n° 780
- clef: cazes_musee_1999-1
  note: p. 120
- clef: esperandieu_recueil_1908
  note: p. 173, n° 973
- clef: gonse_les_1904
  note: p. 330
- clef: joulin_les_1901
  note: p. 332, pl. XVII, n° 263 E
- clef: labrousse_art_1956
  note: p. 31, n° 33
- clef: massendari_haute-garonne_2006-1
  note: p. 242, fig. 99
- clef: musee_saint-raymond_cirque_1990-1
  note: p. 52-53, n° 8
- clef: musee_saint-raymond_regard_1995
  note: p. 212
- clef: poulsen_probleme_1937
  note: p. 21, n° 2, pl. XXXIX-XL
- clef: pugliese_carratelli_enciclopedia_1997
  note: ''
- clef: queyrel_antonia_1992
  note: p. 79-80
- clef: rachou_catalogue_1912
  note: p. 69, n° 165
- clef: salviat_a_1980
  note: p. 12-15
- clef: vessberg_studien_1941
  note: p. 226
- clef: veyne_kunst_2009
  note: p. 54
- clef: zanker_bildnis_1981
  note: p. 361, n° 39

---
Autrefois dit de Jules César, ce portrait est conçu selon des principes caractéristiques de l'ancienne République romaine : réalisme, austérité et gravité. Il révèle donc un personnage encore attaché aux valeurs traditionnelles. L'effigie possède des points communs avec un autre portrait de la province de Narbonnaise, celui d'Ensérune, également dit, à une certaine époque, de César.

La coiffure aux cheveux coupés court témoigne bien de cet attachement à une mode ancienne porteuse de valeurs morales bien précises. De manière générale, le rendu des mèches, formant une série de faucilles ou de virgules, pourrait imiter l'aspect des chevelures de portraits en bronze. Au-dessus de l'œil droit, le sculpteur a atténué la rigueur de la frange en la détachant du front, à l'aide d'un trépan, afin de créer le renfoncement nécessaire. Autre caractéristique de la chevelure : derrière l'oreille droite, sur la nuque, on distingue un très original épi oblong, implantation capillaire propre, à n'en pas douter, au modèle représenté. Au rendu de la chevelure s'ajoute la forme arrondie du buste, caractéristique d'une époque. Ces éléments distinctifs autorisent à dater ce magnifique portrait de la seconde partie du règne d'Auguste ou du début du règne de Tibère ; autrement dit, des vingt premières années du I<sup>er</sup> siècle.

D'après J.-C. Balty 2005, _Les portraits romains , 1 : Époque julio-claudienne, 1.1_ (_Sculptures antiques de Chiragan (Martres-Tolosane)_, Toulouse, p. 127-144.