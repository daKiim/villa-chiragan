---
title: Buste cuirassé de Septime Sévère
id_inventaire: Ra 66 a
donnees_biographiques1: 145 - 211
donnees_biographiques2: Empereur de 193 à 211
type_oeuvre: " dit « de l'adoption »"
date_creation: Entre 195 et 203
materiau: Marbre de Göktepe 3 (Turquie)
hauteur: '73,5'
largeur: "66"
longueur: 
profondeur: "36"
epaisseur: 
id_wikidata: Q26703870
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 210
priority: 3
id_notice: ra-66-a
image_principale: ra-66-a-1
vues: 8
image_imprime: 
redirect_from:
- ra-66-a
- ark:/87276/a_ra_66_a
id_ark: a_ra_66_a
precisions_biblio:
- clef: balty_les_2012
  note: p. 45, fig. 44, p. 264, fig.196
- clef: balty_les_2021
  note: 'p. 14-16, 94-104, fig. 6-7 '
- clef: balty_nouveau_1962
  note: p. 190-191, pl. XXXV 9-10
- clef: bernoulli_romische_1882
  note: vol. II-3, p. 26, n° 57
- clef: braemer_les_1952
  note: p. 145-146
- clef: cazes_musee_1999-1
  note: p. 129
- clef: centro_de_exposiciones_arte_canal_roma_2007
  note: n° 23, p. 104-105
- clef: clarac_musee_1841
  note: p. 587
- clef: du_mege_description_1835
  note: p. 119, n° 212
- clef: du_mege_description_1844
  note: n° 373
- clef: du_mege_notice_1828
  note: p. 68-69, n° 132
- clef: esperandieu_recueil_1908
  note: p. 79-80, n° 981
- clef: felletti_maj_museo_1953
  note: p. 128
- clef: heintze_studien_1966
  note: p. 198, n° 42, n° 4
- clef: joulin_les_1901
  note: p. 121-337, n° 299, pl. XXIII
- clef: lorange_apotheosis_1947
  note: p. 141, n° 11
- clef: mccann_portraits_1968
  note: p. 147-148, n° 34, pl. XLV
- clef: musee_royal_mariemont_au_2018
  note: p. 84, fig. 5
- clef: musee_saint-raymond_regard_1995
  note: p. 174
- clef: rachou_catalogue_1912
  note: p. 44-45, n° 66 a
- clef: roschach_catalogue_1865
  note: n° 66 a
- clef: roschach_catalogue_1892
  note: p. 34-35, n° 66 a
- clef: rosso_image_2006
  note: p. 470-471, n° 226
- clef: soechting_portrats_1972
  note: p. 167-168, n° 52
- clef: ventura_il_1997
  note: p. 69, fig. 105

---
Comme pour Trajan, pas moins de quatre portraits de Septime Sévère ont été découverts à Chiragan. C'est tout à fait exceptionnel. L'atelier romain à l'origine de la tête est peut-être aussi l'auteur de deux autres, de même type, conservés à Copenhague (Ny Carlsberg Glyptotek) et à <a href="/images/comp-ra-66-a-2.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-66-a-2.jpg" alt="Buste de Septime Sévère, Palazzo Massimo alle Terme, Folegandros / Wikimedia Commons CC BY-SA"/></span>Rome (Palazzo Massimo alle Terme)</a>. Confronté à une [autre tête de Chiragan](/ra-120-a), du type «de l'avènement», ce portrait dénote davantage une tendance à l'idéalisation des traits et associe une implantation capillaire plus haute de part et d'autre de la mèche frontale centrale.

L'effigie correspond au type dit «de l'adoption», en raison de la très grande ressemblance de ce visage avec celui des portraits d'Antonin le Pieux ou encore de Marc Aurèle. Il s'agit là d'un exemple de l'assimilation («Angleichung») que certains empereurs adoptent à des fins politiques pour se rapprocher le plus possible d'un modèle antérieur. En 195, Septime Sévère se proclame fils de Marc Aurèle et donc frère de Commode. Comme sur les monnaies, le profil du portrait en marbre de Septime trahit le désir de se rapprocher formellement de ces empereurs antonins, dirigeants exemplaires, notamment du [type cuirassé de Marc Aurèle](/ra-61-b), dont un exemplaire fut également découvert dans la _villa_.

La tête de Chiragan a été remontée — apparemment dès l’Antiquité — sur un buste qui en comportait initialement une autre. Pour masquer au mieux le raccord, la tête rapportée a été retaillée immédiatement en-dessous des mèches de cheveux de la nuque et sous la barbe, et le cou du buste originel taillé en biais dans deux sens divergents (∧) de manière à ce que la tête puisse venir y reposer et prendre appui sur ces deux plans inclinés. Ce procédé technique est connu ; on peut, entre autres, citer le cas du portrait de Septime Sévère du Louvre (inv. Ma 1115). Mais il est plus difficile de déterminer les raisons qui ont conduit à l’adopter : simple réparation à la suite d’un bris, d’un accident d’atelier ? réutilisation du buste ou de la statue après une condamnation à l'oubli (_damnatio memoriae_) ?

Ce type de cuirasse à écailles a souvent été considéré comme caractéristique des militaires de la garde prétorienne. Ces cuirasses à écailles pourraient bien n’avoir été choisies, dans l’iconographie impériale, que par des empereurs qui souhaitaient, de la sorte, se mettre dans les bonnes grâces de ce corps d’élite qui fit et défit tant de règnes. Septime Sévère, cependant, le haïssait. Ne l’avait-il pas ridiculisé, en 193, dès son entrée dans Rome, en ordonnant « aux prétoriens de venir à sa rencontre en tunique et sans armes », puis en les convoquant « dans la même tenue à la tribune de commandement qu’entouraient de toutes parts des hommes armés »  {% cite noauthor_histoire_nodate -L none -l v. Sev. 6.11 %} ) ? Une fois dans la ville, « en armes, escorté de soldats armés, il monta au Capitole d’où, dans la même tenue, il gagna le palais, précédé par les enseignes qu’il avait enlevées aux prétoriens, et que l’on portait avec les pointes non pas en l’air mais tournées vers le sol » {% cite noauthor_histoire_nodate -L none -l v. Sev. 7.1 %}. Il est donc surprenant de rencontrer un buste de Sévère revêtu d’une cuirasse aussi particulièrement connotée, encore qu’on la trouve sur une émission de sesterces de 195.

Ce type de cuirasse à écailles n’est, par ailleurs, qu’assez rarement choisi dans la statuaire impériale. Mais on le trouve, en revanche, sur plusieurs bustes en or, en argent ou en bronze, souvent de petite taille, qui témoignent indiscutablement d’une tradition iconographique différente — également attestée par les monnaies.

La cuirasse à écailles de notre buste pourrait-elle avoir appartenu à un portrait de Plautien ? Il serait compréhensible qu’après sa disgrâce et son exécution, certaines de ses effigies aient été transformées en portraits de Septime Sévère. On ne peut, cependant, absolument pas le prouver. Remployer une telle cuirasse pour une effigie de Sévère demeure par conséquent difficilement explicable.

Deux détails iconographiques distinguent encore le buste toulousain des parallèles qu’on peut y associer. Le _paludamentum_, replié et agrafé sur l’épaule gauche, retombe plus bas que la limite inférieure de la cuirasse, jusqu’à mi-hauteur de la tablette. Ce détail est l’un des éléments de l’évolution que subissent les bustes, dans l’histoire du portrait romain, et qui conduit à ces « buste demi-corps » (« Halbkörperbüste ») du deuxième quart du III<sup>e</sup> siècle. Les plus anciens exemples de _paludamentum_ présentant cette disposition particulière sont sur les bustes d’Antonin le Pieux (Naples, Mus. nat. 6031), de Marc Aurèle (Rome, Musée du Capitole, Imp. 28, inv. 448), de Lucius Vérus (Paris, Musée du Louvre MA 1101), ainsi que celui dans lequel a été retaillé le portraitde Septime Sévère de type III provenant également de Chiragan ([inv. Ra 66b](/ra-66-b)). Ils demeurent donc relativement rares.                                                                                                Plus exceptionnel est la représentation des franges qui bordaient le manteau et sont figurées ici sur le rebord du buste, de manière quelque peu inattendue, sous la masse du _paludamentum_.

On remarquera enfin l’arrière du buste, particulièrement soigné et entièrement lissé, et la courbure des côtés du support axial dessinée et taillée avec la plus grande rigueur.

D'après J.-C. Balty 2020, _Les Portraits Romains : L’époque des Sévères_, Toulouse, p. 94-104.