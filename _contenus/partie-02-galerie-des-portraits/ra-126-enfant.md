---
title: Portrait d'enfant
id_inventaire: Ra 126
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Vers 150 - 160
materiau: Marbre de Göktepe 3 (Turquie)
hauteur: "20"
largeur: '17,5'
longueur: ''
profondeur: "18"
epaisseur: 
id_wikidata: Q24489352
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 130
priority: 3
id_notice: ra-126
image_principale: ra-126-1
vues: 8
image_imprime: 
redirect_from:
- ra-126
- ark:/87276/a_ra_126
id_ark: a_ra_126
precisions_biblio:
- clef: cazes_marbres_2001
  note: p. 19 n°15
- clef: balty_les_2012
  note: p. 70-72, fig. 68-70, p. 245-247, fig. 174-178
- clef: esperandieu_recueil_1908
  note: p. 85, n° 991
- clef: eydoux_france_1962-1
  note: p. 186, fig. 204
- clef: fittschen_prinzenbildnisse_1999
  note: p. 92, n° 90, pl. 170 a, b, c, d
- clef: joulin_les_1901
  note: p. 120, n° 288, pl. XXI, n° 288 D
- clef: musee_saint-raymond_image_2011
  note: p. 82-83
- clef: musee_saint-raymond_marbres_2007
  note: p. 26-27
- clef: rachou_catalogue_1912
  note: p. 61, n° 126

---
Encore une fois, comme dans le portrait d'enfant daté de l'époque d'Hadrien ([_inv_. Ra 124](/ra-124)), l’abondante masse de cheveux témoigne de la virtuosité d'un sculpteur qui traite les boucles avec un soin extrême. Ces ondulations sophistiquées contrastent avec le visage lisse au niveau duquel le marbre fut extrêmement poli.

Proche d’une tête de Trèves (Allemagne), cette œuvre pourrait figurer un jeune prince. Peut-être s'agit-il de l’un des deux premiers fils de Marc Aurèle et Faustine la Jeune, Titus Aelius Antonius, né en 152, ou Titus Aelius Aurelius, né en 158, tous deux morts en bas âge. Cependant, en l’absence de réplique provenant d’un contexte officiel, rien ne permet de l’assurer.

D'après J.-C. Balty 2012, _Les portraits romains , 1 : Le siècle des Antonins, 1.2_ (_Sculptures antiques de Chiragan (Martres-Tolosane)_, Toulouse, p. 243-248.