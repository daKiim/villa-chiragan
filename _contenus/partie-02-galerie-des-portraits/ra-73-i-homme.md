---
title: Portrait d'homme
id_inventaire: Ra 73 i
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Vers 225 - 250
materiau: Marbre de Göktepe 3 (Turquie)
hauteur: '34,5'
largeur: '18,5'
longueur: 
profondeur: "23"
epaisseur: 
id_wikidata: Q29005580
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 4
order: 350
priority: 3
id_notice: ra-73-i
image_principale: ra-73-i-1
vues: 8
image_imprime: 
redirect_from:
- ra-73-i
- ark:/87276/a_ra_73_i
id_ark: a_ra_73_i
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 140
- clef: du_mege_description_1835
  note: n° 224
- clef: du_mege_notice_1828
  note: n° 139
- clef: esperandieu_recueil_1908
  note: n° 994
- clef: joulin_les_1901
  note: n° 309
- clef: massendari_haute-garonne_2006-1
  note: p. 247, fig 111
- clef: rachou_catalogue_1912
  note: n° 73
- clef: roschach_catalogue_1892
  note: n° 73 i

---
Les grands yeux avec indication du creusement de l'iris, la chevelure près du crâne traitée _a penna_ ("à la plume") et le collier de barbe abondant avec une moustache très fine sont caractéristiques des portraits du III<sup>e</sup> siècle.

L'inquiétude du regard et les rides d'expression profondément creusées expriment le nouveau tournant pris par l'art du portrait romain en cette période de crises (gouvernementale, économique, financière et sociale) et d'interrogations. On a anciennement reconnu ici Volusien, empereur de 251 à 253.

P. Capus