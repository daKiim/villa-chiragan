---
title: Portrait d'enfant
id_inventaire: Ra 167
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Années 240
materiau: Marbre de Göktepe 4 (Turquie)
hauteur: "28"
largeur: "21"
longueur: ''
profondeur: "15"
epaisseur: 
id_wikidata: Q27712278
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 4
order: 390
priority: 3
id_notice: ra-167
image_principale: ra-167-1
vues: 8
image_imprime: 
redirect_from:
- ra-167
- ark:/87276/a_ra_167
id_ark: a_ra_167
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 141
- clef: esperandieu_recueil_1908
  note: p. 88, n° 997
- clef: joulin_les_1901
  note: p. 219-300, XXV pl., fig. 274 E
- clef: rachou_catalogue_1912
  note: n° 167

---
Ce beau portrait a malheureusement été très endommagé, brisé horizontalement, au niveau de la bouche. Les deux parties sont aujourd'hui rassemblées par un comblement. Bout du nez, menton et joue gauche ont disparu.

L'enfant a été rapproché du portrait féminin dans lequel fut reconnue Otacilia Severa, épouse de l'empereur Philippe l'Arabe, ou encore, plus probablement, Tranquillina, mariée à Gordien III ([_inv_. Ra 166](/ra-166)). Cette relation se fonde sur quelques points, communs aux deux œuvres : la tunique et son plissé, les petits boutons ronds qui maintiennent celle-ci sur l'épaule et la double cavité qui marque les pupilles. Cependant, si le portrait féminin représente bien Tranquillina, il est difficile de lui associer ce petit garçon, le couple impérial n'ayant pas eu d'enfant.

D. Cazes