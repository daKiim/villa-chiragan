---
title: Tête d’Antonin le Pieux
id_inventaire: Ra 60 (1)
donnees_biographiques1: 86 - 161
donnees_biographiques2: Empereur de 138 à 161
type_oeuvre: 
date_creation: Vers 138 - 140
materiau: Marbre de Göktepe 3 (Turquie)
hauteur: "42"
largeur: '25,5'
longueur: ''
profondeur: '26,5'
epaisseur: 
id_wikidata: Q24436741
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 140
priority: 3
id_notice: ra-60-1
image_principale: ra-60-1-1
vues: 8
image_imprime: 
redirect_from:
- ra-60-1
- ark:/87276/a_ra_60_1
id_ark: a_ra_60_1
precisions_biblio:
- clef: balty_les_2012
  note: p. 29-34, fig. 20-24, 27, p. 197-198, 200, 202-205, fig. 123, 124, 127, 130, 132, 134-136
- clef: braemer_les_1952
  note: p. 145
- clef: braemer_restauration_1981
  note: p. 56
- clef: cazes_musee_1999-1
  note: p. 124
- clef: centro_de_exposiciones_arte_canal_roma_2007
  note: p. 90-91 n° 16
- clef: du_mege_description_1835
  note: p. 113-5, n° 201
- clef: du_mege_notice_1828
  note: p. 64-65
- clef: esperandieu_recueil_1908
  note: p. 67, n° 962
- clef: fittschen_katalog_1985
  note: p. 180
- clef: joulin_les_1901
  note: n° 276 B p. 118
- clef: landes_stade_1994
  note: p. 193, n° 30
- clef: massendari_haute-garonne_2006-1
  note: p. 244, fig. 106
- clef: musee_darcheologie_mediterraneenne_egypte_1997
  note: n° 22
- clef: musee_saint-raymond_image_2011
  note: p. 10, 41
- clef: musee_saint-raymond_regard_1995
  note: p. 43, n° 5
- clef: rachou_catalogue_1912
  note: p. 42-3, n° 60
- clef: rosso_image_2006
  note: p. 458-459, n° 219
- clef: wegner_herrscherbildnisse_1939
  note: p. 151, 279, 282
- clef: wegner_verzeichnis_1979
  note: p. 121

---
À la différence de ce que l’on connaît pour son prédécesseur Hadrien, l’empereur Antonin, dit le Pieux, n’eut qu’un seul type de portrait, tant sur les monnaies que pour les effigies en ronde-bosse taillées dans le marbre alors qu'il régna pourtant vingt-trois années. Celui-ci, à la différence de tous les autres portraits de cet empereur, semble être le seul sur lequel iris et pupille ne sont pas incisés. On sait pourtant que cette indication apparaît aux alentours de 130, se substituant ainsi à la seule peinture qui, jusque-là, donnait une vie supplémentaire aux représentations.

Ce portrait de grande qualité, conçu à Rome, montre encore trois petites « verrues », derrière la tête, oubliées par le sculpteur chargé du polissage final. En effet, la multiplication d'un portrait officiel nécessitait la reproduction fidèle d'un prototype, en plâtre ou en terre, qui servait de référence pour toutes les copies exécutées dans un matériau noble tel que le marbre. Un compas et un fil à plomb permettaient de reporter sur le bloc de marbre les points de repère, sélectionnés sur le modèle à copier. Les points de mesure encore présents sur cette œuvre prouvent que sur les meilleurs exemplaires sortis des ateliers de la capitale, l’arrière était copié avec autant de soin que le visage. Ce portrait fut probablement envoyé à Chiragan peu après l’avènement d’Antonin le Pieux afin de compléter la série des effigies impériales de ce domaine.

D'après J.-C. Balty 2012, _Les portraits romains , 1 : Le siècle des Antonins, 1.2_ (_Sculptures antiques de Chiragan (Martres-Tolosane)_, Toulouse, p. 195-205.
