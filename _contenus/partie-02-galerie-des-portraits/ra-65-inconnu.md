---
title: Buste à paludamentum frangé d’un haut fonctionnaire de l’Empire
id_inventaire: Ra 65
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: 'Vers 195 - 205 '
materiau: Marbre de Göktepe 3 (Turquie)
hauteur: '62,5'
largeur: "58"
longueur: 
profondeur: "30"
epaisseur: 
id_wikidata: Q30160658
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 280
priority: 3
id_notice: ra-65
image_principale: ra-65-1
vues: 8
image_imprime: 
redirect_from:
- ra-65
- ark:/87276/a_ra_65
id_ark: a_ra_65
precisions_biblio:
- clef: balty_les_2012
  note: p. 27, fig. 18-19, p. 268, fig.203
- clef: balty_les_2021
  note: p. 48, 191-198
- clef: cazes_musee_1999-1
  note: p. 137
- clef: du_mege_description_1835
  note: n° 200
- clef: du_mege_description_1844
  note: n° 363
- clef: du_mege_notice_1828
  note: p. 63-64, n° 124
- clef: esperandieu_recueil_1908
  note: p. 72, n° 970
- clef: fittschen_zum_1971
  note: p. 237-240, fig. 31-32
- clef: guillevic_toulouse_1989
  note: n° 36
- clef: joulin_les_1901
  note: p. 337, pl. XXIV, n° 308
- clef: musee_saint-raymond_regard_1995
  note: p. 91, n° 49
- clef: nguyen-van_reconstruire_2015
  note: ''
- clef: rachou_catalogue_1912
  note: n° 65 ou 73 D
- clef: roschach_catalogue_1892
  note: n° 65 ou 73 D

---
Une fois encore, on est ici devant la réalisation d’une des meilleures officines de l’Urbs, et l’on aimerait percer l’anonymat de cet homme qu’on ne peut que soupçonner d’être un haut fonctionnaire de l’Empire, dans l’entourage même de l’empereur, sans pour autant pouvoir désigner la charge qu’il eut dans le domaine de Chiragan, du moins avec une précision suffisante.

Différents portraits du tout début de l’époque sévérienne prolongent une mode de coiffure dont de jeunes gens attestent l’existence depuis l’époque antonine : particulièrement bouffante et peignée, à partir du vertex, en mèches ondulées, apparemment désordonnées, qui retombent assez bas sur le front et dans la nuque, elle s’inspire notamment de celle des effigies d’Antinoüs, favori de l'empereur Hadrien (dont on sait l’influence sur les portraits de jeunes gens du début de l’époque antonine) et des effigies de Lucius Vérus enfant. Jusqu’au début de l’époque sévérienne, plusieurs portraits, renonçant aux boucles coquillées de la plupart des œuvres de l’époque antonine, attestent le succès de ces coiffures bouffantes à mèches balayant tout le front. Ce haut fonctionnaire de Chiragan est peut-être un des plus âgés à l’arborer encore.

Au plan technique, c’est assurément un des plus beaux portraits mis au jour à Martres-Tolosane. Le modelé particulièrement sensible des joues et de la « surface oculaire » (avec ces fines rides incisées sous la paupière inférieure gauche), l’étonnante expression d’un regard tout à la fois perçant et quelque peu ironique, la force émanant du bas du visage aux mâchoires serrées composent un extraordinaire portrait psychologique et en font indiscutablement un des chefs-d’oeuvre de l’art romain. Quel dommage que les restaurateurs du début du XIX<sup>e</sup> siècle aient si profondément entaillé le nez pour y fixer la prothèse que montrent les anciennes photographies ! Le profil s’en trouve irrémédiablement détruit.

D'après J.-C. Balty 2020, _Les Portraits Romains : L’époque des Sévères_, Toulouse, p. 191-198.