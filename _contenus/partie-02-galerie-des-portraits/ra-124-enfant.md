---
title: Buste d'enfant
id_inventaire: Ra 124
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Vers 130 - 140
materiau: Marbre de Göktepe 3 (Turquie)
hauteur: "31"
largeur: "18"
longueur: ''
profondeur: "17"
epaisseur: 
id_wikidata: Q24490087
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 120
priority: 3
id_notice: ra-124
image_principale: ra-124-1
vues: 8
image_imprime: 
redirect_from:
- ra-124
- ark:/87276/a_ra_124
id_ark: a_ra_124
precisions_biblio:
- clef: balty_les_2012
  note: p. 67-69, fig. 64-67, p. 187-190, 192-194, fig. 113-119, 121-122
- clef: cazes_musee_1999-1
  note: p. 122
- clef: esperandieu_recueil_1908
  note: p. 84, n° 988
- clef: joulin_les_1901
  note: p. 331, pl. XVIII, n° 273 D
- clef: lebegue_notice_1892
  note: p. 415, pl. XXIX, n° 4
- clef: massendari_haute-garonne_2006-1
  note: p. 249, fig. 116
- clef: musee_saint-raymond_image_2011
  note: p. 82-83
- clef: musee_saint-raymond_regard_1995
  note: p. 50, n° 10
- clef: rachou_catalogue_1912
  note: n° 124

---
Malgré le gros éclat oblique sur la partie antérieure du crâne qui défigure partiellement l’œuvre, cette tête compte au nombre des plus belles images de l’enfance. À l’abondante chevelure aux mèches ondulées, répondent le visage lisse et le buste nu, très finement modelés.

Dater précisément une telle œuvre n'est pas simple. Si l'aspect lisse du globe de l'œil, certainement peint à l'origine, permettrait d'exclure les années après 130, époque à partir de laquelle iris et pupille furent gravés, il faut se souvenir que certains portraits postérieurs demeurent encore dépourvus de ces détails oculaires ; on pensera en particulier au [portrait d'Antonin](/ra-60-1). Les mèches en faucille et leur mouvement, la forme du buste et le départ encore embryonnaire des bras, incitent à situer cette œuvre d'une qualité impressionnante durant la seconde partie du règne d'Hadrien. Les portraits privés affichent alors une très grande variété, rendant difficile toute datation précise.

L'art romain sut rendre avec délicatesse les visages espiègles et souvent rêveurs des enfants. On sculpta ces derniers de leur vivant afin de compléter, dans les couches aisées de la société, des ensembles familiaux qui imitaient les assemblages de portraits de la maison impériale (_domus Augusta_) dans lesquels les enfants apparaissent très tôt, qu'ils soient encore en partie conservés dans le <a href="/images/comp-ra-124-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-124-1.jpg" alt="Groupe dynastique de Béziers, inv. Ra 226 à Ra 342, musée Saint-Raymond, Christelle Molinié / Wikimedia Commons CC BY-SA"/></span>groupe dynastique de Béziers</a> ou seulement mentionnés dans celui de Thespies. L’identité du petit garçon demeure malheureusement pour nous un mystère ; nous pouvons supposer qu’il fut le fils d’un haut fonctionnaire de l’Empire.

D'après J.-C. Balty 2012, _Les portraits romains , 1 : Le siècle des Antonins, 1.2_ (_Sculptures antiques de Chiragan (Martres-Tolosane)_, Toulouse, p. 185-194.