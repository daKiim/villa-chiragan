---
title: Buste d’un intellectuel (?)
id_inventaire: Ra 59
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Vers 210 - 220
materiau: Marbre de Dokimium (Turquie)
hauteur: "84"
largeur: "58"
longueur: ''
profondeur: "33"
epaisseur: 
id_wikidata: Q27887688
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 300
priority: 3
id_notice: ra-59
image_principale: ra-59-1
vues: 8
image_imprime: 
redirect_from:
- ra-59
- ark:/87276/a_ra_59
id_ark: a_ra_59
precisions_biblio:
- clef: balty_les_2012
  note: p. 49, 58, 60, fig. 49, 57, 58, p. 269, fig. 204
- clef: balty_les_2021
  note: p. 46-48, 223-231
- clef: cazes_musee_1999-1
  note: p. 138
- clef: du_mege_description_1835
  note: p. 112-113, n° 198
- clef: du_mege_description_1844
  note: f° 361
- clef: esperandieu_recueil_1908
  note: p. 71, n° 969
- clef: icahiers_alsaciens_darcheologie/i_cahiers_2010
  note: p. 100, fig. 25
- clef: joulin_les_1901
  note: p. 118 et pl. XVIII, n° 266 B
- clef: meischner_privatportrats_1982
  note: p. 412, n° 12, fig. 16
- clef: musee_saint-raymond_regard_1995
  note: p. 160
- clef: rachou_catalogue_1912
  note: p. 42, n° 59
- clef: roschach_catalogue_1892
  note: p. 32-33, n° 59
- clef: weski_antiquarium_1987
  note: p. 257-258, pl. 143

---
La largeur du buste de ce portrait, imposant, impressionne. Un manteau — le _pallium_ des hommes de lettres, philosophes et rhéteurs depuis l’époque classique — repose à peine sur les épaules, dont il glisse, tant à droite qu’à gauche, et dégage entièrement le buste, à la musculature assez nettement marquée ; à gauche, il recouvre en partie le haut du bras et devait être maintenu contre le corps par le coude selon un schéma fréquemment attesté dans la statuaire antique ; à droite, il dénude plus complètement l’épaule et passait selon toute vraisemblance sous le bras avant de revenir couvrir l’abdomen. Ce mouvement assez artificiel et ostentatoire du vêtement met en évidence la puissance du torse mais anime aussi tout le buste et répond au mouvement de la tête, légèrement inclinée et plus nettement tournée vers la droite, visant ainsi à accentuer la fougue déclamatoire ou la force de persuasion de ces rhéteurs et sophistes des II<sup>e</sup> et III<sup>e</sup> siècles.

Ce vêtement seul — et la manière de le draper — suffit-il à faire du personnage qui le porte, dans le monde romain, un intellectuel, homme de lettres, philosophe ou rhéteur, ou connote-t-il plus simplement le désir de celui qui se fait représenter de la sorte d’être considéré comme un homme cultivé ? À Chiragan, à côté des portraits d’empereurs et de membres de la Maison impériale (_domus Augusta_), force est de penser qu’il s’agit de hauts fonctionnaires de l’Empire que leurs écrits invitaient à assimiler en quelque sorte aux penseurs et aux orateurs : on songera tout particulièrement aux jurisconsultes, dont le rôle auprès de l’empereur fut des plus important à l’époque des Sévères.

Le front buriné de longs plis, de fortes arcades sourcilières, le regard à la fois réfléchi et soucieux, la bouche serrée, le menton volontaire, tout, dans ce magnifique portrait, trahit l’extraordinaire concentration du personnage, sans la brutalité qui émanait des effigies menaçantes bien connues de Caracalla avec lesquelles l’oeuvre de Chiragan appelle cependant la comparaison.                                                                                                    Si les mèches de la frange frontale ont conservé, tout en l’atténuant considérablement, quelque chose du volume et du mouvement des mèches coquillées de la fin de l’époque antonine et du règne de Septime Sévère, cheveux et barbe courts sont bien ceux des années de règne de Caracalla.

D'après J.-C. Balty 2020, _Les Portraits Romains : L’époque des Sévères_, Toulouse, p. 223-231.