---
title: Buste héroïsé d'un homme d'âge mûr
id_inventaire: Ra 70
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Fin du II<sup>e</sup> siècle
materiau: Marbre de Göktepe 3 (Turquie)
hauteur: '55,5'
largeur: "43"
longueur: 
profondeur: "23"
epaisseur: 
id_wikidata: Q28732552
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 190
priority: 3
id_notice: ra-70
image_principale: ra-70-1
vues: 8
image_imprime: 
redirect_from:
- ra-70
- ark:/87276/a_ra_70
id_ark: a_ra_70
precisions_biblio:
- clef: balty_les_2021
  note: p. 36-38, 169-175, fig. 25, 26
- clef: cazes_musee_1999-1
  note: p. 134
- clef: du_mege_description_1835
  note: n° 221
- clef: du_mege_notice_1828
  note: n° 136
- clef: esperandieu_recueil_1908
  note: n° 982
- clef: fittschen_bildnis_1980
  note: p. 108-114
- clef: gelzer_eikones:_1980
  note: ''
- clef: joulin_les_1901
  note: n° 306
- clef: nocentini_sculture_1965
  note: p. 68-71, fig. pl. XVI, XVII
- clef: rachou_catalogue_1912
  note: n° 70
- clef: roschach_catalogue_1892
  note: n° 70

---
Le buste nu, héroïsé, le _paludamentum_ replié sur l’épaule gauche et maintenu par une fibule ronde en forme de rosace à six pétales, s’inscrit dans une tradition qui remonte aux toutes dernières années du I<sup>e</sup> siècle de notre ère et comportait souvent un baudrier passé en oblique sur le torse. Les plus anciens exemples sont, en effet, les deux bustes figurant Domitien de Leipzig (aujourd'hui détruit) et de Toledo (USA) et, au nombre des portraits privés, un buste du Capitole (inv. 678), généralement daté de la fin du règne de Domitien. Le buste toulousain, dont la découpe « en plastron » — sans la moindre indication de départ des bras — ne trouve d’équivalents qu’au tout début du II<sup>e</sup> siècle, surprend donc quelque peu à la date qui est la sienne et semble témoigner d’un retour à des formes anciennes qui n’avaient généralement plus cours à ce moment.

Le réalisme si accentué des traits de ce magnifique portrait toulousain est étonnant : la calvitie marquée (avec une petite touffe de cheveux isolée, à l’avant du vertex), l’étagement des plis du front, le froncement des sourcils et le modelé tout à fait extraordinaire de toute la zone de l’œil avec ces « pattes d’oie » (uniquement à l’œil droit) et ces poches sous la paupière inférieure, les plis naso-labiaux prolongés, du côté gauche, par deux plis supplémentaires au départ de la barbe, une certaine dissymétrie aussi de l'expression. La sensibilité accordée au regard, associée au pivotement de la tête vers la droite, semble retranscrire une vie intérieure riche et une grande vitalité intellectuelle. K. Fittschen a souligné la ressemblance entre cette œuvre et <a href="/images/comp-ra-70-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-70-1.jpg" alt="Buste, musée Bardini Florence, Sailko / Wikimedia Commons CC BY-SA"/></span>un portrait</a>, mais dont le buste est vêtu, conservé au musée Bardini de Florence {% cite fittschen_bildnis_1980 -l 113 %}.

Verra-t-on, dans le buste de Chiragan, le portrait d’un intellectuel ? Froncement des sourcils — comme signe de concentration de l’individu — et calvitie suffisent-ils à le qualifier comme tel ? Le _paludamentum_ replié sur l’épaule gauche renvoie au domaine « militaire », ou, sans doute plus exactement, à ces hauts fonctionnaires de l’Empire dont les charges peuvent être aussi bien militaires qu’administratives.                                                                               La culture est une des valeurs qui, en cette fin du II<sup>e</sup> siècle, confère un réel prestige et favorise l’accès à ces hautes fonctions au sommet de l’État romain : les grands juristes du moment, dont plusieurs firent partie du conseil de l’empereur sous Septime Sévère et Sévère Alexandre et dont certains accédèrent à la préfecture du prétoire, appartiennent, notamment, à cette catégorie de personnes.

D'après J.-C. Balty 2020, _Les Portraits Romains : L’époque des Sévères_, Toulouse, p. 169-175.