---
title: Buste cuirassé et drapé dans un paludamentum d’un haut fonctionnaire de l’Empire
id_inventaire: Ra 71
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: 220 - 235
materiau: Marbre de Göktepe 3 (Turquie)
hauteur: "80"
largeur: "67"
longueur: 
profondeur: "38"
epaisseur: 
id_wikidata: Q30160698
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 331
priority: 3
id_notice: ra-71
image_principale: ra-71-1
vues: 8
image_imprime: 
redirect_from:
- ra-71
- ark:/87276/a_ra_71
id_ark: a_ra_71
precisions_biblio:
- clef: balty_les_2021
  note: p. 39-41, 245-252
- clef: cazes_musee_1999-1
  note: p. 140
- clef: du_mege_description_1835
  note: n° 223
- clef: du_mege_notice_1828
  note: n° 137
- clef: esperandieu_recueil_1908
  note: n° 977
- clef: joulin_les_1901
  note: n° 327
- clef: mesple_raccords_1948
  note: p. 157
- clef: rachou_catalogue_1912
  note: n° 71
- clef: roschach_catalogue_1892
  note: n° 137

---
Si le traitement « _a penne_ » (c’est-à-dire en forme de petites plumes) des mèches de la chevelure apparaît dès les années 215 sur certaines effigies de Caracalla, il se maintient, selon les ateliers, jusqu’au règne de Dèce (249-251). Force est donc, dans le cas d’oeuvres anonymes, d’opter pour une fourchette chronologique relativement large qui laisse toute sa place aux différentes pratiques d’atelier.

Le buste toulousain étant celui d’un véritable « sosie » de <a href="/images/comp-ra-71-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-71-1.jpg" alt="Dèce, inv. MC 482, Musées du Capitole, Sailko/Wikimedia Commons CC BY"/></span>Dèce</a>, on en viendrait presque à se demander s’il ne pourrait s’agir d’un portrait de l’empereur antérieur de quelques années à la proclamation par les troupes de Pannonie en 249. Il faut pourtant se garder de toute proposition d’identification hâtive et simplement constater, une fois de plus, que l’on a là l’image d’un de ces fonctionnaires de l’entourage immédiat de l’empereur, ce qui, dans le contexte de Chiragan, ne surprendra guère.

Tête et buste, réunis depuis la restauration qui suivit immédiatement leur découverte, n’appartenaient pas à la même oeuvre. Il s’agit là d’un remontage moderne des sculpteurs toulousains pour la présentation des oeuvres dans la « Galerie des empereurs » de 1835. Le buste remonte à l’époque antonine, donc au siècle qui précède le type du portrait lui-même, comme en témoigne l’amorce des bras déjà assez nettement marquée, la largeur à hauteur des épaules et cette forme ovale que l’on retrouve sur plusieurs effigies d’Antonin le Pieux.                                                                                                            Outre cette recomposition de l'époque moderne, la tête elle-même  conserve la trace de reprises importantes, qui pourraient témoigner d’une véritable retaille dans l’Antiquité. Le pavillon de l’oreille gauche semble bien avoir été retaillé dans un portrait dont les oreilles, apparemment plus grandes, étaient en partie recouvertes par les boucles de la chevelure ; à l’arrière de l’oreille gauche, comme du côté droit, le grossier piquetage qui en cerne le contour diffère nettement du travail soigné des _penne_ du reste de la coiffure. Enfin, la grosseur du cou par rapport à la tête, étroite, pourraient également s’expliquer par le remodelage systématique d’un portrait antérieur.

Cependant, le portrait est d'une grande qualité: subtilité du modelé du visage, intensité du regard, soin mis à détailler les _penne_ de la chevelure et les mèches de la barbe prouvent la virtuosité des artisans de ces officines de l’_Urbs_ (Rome) dans ce type de travail.

D'après J.-C. Balty 2020, _Les Portraits Romains : L’époque des Sévères_, Toulouse, p. 245-252.