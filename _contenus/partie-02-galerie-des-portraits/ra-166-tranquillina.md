---
title: Portrait de Tranquillina (?)
id_inventaire: Ra 166
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: Vers 241 - 244
materiau: Marbre de Göktepe 3 (Turquie)
hauteur: "50"
largeur: "34"
longueur: 
profondeur: '21,5'
epaisseur: 
id_wikidata: Q26704019
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 4
order: 380
priority: 3
id_notice: ra-166
image_principale: ra-166-1
vues: 8
image_imprime: 
redirect_from:
- ra-166
- ark:/87276/a_ra_166
id_ark: a_ra_166
precisions_biblio:
- clef: bergmann_studien_1977
  note: p. 39
- clef: braemer_culte_1999
  note: p. 53, fig. 2
- clef: bulletin_1936
  note: p. 837
- clef: cazes_musee_1999-1
  note: p. 141
- clef: esperandieu_recueil_1908
  note: p. 90-91, n° 1001
- clef: felletti_maj_iconografia_1958
  note: p. 179, n° 220
- clef: joulin_les_1901
  note: n° 298 E
- clef: poulsen_catalogue_1951
  note: p. 523, n° 754
- clef: rachou_catalogue_1912
  note: n° 166
- clef: rosso_image_2006
  note: p. 482-484, n° 234
- clef: veyne_empire_2005
  note: fig. 38
- clef: wegner_verzeichnis_1979
  note: p. 56

---
La tunique plissée, attachée sur l'épaule droite par de petits boutons, est très souple et délicate. Le manteau forme un large demi-cercle qui valorise le portrait. Dans le cou, la surface piquetée prouve que la partie basse du chignon était rapportée.

On connaît dix autres portraits, de même type, conservés à Rome, Copenhague, Dresde, Florence, Liverpool et Londres. Ceci prouve qu'il s'agit bien ici d'une représentation impériale. On a pu reconnaître, dans cette très jeune femme à l'expression mélancolique, Otacilia Severa, épouse de l'empereur Philippe l'Arabe, ou encore, avec davantage de probabilités, Tranquillina, femme de Gordien III (238-244). Mariée à l'empereur en 241, elle mourut trois ans plus tard, très jeune. Elle ne subit pas de condamnation à l'oubli après sa mort (_damnatio memoriae_) ce qui explique sans doute l'existence des répliques, nombreuses pour cette époque, encore conservées.

D'après E. Rosso 2006, _L'image de l'empereur en Gaule romaine, portraits et inscriptions_, p. 482-484.