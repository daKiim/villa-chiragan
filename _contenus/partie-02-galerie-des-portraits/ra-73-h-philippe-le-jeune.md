---
title: Portrait de Philippe le Jeune
id_inventaire: Ra 73 h
donnees_biographiques1: 237 - 249
donnees_biographiques2: Co-empereur de 247 à 249. Fils de Philippe l'Arabe et d'Otacilia
  Severa
type_oeuvre: 
date_creation: Entre 247 et 249
materiau: Marbre de Göktepe 3 (Turquie)
hauteur: '24,5'
largeur: "17"
longueur: 
profondeur: "19"
epaisseur: 
id_wikidata: Q26707676
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 4
order: 410
priority: 3
id_notice: ra-73-h
image_principale: ra-73-h-1
vues: 8
image_imprime: 
redirect_from:
- ra-73-h
- ark:/87276/a_ra_73_h
id_ark: a_ra_73_h
precisions_biblio:
- clef: andreae_art_1973
  note: p. 294-295, fig. 126
- clef: bergmann_studien_1977
  note: p. 36-37, n° 3, pl. 5.1-3
- clef: braemer_les_1952
  note: p. 143-148, pl. III, fig. 4
- clef: braemer_portrait_1982
  note: p. 165, fig. 232
- clef: cazes_musee_1999-1
  note: p. 141
- clef: du_mege_description_1835
  note: p. 124, n° 222
- clef: du_mege_description_1844
  note: n° 381
- clef: du_mege_notice_1828
  note: p. 72-73, n° 138
- clef: esperandieu_recueil_1908
  note: p. 92, n° 1003
- clef: felletti_maj_iconografia_1958
  note: p. 83
- clef: fittschen_bemerkungen_1969
  note: p. 211, pl. 24
- clef: ic.i.l./i_corpus_1888
  note: n°4227
- clef: joulin_les_1901
  note: p. 123, pl. XXIII, n° 296 b
- clef: musee_saint-raymond_cirque_1990-1
  note: n° 14
- clef: musee_saint-raymond_regard_1995
  note: p. 44, n° 6
- clef: rachou_catalogue_1912
  note: n° 73 h
- clef: roschach_catalogue_1865
  note: n° 73 h
- clef: roschach_catalogue_1892
  note: p. 37-38, n° 73 h
- clef: rosso_image_2006
  note: p. 480-482, n° 233
- clef: wegner_verzeichnis_1979
  note: p. 44, 50 pl. 15 a-b
- clef: wiggers_caracalla_1971
  note: p. 199

---
Cet enfant, au regard doux et rêveur, est apparemment âgé d'une dizaine d'années. La coupe très courte est typique de la mode du milieu du III<sup>e</sup> siècle. Les cheveux sont rendus au moyen de fines incisions, une technique de sculpture développée sous le règne de Philippe l'Arabe (244-249).

On a reconnu ici Sévère Alexandre (222-235) mais, plus majoritairement, Philippe le Jeune, en raison de la forte ressemblance avec les profils monétaires de cet empereur {% cite fittschen_bemerkungen_1969 rosso_image_2006 -L none -l p. 211, fig. 24 -L none -l p. 480-482, fig. 181 %}. La petite fourche formée par les mèches à l'angle du front est parfois visible sur les monnaies. Iulius Philippus est élevé au rang d'Auguste en 247 ; il n'avait alors que dix ans. Cet événement est sans doute à l'origine de la création de ce portrait officiel dont les joues pleines témoignent du jeune âge. Son père Philippe l'Arabe fut assassiné en 249 par la garde prétorienne (soldats à la disposition de l'empereur) ou par des soldats de Dèce, son ennemi et successeur au trône. Philippe le Jeune connut immédiatement le même sort funeste. Il n'avait que douze ans. Les deux courtes années durant lesquelles il put exercer ses fonctions d'Auguste aux côtés de son père furent cependant suffisantes pour qu'il soit honoré d'une statue, élevée à Narbonne par la colonie de Béziers, comme en témoigne la base inscrite conservée {% cite rosso_image_2006 hirschfeld_cil_1888 -l 389-390 -L none -l n°4227 %}.

P. Capus