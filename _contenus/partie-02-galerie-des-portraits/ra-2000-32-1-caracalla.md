---
title: 'Portrait fragmentaire de Caracalla '
id_inventaire: 2000.32.1
donnees_biographiques1: 188 - 217
donnees_biographiques2: Fils de Septime Sévère et Julia Domna, frère de Géta. Empereur
  de 211 à 217
type_oeuvre: "« Alleinherrscher » "
date_creation: 211 - 217
materiau: Marbre
hauteur: "16"
largeur: "19"
longueur: 
profondeur: "22"
epaisseur: 
id_wikidata: Q63787582
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 242
priority: 3
id_notice: 2000-32-1
image_principale: 2000-32-1-1
vues: 3
image_imprime: 
redirect_from:
- 2000-32-1
- ark:/87276/a_2000_32_1
id_ark: a_2000_32_1
precisions_biblio:
- clef: balty_les_2021
  note: p. 64-65, 146-150
- clef: wood_roman_1986
  note: ''
- clef: sande_greek_1991
  note: ''
- clef: rosso_image_2006
  note: p. 479-480, n°232
- clef: leitmer_getas_2007
  note: ''
- clef: joulin_les_1901
  note: pl. XXV, n° 319 D
- clef: balty_universalite_1986
  note: ''
- clef: fittschen_sul_1985
  note: ''

---
Bien que ce portrait ne nous soit parvenu qu'à l'état fragmentaire, on distingue encore ce que fut sa qualité dans les moindres détails encore subsistants : barbe, mèches de cheveux, modelé des lèvres, passages de plans subtils autour de l'angle interne de l'œil. Tout dénote le travail de l'un des meilleurs ateliers officiels de Rome. Ceci n'a rien d'étonnant ; en effet, dans le cadre de la série des représentations de l'empereur dépendant de ce type, dit «Alleinherrscher», les «têtes de série», ou « Leitstücke » (les plus proches du prototype créé à l'origine par l'officine impériale, ou «Urbild») sont aujourd'hui comptées parmi les meilleurs portraits de l'époque. L’artiste qui, à la Cour, fut chargé de créer l’« Urbild », avait réalisé, avec ce modèle, l'un des types les plus impressionnants de l’Antiquité, au point que l'on a pu parler à son égard de «Caracalla Master » {% cite wood_roman_1986 -l 49 %}. Le célèbre <a href="/images/comp-2000-32-1-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-2000-32-1-1.jpg" alt="Brutus par Michel-Ange, musée du Bargello Florence, Miguel Hermoso Cuesta / Wikimedia Commons CC BY-SA"/></span>buste de Michel-Ange évoquant Brutus</a> s’inspirait de ce type tout en métamorphosant ce mouvement de tête brutal qui s'ajoutait à un regard furieux afin de démontrer la fougue du personnage {% cite fittschen_sul_1985 balty_universalite_1986 -L none -l p. 410-411, fig. 385-390 -L none -l p. 324, pl. XI, 3-4 %}.

La facture de l'œuvre de Chiragan a été rapprochée de celle du <a href="/images/comp-2000-32-1-2.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-2000-32-1-2.jpg" alt="Portrait de l'empereur Caracalla, inv. 6603, musée national archéologique de Naples, Marie-Lan Nguyen / Wikimedia Commons CC BY"/></span>portrait du musée archéologique de Naples</a>, si renommée : modelé analogue de la bouche - à la lèvre inférieure encore plus courte, mais tout aussi charnue, à Toulouse - et touffes de la barbe qui présentent « le même usage d’un trépan extrêmement fin qui vient en animer souplement les boucles, dont les détails sont simplement incisés » {% cite rosso_image_2006 -l 480%}. La douceur des passages des plans du visage, le rendu du volume des boucles courtes de la barbe, ici ponctuées d’un coup de foret, se retrouvent également sur une belle tête d’Oslo (ancienne collection Fett), qui appartient au type immédiatement antérieur («Thronfolgertypus II», ou «deuxième type de la succession») {% cite sande_greek_1991 -L none -l n° 63 p. 77-78, pl. LXII %}. De cet exemplaire d’Oslo à celui de Chiragan, la filiation stylistique est particulièrement claire : Septime Sévère avait artificiellement rattaché sa personne à la dynastie précédente des Antonins, faisant par conséquent de ses deux enfants, Caracalla et Géta, dans leurs premiers portraits, de véritables petits princes antonins. Cependant, tous les arguments qui tentaient de justifier cette filiation se sont petit à petit estompés. Ainsi, à partir des exemplaires de ce « 2<sup>e</sup> type de la succession », durant les dernières années de règne de l’empereur, les portraits visent essentiellement à mettre en évidence la _Concordia Augustorum_, la concorde (de façade) entre les Augustes (confirmée sur des revers de monnaies par la légende _CONCORDIA AVGVSTORVM_ ou _CONCORDIAE AVGG_) {% cite leitmeir_getas_2007 -L none -l p. 15-16 et n° 57 %}. Une mutation profonde s’installe, loin désormais de l’image de l’empereur qui avait été celle du siècle précédent, d’Hadrien à Septime Sévère. La création révolutionnaire du «Caracalla Master» ne fera que concrétiser cette transformation de l'image du souverain. Caracalla est bien le premier des «empereurs militaires» et ce type de portrait, qui rompt si profondément avec la physionomie habituelle des empereurs du II<sup>e</sup> siècle, influencera durablement l’iconographie de plusieurs de ses successeurs.

D'après J.-C. Balty 2020, _Les Portraits Romains : L’époque des Sévères_, Toulouse, p. 146-150.