---
title: Tête d’homme, aujourd’hui montée sur un buste en pavonazzetto
id_inventaire: Ra 121
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: 'Début des Sévères : 195 - 205 (buste sans doute un peu plus tardif)'
materiau: Marbre phrygien de Synnada dit pavonazzetto (Dokimium / Iscehisar, près
  d'Afyon, Turquie) pour le buste et marbre de Göktepe 4 (Turquie) pour la tête
hauteur: "92"
largeur: "64"
longueur: ''
profondeur: "32"
epaisseur: 
id_wikidata: Q27145497
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 250
priority: 3
id_notice: ra-121
image_principale: ra-121-1
vues: 8
image_imprime: 
redirect_from:
- ra-121
- ark:/87276/a_ra_121
id_ark: a_ra_121
precisions_biblio:
- clef: bulletin_1936
  note: p. 833
- clef: cazes_musee_1999-1
  note: p. 135-136
- clef: esperandieu_recueil_1908
  note: n° 967
- clef: fittschen_katalog_1983
  note: p. 108-109, n° 162 (œuvre en rapport)
- clef: joulin_les_1901
  note: n° 303 E (tête), n°315 D (buste)
- clef: musee_saint-raymond_regard_1995
  note: n° 102
- clef: rachou_catalogue_1912
  note: n° 121

---
La couleur du marbre de la tête témoigne des application d'acides, utilisés dans le cadre des restaurations des œuvres afin de se débarrasser des concrétions. Le résultat fut calamiteux pour un grand nombre de portraits.

Si les marbres utilisés sont différents, tous deux sont cependant orientaux (Turquie). Il faut souligner que de telles associations de matériaux contrastés n'ont rien d'étonnant durant l'époque impériale et étaient, au contraire, très appréciées.                                             Le marbre micrasiatique _pavonazzetto_ (de l'italien _pavone_, paon) fut particulièrement apprécié en raison de son aspect flamboyant : ses veines et ses tâches - violettes pourpres, bleues ou vertes - seyaient bien à l'extravagance ostentatoire du pouvoir à Rome. Les carrières de Synnada  étaient propriété impériale et le début de leur exploitation intensive remonte à l'époque augustéenne. On conçut ainsi, dans le marbre de la région d'Afyon (appelé _lithos Synnadikos_ par les Romains), les colonnes de la Maison d'Auguste, sur le Palatin, ainsi que, mêlés à d'autres marbres, les portiques du Forum d'Auguste et les dalles de pavement du temple de Mars Ultor {% cite pensabene_cave_2010 -l 78 %}. Mais c'est surtout à partir de la fin du I<sup>er</sup> siècle que les marbres colorés micrasiatiques parvinrent en très grande quantité à Rome afin d'être employés, en premier lieu, dans les espaces  monumentaux et somptueux décidés par le dernier des empereurs flaviens, Domitien. Le _pavonazzetto_ d'Afyon sera également, au début du siècle suivant, utilisé dans le domaine de la sculpture, comme en témoignent les impressionnantes et très expressives représentations des guerriers daces captifs  destinées au Forum de Trajan.

Le _paludamentum_ (manteau des généraux et des empereurs) est attaché par une fibule ronde bosselée et cache en partie la tête de Gorgone de la cuirasse. Sur cette dernière, à gauche, apparaît également, dans l'anneau de la courroie, un lacet dont l'un des bouts s'achève en feuille de lierre. La tête est apparemment légèrement antérieure au buste ; ils furent associés lors des restaurations des marbres, au XIX<sup>e</sup> siècle ou dans le courant du XX<sup>e</sup> siècle.

P. Capus