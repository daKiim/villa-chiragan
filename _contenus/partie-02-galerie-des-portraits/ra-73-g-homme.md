---
title: Tête d'homme
id_inventaire: Ra 73 g
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: "«Pérouse - Toulouse»"
date_creation: Entre 195 et 205
materiau: Marbre de Göktepe 3 (Turquie)
hauteur: "37"
largeur: '24,5'
longueur: 
profondeur: "25"
epaisseur: 
id_wikidata: Q27164560
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 270
priority: 3
id_notice: ra-73-g
image_principale: ra-73-g-1
vues: 8
image_imprime: 
redirect_from:
- ra-73-g
- ark:/87276/a_ra_73_g
id_ark: a_ra_73_g
precisions_biblio:
- clef: balty_les_2021
  note: p. 44-46, 211-215
- clef: cazes_musee_1999-1
  note: p. 138
- clef: dareggi_proposito_1988
  note: p. 321-322, fig.1,3, p. 322-323, fig. 2,4
- clef: du_mege_description_1835
  note: n° 220
- clef: esperandieu_recueil_1907
  note: n° 980
- clef: fittschen_zum_1971
  note: p. 214-252
- clef: joulin_les_1901
  note: n° 297 b
- clef: rachou_catalogue_1912
  note: n° 73 g
- clef: roschach_catalogue_1892
  note: n° 73 g

---
Gallien, Tétricus, ces noms d’empereurs du III<sup>e</sup> siècle ont été successivement proposés pour identifier le personnage, qui n’est cependant pas un empereur mais un personnage important de son entourage comme l’indique la présence d’une réplique à Pérouse, reconnue par G. Dareggi {% cite dareggi_proposito_1988 -L none -l p. 322-323, fig. 2 et 4, p. 321-322, fig. 1 et 3 %}. Ce portrait, d’une extraordinaire qualité plastique, se signale dès l’abord par la fougue qui émane d’un très net mouvement de la tête vers la droite — mouvement que souligne la direction du regard — et par une chevelure assez bouffante, aux longues mèches frontales légèrement soulevées par rapport à la surface du front, qui imprime également à l’ensemble un réel dynamisme. Plus lisses sur le dessus du crâne et à l’arrière, les cheveux descendent assez bas dans la nuque. La barbe, rase, remonte jusque sous les pommettes et « mange » tout le bas du visage — ce qui a vraisemblablement conduit à y reconnaître autrefois un portrait de Gallien. À la différence de l’exemplaire toulousain, la tête de Pérouse présente une inclinaison du visage et une direction du regard vers la gauche ; sa facture est également beaucoup moins soignée.

Le type de coiffure adopté, aux longs cheveux peignés près du crâne mais où les mèches frontales se répartissent de façon assez symétriques à partir d’une fourche plus ou moins axiale, caractérise toute une série de portraits de jeunes gens et d’hommes d’âge mûr qui sont, dans l’ensemble, contemporains des premières effigies de Caracalla et Géta.

D'après J.-C. Balty 2020, _Les Portraits Romains : L’époque des Sévères_, Toulouse, p. 211-215.