---
title: Tête d’Antonia Minor
id_inventaire: Ra 31
donnees_biographiques1: 36 avant notre ère - 37
donnees_biographiques2: Fille de Marc Antoine et d’Octavie (sœur d’Auguste), épouse
  de Drusus l’Ancien (frère de Tibère), mère de Germanicus et de l'empereur Claude
type_oeuvre: dit « Schläfenlöckchen » (« aux boucles sur les tempes »)
date_creation: Entre 37 et 54
materiau: Marbre lychnites (île de Paros)
hauteur: "27"
largeur: '21,2'
longueur: ''
profondeur: "21"
epaisseur: 
id_wikidata: Q26721238
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 1
order: 30
priority: 3
id_notice: ra-31
image_principale: ra-31-1
vues: 8
image_imprime: 
redirect_from:
- ra-31
- ark:/87276/a_ra_31
id_ark: a_ra_31
precisions_biblio:
- clef: balty_les_2005
  note: p. 51-52, fig. 24-25, p. 147-160, fig. p. 146-149, 151 (n° 77), 152 (n° 79), 153 (n° 81), 156 (n° 83), 161
- clef: balty_portraits_1995
  note: p. 107, n° 104
- clef: boschung_gens_2002
  note: p. 129, 131, n° 45.3, pl. 95.3-4
- clef: du_mege_description_1835
  note: p. 84, n° 158
- clef: du_mege_notice_1828
  note: p. 36, n° 71
- clef: fittschen_katalog_1977
  note: p. 61, n° 7, n° 4 (à propos du n° 19)
- clef: joulin_les_1901
  note: p. 93, n° 73
- clef: kunzl_antonia_1997
  note: p. 459, cat. B8, p. 488-489
- clef: massendari_haute-garonne_2006-1
  note: p. 242, fig. 101
- clef: queyrel_antonia_1992
  note: p. 69-77
- clef: rachou_catalogue_1912
  note: p. 31, n° 31
- clef: rosso_image_2006
  note: n° 211
- clef: rosso_portrait_2000
  note: p. 321, fig. 10

---
Cette tête, dans laquelle fut reconnue Antonia Minor, est probablement entrée dans les collections toulousaines durant la première moitié du XIX<sup>e</sup> siècle. Elle fut complétée et montée sur un buste en stuc conçu à cet effet. Aucun document ne prouve assurément que le portrait provient bel et bien de Chiragan.

Nièce d'Auguste et mère d'un prince adulé, Germanicus, Antonia ne pouvait que bénéficier d'une image glorieuse. Le portrait de cette princesse, réputée pour sa beauté, fut amplement répandu à travers l'Empire, depuis 16 ou 15 avant n. è., jusqu'après sa mort, sous le règne de Claude (41-54 de n. è.). Elle reprit la charge de prêtresse du culte d'Auguste divinisé, succédant ainsi à l’impératrice Livie, morte en l'an 29 de n. è. Belle-sœur de Tibère, Antonia avait informé celui-ci du complot fomenté par le préfet de la garde prétorienne, Séjan, à l'automne 31. Ainsi l'empereur lui témoigna-t-il sa reconnaissance en lui élevant sculptures et dédicaces associées. Mais c'est sous le règne de Claude, son fils, qu'elle reçut le plus grand nombre d'hommages, témoignant de son rôle clé au sein de la maison impériale. Antonia est par conséquent la princesse la plus honorée en Gaule, comme en témoigne la série de portraits et de dédicaces découverte à Nîmes, Cimiez, Béziers, Chiragan et Vienne. Dans cette dernière ville, c'est un portrait colossal de la princesse, posthume probablement, portant le diadème, qui fut mis au jour dans le théâtre.

La tête toulousaine, malheureusement très endommagée, fut taillée dans un marbre grec oriental, probablement dans un atelier de Rome. On y reconnaît la simplicité de la coiffure d'Antonia, formant des vagues de part et d'autre d'une raie centrale, caractéristique des figures idéales grecques, un style de coiffure qui connaît une grande diffusion dans l'art du portrait, surtout à partir du début du règne d'Auguste. Mais la chevelure est ici maintenue par un sobre serre-tête qui semble torsadé : peut-être simple ornement ou symbole du statut de prêtresse du culte d'Auguste divinisé, endossé par Antonia, d'après Emmanuelle Rosso {% cite rosso_image_2006 --locator 446 %}. Enfin, à l'arrière du crâne, avaient été rajoutées, par collage, des mèches ondulées maintenues par un catogan. Ce type d'adjonction était courant dans la sculpture hellénistique, durant les derniers siècles avant n. è.

L'ensemble de ces caractéristiques (mèches ondées et quelque peu relâchées, raie médiane et catogan), non seulement visible dans les groupes dynastiques provinciaux mais également diffusé par les monnaies frappées à l'effigie d'Antonia, fut à l'origine d'une véritable mode féminine sous le règne de Tibère. Car le modèle de sagesse et de vertu que personnifiait l'épouse de Drusus résonne dans le choix de cette coiffure sobre, qui fut alors préférée aux tresses compliquées et aux mèches retombant sur le front de la fin du règne d'Auguste.

D'après J.-C. Balty 2005, _Les portraits romains , 1 : Époque julio-claudienne, 1.1_ (_Sculptures antiques de Chiragan (Martres-Tolosane)_, Toulouse, p. 145-161.
