---
layout: home
---

La découverte de dizaines de sculptures en marbre sur le site de la *villa* romaine de Chiragan, à Martres-Tolosane au XIX<sup>e</sup> siècle, représente encore aujourd’hui l’un des événements parmi les plus impressionnants de l’archéologie nationale. En France, aucun autre site antique n’a révélé autant d’œuvres et bien peu de lieux des anciens territoires de l’Empire romain, sinon la *villa* de l’empereur Hadrien à Tivoli ou la *villa* des Papyrus à Herculanum, peuvent témoigner d’une telle parure de marbres dans un contexte privé.

Ainsi, les collections publiques toulousaines peuvent-elles s’enorgueillir de présenter une telle quantité de portraits et de sculptures mythologiques issues d’un seul et même lieu. Ce catalogue numérique donnera, nous l’espérons, aux amateurs, curieux et étudiants le désir de venir observer cette spectaculaire collection au musée Saint-Raymond, musée d’Archéologie de Toulouse. 
