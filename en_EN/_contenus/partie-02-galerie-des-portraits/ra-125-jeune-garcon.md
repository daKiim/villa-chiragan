---
title: Bust of a young boy
id_inventaire: Ra 125
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: Second third of the 3\<sup>rd\</sup> century
materiau: Dokimium marble (Turkey)
hauteur: '36,5'
largeur: "25"
longueur:
profondeur: "17"
epaisseur:
id_wikidata: Q28764831
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 4
order: 360
priority: 3
id_notice: ra-125
image_principale: ra-125-1
vues: 8
image_imprime:
redirect_from:
- ra-125
- ark:/87276/a_ra_125
id_ark: a_ra_125
precisions_biblio:
- clef: backe-dahmen_welt_2008
  note: ''
- clef: baratte_deux_1993
  note: fasc. 1, p. 101-110
- clef: cazes_musee_1999-1
  note: p. 141
- clef: derriks_tete_1998
  note: p. 91-103
- clef: esperandieu_recueil_1907
  note: p. 93, n° 1006
- clef: goette_romische_1989
  note: p. 203-217
- clef: gonzenbach_untersuchungen_1957
  note: ''
- clef: joulin_les_1901
  note: p. 336, pl. XVIII, n° 272 D
- clef: musee_saint-raymond_regard_1995
  note: p. 53, n° 13
- clef: rachou_catalogue_1912
  note: n° 125
- clef: vonderstein_cirrus_2011
  note: p. 161-176

---

According to the 2nd century rhetorician Julius Pollux from Naucratis, some parts of children's hair were left to grow, and later offered to the god under whose protection the child had been placed. This offering was meant to protect them from the risks inherent to infancy {% cite pollux_grammaticus_onomasticon_nodate -L none -l II, 30 %}. The intriguing locks of hair at the back of the skull of this bust, whose hair is otherwise cut very short, has been seen as a possible indication that this child was himself entrusted to the protection of a divinity. The Egyptian goddess Isis inevitably comes to mind by assimilation, as these long curls are reminiscent of a capillary practice known as "Horus lock".

This practice, inherited from Ancient Egypt, was maintained during the Roman imperial period as shown by iconography, and during the 3rd century in particular. As for the long locks left uncut at the back of this skull, which are seen as distinctive proof that the child's wellbeing had been entrusted to Isis, they are present on no more than fourteen children's portraits {% cite goette_romische_1989 -l 208 %}. That is very few when compared to the young individuals shown sporting a "Horus lock" which is more in keeping with tradition, in other words worn in the form of a sidelock, on the right-hand side of the head (and more rarely on the left side) that falls onto the shoulder. Whereas in Ancient Egypt it was a marker of one's social position and status, in the Roman world it may simply have been a direct reference to Isis {% cite backe-dahmen_roman_2018 -l 538 %}.

Although these Nilotic deities were certainly present in Chiragan in the form of large statues, this does not prove the consecration of this child by the great goddess of the Nile Delta. The child's hairstyle may simply mean that the child was essentially frail, and was therefore placed under the protection of the goddess as a way of guaranteeing his good health and posterity.

P. Capus
