---
title: Bust of Trajan
id_inventaire: Ra 117
donnees_biographiques1: 53 - 117
donnees_biographiques2: Emperor from 98 to 117
type_oeuvre: Of the "sacrificial" type
date_creation: Between 108 and 113
materiau: Carrara marble
hauteur: "56"
largeur: "36"
longueur:
profondeur: "25"
epaisseur:
id_wikidata: Q24436503
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 70
priority: 3
id_notice: ra-117
image_principale: ra-117-1
vues: 8
image_imprime:
redirect_from:
- ra-117
- ark:/87276/a_ra_117
id_ark: a_ra_117
precisions_biblio:
- clef: balty_les_2012
  note: p. 65, fig. 60. p. 104-111, fig. 28-39. p. 261, fig.192
- clef: balty_nouveau_1977
  note: n° 108 p. 60
- clef: braemer_les_1952
  note: p. 145, Trajan II
- clef: cazes_musee_1999-1
  note: p. 122
- clef: centro_de_exposiciones_arte_canal_roma_2007
  note: p. 92-93, n° 17
- clef: esperandieu_recueil_1908
  note: p. 64, n° 956
- clef: fittschen_katalog_1985
  note: p. 43, n° 44, réplique 5
- clef: gross_bildnisse_1940
  note: n° 64
- clef: joulin_les_1901
  note: p. 115, pl. XVII, n° 261 d
- clef: mansuelli_galleria_1958
  note: p. 82
- clef: musee_saint-raymond_image_2011
  note: p. 24
- clef: musee_saint-raymond_regard_1995
  note: p. 215
- clef: pugliese_carratelli_enciclopedia_1997
  note: p. 962-965
- clef: rachou_catalogue_1912
  note: p. 59, n° 117
- clef: rosso_image_2006
  note: p. 453-455 n° 216
- clef: sanchez_montes_civilizacion._2006
  note: p. 210

---

The naked torso may originally have been partly covered with the aegis (the armour of the goddess Athena/Minerva, given by Zeus/Jupiter), as shown by two other busts kept in <a href="/images/comp-ra-117-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-117-1.jpg" alt="Bust of Trajan in the Caracalla baths in Rome, Ny Carlsberg Glyptotek, Copenhague, Carole Raddato / Wikimedia Commons CC BY-SA"/></span>Copenhagen</a> and Munich. In addition to this cuirass, the Danish bust boasts an additional calyx of acanthus leaves at its base. These features explain the large and rounded part that is missing on the left shoulder of the Chiragan bust, as well as the very peculiar triangular gap between the two pectorals.

The aegis first appeared in portraits of Alexander the Great, and served as a reminder of the sovereign's divine filiation. It is relatively frequent in Trajan's coinage, right from the first monetary issues. It is a particularly enlightening testimony to the "Jovian (Jupiter-related) theology of imperial power" that characterised the reign of the *Optimus Princeps* (best ruler), a doctrine echoed by the writer Pliny the Younger in his "Panegyric in praise of Trajan". Seeing as the Empire had reached its widest territorial expansion, the emperor’s universal domination could not have been better represented.

The bowl cut and hair combed from back to front on the earliest portraits of the *Princeps* is more complex here, and heralds that of the portraits produced during the reign of Hadrian, his successor. The arrangement of the hairs of the fringe and temples is portrayed in keeping with a very specific pattern, which appears in scenes depicted on Trajan's Column in Rome, where the emperor is seen performing a sacrifice. Such is the origin of the name given to this new iconographic type, and it is undoubtedly one of the last, if not *the* last of his reign. As already said, it is confirmed on Trajan's Column, and therefore predates this dedication dated 12<sup>th</sup> May 113.

According to J.-C. Balty 2012, *Les portraits romains, 1 : Le siècle des Antonins, 1.2* (*Sculptures antiques de Chiragan (Martres-Tolosane)*, Toulouse, p. 102-112.
