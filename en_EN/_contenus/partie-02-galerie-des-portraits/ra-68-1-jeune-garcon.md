---
title: Head of a young boy (Gaius Fulvius Plautius Hortensianus ?)
id_inventaire: Ra 68 (1)
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: Between 202 and 205
materiau: Marble
hauteur: "21"
largeur: "18"
longueur:
profondeur: "21"
epaisseur:
id_wikidata: Q31271612
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 260
priority: 3
id_notice: ra-68-1
image_principale: ra-68-1-1
vues: 8
image_imprime:
redirect_from:
- ra-68-1
- ark:/87276/a_ra_68_1
id_ark: a_ra_68_1
precisions_biblio:
- clef: balty_les_2021
  note: 'p. 17-20, 161-168, fig. 9, 10 '
- clef: cazes_musee_1999-1
  note: p. 132
- clef: clarac_musee_1841
  note: p. 581
- clef: du_mege_description_1835
  note: n° 216
- clef: du_mege_notice_1828
  note: n° 134
- clef: esperandieu_recueil_1908
  note: n° 1002
- clef: fittschen_katalog_1985
  note: p. 90, n° 79, pl. 95-6
- clef: fittschen_prinzenbildnisse_1999
  note: p. 75-77, pl.
- clef: fittschen_review_1978
  note: p. 147, n° 5
- clef: hausmann_zur_1981
  note: p. 147, n° 5
- clef: joulin_les_1901
  note: n° 284
- clef: rachou_catalogue_1912
  note: n° 68
- clef: roschach_catalogue_1865
  note: n° 68
- clef: roschach_catalogue_1892
  note: n° 68
- clef: rosso_image_2006
  note: p. 467-468, n° 224
- clef: wiggers_caracalla_1971
  note: p. 110

---

The manner in which this lovely melancholy portrait has been rendered is proof of the sculptor's great dexterity in his portrayal of the hair and almond-shaped eyes, with the shadows beneath them. The head has a receding chin, a sulky mouth, and hair brushed down from the top of the skull towards the ears and forehead to form a rather flat hairstyle composed of independent wavy strands, especially on the sides. While this clue may still point to hairstyles that were popular during the Antonine dynasty, it is nevertheless still found in portraits of Septimius Severus and the young Caracalla.

A <a href="/images/comp-ra-68-1-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-68-1-1.jpg" alt="Capitole Museum, Rome / Institut archéologique allemand, Rome"/></span>second head</a> of the same type is kept in the Capitoline Museums in Rome. These two specimens of the same iconographic type having been discovered in two different places in the Empire, it seemed logical to assume that this is not the son of a rich citizen but that of a prince. Since the early images of Princes Caracalla and Geta are known to us, for a long time only Pertinax Caesar was seen as a credible candidate for this portrait {% cite fittschen_katalog_1985 fittschen_prinzenbildnisse_1999 -L none -l n° 79, p. 90, pl. 95-96 -l 75-76 %}. As the son of Pertinax, himself successor to Commodus, Pertinax the Younger assumed the role of Caesar and *princeps iuventutis* in 193, less than a hundred days before his father was assassinated after reigning for only three months. He himself suffered the wrath of the angry Caracalla, who had him put to death in 212 or 213. Pertinax the Younger and his mother, Flavia Titiana, received no honours or titles, and were not represented on the coins that circulated in Rome, although they did figure on a few that were minted in Alexandria.

It is therefore unlikely that this lovely head unearthed in Chiragan is that of Pertinax the Younger. It would seem more plausible to assume that it is the head of Gaius Fulvius Plautius Hortensianus, son of Plautianus, the Praetorian prefect, and friend of Septimius Severus. The latter two shared a common origin in Africa (both were probably born in Leptis Magna) and may even have belonged to the same family. (?). They engaged and subsequently married their two children Caracalla and Plautilla in 202, both then fourteen years of age. The years 202-205, during which the two families were united by their children's marriage could allow us to date the design of the portrait of Gaius Fulvius Plautius the Younger. An inscription in Timgad, which can also only date from this period, states that he bore the title of *clarissimus puer*, which unquestionably refers to a teenager under the age of 17 who has not yet taken to wearing the *toga virilis*. Having been exiled with his sister to the Aeolian Islands (or Sicily, according to sources) in 205, he was put to death there by Caracalla in 212.

This work represents a key part of the series of portraits from the early 3rd century, introducing stylistic characteristics that are particular to the Severan dynasty. Very similar to the images of Caracalla and Geta, and inspired, as they were, by those of the young princes of the Antonine dynasty to which Severus claimed to belong, these "Capitole - Toulouse" type portraits of teenagers are also those of a whole generation.

According to J.-C. Balty 2020, *Les Portraits Romains : L’époque des Sévères*, Toulouse, p. 161-168.
