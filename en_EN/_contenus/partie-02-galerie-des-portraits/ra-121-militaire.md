---
title: Man’s head, now mounted on a bust of Pavonazzetto marble
id_inventaire: Ra 121
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: Beginning of the Severan dynasty:195 - 205 (the bust may be slightly more recent)
materiau: Phrygian Synnadic marble, also known as Pavonazzetto marble (Dokimium / Iscehisar, near Afyon province, Turkey) for the bust, and Göktepe marble, district 4 (Turkey) for the head
hauteur: "92"
largeur: "64"
longueur: ''
profondeur: "32"
epaisseur:
id_wikidata: Q27145497
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 250
priority: 3
id_notice: ra-121
image_principale: ra-121-1
vues: 8
image_imprime:
redirect_from:
- ra-121
- ark:/87276/a_ra_121
id_ark: a_ra_121
precisions_biblio:
- clef: bulletin_1936
  note: p. 833
- clef: cazes_musee_1999-1
  note: p. 135-136
- clef: esperandieu_recueil_1908
  note: n° 967
- clef: fittschen_katalog_1983
  note: p. 108-109, n° 162 (œuvre en rapport)
- clef: joulin_les_1901
  note: n° 303 E (tête), n°315 D (buste)
- clef: musee_saint-raymond_regard_1995
  note: n° 102
- clef: rachou_catalogue_1912
  note: n° 121

---

The colour of the marble head is the result of acids being used to remove accretions during the restoration works. This practice proved disastrous for many portraits.

Although different, both marbles are oriental (Turkey). Such combinations of contrasting materials are far from surprising, as they were highly appreciated during the imperial period. This Micrasian *Pavonazzetto* marble (from the Italian word *pavone*, meaning peacock) was particularly appreciated for its flamboyant appearance: its purple, violet, blue and green veins and spots suited the ostentatious extravagance of powerful men in Rome. The quarries of Synnadic stone were imperial property, and their intense excavation began during the Augustan dynasty. Thus, this marble from the Afyon region (known as *lithos Synnadikos* to the Romans), was used to make the columns of the House of Augustus on Palatine Hill, as well as, combined with other marbles, the porticoes of the Forum of Augustus, and the paving slabs of the temple of Mars Ultor {% cite pensabene_cave_2010 -l 78 %}. But it wasn’t until the end of the 1st century that very large quantities of coloured Micrasian marbles began arriving in Rome to be used, first and foremost, in the monumental and sumptuous sites chosen by Domitian, the last of the Flavian emperors. At the beginning of the following century, *Pavonazzetto* marble from Afyon province was also used to produce sculptures, as evidenced by the impressive and very expressive depiction of captive Dacian warriors in Trajan’s Forum.

The *paludamentum* (a cloak worn by generals and emperors) is fastened with an embossed round fibula that partly hides the Gorgon head on the cuirass. On the left of the latter, a lace is seen hanging from the buckle of the strap, one end of which ends in an ivy leaf. The head is apparently slightly older than the bust, as both elements were paired during 19th and 20th century marble restoration campaigns.

P. Capus
