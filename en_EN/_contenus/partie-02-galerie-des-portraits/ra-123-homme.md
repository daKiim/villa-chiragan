---
title: Heroic bust of a man
id_inventaire: Ra 123
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: Approximately 230 - 240
materiau: Göktepe marble, district 3 (Turkey)
hauteur: '61,5'
largeur: "53"
longueur:
profondeur: "29"
epaisseur:
id_wikidata: Q27106491
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 4
order: 311
priority: 3
id_notice: ra-123
image_principale: ra-123-1
vues: 8
image_imprime:
redirect_from:
- ra-123
- ark:/87276/a_ra_123
id_ark: a_ra_123
precisions_biblio:
- clef: balty_les_2012
  note: p. 66, fig. 61, p. 269, fig. 205
- clef: cazes_musee_1999-1
  note: p. 138
- clef: de_lachenal_museo_1986
  note: p. 48-53
- clef: esperandieu_recueil_1908
  note: n° 972
- clef: joulin_les_1901
  note: n° 269 D
- clef: massendari_haute-garonne_2006-1
  note: p. 249, fig. 117
- clef: musee_saint-raymond_regard_1995
  note: p. 159, n° 114
- clef: perrot_rapport_1891
  note: ''
- clef: rachou_catalogue_1912
  note: n° 123

---

Following the assassination of Severus Alexander, the 3rd century was marked by almost continuous civil war. Generals were raised to the head of the Empire by their armies, yet none of these emperors reigned for long, and ended up being assassinated by their successors. This period was known as "Military Anarchy".

This work is reminiscent of the style adopted for the sculpted portraits of military emperors Maximinus Thrax (235-238) and Gordian I (238). The hair, both very short and soft-looking, is also characteristic of the art of portraiture during this period. The face is calm and restrained. The bare chest occupies a lot of space, the folds of the cloak are deeply furrowed, and the gaze is expressive thanks to the engraved irises and the pupils marked by two dots.

There are holes above and behind the left ear as well as on the right side of the skull. Also worth noting are the strange shapes of the helix, anthelix and orifice, which is even more obvious on the right ear, where there is a strange oblong-shaped cavity. So many abnormalities that testify to hesitations, and maybe even to the re-carving of at least part of the portrait.

P. Capus
