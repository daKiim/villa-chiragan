---
title: Bust of a child
id_inventaire: Ra 124
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: Approximately 130 - 140
materiau: Göktepe marble, district 3 (Turkey)
hauteur: "31"
largeur: "18"
longueur: ''
profondeur: "17"
epaisseur:
id_wikidata: Q24490087
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 120
priority: 3
id_notice: ra-124
image_principale: ra-124-1
vues: 8
image_imprime:
redirect_from:
- ra-124
- ark:/87276/a_ra_124
id_ark: a_ra_124
precisions_biblio:
- clef: balty_les_2012
  note: p. 67-69, fig. 64-67, p. 187-190, 192-194, fig. 113-119, 121-122
- clef: cazes_musee_1999-1
  note: p. 122
- clef: esperandieu_recueil_1908
  note: p. 84, n° 988
- clef: joulin_les_1901
  note: p. 331, pl. XVIII, n° 273 D
- clef: lebegue_notice_1892
  note: p. 415, pl. XXIX, n° 4
- clef: massendari_haute-garonne_2006-1
  note: p. 249, fig. 116
- clef: musee_saint-raymond_image_2011
  note: p. 82-83
- clef: musee_saint-raymond_regard_1995
  note: p. 50, n° 10
- clef: rachou_catalogue_1912
  note: n° 124

---

Despite the large diagonal break on the front part of the skull, which partially disfigures the work, this head is one of the loveliest images of childhood. The thick mop of wavy hair is beautifully offset by the smooth face and very finely modelled naked bust.

Attempting to date such a work is no easy feat. Although the smooth surface of the eyes, which were certainly painted, would seem to exclude it having been made after AD 130, which is when sculptors started engraving irises and pupils, it should be noted that some later portraits were still devoid of such details; the portrait of Antoninus being one such example. The movement imparted to the sickle-shaped strands of hair, the shape of the bust and the still embryonic top of the arms, all contribute towards dating this impressive and beautifully rendered work to the second part of Hadrian's reign. There was a great deal of variety in private portraits at that time, which is why they are difficult to date with any accuracy.

Roman art was very good at rendering the delicate features of the mischievous and often dreamy faces of little children. In affluent families, the images of the latter were set in stone during their lifetime in order to complete a series of sculpture portraits of the entire family, as a way of emulating those of the imperial household (*domus Augusta*) in which children appeared very early on, as shown by the partly extant <a href="/images/comp-ra-124-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-124-1.jpg" alt="Imperial statuary group of Béziers, inv. Ra 226 à Ra 342, musée Saint-Raymond, Christelle Molinié / Wikimedia Commons CC BY-SA"/></span>imperial statuary group of Béziers</a>, or those only mentioned in Thespiae. Unfortunately, the identity of this little boy remains a mystery. We can only assume that he was the son of a high-ranking official of the Roman Empire.

According to J.-C. Balty 2012, *Les portraits romains, 1 : Le siècle des Antonins, 1.2* (*Sculptures antiques de Chiragan (Martres-Tolosane)*, Toulouse, p. 185-194.
