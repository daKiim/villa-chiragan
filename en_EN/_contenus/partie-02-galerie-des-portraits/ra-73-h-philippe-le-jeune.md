---
title: Portrait of Philip the Younger
id_inventaire: Ra 73 h
donnees_biographiques1: 237 - 249
donnees_biographiques2: Co-emperor from 247 to 249. Son of Philip the Arab and Otacilia Severa
type_oeuvre:
date_creation: Between 247 and 249
materiau: Göktepe marble, district 3 (Turkey)
hauteur: '24,5'
largeur: "17"
longueur:
profondeur: "19"
epaisseur:
id_wikidata: Q26707676
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 4
order: 410
priority: 3
id_notice: ra-73-h
image_principale: ra-73-h-1
vues: 8
image_imprime:
redirect_from:
- ra-73-h
- ark:/87276/a_ra_73_h
id_ark: a_ra_73_h
precisions_biblio:
- clef: andreae_art_1973
  note: p. 294-295, fig. 126
- clef: bergmann_studien_1977
  note: p. 36-37, n° 3, pl. 5.1-3
- clef: braemer_les_1952
  note: p. 143-148, pl. III, fig. 4
- clef: braemer_portrait_1982
  note: p. 165, fig. 232
- clef: cazes_musee_1999-1
  note: p. 141
- clef: du_mege_description_1835
  note: p. 124, n° 222
- clef: du_mege_description_1844
  note: n° 381
- clef: du_mege_notice_1828
  note: p. 72-73, n° 138
- clef: esperandieu_recueil_1908
  note: p. 92, n° 1003
- clef: felletti_maj_iconografia_1958
  note: p. 83
- clef: fittschen_bemerkungen_1969
  note: p. 211, pl. 24
- clef: ic.i.l./i_corpus_1888
  note: n°4227
- clef: joulin_les_1901
  note: p. 123, pl. XXIII, n° 296 b
- clef: musee_saint-raymond_cirque_1990-1
  note: n° 14
- clef: musee_saint-raymond_regard_1995
  note: p. 44, n° 6
- clef: rachou_catalogue_1912
  note: n° 73 h
- clef: roschach_catalogue_1865
  note: n° 73 h
- clef: roschach_catalogue_1892
  note: p. 37-38, n° 73 h
- clef: rosso_image_2006
  note: p. 480-482, n° 233
- clef: wegner_verzeichnis_1979
  note: p. 44, 50 pl. 15 a-b
- clef: wiggers_caracalla_1971
  note: p. 199

---

This child, with his sweet and dreamy look, is apparently about ten years old. The very short haircut is typical of fashion during the middle of the 3rd century, and the hair is rendered by means of fine incisions, a sculpting technique developed during the reign of Philip the Arab. (244-249).

This statue portrait was believed to be that of Severus Alexandre (222-235) by some, but mainly it is thought to represent Philip the Younger, owing to its strong similarity with the emperor’s profiles on coins {% cite fittschen_bemerkungen_1969 rosso_image_2006 -L none -l p. 211, fig. 24 -L none -l p. 480-482, fig. 181 %}, on which the small cowlick formed by the fringe at the side of the forehead is sometimes visible. Julius Philippus was elevated to the rank of Augustus in 247, at the tender age of ten. This event is undoubtedly at the origin of the creation of this official portrait, the full cheeks being evidence of his young age. His father Philip the Arab was assassinated in 249 by the Praetorian Guard (soldiers at the disposal of the emperor) or by the soldiers of Decius, his enemy and successor to the throne, and Philip the Younger, who was now twelve, immediately met with the same fate. The two short years during which he was able to exercise his functions as Augustus alongside his father were however sufficient for him to be honoured with a statue, raised in Narbonne by the colony of Béziers, as shown by the extant inscribed plinth {% cite rosso_image_2006 hirschfeld_cil_1888 -l 389-390 -L none -l n°4227 %}.

P. Capus
