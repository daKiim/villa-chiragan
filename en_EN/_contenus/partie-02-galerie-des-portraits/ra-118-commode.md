---
title: Bust de Commodus
id_inventaire: Ra 118
donnees_biographiques1: 161 - 192
donnees_biographiques2: Emperor from 180 to 192
type_oeuvre: Type III, or of the "Vatican, Busti 368" type
date_creation: End of AD 180
materiau: Chorodaki marble, Paros-2 field
hauteur: "44"
largeur: "39"
longueur: ''
profondeur: '23,5'
epaisseur:
id_wikidata: Q24477272
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 180
priority: 3
id_notice: ra-118
image_principale: ra-118-1
vues: 8
image_imprime:
redirect_from:
- ra-118
- ark:/87276/a_ra_118
id_ark: a_ra_118
precisions_biblio:
- clef: balty_les_2012
  note: p. 56, 59, 73-74, fig. 55, 57, 71-73, p. 250, 252-256, fig. 179-180, 182-184, 186-188
- clef: braemer_les_1952
  note: p. 143-148
- clef: cazes_musee_1999-1
  note: p. 127
- clef: esperandieu_recueil_1908
  note: p. 68-69, n° 965
- clef: joulin_les_1901
  note: p. 118-119, pl. XX, n° 283 d
- clef: musee_royal_mariemont_au_2018
  note: III.A.2, p. 309
- clef: musee_saint-raymond_cirque_1990-1
  note: n° 12
- clef: musee_saint-raymond_image_2011
  note: p. 72
- clef: rachou_catalogue_1912
  note: p. 59, n° 118
- clef: rosso_image_2006
  note: p. 465-466, n° 223
- clef: wegner_herrscherbildnisse_1939
  note: p. 272
- clef: wegner_verzeichnis_1979
  note: p. 12-116, 180

---

The sinister Emperor Commodus, son of Marcus Aurelius and Faustina the Younger, was to be the last representative of the Antonine dynasty. He was born in the family *villa*, in *Lanuvium* in the Alban Hills, south of Rome. As the grandson of Antoninus Pius, he spent his childhood at court, and was involved in the workings of the Empire at his father’s behest by the age of sixteen. The twelve and a half-year reign of this emperor who was fascinated by gladiators resulted in a number of iconographic types, eight or nine in fact: three throughout his childhood and five or six throughout adulthood.

Type III, to which the Toulouse bust belongs, was created when the prince became the sole emperor following the death of his father in 180. It is therefore generally dated to the last months of the year. This portrait is the first to show the emperor wearing a full beard that covers his cheeks and chin. Commodus was 19 years old at the time, and therefore an adult. There are currently ten such replicas, now kept in the Sala dei Busti at the Vatican (368, inv. 617, first issue, or "Leitstück", because of its quality and state of conservation), in the Loggia Scoperta, also at the Vatican (25 \[16\]), in Copenhagen (Glyptothek Ny Carlsberg 715, inv. 1901), Liverpool (Ince Blundell Collection 438), Malibu (J. Paul Getty Museum, inv. 92.SA48, formerly at Castle Howard), Mantua (Palazzo Ducale, inv. 6730), which is the closest example to the one in Toulouse in many respects, Naples (Museo nazionale), Paris (Louvre MA 1127), and Rome (Musée des Thermes 699, inv. 124489).

The work at the Musée Saint-Raymond is characterised by the lack of definition of hair and face, a strange blur that gives the impression of an unfinished portrait. Behind the ears, two areas show where parts that were certainly broken when the sculptor attempted to further detail the pavilion of the ear or nape of the neck were once reattached with glue or mortises.

According to J.-C. Balty 2012, *Les portraits romains, 1 : Le siècle des Antonins, 1.2* (*Sculptures antiques de Chiragan (Martres-Tolosane)*, Toulouse, p. 249-257.
