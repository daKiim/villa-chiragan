---
title: Bust of an unknown man
id_inventaire: Ra 165
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: First quarter of the 1<sup>st</sup> century
materiau: Lychnites marble (island of Paros)
hauteur: '43,5'
largeur: "25"
longueur: ''
profondeur: "22"
epaisseur:
id_wikidata: Q28735813
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 1
order: 20
priority: 3
id_notice: ra-165
image_principale: ra-165-1
vues: 8
image_imprime:
redirect_from:
- ra-165
- ark:/87276/a_ra_165
id_ark: a_ra_165
precisions_biblio:
- clef: balty_les_2005
  note: p. 63-67, fig. 38-41, p. 129-142, fig. p. 128-132, 134, 138, 140, 143-144
- clef: bergmann_chiragan_1999
  note: p. 28-30, n° 147
- clef: braemer_art_1963
  note: p. 173, n° 780
- clef: cazes_musee_1999-1
  note: p. 120
- clef: esperandieu_recueil_1908
  note: p. 173, n° 973
- clef: gonse_les_1904
  note: p. 330
- clef: joulin_les_1901
  note: p. 332, pl. XVII, n° 263 E
- clef: labrousse_art_1956
  note: p. 31, n° 33
- clef: massendari_haute-garonne_2006-1
  note: p. 242, fig. 99
- clef: musee_saint-raymond_cirque_1990-1
  note: p. 52-53, n° 8
- clef: musee_saint-raymond_regard_1995
  note: p. 212
- clef: poulsen_probleme_1937
  note: p. 21, n° 2, pl. XXXIX-XL
- clef: pugliese_carratelli_enciclopedia_1997
  note: ''
- clef: queyrel_antonia_1992
  note: p. 79-80
- clef: rachou_catalogue_1912
  note: p. 69, n° 165
- clef: salviat_a_1980
  note: p. 12-15
- clef: vessberg_studien_1941
  note: p. 226
- clef: veyne_kunst_2009
  note: p. 54
- clef: zanker_bildnis_1981
  note: p. 361, n° 39

---

Formerly believed to be that of Julius Caesar, this portrait was designed according to the principles that were characteristic of the ancient Roman Republic, which favoured realism, austerity and gravity. This person was therefore still attached to traditional values. The likeness shares some similarities with another portrait from Ensérune in [Gallia Narbonensis](https://en.wikipedia.org/wiki/Gallia_Narbonensis), also thought to be of Caesar at some point in the past.

The short hairstyle is a good example of this attachment to an earlier fashion with very specific moral values. Generally speaking, the rendering of the sickle or comma-shaped strands of hair of the fringe may reflect the wish to imitate the hairstyles of bronze portraits. Above the right eye, the sculptor has sought to lessen the harshness of the fringe by lifting it from the forehead, using a trepanning tool to create an underlying hollow. Another noteworthy feature of the hair in this portrait: behind the right ear and at the nape of the neck, there is a very unusual oblong-shaped area of spiky hair, which no doubt corresponded to the way in which the model's hair actually grew. In addition to the rendering of the hair, there is also the rounded shape of the bust, which is characteristic of that particular time. These distinctive elements allow us to date this magnificent portrait to the second half of the reign of Augustus or the beginning of the reign of Tiberius; that is to say to the first twenty years of the first century.

According to J.-C. Balty 2005, *Les portraits romains, 1 : Époque julio-claudienne, 1.1* (*Sculptures antiques de Chiragan (Martres-Tolosane)*, Toulouse, p. 127-144.
