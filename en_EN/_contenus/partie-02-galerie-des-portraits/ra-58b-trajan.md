---
title: Bust of Trajan
id_inventaire: Ra 58 b
donnees_biographiques1: 53 - 117
donnees_biographiques2: Emperor from AD 98 to 117
type_oeuvre: Known as "of the Decennalia" type
date_creation: Approximately AD 108
materiau: Göktepe marble, district 3 (Turkey)
hauteur: "41"
largeur: '26,5'
longueur: ''
profondeur: "24"
epaisseur:
id_wikidata: Q24302048
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 60
priority: 3
id_notice: ra-58-b
image_principale: ra-58-b-1
vues: 8
image_imprime:
redirect_from:
- ra-58-b
- ark:/87276/a_ra_58_b
id_ark: a_ra_58_b
precisions_biblio:
- clef: balty_les_2012
  note: p. 22-23, fig.10-11, p. 92-98, 100-101, fig. 12-13, 15-16, 19-21, 24-26, p. 260, fig. 191
- clef: balty_nouveau_1977
  note: n° 109 p. 60
- clef: braemer_les_1952
  note: p. 143-148
- clef: centre_culturel_abbaye_de_daoulas_rome_1993
  note: n° 36.01
- clef: du_mege_description_1835
  note: p. 111, n° 195
- clef: du_mege_description_1844
  note: n° 359-361
- clef: du_mege_notice_1828
  note: p. 63, n° 122
- clef: esperandieu_recueil_1908
  note: p. 64-65, n° 958
- clef: joulin_les_1901
  note: p. 115 et pl. XVII, n° 262 b
- clef: massendari_haute-garonne_2006-1
  note: p. 243, fig. 103
- clef: musee_saint-raymond_cirque_1990-1
  note: n° 11
- clef: musee_saint-raymond_image_2011
  note: p. 20
- clef: musees_departementaux_de_loire-atlantique_tresor_1987
  note: p. 75, n° 94
- clef: rachou_catalogue_1912
  note: p. 42, n° 58 b
- clef: roschach_catalogue_1865
  note: n° 58 b
- clef: roschach_catalogue_1892
  note: p. 32, n° 58 b
- clef: rosso_image_2006
  note: p. 451-453, n° 215

---

After the assassination of Emperor Domitian on the 18<sup>th</sup> September 96, and hence the end of the Flavian dynasty, the reign of Nerva (96-98) represented a short but necessary transition in the history of the Principate. His reign brought about the Antonine dynasty, named after Antoninus Pius, who, more than any other emperor, symbolised the stability of the Empire. The new imperial house opted for a system based on selecting the best candidate rather than simply favouring blood ties. Unfortunately, Marcus Aurelius, the penultimate representative of this artificial lineage, later chose to go back to the old ways. After Nerva, power was concentrated in the hands of six emperors: Trajan, Hadrian, Antoninus Pius, Marcus Aurelius (associated for a time with Lucius Verus) and Commodus.

Trajan came from a family of Italian settlers who moved to the province of Hispania Baetica (current Andalusia) in *Italica* (modern-day Santiponce, in the province of Seville). Having been adopted by Nerva, he married Pompeia Plotina, whose family was from Nîmes. The changes to his image, as depicted on sculptures, were dictated by the major events that occurred during his reign. Four types of portraits are recognised today, often differentiated only by the different arrangement of stands of hair.

This copy is the work of one of the best workshops in Rome, as evidenced by the lifelike strands on the forehead and around the temples, and the sensitively modelled auricles and earlobes. The large pitted area at the base of the neck indicates that this bust was originally clothed in a *paludamentum* (the military cloak of generals and emperors). The head, vigorously turned to the right, is proof of the emperor’s energy and moral strength.

The series to which this portrait belongs shows, for the first time, the beginning of the arms, very clearly marked by an incision or a slight depression that distinguishes them from the general shape of the torso. This decisive step towards creating a more natural depiction has been furthered by accentuating the chest muscles. This very unusual iconographic type is portrayed on the reliefs of Trajan’s Column in Rome and must therefore predate 113. It has long been linked to the celebration of the *decennalia* (ten years of reign) in 108. Some, however, have chosen to link its creation to the second triumph over Dacia in 107.

According to J.-C. Balty 2012, *Les portraits romains, 1 : Le siècle des Antonins, 1.2* (*Sculptures antiques de Chiragan (Martres-Tolosane)*, Toulouse, p. 92-101.
