---
title: Portrait of a child
id_inventaire: Ra 167
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: AD 240s
materiau: Göktepe marble, district 4 (Turkey)
hauteur: "28"
largeur: "21"
longueur: ''
profondeur: "15"
epaisseur:
id_wikidata: Q27712278
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 4
order: 390
priority: 3
id_notice: ra-167
image_principale: ra-167-1
vues: 8
image_imprime:
redirect_from:
- ra-167
- ark:/87276/a_ra_167
id_ark: a_ra_167
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 141
- clef: esperandieu_recueil_1908
  note: p. 88, n° 997
- clef: joulin_les_1901
  note: p. 219-300, XXV pl., fig. 274 E
- clef: rachou_catalogue_1912
  note: n° 167

---

This fine portrait is unfortunately badly damaged, broken horizontally at jaw level. Both parts have since been reunited, and the break has been filled in, although the tip of the nose, chin and left cheek are now lost.

This child has been connected to the female portrait that was believed to depict Otacilia Severa, wife of Emperor Philip the Arab, or, more probably, Tranquillina, who was married to Gordian III ([*inv*. Ra 166](/en/ra-166)). This presumed relationship is based on a few aspects that are common to both works: the pleated fabric of the tunic, the small round buttons that fasten it above the shoulder, and the double dot of the pupils of the eyes. However, although the female portrait is indeed that of Tranquillina, it is difficult to associate this little boy with her, as the imperial couple had no children.

D. Cazes
