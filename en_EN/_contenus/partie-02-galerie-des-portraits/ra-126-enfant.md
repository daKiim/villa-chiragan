---
title: Portrait of a child
id_inventaire: Ra 126
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: Approximately 150 - 160
materiau: Göktepe marble, district 3 (Turkey)
hauteur: "20"
largeur: '17,5'
longueur: ''
profondeur: "18"
epaisseur:
id_wikidata: Q24489352
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 2
order: 130
priority: 3
id_notice: ra-126
image_principale: ra-126-1
vues: 8
image_imprime:
redirect_from:
- ra-126
- ark:/87276/a_ra_126
id_ark: a_ra_126
precisions_biblio:
- clef: cazes_marbres_2001
  note: p. 19 n°15
- clef: balty_les_2012
  note: p. 70-72, fig. 68-70, p. 245-247, fig. 174-178
- clef: esperandieu_recueil_1908
  note: p. 85, n° 991
- clef: eydoux_france_1962-1
  note: p. 186, fig. 204
- clef: fittschen_prinzenbildnisse_1999
  note: p. 92, n° 90, pl. 170 a, b, c, d
- clef: joulin_les_1901
  note: p. 120, n° 288, pl. XXI, n° 288 D
- clef: musee_saint-raymond_image_2011
  note: p. 82-83
- clef: musee_saint-raymond_marbres_2007
  note: p. 26-27
- clef: rachou_catalogue_1912
  note: p. 61, n° 126
  
---

Once again, as in the child's portrait that dates back to Hadrian's time ([*inv*. Ra 124](/en/ra-124)), the abundant mass of hair bears witness to the expertise of a sculptor who was highly skilled in reproducing curls. These sophisticated wavy locks contrast with the smooth face and its highly polished marble surface.

This work, which is similar to a head in Trier (Germany), could be that of a young prince. Perhaps he was one of the first two sons of Marcus Aurelius and Faustina the Younger, Titus Aelius Antoninus, born in 152, or Titus Aelius Aurelius, born in 158, both of whom died in infancy. Yet in the absence of a replica from an official context, there is no evidence of this.

According to J.-C. Balty 2012, *Les portraits romains, 1 : Le siècle des Antonins, 1.2* (*Sculptures antiques de Chiragan (Martres-Tolosane)*, Toulouse, p. 243-248.
