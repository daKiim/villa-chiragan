---
title: Head of an unknown man
id_inventaire: Ra 69
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: Approximately 215 - 220
materiau: Göktepe marble, district 3 (Turkey)
hauteur: "30"
largeur: "19"
longueur: ''
profondeur: "22"
epaisseur:
id_wikidata: Q27096208
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 330
priority: 3
id_notice: ra-69
image_principale: ra-69-1
vues: 8
image_imprime:
redirect_from:
- ra-69
- ark:/87276/a_ra_69
id_ark: a_ra_69
precisions_biblio:
- clef: balty_les_2021
  note: p. 27-36, 239-243, fig. 20, 23
- clef: braemer_les_1952
  note: p. 143-147
- clef: du_mege_description_1835
  note: n° 217
- clef: du_mege_notice_1828
  note: n° 135
- clef: esperandieu_recueil_1908
  note: n° 974
- clef: joulin_les_1901
  note: n° 313 E
- clef: meischneir_privatportrats_1984
  note: p. 319-351
- clef: rachou_catalogue_1912
  note: n° 69
- clef: roschach_catalogue_1892
  note: n° 69

---

This somewhat cubic and vigorously-shaped head, with its very particular expression, its hooded eyes beneath wide, open eyebrows, its right-looking, sideward glance, and its profile, with its low and rounded forehead, marked bridge of the nose, and short and receding lower lip, never fail to strike the viewer. The hair, laying very flat on the head, is combed into long strand that end in two points: a prelude to the *"a penna"* technique adopted between 215 and the 250s. All of this, along with the relatively short moustache, and the curly beard trimmed fairly low on the chin, all refer to a type of hairstyle and fashion that are characteristic of the middle and end of the Severan dynasty, of which a portrait, once displayed in the park of Villa Borghese, and now kept in the reserves of the Capitoline Museums (inv. 3387), provides a good example, and constitutes a precious stylistic parallel for the head in Toulouse.

On the other hand, the "2. Thronfolgertypus" (or "Consulatstypus") type portraits of Caracalla, dating back to 208 and later, with their albeit much shorter hairstyles and curly beards, also serve as a useful chronological marker. Several portraits produced during the second quarter of the same century have also been portrayed with their eyes looking sideward, their pupils and irises resolutely depicted in the corners of their eyes in a somewhat "expressionist" manner, which, as has often been said, is an expression of anxiety and even anguish in times of crisis, such as those in which the "soldier emperors" (Soldatenkaiser) had plunged the Empire.

According to J.-C. Balty 2020, *Les Portraits Romains : L’époque des Sévères*, Toulouse, p. 239-243.
