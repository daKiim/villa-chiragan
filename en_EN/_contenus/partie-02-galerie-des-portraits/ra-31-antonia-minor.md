---
title: Head of Antonia Minor
id_inventaire: Ra 31
donnees_biographiques1: 36 BC – AD 37
donnees_biographiques2: "Daughter of Mark Antony and Octavia Minor (sister of Augustus), spouse of Drusus the Elder (brother of Tiberius), mother of Germanicus and Claudius, Emperor of Rome"
type_oeuvre: Known as "of the Schläfenlöckchen type" (with sidelocks)
date_creation: Between AD 37 and 54
materiau: Lychnites marble (island of Paros)
hauteur: "27"
largeur: '21,2'
longueur: ''
profondeur: "21"
epaisseur:
id_wikidata: Q26721238
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 1
order: 30
priority: 3
id_notice: ra-31
image_principale: ra-31-1
vues: 8
image_imprime:
redirect_from:
- ra-31
- ark:/87276/a_ra_31
id_ark: a_ra_31
precisions_biblio:
- clef: balty_les_2005
  note: p. 51-52, fig. 24-25, p. 147-160, fig. p. 146-149, 151 (n° 77), 152 (n° 79), 153 (n° 81), 156 (n° 83), 161
- clef: balty_portraits_1995
  note: p. 107, n° 104
- clef: boschung_gens_2002
  note: p. 129, 131, n° 45.3, pl. 95.3-4
- clef: du_mege_description_1835
  note: p. 84, n° 158
- clef: du_mege_notice_1828
  note: p. 36, n° 71
- clef: fittschen_katalog_1977
  note: p. 61, n° 7, n° 4 (à propos du n° 19)
- clef: joulin_les_1901
  note: p. 93, n° 73
- clef: kunzl_antonia_1997
  note: p. 459, cat. B8, p. 488-489
- clef: massendari_haute-garonne_2006-1
  note: p. 242, fig. 101
- clef: queyrel_antonia_1992
  note: p. 69-77
- clef: rachou_catalogue_1912
  note: p. 31, n° 31
- clef: rosso_image_2006
  note: n° 211
- clef: rosso_portrait_2000
  note: p. 321, fig. 10

---

This head, which is believed to be that of Antonia Minor, was probably added to the collection in Toulouse during the first half of the 19th century. It was completed and mounted on a specially-designed stucco bust. There are no documents to prove that this head was actually found in Chiragan.

As the niece of Augustus and the mother of Germanicus, a much-loved prince, Antonia was bound to enjoy a glowing reputation. The portrait of this princess, who was famed for her beauty, was widely distributed throughout the Empire, from 16 or 15 BC up until after her death, during the reign of Claudius (AD 41-54). She was to take over as the priestess at the head of the cult for the deified Augustus, thus succeeding Empress Livia, who died in the year AD 29. As sister-in-law to Tiberius, Antonia had informed the latter of the plot against him fomented by Sejanus, the prefect of the Praetorian Guard, in the autumn of 31. The emperor therefore showed his gratitude by erecting sculptures and associated dedications. But it was during the reign of her son Claudius that she was paid the greatest number of tributes, testifying to her key role within the imperial house. Antonia was therefore the most honoured princess in Gaul, as evidenced by the series of portraits and dedications discovered in Nîmes, Cimiez, Béziers, Chiragan and Vienne. In the latter, a colossus of the princess, probably posthumous, wearing a diadem, was unearthed in the theatre.

The head in Toulouse, which is unfortunately very damaged, was carved from oriental Greek marble, probably in a workshop in Rome. It shows Antonia's usual simple hairstyle, coiffed in waves on either side of a centre parting, which is characteristic of ideal Greek figures, a style that became widespread in the art of portraiture, especially from the beginning of the reign of Augustus. But here the hair is held in place by a plain headband that seems to be twisted: perhaps intended as a simple ornament or as a symbol of the status of the priestess of the cult of the deified Augustus, as endorsed by Antonia, according to Emmanuelle Rosso. {% cite rosso_image_2006 –locator 446 %}. Finally, wavy strands of hair had been glued to the back of the head, and held in place by a bow. This type of add-on was common in Hellenistic sculpture during the last centuries BC.

All these characteristics (wavy and slightly loose strands of hair, the centre parting and bow), which were not only visible in provincial dynastic groups but also disseminated through the coins minted with the image of Antonia, resulted in a genuine fashion among women during the reign of Tiberius. Drusus’s wife was indeed seen as a model of wisdom and virtue, echoed in this understated hairstyle, which was preferred to the hairstyles that relied on complex braids and locks of hair that covered the forehead, all of which had been popular during the reign of Augustus.

According to J.-C. Balty 2005, *Les portraits romains, 1 : Époque julio-claudienne, 1.1* (*Sculptures antiques de Chiragan (Martres-Tolosane)*, Toulouse, p. 145-161.
