---
title: Heroic bust of a mature man
id_inventaire: Ra 70
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 2\<sup>nd\</sup> century
materiau: Göktepe marble, district 3 (Turkey)
hauteur: '55,5'
largeur: "43"
longueur:
profondeur: "23"
epaisseur:
id_wikidata: Q28732552
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 190
priority: 3
id_notice: ra-70
image_principale: ra-70-1
vues: 8
image_imprime:
redirect_from:
- ra-70
- ark:/87276/a_ra_70
id_ark: a_ra_70
precisions_biblio:
- clef: balty_les_2021
  note: p. 36-38, 169-175, fig. 25, 26
- clef: cazes_musee_1999-1
  note: p. 134
- clef: du_mege_description_1835
  note: n° 221
- clef: du_mege_notice_1828
  note: n° 136
- clef: esperandieu_recueil_1908
  note: n° 982
- clef: fittschen_bildnis_1980
  note: p. 108-114
- clef: gelzer_eikones:_1980
  note: ''
- clef: joulin_les_1901
  note: n° 306
- clef: nocentini_sculture_1965
  note: p. 68-71, fig. pl. XVI, XVII
- clef: rachou_catalogue_1912
  note: n° 70
- clef: roschach_catalogue_1892
  note: n° 70
  
---

The heroic and naked bust, and the *paludamentum* folded over the left shoulder and held in place by a round fibula in the shape of a six-petalled rosette, is part of a tradition that dates back to the very last years of the 1st century AD, and often included a baldric worn obliquely over the torso. The oldest examples are two busts depicting Domitian in Leipzig (now destroyed) and Toledo (USA) and, among the private portraits, a bust at the Musei capitolini (inv. 678), usually dated to the end of Domitian's reign. The bust in Toulouse, with its "breastplate" shape - without the slightest indication of where the arms begin - the equivalent of which is only found at the very beginning of the 2nd century, is therefore somewhat surprising for its time, and seems to bear witness to a return to ancient forms that were generally not popular at that time.

The extreme realism of the features of this magnificent portrait in Toulouse is astonishing, as evidenced by the noticeable baldness (with a small isolated tuft of hair just in front of the top of the head), the wrinkled forehead, the frowning eyebrows, and the quite extraordinary modelling of the area around the eyes, with its "crow's feet" (on the right eye only) and bags, the naso-labial folds that extend on the left side into two additional folds at the beginning of the beard, as well as the certain lack of symmetry in the expression. The sensitive gaze, combined with the head turned to the right, seems to reflect a rich inner life and great intellectual vitality. K. Fittschen emphasised the similarity between this work and <a href="/images/comp-ra-70-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-70-1.jpg" alt="Bust, Bardini Florence Museum, Sailko / Wikimedia Commons CC BY-SA"/></span>a portrait</a>, the bust of which is clothed, that is kept at the Bardini Museum in Florence {% cite fittschen_bildnis_1980 -l 113 %}.

Could this bust discovered in Chiragan be that of an intellectual? Are the frowning eyebrows – meant to convey the individual’s concentration - and baldness, enough to make him so? The *paludamentum* folded over the left shoulder refers to the 'military' world, or, perhaps more accurately, to those high-ranking officials of the Empire whose duties can be both military and administrative. At the end of the 2nd century, culture was indeed one of the values that conferred real prestige, and granted access to lofty functions at the top of the Roman state: the great jurists in particular, many of whom were members of the Emperor's Council under Septimius Severus and Severus Alexander, and some of whom were appointed Prefects of the Praetorian guard, belonged to this category.

According to J.-C. Balty 2020, *Les Portraits Romains : L’époque des Sévères*, Toulouse, p. 169-175.
