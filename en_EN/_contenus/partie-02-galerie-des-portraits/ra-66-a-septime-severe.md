---
title: Bust of Septimius Severus wearing a cuirass
id_inventaire: Ra 66 a
donnees_biographiques1: 145 - 211
donnees_biographiques2: Emperor from 193 to 211
type_oeuvre: of the "adoption" type
date_creation: Between 195 and 203
materiau: Göktepe marble, district 3 (Turkey)
hauteur: '73,5'
largeur: "66"
longueur:
profondeur: "36"
epaisseur:
id_wikidata: Q26703870
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 210
priority: 3
id_notice: ra-66-a
image_principale: ra-66-a-1
vues: 8
image_imprime:
redirect_from:
- ra-66-a
- ark:/87276/a_ra_66_a
id_ark: a_ra_66_a
precisions_biblio:
- clef: balty_les_2012
  note: p. 45, fig. 44, p. 264, fig.196
- clef: balty_les_2021
  note: 'p. 14-16, 94-104, fig. 6-7 '
- clef: balty_nouveau_1962
  note: p. 190-191, pl. XXXV 9-10
- clef: bernoulli_romische_1882
  note: vol. II-3, p. 26, n° 57
- clef: braemer_les_1952
  note: p. 145-146
- clef: cazes_musee_1999-1
  note: p. 129
- clef: centro_de_exposiciones_arte_canal_roma_2007
  note: n° 23, p. 104-105
- clef: clarac_musee_1841
  note: p. 587
- clef: du_mege_description_1835
  note: p. 119, n° 212
- clef: du_mege_description_1844
  note: n° 373
- clef: du_mege_notice_1828
  note: p. 68-69, n° 132
- clef: esperandieu_recueil_1908
  note: p. 79-80, n° 981
- clef: felletti_maj_museo_1953
  note: p. 128
- clef: heintze_studien_1966
  note: p. 198, n° 42, n° 4
- clef: joulin_les_1901
  note: p. 121-337, n° 299, pl. XXIII
- clef: lorange_apotheosis_1947
  note: p. 141, n° 11
- clef: mccann_portraits_1968
  note: p. 147-148, n° 34, pl. XLV
- clef: musee_royal_mariemont_au_2018
  note: p. 84, fig. 5
- clef: musee_saint-raymond_regard_1995
  note: p. 174
- clef: rachou_catalogue_1912
  note: p. 44-45, n° 66 a
- clef: roschach_catalogue_1865
  note: n° 66 a
- clef: roschach_catalogue_1892
  note: p. 34-35, n° 66 a
- clef: rosso_image_2006
  note: p. 470-471, n° 226
- clef: soechting_portrats_1972
  note: p. 167-168, n° 52
- clef: ventura_il_1997
  note: p. 69, fig. 105
---

As with Trajan, no less than four portraits of Septimius Severus were discovered in Chiragan, which is quite exceptional. The Roman workshop that created this head may also be the author of two others of the same type, kept in Copenhagen (Ny Carlsberg Glyptotek) and Rome (Palazzo Massimo alle Terme). Compared to another "accession" type head found in Chiragan, this sculpture portrait tends to idealise the features, and sports a higher hairline on either side of the fringe.

This representation belongs to the so-called "adoption" type, because of the very great similarity between its face and those of portraits of Antoninus Pius and Marcus Aurelius. This is an example of the assimilation ("Angleichung") that some emperors adopted for political purposes in order to resemble an earlier model as closely as possible. In 195, Septimius Severus proclaimed himself to be the son of Marcus Aurelius, and therefore brother of Commodus. As on the coins, the profile of the marble portrait of Septimius betrays his desire to be formally associated with these exemplary Antonine emperor rulers, namely the type that depicted the cuirass-wearing Marcus Aurelius, a copy of which was also discovered in the grounds of the *villa*.

Chiragan's head was re-mounted - apparently as early as antiquity - on a bust that originally belonged to another. In order to hide the connection as much as possible, the head was severed immediately below the locks of hair on the nape of the neck, and under the beard, and the neck of the original bust was trimmed in two divergent directions (∧) to form an angle, so that the head could be fitted onto these two inclined planes. This technical process is well known, and was used on the statue portrait of Septimius Severus in the Louvre (inv. Ma 1115) among others. But it is difficult to determine the reasons for it being done: could it be a simple repair following a breakage or a workshop accident? Or was the bust reused as a result of a condemnation of memory (*damnatio memoriae*)?

This type of scaled cuirass has often been considered characteristic of the soldiers of the Praetorian Guard. In terms of imperial iconography, these scaled breastplates may well have been chosen only by emperors wishing to find favour with this elite body of men capable of bringing about and undoing so many reigns. Yet Septimius Severus clearly hated this corps, and had in fact ridiculed it in 193 as soon as he entered Rome, by ordering "the Praetorians to come to meet him, unarmed, and wearing only tunics", and then by summoning them "in the same uniform to the command rostra, surrounded on all sides by armed men" {% cite noauthor_histoire_nodate -L none -l v. Sev. 6.11 %}. Once in the city, "in arms and escorted by armed soldiers, he went up to the Capitol from where, in the same attire, he advanced on to the palace, preceded by the standards which he had confiscated from the Praetorians, and which were carried upside down" (ibid. 7.1). It is therefore surprising to find a bust of Severus wearing such a particularly connoted cuirass, even though it is also to be found on sestertii issued in 195.

This type of scaled cuirass is, on the other hand, only rarely chosen for imperial statuary. Yet it is depicted on several and often small gold, silver or bronze busts, which indisputably testify to a different iconographic tradition - also confirmed by coins.

Could this scaled breastplate have belonged to a portrait of Plautus? It is possible that after his disgrace and execution, some of his busts were reused for portraits of Septimius Severus. There is no way of proving this, however. The fact that such a breastplate has been used for a statue portrait of Severus is therefore difficult to explain.

Two iconographic details still distinguish the bust in Toulouse from the parallels that can be associated with it. The *paludamentum*, folded over and pinned on the left shoulder, falls below the lower limit of the armour, to half the height of the pedestal. This detail is one of the elements in the evolution of busts in the history of Roman portraiture that resulted in these "half-body busts" (Halbkörperbüste) produced during the second quarter of the 3rd century. The oldest examples of *paludamenta* with this particular arrangement are to be seen on busts of Antoninus Pius (Naples, Mus. nat. 6031), Marcus Aurelius (Rome, the Capitoline Museum, Imp. 28, inv. 448), Lucius Verus (Paris, Musée du Louvre MA 1101), as well as the one that was reworked to fit the type III portrait of Septimius Severus, also from Chiragan. ([[inv. Ra 66b]{.ul}](../../../../../../..//promolang/home/Sauvegarde/ra-66-b)). They are therefore relatively rare. More exceptional still is the representation of the fringe that edged the cloak, shown here, somewhat unexpectedly, on the edge of the bust, under the bulk of the *paludamentum*.

Finally, it is worth noticing the back of the bust, which is particularly neat and perfectly smooth, and the curved sides of the central base, which has been designed and carved with great care.

According to J.-C. Balty 2020, *Les Portraits Romains : L’époque des Sévères*, Toulouse, p. 94-104.
