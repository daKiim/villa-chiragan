---
title: Bust of Septimius Severus wearing a cuirass
id_inventaire: Ra 66 b
donnees_biographiques1: 145 - 211
donnees_biographiques2: Emperor from 193 to 211
type_oeuvre: of the "Serapis" type
date_creation: Between 203 and 211; maybe as early as 199/200
materiau: Dokimium marble (Turkey)
hauteur: "78"
largeur: "64"
longueur: ''
profondeur: "33"
epaisseur:
id_wikidata: Q25221272
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 220
priority: 3
id_notice: ra-66-b
image_principale: ra-66-b-1
vues: 8
image_imprime:
redirect_from:
- ra-66-b
- ark:/87276/a_ra_66_b
id_ark: a_ra_66_b
precisions_biblio:
- clef: attanasio_marbles_2019
  note: ''
- clef: balty_les_2012
  note: p. 43, fig. 41, p. 265, 268, fig. 197, 203
- clef: balty_les_2021
  note: 'p. 12-14, 106-116, fig.3-5 '
- clef: bergmann_kaiserzeitlichen_2007
  note: p. 332
- clef: bernoulli_romische_1882
  note: p. 26, n° 58
- clef: braemer_les_1952
  note: p. 146
- clef: cazes_musee_1999-1
  note: p. 128
- clef: du_mege_description_1835
  note: p. 120, n° 213
- clef: du_mege_description_1844
  note: n° 373
- clef: esperandieu_recueil_1908
  note: p. 75, n° 976
- clef: heintze_studien_1966
  note: p. 198, n° 42, n° 2
- clef: joulin_les_1901
  note: p. 337, n° 293, pl. XXII
- clef: lexicon_1990
  note: p. 692
- clef: massendari_haute-garonne_2006-1
  note: 'p. 245, fig. 108 '
- clef: mccann_portraits_1968
  note: p. 172-173, n° 84, pl. LXXVa
- clef: musee_saint-raymond_essentiel_2011
  note: p. 46-47
- clef: musee_saint-raymond_regard_1995
  note: p. 177
- clef: rachou_catalogue_1912
  note: p. 45, n° 66 b
- clef: raeder_bildnis_2019
  note: ''
- clef: roschach_catalogue_1865
  note: n° 66 b
- clef: roschach_catalogue_1892
  note: p. 35, n° 66 b
- clef: rosso_image_2006
  note: p. 473-474, n° 228
- clef: soechting_portrats_1972
  note: p. 213, n° 118

---

This portrait has been fitted to a bust that originally belonged to a completely different emperor. Marianne Bergmann has noted the lack of proportion between the size of the civic crown (which is unusual, and even rare among statue portraits designed by workshops in Rome) and the face {% cite bergmann_kaiserzeitlichen_2007 -L none -l p. 332 %}. It is also worth noting the abnormal size of the ears, which, along with the civic crown, belonged to the initial bust. When viewed in profile, this discrepancy is particularly striking, as the front part of the crown clearly protrudes beyond the outline of the face, and the recut is clearly visible along the lower edge of the crown, at the front. The neck is also very thick compared to the overall volume of the head, although in profile, it has been trimmed at the back to remove the protruding part of the cuirass that protects the upper part of the spine.

Several portraits of Antoninus Pius, four to be precise, all created by workshops in Rome, sport the same sort of crown. To the left of the nape of the neck, the two or three superimposed strands combed towards the left that extend a large S-shaped strand of hair (almost centred above the neck) are reminiscent of those seen in portraits of Antoninus. It would therefore seem that a bust of this same emperor was reused for this portrait in Toulouse. This certainly does not mean that the emperor's image was "reappropriated" after having been "condemned from memory" (*damnatio memoriae*), but rather that a bust produced in excess during a previous reign almost half a century earlier, and left unused in the reserves of the Roman workshop, was simply recycled. Given the quality of the portrait, and the parallels it suggests, it is highly unlikely that the transformation was made in Gallia Narbonensis. Other portraits of "good emperors", who were therefore not rejected as a result of a *damnatio memoriae,* apparently suffered the same fate, albeit at a much later date, and for very different reasons. The rapid succession of reigns throughout the 3rd century certainly resulted in portraits being reused when the unexpected proclamation of a new emperor meant that they could not therefore be distributed throughout the various provinces. It remains to be explained, however, why a bust of Antoninus Pius was still lingering in a Roman workshop in the very early years of the 3rd century. It is only by discovering similar analogies that the real answer to this question will emerge.

The type to which this third portrait of Septimius Severus from Chiragan belongs includes the most characteristic and numerous images of the emperor. To date, there are almost 90 known specimens. They are easily recognisable thanks to the four corkscrew locks that fall down onto the forehead, and have been likened to images of the god Serapis, created in Alexandria at the very beginning of the Hellenistic period, and generally attributed to the sculptor Bryaxis. It is therefore known as the "Serapis" type.

There is little doubt that Severus was anxious to portray himself as some sort of god: according to the biographer of the *Historia Augusta*, after his stay in Alexandria on his return from his Parthian expedition (late 199 - spring 200), Septimius Severus constantly "spoke of the pleasure he derived from this journey, which enabled him to become acquainted with the cult of Serapis, to contemplate ancient monuments, and to see new animals and places" {% cite noauthor_histoire_nodate -L none -l v. Sev. 17, 4 %}. Serapis is a universal *cosmocrator*, or master of the universe. As the dynastic god of the Lagid rulers, "who guaranteed prosperity during reigns and victory in battles" {% cite noauthor_lexicon_1994 -L none -l p. 692 %}, he served the same function for certain emperors. Following his trip to the *Serapeum* of Alexandria, Septimius Severus could not have chosen a better protective divinity, and he had certainly already heard of Serapis. In Leptis Magna, as in his hometown, and in Sabratha, major sanctuaries were devoted to the god of Alexandria, whose cult had extended to the *emporia* of Tripolitania as early as the Hellenistic period.

With its iconography heralded by the "adoption" type of sculpture that linked Severus to the emperor-philosopher Marcus Aurelius, this "Serapis" type was a decisive step in the representation of the sovereign. In particular, it marked the advent, at the head of the Empire, of men who now originated from the African or Eastern provinces, and their respective gods. Serapis did not replace Jupiter. On the contrary, this merger merely made him twice as powerful. The dual attribute of "master of the universe" and "invincible" fully justified Severus’s assimilation ("Angleichung") with the god of Alexandria, as he himself had just ousted those vying for the Empire, and now reigned over all the provinces, proud of the harmony he believed now sealed the future of a new dynasty. It should be noted, however, that this assimilation did not make the emperor a god.

In spite of the substantial re-sculpting of the entire face, this portrait is one of the best of all the portraits of the Emperor to have survived. What’s more, it has not been subjected to any form of modern restoration (except for the tip of the nose, since removed, where the smoothed surface to which the prosthesis was attached is still visible).

According to J.-C. Balty 2020, *Les Portraits Romains : L’époque des Sévères*, Toulouse, p. 106-116.
