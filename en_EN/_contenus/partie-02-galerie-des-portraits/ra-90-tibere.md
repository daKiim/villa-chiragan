---
title: Head of Tiberius wearing a crown of oak leaves
id_inventaire: Ra 90
donnees_biographiques1: 42 – 37 BC
donnees_biographiques2: Emperor from 14 to 37
type_oeuvre: A reversed version (fringe strands) of the "Berlin-Naples-Sorrente" type
date_creation: Last decade of reign (AD 26/27-31)?
materiau: Lychnites marble (island of Paros)
hauteur: "34"
largeur: "18"
longueur: ''
profondeur: '22,2'
epaisseur: 
id_wikidata: Q26707653
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 1
order: 40
priority: 3
id_notice: ra-90
image_principale: ra-90-1
vues: 8
image_imprime: 
redirect_from:
- ra-90
- ark:/87276/a_ra_90
id_ark: a_ra_90
precisions_biblio:
- clef: balty_les_2005
  note: p. 164-179, n° 6
- clef: cazes_musee_1999-1
  note: p. 118, fig. 120
- clef: centro_de_exposiciones_arte_canal_roma_2007
  note: p. 100-101, n° 21
- clef: esperandieu_recueil_1908
  note: p. 95-96, n° 1012
- clef: felletti_maj_claudio_1959
  note: p. 706
- clef: fittschen_katalog_1985
  note: p. 15, n° 13
- clef: gascou_presence_1996
  note: p. 47-49, fig. 28-30
- clef: hausmann_bemerkungen_1985
  note: 'p. 222 exemplaire "u" '
- clef: hausmann_redeat_1988
  note: 'p. 332 exemplaire "q" '
- clef: leon_alonso_retratos_2000
  note: ''
- clef: rachou_catalogue_1912
  note: p. 53, n° 90
- clef: rosso_image_2006
  note: p. 443-445, n° 211
- clef: salviat_a_1980
  note: p. 28, 30, fig. p. 27
- clef: terrer_nouvel_2003
  note: p. 72 (à propos du n° 143)

---
First mentioned in a catalogue published in 1912, where it was associated with the works unearthed during the Chiragan excavations in 1826 and 1828, this head has not been referred to in any other source. Léon Joulin himself did not mention it, nor did he draw it in his work published in 1901. As a result, and pending a conclusive testimony, one can only remain cautious when it comes to linking this portrait of Tiberius to the *villa*.

Born in 42 BC, to Livia and Tiberius Claudius Nero, Tiberius belongs to the gens Claudia, a family of prestigious lineage. He was three years old when his mother remarried, taking for husband Octavian, Julius Caesar's heir and member of the Second Triumvirate alongside Mark Antony and Marcus Aemilius Lepidus. Following the deaths of all his presumptive heirs, Octavian, who now bore the name of Augustus, was forced to adopt Livia’s son, thus associating the name of the Claudii with the people of the Julii.

The characteristics chosen to describe this heir, who ascended the throne at the age of 55, seem to be diametrically opposed, both psychologically and politically, to those of his stepfather. Indeed, the portraits remain dominated by an uncompromising and objective description far removed from the Greek ideals of his stepfather's representations. However, the arrangement of fringe strands, borrowed from the Augustus of Prima Porta type, betrays his need to appropriate certain details from the images inherent to the previous reign, proof of the sovereign's loyalty to this regime, and an expression of dynastic longevity. The Toulouse head is highly original in this sense: while the strands of hair are identical to those of the main portraits of the "Berlin-Naples-Sorrente" type, they have nevertheless been reversed, and this is something for which we have no real explanation.

It is well known that this character had a complex personality. Fleeing the venomous atmosphere of the capital in AD 27, he went into exile on the island of Capri, where he remained until his death. Chiragan's head is based on a prototype that was created very late; Tiberius was between 65 and 75 years old at the time, and had therefore not lived in Rome for a long time. Yet he had never before been depicted wearing a chaplet of oak leaves. The prince is said to have refused to hang this mark of distinction in the entrance court of his palace. This emblem, which evokes the emperor's clemency, and equates him with Jupiter, is also associated with the title of "Father of the Fatherland" (*Pater Patriae*), an honour challenged by Tiberius himself {% cite tacite_annales_nodate suetone_tibere_nodate dion_cassius_histoire_nodate -L none -l 2, 87 -L none -l 26,4 et 67,2 -L none -l 57, 8, 1 %}.

Part of the crown on the head of the statue in the Musée Saint-Raymond was probably broken from the outset, maybe during the carving of the oak leaves, which is a very delicate operation for the sculptor. This explains the large mortise, located on the right-hand side, which corresponds to a repair and the insertion of a prosthesis. A similar accident explains the hollowed-out <a href="/images/comp-ra-90-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-90-1.jpg" alt="Portrait of Tiberius, Tarragone National Archaeological Museum, Francis Raher / Wikimedia Commons CC BY"/></span>portrait of the emperor discovered in Tarragona</a> {% cite leon_alonso_retratos_2000 -l 81-83 %}.

According to J.-C. Balty 2005, *Les portraits romains, 1 : Époque julio-claudienne, 1.1* (*Sculptures antiques de Chiragan (Martres-Tolosane)*, Toulouse, p. 163-179.