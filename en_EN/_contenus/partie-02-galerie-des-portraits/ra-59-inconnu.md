---
title: Bust of an intellectual (?)
id_inventaire: Ra 59
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: Approximately 210 - 220
materiau: Dokimium marble (Turkey)
hauteur: "84"
largeur: "58"
longueur: ''
profondeur: "33"
epaisseur:
id_wikidata: Q27887688
bibliographie: oui
layout: notice
type: notice
partie: 2
sous_groupe: 3
order: 300
priority: 3
id_notice: ra-59
image_principale: ra-59-1
vues: 8
image_imprime:
redirect_from:
- ra-59
- ark:/87276/a_ra_59
id_ark: a_ra_59
precisions_biblio:
- clef: balty_les_2012
  note: p. 49, 58, 60, fig. 49, 57, 58, p. 269, fig. 204
- clef: balty_les_2021
  note: p. 46-48, 223-231
- clef: cazes_musee_1999-1
  note: p. 138
- clef: du_mege_description_1835
  note: p. 112-113, n° 198
- clef: du_mege_description_1844
  note: f° 361
- clef: esperandieu_recueil_1908
  note: p. 71, n° 969
- clef: icahiers_alsaciens_darcheologie/i_cahiers_2010
  note: p. 100, fig. 25
- clef: joulin_les_1901
  note: p. 118 et pl. XVIII, n° 266 B
- clef: meischner_privatportrats_1982
  note: p. 412, n° 12, fig. 16
- clef: musee_saint-raymond_regard_1995
  note: p. 160
- clef: rachou_catalogue_1912
  note: p. 42, n° 59
- clef: roschach_catalogue_1892
  note: p. 32-33, n° 59
- clef: weski_antiquarium_1987
  note: p. 257-258, pl. 143

---

The width of the bust of this imposing portrait is quite impressive. A cloak - the *pallium* worn by men of letters, philosophers and rhetoricians since classical times - barely covers the shoulders, having slid down to right and left, baring the chest completely, the muscle structure of which is quite clearly marked. On the left, it partly covers the upper arm, and was likely held against the body by the elbow according to a style frequently found in ancient statuary; on the right, more of the shoulder is revealed and the mantle probably passed under the arm before being brought back to cover the abdomen. This rather artificial and ostentatious way of wearing the garment draws attention to the powerful torso but also highlights the whole chest and echoes the movement of the head, which is slightly tilted, and more clearly turned to the right, a gesture that accentuates the declamatory ardour or force of persuasion of 2<sup>nd</sup> and 3<sup>rd</sup> century rhetoricians and sophists.

Could it be that in the Roman world, wearing such an item of clothing and draping it in such a fashion was enough to make whoever was wearing it an intellectual, a man of letters, a philosopher or a rhetorician? Or does it simply mean that whoever had chosen to be portrayed in this manner wanted to be seen as a man of culture? In Chiragan, alongside the portraits of emperors and members of the Imperial Household (domus Augusta), there is no doubt that these are high-ranking officials of the Empire who, through their writings, were to some extent assimilated to thinkers and orators: in particular, the jurisconsults, whose role with regard to the emperor was one of the most influential during the Severan dynasty.

Everything in this magnificent portrait, from the deeply-lined forehead, the strongly etched eyebrow arches, the gaze both thoughtful and worried, the tight lips and the determined chin, betrays the character's extraordinary concentration, with none of the brutality that emanated from Caracalla's well-known and rather fearsome portraits, to which Chiragan's work nonetheless begs to be compared. Although the fringe has retained some of the considerable volume and movement of the shell-shaped strands that were popular during the late Antonine period and the reign of Septimius Severus, the short hair and beard are definitely those sported during the reign of Caracalla.

According to J.-C. Balty 2020, *Les Portraits Romains: L’époque des Sévères*, Toulouse, p. 223-231.
