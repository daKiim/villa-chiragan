---
title: The old fisherman
id_inventaire: Ra 46
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: 3<sup>rd</sup>-first third of the 4<sup>th</sup> century(?)
materiau: Göktepe marble (Turkey)
hauteur: "52"
largeur: "27"
longueur: ''
profondeur: "16"
epaisseur:
id_wikidata: Q27666104
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe:
order: 140
priority: 3
id_notice: ra-46
image_principale: ra-46-1
vues: 8
image_imprime:
redirect_from:
- ra-46
- ark:/87276/a_ra_46
id_ark: a_ra_46
precisions_biblio:
- clef: baratte_exotisme_2001
  note: p. 71
- clef: bergmann_chiragan_1999
  note: p. 55
- clef: cazes_musee_1999-1
  note: p. 111
- clef: clarac_musee_1841
  note: p. 586
- clef: du_mege_description_1835
  note: n° 133
- clef: du_mege_notice_1828
  note: n° 149
- clef: esperandieu_recueil_1908
  note: p. 62, n° 952
- clef: hannestad_tradition_1994
  note: p.135
- clef: joulin_les_1901
  note: fig. 200B, 212 E
- clef: lebegue_ecole_1889
  note: p. 10
- clef: rachou_catalogue_1912
  note: n° 46
- clef: rey-delque_vieux_1975
  note: p. 89 à 95
- clef: roschach_catalogue_1892
  note: n° 46
- clef: slavazzi_italia_1996
  note: fig. 29

---

Sculpture from the Hellenistic period ostensibly reflected an interest in people of low status, and the old and suffering. This uncompromising truth was particularly noticeable between the 3rd and 1st century BC in the Greek world. Yet the crudeness of this focus on those who generally remain invisible is tempered by the use of luxurious materials usually restricted to a small circle. Suffering, humility and street activities thus form a strange contrast with the rarest and most expensive marbles.

Several other replicas of the fisherman's prototype are known today. The most famous remains the version in the Louvre, known as the "Dying Seneca", from the former Borghese collection. But in the context of medium-size statuary comparable to the statue in *Villa* Chiragan, one work in particular, unearthed in Aphrodisias (Turkey), bears uncanny similarities to our own specimen. The white marble replica now kept in the Altes Museum in Berlin, which is remarkable for its quality and the polish of its marble, suffers in comparison: the effect of surprise and fascination afforded by the black stone of the statues discovered in Chiragan and Aphrodisias being lost in this white version {% cite hannestad_tradition_1994 bergmann_chiragan_1999 -L none -l p. 135, 138-39 -l 55 %}.

According to Léon Joulin, Alexandre Du Mège is said to have found an arm holding a fragment of the fishing net. This object has not been found among the hundreds of remains of small and medium-sized statuary unearthed in Chiragan. Conversely, a foot resting against the base of a tree, also in black marble, is still lingering in the museum's reserves, and its dimensions do not seem to be an obstacle to it belonging to this statue. What remains of a supporting structure at the back of the neck suggests that a tree trunk may have been associated with the figure. This is a characteristic of small and medium-sized late oriental statuary well represented in the workshops of Aphrodisias. This connection with the Micrasian workshops of late Antiquity should also be considered when taking into account the strong aesthetic contrast between the back of the sculpture, where the larger parts of the body, including the particularly prominent backbone, have been coarsely rendered and left unpolished, whereas the front part of the body, where the anatomy is defined but relatively streamlined, is characterised by the care that has been taken in polishing the stone {% cite hannestad_tradition_1994 -l 135 %}. The connections with Aphrodisias and oriental workshops are therefore just as convincing here as they are for the medallions of the gods, or the reliefs of Hercules.

Now famous thanks to twenty-five replicas that date back to the imperial period, the original from the 3rd century BC, was likely made of bronze, the glistening appearance of which is evoked by the glossy black marble. This old man, dressed only in a loincloth, is seen making modest offerings to the gods {% cite queyrel_sculpture_2016 -l 315 %}. The smaller replica in the Musée Saint-Raymond features offset folds of loincloth, one side of which falls rather stiffly to conceal the genitals. With some of the other replicas, the old man's genitalia are exposed, in accordance with the pathos in vogue in Hellenistic times: Chiragan's statue has therefore been toned down, making it less brutal and more decorative in nature. Thanks to this simple yet formal modification, the sculpture has a different meaning, adapted as it is to the place for which it was commissioned at a given time. The same Greek iconographic model thus underwent a series of transformations that altered its meaning, depending on the workshop that produced it.

P. Capus
