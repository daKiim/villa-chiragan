---
title: Serapis-Pluton
id_inventaire: Ra 29
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: 3<sup>rd</sup> century – first third of the 4<sup>th</sup> century(?)
materiau: Marble
hauteur: "142"
largeur: "42"
longueur: ''
profondeur: "17"
epaisseur:
id_wikidata: Q26720688
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe:
order: 50
priority: 3
id_notice: ra-29
image_principale: ra-29-1
vues: 1
image_imprime:
redirect_from:
- ra-29
- ark:/87276/a_ra_29
id_ark: a_ra_29
precisions_biblio:
- clef: beckmann_idiom_2020
  note: p. 142-143, fig. 9
- clef: cazes_musee_1999-1
  note: p. 102-103
- clef: du_mege_description_1835
  note: n° 161
- clef: du_mege_notice_1828
  note: n° 74
- clef: esperandieu_recueil_1908
  note: n° 891
- clef: galerie_dart_municipale_luxembourg_les_1989
  note: n° 96
- clef: kater-_sibbes_preliminary_1973
  note: p. 153, n° 810
- clef: massendari_haute-garonne_2006-1
  note: 'p. 256, fig. 143 '
- clef: rachou_catalogue_1912
  note: n° 29
- clef: reinach_repertoire_1897
  note: t. II/1, p. 18, fig. 9 ; t. II/2, p. 698, fig. 2 ; t. III, 8, n° 3
- clef: roschach_catalogue_1865
  note: n° 29
- clef: stirling_shifting_2016
  note: ''

---

The cult to Serapis is believed to have originated in Alexandria during the Macedonian Ptolemaic dynasty. Originally, one of the main functions of this divine entity may have been to federate the various communities that lived side by side in the Nile Delta and beyond. The name itself, Serapis, which is particularly consensual, emerged in Egypt during the Hellenistic period, as a result of the contraction of the names of two divinities, *OSOR-HAPI* or "Osiris-Apis". In Pharaonic Egypt, Osiris had been equated to the bull-god Apis, a symbol of fertility. Serapis replaced the traditional Osiris, became the spouse of the black goddess Isis, and was revered for his healing skills. The portrayal of this life-saving god in Chiragan suggests that he is likened to Pluto (Greek Hades), ruler of the underworld, in the presence of Cerberus, a three-headed dog that guarded the gates of the underworld. Although there are no fragments to link the animal to the body of the deity with any certainty, the similarity of the smooth stone decorated with mouldings seems to suggest a connection between the two figures. To the left of the three-headed dog there is another piece that shows the dog's tail, and a right foot shod with a strapped sandal that is identical to the left foot, also associated with this ensemble, despite the absence of any fragments to connect them in any way. The high relief of this healing god unearthed in the grounds of *Villa* Chiragan can be likened to an in the <a href="/images/comp-ra-29-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-29-1.jpg" alt="Hades enthroned, theatre of Hierapolis, Hierapolis Archeological Museum, © The Tourism Directorate of Denizli Directorate of Pamukkale Tourist Information"/></span>round sculpture of Hades enthroned discovered at the theatre of _Hierapolis_</a> (Pamukkale, Turkey), and to Pluto, also wearing the *modius* headdress, found at the antique theatre of *Emerita Augusta* (Merida, Extremadura). Serapis is wearing a *calathus* (or *modius* in Latin), a basket used as a grain-measure, an attribute rooted in the agricultural context of the festival in honour of Osiris that concluded the fertile flooding of the Nile and the growth of wheat. This symbol is accompanied by another, placed in front of it: the *hedjet*, a triangular mitre surmounted by a bulb-shaped dome. The white crown or *hedjet* of Upper Egypt and the red crown or *deshret* of Lower Egypt combined to form the *pschent*, the double crown worn by Pharaohs.

Were the statues of Serapis, Isis and Harpocrates brought to *Villa* Chiragan for devotional purposes, as assumed by L. Stirling, at least during certain periods {% cite stirling_shifting_2016 -l 60 %}? In the absence of convincing archaeological evidence, it is impossible to say. On the other hand, attributing a merely decorative function to such great effigies hardly seems plausible. A conservative view of these foreign gods, so enthusiastically adopted by the elite from a certain point in time, does seem feasible however, and a nostalgic owner who may have had ties with Egypt may well have decided to recreate an Isiac display within the *villa*. Although these foreign gods had been the object of much, and sometimes violent, opposition by the Roman aristocracy (it is worth remembering the middle of the first century BC in Rome, when the Senate ordered the destruction of places of Isiac worship built from private funds) {% cite dion_cassius_histoire_nodate -L none -l 40, 47 %} the gods of the Delta returned to favour under the Flavian emperors at the end of the first century AD.

It was then, during the reign of Commodus, the last emperor of the Antonine dynasty, that Isis and Serapis were honoured, namely by minting a series of coins. Following in the footsteps of the emperors of the Severan dynasty, it was the Tetrarchs at the end of the 3<sup>rd</sup> century, including the two augusti, Diocletian and Maximian Hercules, who were to elevate the gods of salvation and protection to a lofty position in the pantheon.

The great high relief of Serapis discovered at the Chiragan site could well have been inspired by a model that was particularly well known in Antiquity, and which is reflected in the coins of Alexandria minted during the reigns of Trajan and Hadrian. The god, standing, holds a sceptre in his left hand and extends his right arm over <a href="/images/comp-ra-29-2.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-29-2.jpg" alt="Persephone (in Isis), Cerberus, and Pluto (in Serapis). Provenance: Gortys, Crète, Archeological Museum of Heraklion Jebulon CC0 Wikimedia Commons"/></span>Cerberus' head</a> {% cite amandry_roman_2015 -L none -l III, 4187, 4533, 6032 %}. It is to this model that the work, now in the Musée Saint-Raymond, should be associated, and not to the type of statuary created by the Eastern Greek sculptor Bryaxis, a flamboyant creation designed using luxurious materials. The latter is thought to have represented Hades, whose identity was changed at least 50 years after the god’s creation, in favour of the new god of the Ptolemaic dynasty, whose great temple had recently been built in Alexandria.

The gods of the Delta were imported from Sicily and the island of Delos, where the religion of Serapis had become firmly established. Egyptian traders and slaves were instrumental in the spread of these cults on Italian soil. During the Empire, Serapis *calathophoros* (or basket bearer), healer and escort to the deceased, retained his role as the minor god alongside the more powerful Isis, the black goddess who promised eternal life to those who followed her. In Gallia Narbonensis, as highlighted by L. Bricault, the representations of Serapis abound in the Rhone valley, the foothills of the Alps, the Massif Central, and major urban centres from Marseilles to Vienna, including Nîmes, Arles and Glanum {% cite bricault_atlas_2001 %}. This god’s presence is less widespread in the upper Garonne valley and in Toulouse, where fine examples can nevertheless be seen: bronze busts in Montmaurin, and marble busts in Saint-Michel-du-Touch (no longer extant).

P. Capus
