---
title: Head of Minerva
id_inventaire: Ra 154
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: 2<sup>nd</sup> century
materiau: Göktepe marble (Turkey)
hauteur: "27"
longueur:
largeur: '10,7'
profondeur: '13,8'
epaisseur:
id_wikidata: Q28872036
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe:
order: 150
priority: 3
id_notice: ra-154
image_principale: ra-154-1
vues: 5
image_imprime:
redirect_from:
- ra-154
- ark:/87276/a_ra_154
id_ark: a_ra_154
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 114
- clef: darblade_audoin_nouvel_2006-1
  note: p. 17, n° 024, pl. 20-024
- clef: esperandieu_recueil_1908
  note: p. 42, n° 906
- clef: joulin_les_1901
  note: fig. 130 E
- clef: pasquier_praxite._2007
  note: ''
- clef: rachou_catalogue_1912
  note: n° 154

---

As with many other small and medium size mythological sculptures, the purpose of this representation of Minerva was to decorate a special space in the most private area in the residence. The archaic Latin goddess Minerva, who was gradually endowed with the attributes of the Greek Athena, daughter of Zeus, was the goddess of light, both diurnal and nocturnal, intelligence, warfare, crafts and techniques in general.

This museum piece could belong to a retrospective trend, a recurring tendency in the history of contemporary aesthetics, but one from which Antiquity did not depart. As far as the Roman period is concerned, resorting to a style that is known to have been popular in the past is known to have occurred during the Augustan period, at the end of the first century BC, and the beginning of the following century, as well as during the Antonine period, during the reign of Hadrian in particular, in the first half of the second century {% cite darblade_audoin_nouvel_2006 -L none -l p. 17, n° 024, pl. 20-024 %}. The popularity of certain ancient Greek prototypes never seems to have waned. Beyond the interest that these models still aroused in the 4th century and obviously well into the 5th century, there is an undeniable reverence for the works collected during the High Empire. These ancient collections were enriched in Late Antiquity with new mythological images, in keeping with their emulation of small-sized sculptures, which were abundantly produced at the time, and intended for private spaces, namely vestibules and bathing areas {% cite stirling_shifting_2016 -l 274-275 %}. Minerva's head, whose general characteristics are based on a few famous prototypes (such as the "Velletri" or "Giustiniani" types), is part of this fashion, as are dozens of other sculptures discovered in Chiragan, a number of which were found in the riverside area.

Yet the work remains unique, not so much because of the owl that stands on the goddess's helmet, as shown by the claws, both of which are still visible, but because of the very subtle stylised version of the same night bird on the back of the headpiece. With Minerva on one side and an owl on the other, this head is therefore ambiguous. The sense of ambivalence produced here, which invites the viewer to discover a work within another work, is also seen as redundant in iconographic terms, as it duplicates the deity's attribute, which is entirely merged with her own iconography, and vice versa. This zoomorphic helmet is therefore truly original, as virtually unparalleled in ancient statuary production. The owl was associated with the tutelary goddess of Athens, and became her emblem at least from the beginning of the 6th century BC onwards. The bird, which foreshadowed death, was also synonymous with vigilance, as witnessed by its piercing eyes. These eyes, like those of the goddess Athena, have always fascinated mankind. The piercing gaze of the bird of prey, capable of warding off negative forces, also explains its function as a bearer of good-luck in the Hellenic world.

The model of the helmet itself seems different from the Corinthian type to which the sculptors, painters and modellers of Antiquity have accustomed us. With its hemispherical dome, it is more reminiscent of a Chalcidian helmet. As for the paragnathides (cheek pieces), they must have existed, and were most likely made of bronze. They may have been attached to a metal rim, which would have encircled the concave lower part of the helmet. In any case, the two large mortises are evidence of a fastening system.

P. Capus
