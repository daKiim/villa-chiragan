---
title: Statuette in the manner of the Eros of Centocelle
id_inventaire: Ra 184
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: 2<sup>nd</sup> century(?)
materiau: Marble
hauteur: "30"
largeur: '15,5'
longueur: ''
profondeur: "12"
epaisseur:
id_wikidata: Q41583653
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe:
order: 90
priority: 3
id_notice: ra-184
image_principale: ra-184-1
vues: 3
image_imprime:
redirect_from:
- ra-184
- ark:/87276/a_ra_184
id_ark: a_ra_184
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 114
- clef: joulin_les_1901
  note: fig. 188 E
- clef: pasquier_praxite._2007
  note: p. 358, n° 96
- clef: rachou_catalogue_1912
  note: n° 184
  
---

This is one of the thirty antique replicas referred to as <a href="/images/comp-ra-184-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-184-1.jpg" alt="Eros Farnese of the Centocelle type, National Archaeological Museums of Naples, Marie-Lan Nguyen / Wikimedia Commons CC BY"/></span> "of the Centocelle type"</a> because of the specimen discovered during the 18<sup>th</sup> century in Centocelle on the outskirts of Rome. The work was included in the Pontifical collections at the Vatican. With a quiver at his feet, a bow in his left hand and an arrow in his right, this winged Eros looks languidly to the right. Although it has long been regarded as a replica of a masterpiece by Praxiteles or his school, because of its unexpected combinations, this iconographic type could be considered a "classicising" work. Admittedly, Eros does retain the main lines of classical Greek sculpture, such as standing with his weight on one leg, or the smooth lines of the model, but some later elements, such as the abundant curly hair, make it more difficult to determine a date and style. It is therefore likely that this Eros is a composite Roman creation that reflects widely known classical and Hellenistic artistic processes {% cite pasquier_praxite._2007 -L none -l p. 356 et 358, n° 96 %}.

This creation must have enjoyed great popularity among the Roman elite, as it was widely reproduced and spread throughout the Mediterranean basin, as shown by the small version discovered at *Villa* Chiragan. These pastiches are therefore characteristic of a type of statuary created in Roman times for purely ornamental purposes.

P. Capus
