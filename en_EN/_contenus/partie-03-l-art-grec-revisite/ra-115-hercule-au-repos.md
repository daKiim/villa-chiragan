---
title: Hercules resting
id_inventaire: Ra 115
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: 2<sup>nd</sup>- 3<sup>rd</sup> century
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: "62"
largeur: "45"
longueur: ''
profondeur: "21"
epaisseur:
id_wikidata: Q26159966
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe:
order: 80
priority: 3
id_notice: ra-115
image_principale: ra-115-1
vues: 4
image_imprime:
redirect_from:
- ra-115
- ark:/87276/a_ra_115
id_ark: a_ra_115
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 110-111
- clef: centro_de_exposiciones_arte_canal_roma_2007
  note: p. 171, n° 102
- clef: esperandieu_recueil_1908
  note: n° 893
- clef: joulin_les_1901
  note: fig. 138 D, 147 E
- clef: rachou_catalogue_1912
  note: n° 115
- clef: slavazzi_italia_1996
  note: fig. 30

---

In the 4<sup>th</sup> century BC, the Greek sculptor Lysippos designed a bronze sculpture depicting *Hercules at rest*. The prototype proved a huge success, as shown by the numerous copies and by-products from the Hellenistic and Roman periods.

In this portrayal, the hero is seen resting after having completed all the Labours imposed on him by Eurystheus, his cousin and rival. He is leaning on a club carved out of olive wood covered with the skin of the Nemean lion. In his right hand, which was held behind his back, he held the golden apples of the Garden of the Hesperides, offered to Hera in honour of her marriage to Zeus, and which Hercules had been obliged to bring back from the far West, from the kingdom of the setting sun and death, on the Atlantic coast of Morocco.

This type of statuary is well known, firstly because of <a href="/images/comp-ra-115-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-115-1.jpg" alt="Hercules Farnese, National Archaeological Museums of Naples, Inv. 6001, Marie-Lan Nguyen / Wikimedia Commons CC BY"/></span>the monumental sculpture standing almost three metres high</a>, once part of the former Farnese collection discovered in Rome in 1546 in the Baths of Caracalla, and sent to Naples in 1787, as were all prestigious finds. During the 3rd century AD in particular, Lysippos's work was reproduced on the reverse side of the coins of Caracalla and Gordian III.

Owing to a few significant modifications, the Roman variants tend to blur the vision of what the Greek original must really have been like. The version discovered at *Villa* Chiragan, could well correspond to the second of three types identified by P. Moreno {% cite moreno_lisippo_1995 -l 242-250 %}: here the lion's pelt may have been given greater importance, the tilt of the head, which is unfortunately missing in the case of our specimen, appears to be more pronounced than on other types, and the muscle mass less exaggerated than in Naples or Florence (Palazzo Pitti), where, unlike the Chiragan variant, the left leg stands in front of the right, causing the feet to be aligned. The position of the right leg alone, which is outstretched in the case of the marble statue in Toulouse, thus puts it in the same league as the *Heracles at rest* discovered in Foligno, and kept in the Louvre museum.

The right hand was held behind the back. It has since disappeared, and may have been recreated in stucco as early as Antiquity. This sculpture was not imported to Chiragan but made on site, as evidenced by the use of Pyrenean marble.

P. Capus
