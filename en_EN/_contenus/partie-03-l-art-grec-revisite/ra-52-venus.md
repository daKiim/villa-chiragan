---
title: Head of Venus
id_inventaire: Ra 52
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: 1<sup>st</sup>-2<sup>nd</sup> century
materiau: Lychnites marble (island of Paros)
hauteur: "40"
largeur: "24"
longueur: ''
profondeur: "31"
epaisseur:
id_wikidata: Q24628970
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe:
order: 10
priority: 3
id_notice: ra-52
image_principale: ra-52-1
vues: 8
image_imprime:
redirect_from:
- ra-52
- ark:/87276/a_ra_52
id_ark: a_ra_52
precisions_biblio:
- clef: bassal_venus_1996
  note: ''
- clef: bulletin_1936
  note: pl. p. 605
- clef: cazes_musee_1999-1
  note: p. 104-107
- clef: clarac_musee_1841
  note: p. 588
- clef: du_mege_description_1835
  note: n° 140
- clef: du_mege_notice_1828
  note: n° 60
- clef: esperandieu_recueil_1908
  note: n° 902
- clef: gachon_histoire_1926
  note: pl. V, p. 72-73
- clef: godechot_visages_1949
  note: p. 70
- clef: joulin_les_1901
  note: fig. 121 B
- clef: lebegue_ecole_1889
  note: p. 9
- clef: massendari_haute-garonne_2006-1
  note: 'p. 255, fig. 141 '
- clef: musee_saint-raymond_essentiel_2011
  note: p. 32-33
- clef: pages_venus_1867
  note: ''
- clef: pasquier_praxite._2007
  note: p. 182-183, n° 38
- clef: pasquier_venus_1985-1
  note: ''
- clef: praviel_toulouse_1935
  note: p. 247
- clef: pugliese_carratelli_enciclopedia_1997
  note: ''
- clef: rachou_catalogue_1912
  note: n° 52
- clef: ramet_histoire_1935
  note: p. 26-27
- clef: roschach_catalogue_1865
  note: n° 52
- clef: roschach_histoire_1904
  note: p. 211
- clef: slavazzi_italia_1996
  note: p. 41-45 et fig. 28

---

Excavations in 1826 unearthed this remarkable head of Aphrodite or Venus as she was called by the Romans. The lower edge ends at the top of the sternum, at the manubrium, and includes the start of the left shoulder. In spite of the cleaning process, which was often excessive in the 19<sup>th</sup> century, a few limestone concretions remain on the left side, and especially in the hair, as described by the Count of Clarac, Curator of Antiquities at the Louvre, who saw the head shortly after it was discovered {% cite clarac_musee_1841 -l 588 %}. The posterior part forms a pitted protrusion that is characteristic of an embedding plug that allows us to assume that the head was inserted into the body of a statue. Yet the fragments were not found in the museum's reserves, despite Alexandre Du Mège's mention of "portions of arms that appear to have been part of this statue" {% cite du_mege_description_1835 -L none -l p. 79, n° 140 %}. It may also have been fitted to a bust or even a herma (pillar).

The work became so famous that it was named "The Venus de Martres", which set it apart and bore witness to the prestige that had been bestowed on it. The expert and discerning eye of the Count of Clarac considered this Venus to be "one of the loveliest of all, if not the most beautiful", when compared to those of <a href="/images/comp-ra-52-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-52-1.jpg" alt="Medici Venus, Galerie des Offices Florence, Sailko / Wikimedia Commons CC BY"/></span>Medici</a>, <a href="/images/comp-ra-52-2.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-52-2.jpg" alt="Arles Venus, the Louvre, Alain Darles / Wikimedia Commons CC BY"/></span>Arles</a> and <a href="/images/comp-ra-52-3.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-52-3.jpg" alt="Milo Venus, the Louvre, Kimberly Vardeman / Wikimedia Commons CC BY"/></span>Milo</a>, all of which were the ultimate references in the first half of the neo-classical 19th century. Exhibited in Paris in 1867, this statue was attributed to the Greek Praxiteles {% cite pages_venus_1867 %} before being more reasonably seen as one of the replicas of the Aphrodite of Knidos, the coastal town of Caria (south-western modern-day Turkey). Although the original work created around 360 BC has since disappeared, it was copied extensively. Its reputation owes much to the testimony of Pliny the Elder, who described the sculptor in the following words: "His Venus towers above not only his previous work, but also the works of all the artists in the world" {% cite pline_lancien_histoire_nodate -L none -l XXXVI, 20 %}. The same Pliny reports that if Praxiteles had been given the opportunity to select one of his works, the artist would, without hesitation, have chosen “those which had been touched by the hands of Nicias”. The latter had indeed mastered the art of applying colours to the polished marble sculptures, and primarily those created by his fellow-artist Praxiteles.

Comparative studies of the dozens of listed replicas of the statue, considered to be one of the pinnacles of sculpture in the second classical period in Greece, have both cast doubt on the actual appearance of Praxiteles' masterpiece, and sharpened our appreciation of his various copies or variants. Thus, the descriptive information provided by some ancient authors, and the reproduction of the sculpture on the coins of Knidos, must be compared and evaluated in relation to all the replicas from the Hellenistic and Roman period, which are likely to provide us with a more or less faithful copy.

It is known that the naked body of the goddess had initially surprised and even shocked those who saw it. So much so in fact, that the people of Kos, neighbours and rivals of those of Knidos, refused the statue they had commissioned for their temple. They ceded it to the Knidians, who were perhaps more daring, but who appear to have substituted it for an already naked religious effigy of Eastern tradition mainly devoted to fertility. Moreover, this petrified body was in no way hidden. It was installed in a mainly open chapel, a small circular temple built within a sacred enclosure planted with myrtles, cypresses, plane trees and vines. Aphrodite’s right hand hovers in front of her pubic area, a gesture interpreted as an act of modesty, as if she had been surprised in a moment of intimacy, but meant to allow the goddess of fertility to draw attention to her reproductive organs. Her left hand holds a cloth draped over a bronze hydra. The presence of this vessel is an unambiguous indication that Aphrodite is bathing, which means that her slightly tilted bust may have been reflected in a washbasin. This composition, which was more than a simple genre scene depicting Aphrodite surprised while bathing, probably referred to the ritual nature of ablutions.

Something of the mystery behind this face, body and attitude still imbues the most beautiful replicas that have survived to this day. The forward-tilted head found in Martres reminds us of the graceful angle of the head of the Knidian Venus. The chignon at the back of her head reveals the graceful line of her neck, which in turn echoes the slope of her back. Long wavy strands of hair are divided by a middle parting and held in place by a smooth double band that encircles her skull. Does this band allude to Aphrodite's own embroidered band or *kestos himas* ("rolled belt")? This *cestus veneris* made the goddess irresistible, and an object of desire for anyone who approached her. Except for her slightly asymmetrical eyes, her features are regular. And unlike the harsh features of certain replicas, such as the head of the <a href="/images/comp-ra-52-4.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-52-4.jpg" alt="Borghèse Head, the Louvre, Marie-Lan Nguyen / Wikimedia Commons CC BY"/></span>Borghese Venus in the Louvre</a>, or the <a href="/images/comp-ra-52-5.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-52-5.jpg" alt="Colonna Venus, Museums at the Vatican, Wikimedia Commons Public Domain"/></span>Colonna Venus at the Vatican</a>, which most art historians consider to be closer to Praxiteles' work, the features of the Venus of Martres, or those of the <a href="/images/comp-ra-52-6.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-52-6.jpg" alt="Kaufmann head, the Louvre, Marie-Lan Nguyen / Wikimedia Commons CC BY"/></span>Kaufmann head in the Louvre</a>, seem less harsh and more subtly softened. Lifelike skin and full lips make her more sensual. Yet while she may seem more human, her sanctity remains obvious, despite her expression, which seems to be in the act of drifting into a tender dreamlike state.

Although the Kaufmann head, discovered in Tralles in Turkey, and dating back to the 2nd century BC, could be seen as a reinterpretation of the Hellenistic period, {% cite pasquier_venus_1985 -l 58-59 %}, it is impossible to date the Venus of Martres with the same degree of accuracy. Its marble, analysed in 2011, is indeed Greek, and comes from Paros. This alone cannot be used as an argument to date the work, as Aegean Island marbles were still being used throughout the High Roman Empire. At the most, one could venture a date before the second half of the 2nd century AD. It has been suggested that it was probably made during the middle of the 1st century AD, and by an oriental workshop {% cite slavazzi_italia_1996 -l 186-187 %}. It is a well-known fact that during the first two centuries AD, the great classics of Greek art became very popular in the imperial sphere, as well as in some luxurious Italian or provincial estates. And it appears that Chiragan did not ignore this trend, as can be seen by the small marble tablets related to the so-called neo-Attic artistic movement, the two figures of Athena, as well as a whole series of small artefacts that are replicas, or variants, of Greek originals.

P. Capus
