---
title: Athena
id_inventaire: Ra 112
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: 2<sup>nd</sup> century
materiau: Marble
hauteur: '14,5'
largeur: "52"
longueur: ''
profondeur: "32"
epaisseur:
id_wikidata: Q25114990
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe:
order: 20
priority: 3
id_notice: ra-112
image_principale: ra-112-1
image_imprime:
vues: 3
redirect_from:
- ra-112
- ark:/87276/a_ra_112
id_ark: a_ra_112
precisions_biblio:
- clef: bulletin_1936
  note: p. 830
- clef: cazes_musee_1999-1
  note: p. 107-108
- clef: daltrop_athena_1983
  note: ''
- clef: daltrop_il_1980
  note: ''
- clef: esperandieu_recueil_1908
  note: n° 911
- clef: jamot_lathena_1930
  note: p. 59-70, 1 pl.
- clef: joulin_les_1901
  note: n° 148 D
- clef: lechat_notes_1910
  note: p. 137 ss
- clef: lechat_notes_1913
  note: p. 130 ss
- clef: michon_seance_1908
  note: ''
- clef: musee_paul_valery_paul_1995
  note: p. 120
- clef: musee_saint-raymond_essentiel_2011
  note: p. 38-39
- clef: pollak_athena_1909
  note: ''
- clef: rachou_catalogue_1912
  note: n° 112
- clef: reinach_courrier_1910
  note: p. 78
- clef: reinach_repertoire_1897
  note: t. II, vol. II, p. 674, fig. 2
- clef: sauer_athena-marsyasgruppe_1907
  note: ''
- clef: sauer_marsyasgruppe_1908
  note: pl. III-IV
- clef: slavazzi_italia_1996
  note: p. 30-31, fig. 25, p. 182-183

---

The large sculpture of Athena discovered at *Villa* Chiragan is a reproduction of one of the works which, according to written sources, were created, by Myron of Eleutherae, an artist who was active in 450/440 BC. The goddess, originally made of bronze, was accompanied by another sculpture made from the same alloy, representing a silenus named Marsyas, a member of the procession of Dionysus {% cite boardman_greek_1985 junker_athena-marsyas-gruppe_2002 -L none -l fig. 61-64 -L none -l p. 130-131, Pl. 1-6 et p. 138-139, Pl. 11-14 %}. Part human and part animal, this hirsute being, who originated in Phrygia, a region located in the heart of Asia Minor, has been widely represented in Greek art. The pair he formed with Athena was created for the Acropolis, the most important religious site in Athens. The staging of these two mythological beings may have originated in the performance of a lyrical theatrical work dedicated to Dionysus, a dithyramb written by a poet of the 5th century BC known as Melanippides. Six centuries after its creation, Pausanias, a Greek writer of the 2nd century, described this bronze pair, the fame of which was such that many variants were designed for the most beautiful estates of the Empire in his time {% cite pausanias_periegese_nodate -L none -l I, 24, 1 %}.

This marble image reproduced by workshops specialised in making copies thus more or less accurately reflected the moment in history to which Myron's bronze originally referred. The episode chosen by the Greek sculptor was that of the rejection of the *aulos*, the double flute, by the wise and radiant Athena, who had just realised how disfigured she looked when she blew into this instrument {% cite plutarque_cohibenda_nodate -L none -l 6, 456 b %}. Marsyas then appears and goes to pick up the instrument, but checks himself because of the goddess's injunction forbidding anyone to use it. Satyrs are known to be music lovers, and Marsyas himself is intimately associated with wind instruments such as the *aulos* and *syrinx* (pan flute). The numerous musical exercises, and the efforts the creature made to tame the instrument forsaken by the goddess subsequently resulted in Dionysus’s companion provoking Apollo, the god of music *par excellence*, and intimately associated with another family of instruments, the strings. Marsyas ended up paying a high price; at the end of a competition (*agon*) that he had himself initiated, he was defeated by the Olympian deity, protector of poetry, who had him hung from a tree and flayed alive. Thus the lyre prevailed over the *aulos* {% cite plutarque_symposiaques_nodate -L none -l VII8. 713b %}.

The silenus in Myron's bronze ensemble is best known by the beautiful and powerful replica preserved in the <a href="/images/comp-ra-112-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-112-1.jpg" alt="Athena and Marsyas, Vatican Museums, Sailko / Wikimedia Commons CC BY"/></span>Museo Gregoriano Profano in the Vatican</a>. The variants from the <a href="/images/comp-ra-112-2.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-112-2.jpg" alt="Athena of Myron, inv. 195 Liebieghaus Museum, Frankfurt, Carole Raddato / Wikimedia Commons CC BY-SA"/></span>Liebieghaus in Frankfurt</a>, as well as those from the Louvre, the Prado, or the Lancelotti collection in Rome show a classical-style sculpture, the dynamic gestures and graceful attitudes of which are reminiscent of a choreography that tends to distinguish Myron from some of his contemporaries, above all Phidias. None of the numerous fragments kept in the Musée Saint-Raymond prove, however, that Marsyas stood alongside Athena at *Villa* Chiragan.

When comparing the tunic (*peplos*) worn by the goddess with other Roman replicas, one notices a greater variety in the rendering of the folds on the specimen in Toulouse. The general structure of the work remains consistent with other known and previously mentioned works. Athena held the lance, of which only a fragment remains, against her right leg, on which she rests. Her recoil, as the shaggy Marsyas emerges, is emphasised by the slight bend and rotation of her left leg. With her left arm outstretched, the goddess orders the silenus to move away, forbidding him to touch the flutes that remained on the ground. Although this statue is now headless, the visible tension in the neck suggests that the head is turned. Finally, at the nape of the neck there are traces of a reinforcement that should not be overlooked. These marble props could well be characteristic of certain workshops that were in activity in the coastal cities of Pamphylia (Asia Minor) at that time. {% cite slavazzi_italia_1996 -l 140 %}. Could the statue unearthed in Chiragan have originated in this region, which is renowned for its abundant sculptural heritage?

P. Capus
