---
title: Two satyrs resting in a landscape setting
id_inventaire: Ra 32
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: '1<sup>st</sup>-2<sup>nd</sup> century '
materiau: Marble
hauteur: "35"
largeur: "35"
longueur: ''
profondeur: "8"
epaisseur:
id_wikidata: Q28732530
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe:
order: 120
priority: 3
id_notice: ra-32
image_principale: ra-32-1
vues: 1
image_imprime:
redirect_from:
- ra-32
- ark:/87276/a_ra_32
id_ark: a_ra_32
precisions_biblio:
- clef: du_mege_description_1835
  note: n° 183
- clef: du_mege_notice_1828
  note: n° 91
- clef: esperandieu_recueil_1908
  note: n° 932
- clef: joulin_les_1901
  note: n° 171
- clef: massendari_haute-garonne_2006-1
  note: p. 257, fig. 145
- clef: rachou_catalogue_1912
  note: n° 32
- clef: roschach_catalogue_1892
  note: n° 32

---

In addition to the tablet of the abduction of Persephone, which is a very rare type of work, a second marble *pinax* was also found at the *villa* site. It represents two satyrs, male and female, instantly recognisable by their goat feet. The scene is set in front of a cave, at the foot of a stone pine (*Pinus pinea*) or umbrella pine. The creature on the right is reclining, leaning on its left arm, around which the *pardalide* (panther pelt) is wrapped. His other arm is extended upwards towards one of the three pinecones (*strobili*) seen hanging in the tree. The smaller female satyr faces him. She sits on a rock, her body wrapped in a goatskin, one of the legs of which hangs down in front of her left shoulder. Both creatures are unfortunately headless. Only the trace of a tenon, on the right, remains of the connection between the head of the reclining creature and the surface of the small tablet. In addition to the severed heads, other parts are also missing: the left leg of the male satyr, and the right arm and leg of the female satyr, which are also broken. Finally, when comparing this relief with the picture representing Hades and Persephone, it is clear that its proportions are not quite right, a sign perhaps that this work was hastily made.

Satyrs are small pastoral spirits that evoke untamed nature, as do nymphs or silvani. This theme is intimately related to Dionysus, whose close companions and related symbols are overwhelmingly present in the decoration of the *villa*. This is also reflected in the dozens of fragments stored in the museum's reserves. The representation of satyrs, partners of the god of wild worldly forces, is therefore in perfect harmony with this Bacchic environment.

The pinecones allude to the pinecone staff (*thyrsus*) of the god of oriental origin. What’s more, the passage through the cave resonates with the sounds of another world, the threshold of which remains a powerful symbol. This theme associated with limitation, mystery and the afterlife seems to be particularly recurrent in iconography and was visibly favoured by owners who were probably interested in spiritual, philosophical and eschatological concepts. This natural cavity is also a reminder of an episode from the god’s childhood, when nursemaids placed the son of Zeus and Semele in a pine box, and hid him in a cave so that he would not be harmed by the wrathful and jealous Hera {% cite motte_lexpression_1986 -l 215 %}. Nor should we forget that in the context of the Dionysian Mysteries, certain initiations in the form of katabases (symbolic descents into the underworld) apparently took place in rocky caves {% cite burkert_les_2003 -l 95 %}. Thus, Pausanias of Magnesia evokes the "cave sanctuary of Dionysus" (*Periegesis Hellados*, V, 19, 6). The pastoral atmosphere associated with the creatures that form the procession (*thiasus*) and environment of Dionysus, also known as Bacchus, as well as a sense of spirituality are symbolised by the two natural elements that are the pine tree and the cave.

P. Capus
