---
title: Bacchus
id_inventaire: Ra 134-Ra 137
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: '3<sup>rd</sup>- 4<sup>th</sup> century'
materiau: Göktepe marble (Turkey)
hauteur: "49"
largeur: "30"
longueur: ''
profondeur: "21"
epaisseur:
id_wikidata: Q25114454
bibliographie: oui
layout: notice
type: notice
partie: 3
sous_groupe:
order: 130
priority: 3
id_notice: ra-134-ra-137
image_principale: ra-134-ra-137-1
vues: 3
image_imprime:
redirect_from:
- ra-134-ra-137
- ark:/87276/a_ra_134_ra_137
id_ark: a_ra_134_ra_137
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 111-112
- clef: esperandieu_recueil_1908
  note: p. 47 n° 919, p. 54 n° 935
- clef: joulin_les_1901
  note: fig. 155E, 160E
- clef: massendari_haute-garonne_2006-1
  note: p. 253, fig. 136
- clef: mesple_raccords_1948
  note: p. 156, fig. 1-2
- clef: musee_saint-raymond_essentiel_2011
  note: p. 36-37
- clef: musee_saint-raymond_odyssee_2013
  note: p. 32
- clef: pasquier_praxite._2007
  note: p. 340-341, n° 87
- clef: rachou_catalogue_1912
  note: n° 134, 137
- clef: stirling_learned_2005
  note: ''

---

The god Bacchus (known as Dionysos to the Greeks) is intimately associated with untamed nature and the powers of the wild world. God of wine and theatre, here he is depicted crowned and full of languor, leaning against a vine stock, to which he is also connected via numerous props and bridges. His curly hair is characterised by furrows made with a straight chisel and hollows made with a trepanning tool, a technique common to many other figures unearthed at the *villa*, both small and medium sized. This figure would have been accompanied by at least one other, perhaps <a href="/images/comp-Ra134-137-3.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-Ra134-137-3.jpg" alt="Bacchus group at Mithra temple, Museum of London, inv. 18496, Ethan Doyle White /Wikimedia Commons CC BY-SA"/></span>a panther or a satyr</a>. His body is that of a pre-adolescent in the ephebic aesthetic style that was popular in the Greek world during the 4<sup>th</sup> century BC. The androgynous body is well balanced, with sinuous shapes in keeping with those of the <a href="/images/comp-ra-134-ra-137-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-134-ra-137-1.jpg" alt="Apollon Sauroctone, the Louvre, Paris, inv. MR 78, Ma 44, Marie-Lan Nguyen /Wikimedia Commons CC BY"/></span>_Apollo Sauroctonus_</a> and more generally with the works of the Greek sculptor Praxiteles, in this case a "pastiche" {% cite pasquier_praxite._2007 -l 340 %}.

These characters, which are typical of the second classical Greek period, appear to have been revisited eight centuries later. In any case, it would appear that classical art sculptures were still popular in the domestic spheres of the elite seeking to secure their identity through aesthetic markers. The Bacchus of Chiragan may well be evidence of this. It conveys an artistic sensibility reminiscent of a series of sculptures produced by oriental workshops. Thus, the polish of the marble, the dividing chisel grooves, the location of the drill holes in the long sinuous curls, the outlined eyebrows, and the hollowed-out pupils link our juvenile god to a famous 5th-century statuette from Carthage that depicts Ganymede and the eagle, for example. Although the plinth to this Bacchus has unfortunately been lost, that of the Carthaginian mythological figure, formed by two bands divided by a gorge, is similar in type to certain plinths that support the mythological figures that were unearthed in Chiragan, such as that of the statue of Diana, discovered in the *villa* in Saint Georges-de-Montagne (Gironde). So many works that can be likened to a series of more or less fragmentary figures, including a statue of Sol (Apollo), unearthed in <a href="/images/comp-ra-134-ra-137-2.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-134-ra-137-2.jpg" alt="Villa Silahtarağa in Constantinople, Istanbul Museum, Carole Raddato CC BY-SA"/></span>_Villa_</a> Silahtarağa in Constantinople {% cite chaisemartin_les_1984 -L none -l Pl. 4-5 %}.

Dionysus/Bacchus and his entourage constitute the largest group of statuaries discovered in Chiragan; that is approximately half of the single statues: figures belonging to the god's retinue, such as satyrs, silenii, maenads and also Ariadne. But it must be said that the popularity of these deities is common to all the great estates and luxurious residences throughout the Empire. In the context of the *villae* in southwestern Gauls or the Iberian Peninsula during Late Antiquity, the obvious success of Bacchus could be the consequence of the strengthening of his role as a saviour, as well as a supporter of the good fortune of mankind {% cite stirling_learned_2005 koppel_skulpturenausstattung_1993 arasa_i_gil_decoracion_2004 -l 87 -l 201 -l 233 %}.

Notwithstanding these spiritual concerns, it is possible that the purely decorative function overrode the symbolic purpose in this case. Some have voiced the idea that it may have been used as a simple, albeit splendid, table leg {% cite pasquier_praxite._2007 -l 340 %}. There is no evidence, however, of such use. On the contrary, the vine stock, branches and foliage, now unfortunately missing, that framed and served as a backdrop to this ephebic figure, are reminiscent of the type of composition that characterises the Diana found in Saint-Georges-de-Montagne, set against a wreath of greenery. Numerous fragments in the museum's reserves, including a "barbarian's" head, prove that such staging was commonly reproduced. It would seem that these were arrangements used by oriental workshops, which, until quite late, produced a surprising quantity of in the round sculptures intended for the niches of the great aristocratic estates dotted throughout the Empire.

P. Capus
