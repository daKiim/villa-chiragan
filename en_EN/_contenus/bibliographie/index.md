---
layout: page
title: "Bibliography"
def: bibliographie
---
<section class="bibliographie-complete">

{% bibliography --query @*[author || editor || collaborator]  %}

</section>
