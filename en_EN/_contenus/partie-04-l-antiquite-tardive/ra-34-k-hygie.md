---
title: Hygieia
id_inventaire: Ra 34 k
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 3<sup>rd</sup> century
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: "76"
largeur: "73"
longueur:
profondeur: "35"
epaisseur:
id_wikidata: Q25212221
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 250
priority: 3
id_notice: ra-34-k
image_principale: ra-34-k-3
vues: 5
image_imprime:
redirect_from:
- ra-34-k
- ark:/87276/a_ra_34_k
id_ark: a_ra_34_k
precisions_biblio:
- clef: beckmann_idiom_2020
  note: p. 139
- clef: centro_de_exposiciones_arte_canal_roma_2007
  note: p. 151, n° 82
- clef: esperandieu_recueil_1908
  note: p. 32, n° 892, fig. 10
- clef: joulin_les_1901
  note: fig. 57 B
- clef: landes_dieux_1992
  note: n° 2
- clef: long_twelve_1987
  note: p. 10, 285-286
- clef: massendari_haute-garonne_2006-1
  note: p. 252-253, fig. 133
- clef: musee_saint-raymond_essentiel_2011
  note: p. 26-27
- clef: pierron_medaillons_1992
  note: p. 5-6
- clef: rachou_catalogue_1912
  note: n° 34 K

---

Hygieia was the personification of health, cleanliness and hygiene. Like her father Asclepius, she was associated with a snake that is often shown drinking from a cup, a snake cup that has since become the emblem of pharmacists. Here the reptile slithers between the goddess’s shoulders.

P. Capus
