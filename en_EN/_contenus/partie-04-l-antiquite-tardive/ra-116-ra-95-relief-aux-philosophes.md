---
title: Assembly of philosophers (?)
id_inventaire: Ra 116-Ra 95
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre: End of the 3<sup>rd</sup>-4<sup>th</sup> century
date_creation:
materiau: Marble
hauteur: 70 (Ra 116) 26 (Ra 95)
largeur: 30 (Ra 116) 35 (Ra 95)
longueur:
profondeur: 17.5 (Ra 116) 14 (Ra 95)
epaisseur:
id_wikidata: Q28732591
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 205
priority: 3
id_notice: ra-116-ra-95
image_principale: ra-116-ra-95-1
vues: 1
image_imprime:
redirect_from:
- ra-116-ra-95
- ark:/87276/a_ra_116_ra_95
id_ark: a_ra_116_ra_95
precisions_biblio:
- clef: beckmann_idiom_2020
  note: p. 142-144, fig. 10
- clef: bergmann_chiragan_1999
  note: p. 33, 36, 69, pl. 1, 3-5, 10
- clef: bergmann_ensemble_1995
  note: p. 197-205
- clef: bulletin_1936
  note: p. 831
- clef: cazes_musee_1999-1
  note: p. 117
- clef: dillon_ancient_2006
  note: ''
- clef: dontas_eikones_1960
  note: ''
- clef: du_mege_description_1835
  note: p. 95, n° 182
- clef: ensoli_aurea_2000
  note: p. 457-458, n° 53
- clef: esperandieu_recueil_1908
  note: p. 59, n° 946
- clef: hannestad_tradition_1994
  note: p. 128
- clef: hermary_socrate_1996
  note: p. 26-30
- clef: joulin_les_1901
  note: fig. 193 B et D
- clef: lebegue_notice_1891
  note: p. 416, pl. 27, 2
- clef: lippold_griechische_1912
  note: p. 54
- clef: massendari_haute-garonne_2006-1
  note: p. 258, fig. 146
- clef: rachou_catalogue_1912
  note: n° 116
- clef: richter_portraits_1965
  note: p. 118
- clef: roschach_catalogue_1865
  note: p. 24-25, n° 31
- clef: scheibler_sokrates_1989
  note: p. 80
  
---

Two fragments, reunited at the end of the 19th century, make up what remains of a large and badly damaged marble tablet. The lower part, discovered during Albert Lebègue's investigations in 1890-1891, shows a man in the foreground sitting at the foot of a herma, the upper part of which was unearthed as early as 1828-1830, during the excavations conducted by Alexandre Du Mège.

A herma is a composite sculpture that probably originated in Athens in the 6<sup>th</sup> century BC. Often inscribed with moralizing maxims, these so-called hermaic pillars could be used as gravestones, boundary stones and crossroad markers, as well as milestones to indicated distances from the heart of a city. This type of herma was hugely popular throughout the Roman world. Like the one depicted in the Chiragan relief, it boasted a sculpted, bearded head made of marble or bronze inserted into a quadrangular plinth. Here the head is facing right, but although the sculptor has favoured this more conventional profile view of the head, you only have to stand to the right of the tablet to see practically all of the protruding face, that is the whole nose, the beginning of the left brow ridge, the almost complete mouth, and the separate stands of fringe above the forehead. A wide ribbon encircles the skull horizontally and covers the top part of the forehead. It is kept in place by a thinner band that encircles the skull from the nape of the neck to the top of the head. The long, full beard consists of superimposed coils, separated by a deep horizontal furrow forming two vertical rows divided by a drill hole. The curls are made with the same tool, all of which curl around deep circular holes drilled at the ends of the strands of hair around the temples and at the back of the neck. When associated with the design of the sections of fringe, each ending in two or three strands, this working of the beard and the hairstyle are tantamount to a workshop signature, both on the reliefs depicting the Labours of Hercules and on the medallions of the gods.

Although Léon Joulin believed the noble head of this herma to be that of "Jupiter facing characters that have since been lost" {% cite joulin_les_1901 -l 105 %}, A. Hermary, taking as proof the headband that encircles the skull of this male representation, suggested that it could be Pythagoras {% cite hermary_socrate_1996 -l 29-30 %}. The a href="/images/comp-ra-116-95-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-116-95-1.jpg" alt="Bust of Pythagoras in herma Museums of the Capitole (inv. MC0594), Szilas / Wikimedia Commons CC0"/></span>herma head displayed in the Capitoline Museums</a>, now canonical and conventionally identified as being a portrait of the philosopher, could bear some similarities with our work, both in terms of the type of plinth and the long, tapered beard. Yet in this instance, the headband looks more like a turban, and the hair is nothing like the sophisticated long curls cascading onto the shoulders that are characteristic of the plinthed figure discovered in Chiragan. On this last point, <a href="/images/comp-ra-116-95-2.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-116-95-2.jpg" alt="Socrates portrait from the series of busts on shields discovered at Aphrodisias, Aphrodisias Museum, Carole Raddato / Wikimedia Commons CC BY"/></span>another portrait</a>, although not fully compliant with the relief in Toulouse, could perhaps be more akin to it. This copy is part of a series of busts on shields (*imagines clipeatae*), images of immortal gods discovered in Aphrodisias (Caria, Asia Minor) crafted between the end of the 4<sup>th</sup> century and the middle of the next. The marble scenography of the building, depicted as a philosophical school {% cite smith_late_1990 -L none -l p. 127-128 et 130 %}, is a reference to the greatest schools of thought through their main representatives. It therefore includes the head of Pythagoras, mentioned here for the sake of comparison. Discovered in 1968 among the rubble that filled the theatre, it was reunited much later with its original bust on a shield (*clipeus*) discovered in a completely different context in 1981, and engraved with the name of this illustrious intellectual {% cite smith_new_1991 -L none -l p. 142, Pl. XI, 6p. %}. Because of the very symmetrical composition of the locks of hair and beard, the portrait of Aphrodisias, with its impersonal features, which have not been influenced by any known portrait of Pythagoras, and the flat and schematic folds of the *himation* (mantle) is similar in spirit to the medallions of the gods of Chiragan. The headband, although simpler, seems more in keeping with that of the sculpture found on the site located in Haut-Garonne, and bears no relation whatsoever to the imposing cloth of the turban on the head of the herma on display at the Capitoline Museums, which has since been adopted as the canonical portrait of the mathematician.

In front of the hermaic pillar, Plato's master Socrates is easily identified thanks to his pug-nosed features, his small and very deep-set eyes under prominent brow ridges, and his baldness. Plato {% cite platon_banquet_nodate -L none -l 215b-d, 216d, 221d-222a %} and Xenophon {% cite platon_banquet_nodate -L none -l V, 7 %} had each in turn compared the features of the philosopher to those of a silenus. In his amorous speech, Alcibiades insists on this peculiarity, saying that Socrates "very much resembles the sileni found in sculptors' workshops, portrayed by them alongside a syrinx and a flute... I also maintain that he resembles the satyr Marsyas," to which he hastily added "the only difference between Marsyas and you, Socrates, is that you produce the same effect on us using only words, not instruments". In sculpture, this physiognomy, which is so very characteristic, was used to render the face of the Athenian thinker based on a model that was very popular in Rome when seeking to represent him, that is the "B-type" model, designed towards the end of the 4th century BC, which clearly tempers the caricatured silenic features {% cite ahbel-rappe_picturing_2006 -l 112-113 %}. On the relief found in Chiragan, the mantle, simply draped over the shoulders, reveals sagging pectorals. The "deliverer of souls" is seated, and his expression and the fact that his head is resting on his left hand, which has since disappeared, suggest a melancholic and meditative state of mind.

A fragment of relief, showing a now damaged bearded man's head, is believed to be akin to the marble tablets featuring Socrates. The subject stretches beyond the surface area of the relief, and encroaches on the moulded frame that seems to extend the scene depicting the philosophers {% cite cazes_musee_1999 -l 117 %}. The "Pythagorean" herma overflowed in a similar manner, as does the entire Herculean cycle, in which the figures are drawn out beyond the frames. This is indeed a feature which, without being unique in the context of ancient relief sculpture, nevertheless distinguishes, in Chiragan, the compositions of this particular workshop, commissioned to contribute to the lavish decor of the *villa* during Late Antiquity.

Representations of philosophers were frequently found in *villas*. Socrates, the founding father of philosophy, like Aristotle, were displayed in the private or semi-public setting of Roman residences at least from the beginning of the Empire {% cite zanker_maske_1995 sauron_les_2009 -L none -l p. 44, fig. 23 -L none -l p. 272, fig. 229 et p. 275 %}. The fact that they were still part of displays during Late Antiquity should come as no surprise, despite the scarcity of unearthed specimens. As mentioned above with regard to Pythagoras, several figures on shields found at Aphrodisias, in a building that seems to have been a teaching place for the elite, represented great thinkers of Classical and also Late Antiquity {% cite smith_late_1990 -l 127-155 %}. Portraits of philosophers were also presented in the form of hermae, in gardens or peristyles, centuries later, as evidenced by the relief discovered on the Chiragan estate. Hermae portraits maintained a link with the travelling messenger god Hermes, son of Zeus and Maia, after whom they were named, because of his function as a herald (*Hermes Logios*) {% cite dillon_ancient_2006 -l 31 %}. Another residence, <a href="/images/comp-ra-116-95-3.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-116-95-3.jpg" alt="Series of busts discovered in the Villa Welschbillig (Rhineland-Palatinate) Rheinisches Landemuuseum, Kleon3 / Wikimedia Commons CC BY-SA"/></span>Villa Welschbillig (Rhineland-Palatinate)</a>, is yet another example of the importance given to the ancient philosophical schools in Late Antiquity. This luxurious 4th century residence in Belgian Gaul was home to an exceptional series of hermae busts that surrounded a very large basin. Scholars and great military men made up a collection of 68 portraits, the diversity and erudition of which somewhat make up for the mediocre quality of the sculptures. Socrates in included in this group {% cite wrede_spatantike_1972 -l 46-54 %}.

The mosaics of rich homes also reveal that similar iconographic themes were popular among owners. In Germany, the stone flooring discovered in the cathedral in Cologne and dated back to the end of the Severan period, represents a series of pentagons featuring philosophers, along with their names, written in Greek. Diogenes, in the middle, rubs shoulders with Socrates in particular. Yet unlike the statuary found in Chiragan, here, only the inscription, and not the physical attributes, identify the master, who is represented as a conventional and stereotypical scholar {% cite lucciano_les_2012 -L none -l p. 11-12, ill. 1 %}.

In short, although the Philosophers' relief has been linked to the High Roman Empire, the second half of the 2nd century or, more broadly, between the 2nd and 3rd centuries, {% cite cazes_musee_1999 -l 117 %}, its date is in fact more recent, and the panel is to be associated with later productions. It thus joins the main corpus of sculptures made from Saint-Béat marble that characterised the villa from the end of the 3<sup>rd</sup> century, if not the first third of the 4th.

P. Capus
