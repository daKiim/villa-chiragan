---
title: Portrait of Galeria Valeria Eutropia (?)
id_inventaire: Ra 38 (2)
donnees_biographiques1: Wife of Maximian Herculius, mother of Maxentius and Fausta.
donnees_biographiques2: During the 50s of the 3<sup>rd</sup> century – 330
type_oeuvre: 
date_creation: End of the 3<sup>rd</sup>, early years of the 4<sup>th</sup> century
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: "31"
largeur: "28"
longueur: 
profondeur: "25"
epaisseur: 
id_wikidata: Q26707714
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 1
order: 20
priority: 3
id_notice: ra-38-2
image_principale: ra-38-2-1
vues: 8
image_imprime: 
redirect_from:
- ra-38-2
- ark:/87276/a_ra_38_2
id_ark: a_ra_38_2
precisions_biblio:
- clef: balmelle_les_2001
  note: p. 230-231, fig. 125 b
- clef: balty_les_2008
  note: p. 16-17, 96, 98 fig. 74, 76, p. 99 fig. 78, p. 102-104, 106 fig. 86
- clef: bergmann_chiragan_1999
  note: p. 34, 40, pl. 11, 1-4
- clef: cazes_musee_1999-1
  note: p. 143
- clef: ensoli_aurea_2000
  note: p. 458-459, n° 56
- clef: esperandieu_recueil_1908
  note: p. 50-51, n° 927
- clef: joulin_les_1901
  note: p. 319-320, pl. XIII
- clef: massendari_haute-garonne_2006-1
  note: p. 248, fig. 113
- clef: musee_saint-raymond_regard_1995
  note: p. 237, n° 173
- clef: rachou_catalogue_1912
  note: n° 38
- clef: rosso_image_2006
  note: p. 488-489, n° 237

---
The hairstyle is reminiscent of that of Helen, mother of Emperor Constantine, a fashion adopted at the end of the 3<sup>rd</sup> century. Women of very high rank are also seen to wear their hair in this way on the carvings on a number of sarcophagi that date back to the first half of the 4th century. A roll of hair frames the face, and a large braid forms a "turban" which, from the back of the head, rises up in flattened strips that are then slid under the top rolls, ending in the middle of the forehead in eight locks of hair that are curled with an iron. This portrait can be linked to the three other heads presented here. It is therefore a very rare dynastic group.

According to J.-C. Balty, _Les portraits romains, La Tétrarchie, 1.5_ (_Sculptures antiques de Chiragan (Martres-Tolosane)_, Toulouse, 2008, p. 95-109.