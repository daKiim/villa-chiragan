---
title: Vulcan
id_inventaire: Ra 34 d
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 3<sup>rd</sup>-first third of the 4<sup>th</sup> century
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: "47"
largeur: "31"
longueur:
profondeur: "30"
epaisseur:
id_wikidata: Q28536081
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 270
priority: 3
id_notice: ra-34-d
image_principale: ra-34-d-1
vues: 8
image_imprime:
redirect_from:
- ra-34-d
- ark:/87276/a_ra_34_d
id_ark: a_ra_34_d
precisions_biblio:
- clef: balty_les_2008
  note: p. 127, fig. 105
- clef: beckmann_idiom_2020
  note: p. 138-139, fig. 5
- clef: cazes_musee_1999-1
  note: p. 83
- clef: du_mege_description_1835
  note: n° 159
- clef: du_mege_notice_1828
  note: n° 72
- clef: esperandieu_recueil_1908
  note: n° 892, fig. 3
- clef: joulin_les_1901
  note: fig. 55 b
- clef: massendari_haute-garonne_2006-1
  note: p. 252-253, fig. 132
- clef: rachou_catalogue_1912
  note: n° 34 d
- clef: roschach_catalogue_1892
  note: n° 34 b

---

This head originally belonged to the series of medallions that represented the gods. It was therefore one of the images on a shield (*imagines clipeatae*), as suggested by the similarity in height as well as the marks that show that it has been wrenched off at the back.

Vulcan (the Greek Hephaestus) was the god of fire, forges and volcanoes. As a skilled craftsman, he designed many of the weapons used by heroes. He is portrayed here as a middle-aged man, wearing the thick conical leather cap typically worn by artisans. The strength and impression of power of this head are achieved by the very full beard in particular. The thick curls are further enhanced by deep cavities hollowed out with a trepanning tool, a kind of drill bit used to create the areas of shadow that were so characteristic of this workshop, and so popular among its sculptors, strongly influenced as they were by oriental aesthetics.

P. Capus
