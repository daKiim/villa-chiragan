---
title: Capital of a pilaster
id_inventaire: Ra 25
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: 4<sup>th</sup> century
materiau: Marble
hauteur: '35,8'
largeur: "44"
longueur: 
profondeur: '9,3'
epaisseur: 
id_wikidata: Q47546786
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 6
order: 305
priority: 3
id_notice: ra-25
image_principale: ra-25-1
vues: 1
image_imprime: 
redirect_from:
- ra-25
- ark:/87276/a_ra_25
id_ark: a_ra_25
precisions_biblio:
- clef: balmelle_les_2001
  note: p. 223-224, fig. 113
- clef: cazes_musee_1999-1
  note: p. 81
- clef: herrmann_products_2009
  note: p. 63
- clef: joulin_les_1901
  note: pl. V, n° 29 b
- clef: kramer_korinthische_1994
  note: p. 126-127, n° 7-22, pl. 1-3

---
This capital was discovered in 1842 in the _villa_'s former bathing area, a thermal complex excavated between 1840 and 1843 by Société archéologique du Midi de la France {% cite cazes_musee_1999 joulin_les_1901 -L none -l p. 75 et fig. p. 81 -L none -l p. 80, pl. V, 29 B %}. Two rows of acanthus leaves carved using a trepanning tool decorate the _calathus_ (basket). Here, the vertical pattern is formed by three centrally grooved caulicoles (stalks). Above, a third section in low relief repeats the motif of swirls and volutes that were so popular in the Corinthian decorative system from which this crowning feature originates.

This Corinthian order was however extensively renewed by the sculptors of Late Antiquity. "Corinthian-style" capitals very similar to this one existed in Asia Minor during the Tetrarchy. J. Kramer believes they were produced by a workshop that worked with marble from the quarries of _Docimium_ (Phrygia) {% cite balmelle_les_2001 -L none -l p. 223-224, fig 113 %} {% cite kramer_korinthische_1994 -L none -l p. 126-127, n° 7 to 22, l. 1-3 %}. Due perhaps to the relocation of this workshop, the style was exported, resulting in this type of capital being distributed throughout Syria, Greece and Italy {% cite herrmann_products_2009 -l 63 %}.

A particularly instructive piece of information concerning the work discovered in Chiragan comes from analysing the various marbles, a study that began in 2012. This specimen, which is exceptional in Gaul, was indeed carved from Pyrenean marble from Saint-Béat. Thus, the locally sourced material, as well as the style, which can reasonably be attributed to the 4th century, are so many clues that link this capital to the great restoration of the _villa,_ with the installation of a lavish decor created by a workshop that, although it was not oriental, had at least been trained to reproduce works that originated in _Micrasia_.

P. Capus