---
title: Attis
id_inventaire: Ra 34 l
donnees_biographiques1: 
donnees_biographiques2: 
type_oeuvre: 
date_creation: End of the 3<sup>rd</sup> century
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: "95"
largeur: "79"
longueur: 
profondeur: "43"
epaisseur: 
id_wikidata: Q26705206
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 210
priority: 3
id_notice: ra-34-l
image_principale: ra-34-l-3
vues: 5
image_imprime: 
redirect_from:
- ra-34-l
- ark:/87276/a_ra_34_l
id_ark: a_ra_34_l
precisions_biblio:
- clef: beckmann_idiom_2020
  note: p. 139
- clef: cazes_musee_1999-1
  note: p. 84
- clef: du_mege_description_1835
  note: n° 154
- clef: du_mege_notice_1828
  note: n° 67
- clef: ensoli_aurea_2000
  note: p. 458, n° 55
- clef: esperandieu_recueil_1908
  note: p. 31, n° 892, fig. 4
- clef: joulin_les_1901
  note: n° 58 B
- clef: massendari_haute-garonne_2006-1
  note: p. 252-253, fig. 134
- clef: rachou_catalogue_1912
  note: n° 34 b
- clef: roschach_catalogue_1892
  note: n° 34 e
- clef: smith_late_1990
  note: ''
- clef: ziegle_hercule_2002
  note: ''

---
In Phrygia, Attis was the consort of the goddess Cybele. The latter was very much in love with him, and in an access of jealousy, she drove him mad, causing him to cut off his genitals and transform himself into a pine tree. Every 22<sup>nd</sup> March in Rome, the procession of the "Entrance of the Tree" celebrated the death of the god. A cut pine tree, decorated with strips of red woollen cloth and musical instruments, was carried to the temple of Cybele.

The head of the young Anatolian from Chiragan has rightly been compared to that of Hippolyta, Queen of the Amazons, represented in one of the reliefs of the [Labours of Hercules](/ra-28-h) {% cite balty_les_2008 -l 126 %}. They are indeed very similar, wearing the same Phrygian cap and sporting identical curly hair. There is no doubt that they were all made by the same sculptor's workshop.

Images of deities mounted on shields are a frequent theme in Roman art. Four centuries before our era, gods were already being depicted in this way in central Italy. This association between divine entities and the circular shape of the defensive weapon was based on the famous Homeric description of the shield that the goddess Thetis gave to her son Achilles {% cite homere_iliade_nodate -L none -l XVIII, 478-617 %}. Legend has it that this precious weapon served as a cosmic symbol because it represented the Earth, with its circular shape surrounded by the waves of the ocean, and inhabited by city-building humans, either living in peace or engaged in terrible wars. In this context, the gods, as masters and great commanders of human destinies, contributed to the labours and activities of mortals, and their battles in particular. Because they were symbolic of heroism and eternity, in Rome images on shield (_imago clipeata_), whether sculpted, engraved or painted, also served the emperors' aspirations to glory.

The _tondi_ discovered in Chiragan and dated to the late 4th or early 5th century by R. Smith, can be linked to the <a href="/images/comp-ra-34-l-2.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-34-l-2.jpg" alt="Portrait of Pindare from the series of busts on shields discovered in Aphrodisias (Carie, Asie Mineure), © Feridun F. Alkaya / FlickR"/></span>series</a> discovered at Aphrodisias, in Caria {% cite smith_late_1990 -l 127-155 %}. Maybe because they are portraits and not divine representations, these oriental works appear to be more expressive than those found in Chiragan, whose faces, on the other hand, are stylistically very similar to that of the <a href="/images/comp-ra-34-l-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-34-l-1.jpg" alt="Helios/Sol of Esquilin, inv. 623, © Ole Haupt, Ny Carlsberg Glyptotek, Copenhague"/></span>Helios of Esquilin</a> kept in Copenhagen. The heads of Aphrodisias, which are more elongated and have narrower cheekbones, are also devoid of the deep circular trepan lines that are used to depict beards and hair, a characteristic that is typical of the heads of the _villa_ by the river Garonne. The manner in which strands of hair are depicted on the statues found in Chiragan are infinitely more complex. On some of the carvings, individual locks of hair are connected by small but clearly visible "bridges" within the wide trepan lines that visually divide the strands of hair. Numerous fragments, kept in the museum's reserves, bear witness to the importance of this series of _imagines clipeatae_ (images on shields) that were part of the lavish decor, which, in addition to these medallions of the gods, included reliefs of the Labours of Hercules and large pilasters covered with acanthus foliage inhabited by tiny animals.

P. Capus