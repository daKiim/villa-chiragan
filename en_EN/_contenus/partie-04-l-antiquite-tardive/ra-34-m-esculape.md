---
title: Asclepius
id_inventaire: Ra 34 m
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 3<sup>rd</sup> century
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: "78"
largeur: "75"
longueur:
profondeur: "38"
epaisseur:
id_wikidata: Q25212930
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 240
priority: 3
id_notice: ra-34-m
image_principale: ra-34-m-3
vues: 5
image_imprime:
redirect_from:
- ra-34-m
- ark:/87276/a_ra_34_m
id_ark: a_ra_34_m
precisions_biblio:
- clef: beckmann_idiom_2020
  note: p. 139
- clef: cazes_musee_1999-1
  note: p. 84
- clef: centro_de_exposiciones_arte_canal_roma_2007
  note: p. 150, n° 82
- clef: du_mege_description_1835
  note: n° 152
- clef: du_mege_notice_1828
  note: n° 65
- clef: esperandieu_recueil_1908
  note: p. 33, n° 892, fig. 12
- clef: joulin_les_1901
  note: fig. 56 B
- clef: landes_dieux_1992
  note: n° 3
- clef: long_twelve_1987
  note: p. 10, 285-286
- clef: musee_saint-raymond_essentiel_2011
  note: p. 26-27
- clef: pierron_medaillons_1992
  note: ''
- clef: rachou_catalogue_1912
  note: n° 34 m
- clef: roschach_catalogue_1892
  note: n° 34 a

---

Asclepius, or Hepius, was the god of medicine who relieved suffering and raised the dead. The serpent carved above his right shoulder is his attribute; it refers to beneficial forces, and its moult embodies its eternal rebirth. Sometimes depicted coiled around a stick, this symbol is now the emblem of medical professions.

Asclepius's prominent eyes, partly covered by wide drooping upper eyelids, are crowned by sharply angled eyebrows. Above his forehead, two large, sinuous and perfectly symmetrical, S-shaped curls are arranged on either side of a centre parting. Long and generous curls cover his ears and hang down at the nape of his neck. His beard is fuller still. Vertically and very deeply divided by trepan ridges, the rolls of hair have been divided horizontally with a chisel, and end in the front in two spirals which have been made to intertwine at Adam's apple level. Other medallions depicting male deities have been treated in a similar fashion. This is a characteristic feature of the work carried out by the workshop entrusted with this prestigious commission.

Asclepius was already present in Chiragan in the form of a statuette that predates by at least two centuries the creation of the group of divine effigies on medallions. Thus the gods of salvation, Dionysus, Isis, Serapis, Hygieiea and Asclepius, all of whom are responsible for the survival of human beings on earth and in the afterlife, constitute a particularly large proportion of the gods found at the *villa*.

P. Capus
