---
title: Venus (?)
id_inventaire: Ra 34 f
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 3<sup>rd</sup> century
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: "45"
largeur: "32"
longueur:
profondeur: "31"
epaisseur:
id_wikidata: Q28672675
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 290
priority: 3
id_notice: ra-34-f
image_principale: ra-34-f-1
vues: 8
image_imprime:
redirect_from:
- ra-34-f
- ark:/87276/a_ra_34_f
id_ark: a_ra_34_f
precisions_biblio:
- clef: du_mege_description_1835
  note: n° 157
- clef: du_mege_notice_1828
  note: n° 70
- clef: esperandieu_recueil_1908
  note: p. 32, n° 892, fig. 5
- clef: joulin_les_1901
  note: fig. 52 B
- clef: massendari_haute-garonne_2006-1
  note: p. 252-253, fig. 135
- clef: rachou_catalogue_1912
  note: n° 34 f

---

This female head could represent Venus (the Greek Aphrodite), goddess of charm, bewitchment and love. Rome's particular fondness for her is intimately linked to her status as the mother of Aeneas, the mythical ancestor of the Roman people.

Here she is shown wearing a smooth diadem, like Diana, depicted on one of the medallions of the gods, a series of which she was once a part, as evidenced by the size of this head, and the mark at the back that shows that it has been wrenched off.

P. Capus
