---
title: Hercules and king Diomedes
id_inventaire: Ra 28 i
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 3<sup>rd</sup> century
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: 38 (mare) 105 (Hercules) 31 (Diomedes)
largeur: 49 (mare) 54 (Hercules) 30 (Diomedes)
longueur:
profondeur: 13 (mare) 15 (Hercules) 16 (Diomedes)
epaisseur:
id_wikidata: Q24649999
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 110
priority: 3
id_notice: ra-28-i
image_principale: ra-28-i-1
vues: 1
image_imprime:
redirect_from:
- ra-28-i
- ark:/87276/a_ra_28_i
id_ark: a_ra_28_i
precisions_biblio:
- clef: balty_les_2008
  note: p. 47, 128, fig. 107
- clef: bergmann_chiragan_1999
  note: p. 26-43, pl. 4.2, 20.4
- clef: cazes_musee_1999-1
  note: p. 91-93
- clef: du_mege_description_1835
  note: n° 166
- clef: du_mege_notice_1828
  note: n° 78
- clef: ensoli_aurea_2000
  note: p. 457, n° 51
- clef: esperandieu_recueil_1908
  note: p. 36-37, n° 899 (3)
- clef: joulin_les_1901
  note: p. 90, pl. IX, n° 104 b
- clef: massendari_haute-garonne_2006-1
  note: p. 251, fig. 128
- clef: musee_archeologique_henri_prades_cirque_1990
  note: p. 383, n° 106, p. 388, fig. 106
- clef: musee_saint-raymond_cirque_1990-1
  note: p. 76, n° 25
- clef: rachou_catalogue_1912
  note: p. 29, n° 28 i
- clef: roschach_catalogue_1865
  note: n° 28 g
- clef: ziegle_hercule_2002
  note: p. 15

---

The eighth of twelve Labours, the battle with king Diomedes, son of the warrior god Ares/Mars, takes Heracles/Hercules to Thrace. The sovereign owned four mares named *Podargos* (swift), *Lampos* (shining), *Xanthos* (yellow) and *Deinos* (terrible). Foreigners entering the kingdom of Diomedes were put to death by the autocrat, their bodies fed to the royal horses. Hercules, entrusted by Eurystheus to capture the string, killed the sovereign and fed his body to the carnivorous animals. Once their master had been devoured, the hero was able to put the bit between the four mares' teeth {% cite euripide_folie_416 -l 380-385 %} and lead the now docile horses to his cousin's home in Mycenae.

In Greece, the iconography for this work dates back no further than the last quarter of the 6th century BC. It was hardly reproduced in the Roman world itself, apart from on 2nd and 3rd century sarcophagi that depicted the Twelve Labours, as well as a few provincial coin issues, as with a bronze from Thrace, the scene of which is reminiscent of that of Chiragan, except that the picture is reversed {% cite vollkommer_herakles_2013 jongste_twelve_1992 noauthor_rpc_nodate -l 91 -L none -l p. 19-20 et n° F3 -L none -l IV.1, 10483 %}.

Of the mares, originally represented on the relief of Chiragan, only one head remains. Despite the large amount of missing parts, the expressive power of the scene is undeniable. The hero's attitude greatly determines the magnitude of the drama being played out. Seen from behind, his back in three quarter view, and his head turned to the left, Hercules grips Diomedes firmly by the hair. The king has grabbed his attacker by the thigh. The manner in which the lion's pelt is depicted, covering the hero’s head and shoulders like a mane, is similar to that of the hair in the portrait of Emperor Maximian, discovered in the *villa*. This statue of the Tetrarch, obviously created at the same time by the same workshop, was surely displayed in a theatrical manner close to the Labours of the Son of Jupiter, a cycle that was to become a metaphor for imperial achievements.

P. Capus
