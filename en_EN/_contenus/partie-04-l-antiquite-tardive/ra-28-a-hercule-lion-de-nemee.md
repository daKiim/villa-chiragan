---
title: Naked head of Hercules
id_inventaire: Ra 28 a
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 3<sup>rd</sup> century
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: "31"
largeur: "22"
longueur:
profondeur: "20"
epaisseur:
id_wikidata: Q24649089
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 150
priority: 3
id_notice: ra-28-a
image_principale: ra-28-a-1
vues: 3
image_imprime:
redirect_from:
- ra-28-a
- ark:/87276/a_ra_28_a
id_ark: a_ra_28_a
precisions_biblio:
- clef: cazes_musee_1999-1
  note: p. 86
- clef: du_mege_description_1835
  note: n° 176
- clef: du_mege_notice_1828
  note: n° 84
- clef: esperandieu_recueil_1908
  note: p. 36, n° 899, fig. 2
- clef: joulin_les_1901
  note: fig. 111 b
- clef: massendari_haute-garonne_2006-1
  note: 'p. 250, fig. 120 '
- clef: rachou_catalogue_1912
  note: n° 28 a

---

This juvenile head was undoubtedly part of the cycle of the Labours of Hercules. It is most probably the first of them, that is the slaying of the Nemean lion in the Peloponnese {% cite cazes_musee_1999 -l 86 %}. Because the lion's fur was impervious to weapons, Hercules was obliged to strangle it, then use the beast's own claws to skin it. This lion's pelt became an armour that was to protect him in the future.

Thus, Hercules’s hairless face and the absence of the lion's fur that he used to cover his head and was obliged to wear in all subsequent Labours are so many clues towards identifying the hero's ordeal, which took the form of an initiation test. There are similar stories to the myth of the hero fighting the lion in the most ancient civilisations, namely in the Near East with the legendary tale of King Gilgamesh. The lion represented all the forces of nature that only a hero, in other words a being embodying human supremacy, could oppose before dominating them. This can be compared to the precepts of an aristocratic Greek-style education (*paideia*), in which fighting against a fantastic being, or one reputed to be invulnerable, allows the victor to rise above the others and move on to a higher level of action {% cite presicce_eracle_2013 -l 141-142 %}. Hercules, who was still young, was therefore allowed to pursue his highly heroic rather than valiant mission.

P. Capus
