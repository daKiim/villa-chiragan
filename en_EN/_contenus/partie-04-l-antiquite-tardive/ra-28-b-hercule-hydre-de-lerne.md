---
title: Hercules and the Lernaean Hydra
id_inventaire: Ra 28 b
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 3<sup>rd</sup> century
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: "141"
largeur: "88"
longueur:
profondeur: "22"
epaisseur:
id_wikidata: Q24649576
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 60
priority: 3
id_notice: ra-28-b
image_principale: ra-28-b-1
vues: 1
image_imprime:
redirect_from:
- ra-28-b
- ark:/87276/a_ra_28_b
id_ark: a_ra_28_b
precisions_biblio:
- clef: balty_les_2008
  note: p. 134, fig. 111
- clef: cazes_musee_1999-1
  note: p. 86-87
- clef: du_mege_description_1835
  note: n° 162
- clef: du_mege_notice_1828
  note: n° 75
- clef: esperandieu_recueil_1908
  note: n° 7
- clef: hannestad_skulpturenausstattung_2006
  note: ''
- clef: ig_1890
  note: ''
- clef: joulin_les_1901
  note: n° 90 B
- clef: lexicon_1994
  note: ''
- clef: massendari_haute-garonne_2006-1
  note: p. 250, fig. 121
- clef: rachou_catalogue_1912
  note: n° 28 b
- clef: roschach_catalogue_1865
  note: n° 28 b
- clef: ziegle_hercule_2002
  note: p. 11

---

This high relief is part of a series that retraced, within the *villa*, the Twelve Labours of Hercules, the Greek Heracles. Son of Alcmene, queen of Tiryns, and Zeus, king of the gods of Mount Olympus, he was initially known as Alcaeus [or Alcides, a name derived from the Greek noun alkè (ἀλχή) which refers to strength. He then went on to be known as](https://en.wikipedia.org/wiki/Heracles#cite_note-DGRBM2-1) *Hera Cleos*, "Glory of Hera" because, according to Diodorus Siculus, his magnificence had been bestowed on him by the goddess herself. {% cite diodore_de_sicile_bibliotheque_nodate -L none -l IV, 10, 1 %}. According to the Greek myth, Hera, the sister and wife of Zeus, was incensed by her spouse’s adulterous relationship, and aimed to eliminate the fruit of this illegitimate affair by any means possible. She first hastened the birth of Eurystheus, son of Sthenelus, king of Mycenae and Tiryns, thus giving the newborn child precedence over the son of Zeus in terms of his claim to the throne of the latter city. Hera pursued her vengeful ways by putting two snakes into Alcides’ cradle who, to everyone's surprise, grabbed them by the neck and throttled them. As an adult, he who had yet to be known as Heracles faced a worse fate still: Hera caused him to suffer from temporary dementia, and in his delirium, Alcides murdered his own wife, Megara, as well as all his children, believing them to be those of Eurystheus {% cite euripide_folie_nodate -L none -l 965 et suiv. %}. Once he had recovered his senses, the distraught madman, overwhelmed by the gravity of his deed, consulted the oracle of Delphi in Phocis, and had no choice but to place himself under the authority of the tyrannical goddess, to take the name of Heracles and, finally, to alienate himself by complying to the wishes of Eurystheus, who ordered him to undertake a perilous mission every year for a whole decade. These twelve trials fulfilled the demigod's need to atone for his sins. The victorious Heracles was rewarded with an act of apotheosis on the day of his death, and was thus granted immortality.

Around 460 BC the twelve Labours were depicted on the metopes of the temple of Zeus, in Olympia {% cite noauthor_lexicon_1990 -L none -l V, 1705 %}. This is our first iconographic account of these trials (*athla*), which are consistent in number and content with written sources. The presence of such a carved cycle in the great sanctuary of this city of Elis is not accidental since, according to Pindar {% cite pindare_olympiques_nodate -L none -l III, 14-18 %} and Pausanias {% cite pausanias_periegese_nodate -L none -l V, 7, 7 %}, Heracles is said to have initiated the famous games, and planted the olive trees whose branches were originally used to make crowns for the victors. Moreover, it is in Elis that the hero went on to accomplish the feat of cleaning the stables of king Augias.

For a long time, the number and nature of the hero's achievements remained uncertain. The narrative of the Labours was not coalesced into a canonical cycle until the end of the Hellenistic period, when these trials were crystallized by Diodorus Siculus and on the Roman relief known as the *Tabula Albana* {% cite noauthor_ig_1890 -L none -l XIV, n°1293 A %}. Yet Heracles's exploits were already being asserted on Attic black-figured pottery produced in the 6<sup>th</sup> century BC. The theme shows no sign of abating from then onwards, and was regularly found in fields as diverse as architectural reliefs, mosaics, sarcophagi, in the round works, gold and silversmithing, glyptic sculpture and numismatics. And the same narrative still prevailed in Late Antiquity in the large marble panels found on the site of *Villa* Chiragan.

The manner in which Hercules's body was depicted with over-developed muscles, and the position of his legs, corresponds to designs that were introduced several centuries earlier in Eastern Greece (present-day Turkey) during the Hellenistic period, and constantly reused in Roman art. The hero's pose, with his torso facing forward, leaning on one leg which is bent and seen from the side, while the other is outstretched, is reminiscent of several reliefs of this cycle. This is the posture adopted by Zeus fighting Porphyrion on the eastern frieze of the Pergamon Altar, and by Athena, in her confrontation with the Giant Alcyoneus, on the same monument. These theatrical gesticulations come with exaggerated muscles, another feature of the Hellenistic style of the Pergamon monument and its large frieze of the battle between gods and giants designed in the first half of the 2<sup>nd</sup> century BC. These are indeed age-old models whose heritage and popularity are still depicted in the high reliefs found in Chiragan.

Marianne Bergmann's comparison of these reliefs with the artwork of the workshops of Aphrodisias in Caria, in the south-east of present-day Turkey, is particularly convincing. While admitting the Hellenistic origin of such a style in these oriental workshops, the researcher wondered in particular how long such a design, marked by the eclectic nature of the models, could have set apart the sculptures produced in this region of the Empire. It seems to cover a wide span of time, and the beginning of this production can reasonably be traced back to the Julio-Claudian period, in the first century AD. Once this style had been introduced, the workshops were able to find their own bearings at an early stage before repeating these models, which were pursued until the end of Antiquity {% cite bergmann_chiragan_1999 -l 61-62 %}. Analogies with Roman sculpture from Asia Minor, and more particularly the Caria workshops, are not lacking in high reliefs devoted to Hercules. In Chiragan, the torso of the still young hero is, for example, similar to that of the giant anguiped (the lower part of the body being shaped like a serpent's tail), discovered in Silahtaraǧa {% cite chaisemartin_les_1984 -L none -l p. 44-45, Pl. 31-32 %}.

It is difficult to imagine where such decorative elements could have been located within the residence on the banks of the river Garonne. Given the size of the reliefs, it is tempting to imagine that they could have decorated the 45-m long portico that runs along the courtyard to the west {% cite cazes_musee_1999 -l 85 %}. Other areas could also have been suitably large to accommodate this cycle, which were probably displayed alongside the large pilasters with foliage inhabited by various animals and insects, also carved from marble from Saint-Béat. And it is likely that the medallions of the gods, which were made of the same material, were added to the ensemble, either right from the beginning, or later on, as their style bears signs of a later design. The cycle must have been situated in one of the main rooms of the villa, the "reception hall" according to N. Hannestad {% cite hannestad_skulpturenausstattung_2006 -l 204 %} which was indeed the ideal setting for such a lavish decor.

The coins that were minted during the 3<sup>rd</sup> century, at the time of the Soldier Emperors as well as under the Tetrarchy, were a precious source of information and dissemination of imperial ideology, and allow us to understand the important role that Hercules may have played. In 268, workshop II (probably in Cologne) minted an extraordinary series of gold coins for Postumus, the Gallic emperor and usurper (260-269), on the occasion of the 10th anniversary of his reign (*decennalia*). {% cite bastien_les_1958 -l 61 %}. Intended to be given to members of the political and military elite, these issues represent the Twelve Labours in their entirety, as well as Hercules in his struggle against the giant Antaeus. The hero, who had also been given pride of place on the issues of Gordian III coins, was also privileged under the legitimate emperor Gallian (260-268), a contemporary of Postumus, and, of course, by the various representatives of the Tetrarchy between 293 and 305. Maximian in particular, made the son of Jupiter and Alcmene his tutelary deity, and assimilated himself to him as part of his image-driven "propaganda". In his compelling demonstration, Jean-Charles Balty showed the possible associations between the dynastic group, centred on this Hercules Augustus, and the hero with whom the Tetrarch identified {% cite balty_les_2008 -l 133-137 %}. Surely it was the responsibility of the co-emperor in charge of the West to fight the Burgundians, Alemanni and Heruli in Rhineland, the Bagaudae in Gaul, pirates in Spain and Berbers in Mauritania, and thus lead each conflict to a victorious outcome that would elevate the emperor in an act of apotheosis? Moreover, the consistency of workshop styles and practices shows how remarkably similar in appearance the august Emperor and his tutelary hero actually look in this huge collection of marble works discovered in Chiragan. Exactly like the hero, the Tetrarch Emperor is portrayed as opposing and eradicating enemies and nefarious creatures, and ensuring peace and civilization through his travels and territorial control.

In addition to the morphological differences, and the more or less elegant postures in this series of large panels created by several craftsmen, it is impossible to overlook the changes in Hercules's hair. This cannot be seen as a simple aesthetic choice on the part of one sculptor compared to another. And although the absence or presence of a beard serves to indicate a chronological development, and consequently the burden of the flow of time from young adulthood to maturity, they also illustrate a psychological and intellectual evolution on the hero's part; through reasoning, knowledge, diligence and control, the Labours are so many stages that can be compared to the development of a human being subjected to sensitive experience, who gradually grasps the truths of the soul, which is the very essence of fulfilment. Like all human beings Hercules, who has yet to become a god, is in search of the task that will bring about his metamorphosis. Because he was well and truly granted this apotheosis at the end of his long and arduous journey, he remained a model, mainly of moral rectitude, for the aristocracy during Late Antiquity, in its quest for the absolute and immortality following the stages that mark the journey of life on Earth. The fact that his image was reused in the "mixed" iconography of early Christianity testifies to this vision.

### The Lernaean Hydra, the second Labour of Hercules

The slaying of the hydra, a task Hercules is made to perform by his cousin Eurystheus acting under the authority of Hera, is considered to be the second trial, according to the agreed chronological order, notably in the writings of Diodorus {% cite diodore_de_sicile_bibliotheque_nodate -L none -l IV, 5 %} and Hyginus {% cite hygin_fables_nodate -L none -l XXX, 30 %}. The feat took place in a swamp in Lerna, near the Argolid in the Peloponnese. The hydra, whose name refers to water (ὕδωρ/*hudor*), lived there in a lair near the spring of Amymone. The beast had been raised by Juno (the roman equivalent of Hera) to put Hercules (Heracles) to the test. It possessed several heads (up to nine), the exact number varying according to the author. The head in the middle was immortal (and is portrayed as a human head on several sarcophagi during the Roman Empire). Its mouths exuded a deadly breath, comparable to the pestilence of the marshes in which it lived. Even when severed, its heads constantly grew back. While Hercules was cutting the heads off, his nephew Iolaus, who was also involved in this episode according to written sources, cauterised the wounds with burning brands to prevent them from growing back.

The relief depicts an interesting and rather unconventional hierarchy between the three protagonists. Here Hercules occupies most of the space and extends well beyond the frame, which never totally dictates the limits of the action throughout the entire cycle. The sculptors even seem to have enjoyed pushing back this boundary. The hydra, which is wrapped around Hercules's left ankle, has not been given the space generally attributed to it in Greek and Roman iconography, which allows the upper right-hand space to be devoted to Iolaus.

Nothing remains of Hercules's quiver, formerly in the background between the hero's lower limbs, as can be seen from the feathers of the arrows' fletchings, visible behind his right thigh. These arrows were later soaked by Hercules in the poisonous blood of the defeated monster. Behind the demigod’s right thigh, the griffin's head obviously refers to one of the extremities, here decorated, of the bow given to him by Apollo. Still on the subject of weapons, it is interesting to note that Hercules is wielding a club, an unusual object if ever there was one, especially when the aim is to cut off the opponent’s head. You have to go back to the 6<sup>th</sup> century BC to see Hercules using a sword on some black-figured vases from Attica and Eretria, while Iolaus wields a harpe, a weapon with a curved blade. The use of a club follows a literary tradition that was still echoed in the Bibliotheca of Pseudo-Apollodorus towards the end of the 2<sup>nd</sup> century BC: in it, Heracles strikes and crushes the heads with his bludgeon carved out of olive wood, and does not decapitate the hydra. In terms of iconographic semantics, adding a club, regardless of the circumstances, is a good way of ensuring the hero is immediately recognizable.

Young Iolaus, who was frequently depicted on these ceramics, tended to be absent in Roman times. Educated, according to Greek tradition, by the one who was perceived as his tutor and partner (*eromenos*), Iolaus plays the role of an assistant, or *parastates*, according to the terminology used by Euripides, a position that implicitly implies a romantic attachment {% cite jourdain-annequin_heracles_1986 -l 293 %}. On the relief found in Chiragan, only the top half of his body dressed in a fawn skin or nebris knotted on the left shoulder, is visible behind a rock. Although the face has unfortunately been shattered, the long, wavy hair and movement of the head, turned towards Hercules, to whom the young boy is providing support, are clearly visible. Lowering his incandescent club towards the hydra's heads, he prevents them from growing back again by cauterising them. And it is precisely because Iolaus helped Hercules to complete this feat that Eurystheus refused to ratify it, considering that it was up to Hercules alone to carry out all the Labours that he had been asked to perform {% cite pseudo-apollodore_bibliotheque_nodate -L none -l II, 5, 2 %}.

P. Capus
