---
title: Hercules and the cattle of Geryon
id_inventaire: Ra 28 l
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 3<sup>rd</sup> century
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: "155"
largeur: "103"
longueur:
profondeur: "23"
epaisseur:
id_wikidata: Q24659883
bibliographie:
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 100
priority: 3
id_notice: ra-28-l
image_principale: ra-28-l-1
vues: 1
image_imprime:
redirect_from:
- ra-28-l
- ark:/87276/a_ra_28_l
id_ark: a_ra_28_l
precisions_biblio:
- clef: bader_les_1992
  note: ''
- clef: balty_les_2008
  note: p. 129, fig. 109, 136
- clef: beckmann_idiom_2020
  note: p. 141, fig. 8
- clef: bergmann_chiragan_1999
  note: p. 26-43
- clef: bergmann_ensemble_1995
  note: p. 197-205
- clef: bernard_nec_2018
  note: ''
- clef: cazes_musee_1999-1
  note: p. 96-98
- clef: du_mege_description_1835
  note: n° 171
- clef: du_mege_notice_1828
  note: n° 81
- clef: ensoli_aurea_2000
  note: p. 457, n° 52
- clef: eppinger_hercules_2015
  note: ''
- clef: esperandieu_recueil_1908
  note: p. 38, n° 899 (8)
- clef: gricourt_taranis_1991
  note: ''
- clef: joulin_les_1901
  note: n° 110
- clef: jourdain-annequin_heracles_1992
  note: ''
- clef: jourdain-annequin_heracles_1998
  note: ''
- clef: knapp_cambridge_2014
  note: ''
- clef: massendari_haute-garonne_2006-1
  note: p. 252, fig. 130
- clef: rachou_catalogue_1912
  note: p. 29, n° 28 l
- clef: roschach_catalogue_1865
  note: n° 28 a
- clef: ziegle_hercule_2002
  note: p. 15

---

Unanimously placed in tenth position in the succession of Labours by the various ancient authors, this relief that shows Hercules overpowering Geryon nevertheless now precedes, in the museum, the trial pertaining to the man-eating horses of Diomedes and the battle against the queen of the Amazons, a choice that was determined by its size. Measuring some fifteen centimetres more, in both height and width, than all the other marble panels from the same cycle, it can only be assumed that this difference in size was motivated less by aesthetics than by political or ideological considerations. This privileged theme allows us to imagine that the killing of the three-headed giant, who resided in Hispania, was to become a key Labour at the heart of the villa. It was most probably a focal point but also the key to a story told and narrated to prestigious guests who cannot have failed to understand the veiled message put forward by the owner and commissioner of the relief.

The challenge that opposes Hercules to Geryon takes us to the Far West, to the south-east of the Iberian Peninsula, on the island of Erytheia, the "red island". In this place, the city of Gades, now Cadiz, was later founded, south of the mouth of the river Tartessos (the Guadalquivir river) {% cite strabon_geographie_nodate -L none -l III, 2, 1 %}. From this faraway land, the hero was obliged to bring back the oxen, whose red colour resembled the setting sun, as it slid into the sea. The herds grazing in evergreen moist meadows belonged to Geryon, son of Chrysaor and grandson of Poseidon/Neptune. This monster, with its three heads and three bodies or, according to other sources, one body and three heads, lived on this land surrounded by "the waters of the river Ocean" {% cite hesiode_theogonie_nodate -l 287 %}. On his way to Geryon's homeland, Hercules erected two pillars on either side of the seafaring route that took him from Europe to Africa. In the words of Diodorus, these columns or "Pillars of Hercules", now known as the Strait of Gibraltar, were meant to prevent monsters from passing from the Atlantic Ocean into the Mediterranean Sea {% cite diodore_de_sicile_bibliotheque_nodate -L none -l IV, 18 %}. The myth tells us that the city of Gades/Cadiz, founded by the Phoenicians, belongs to this region thus delimited by Hercules himself. Under the Roman Empire, the area still represented the western limit of the known world, the confines of inhabited land {% cite bernard_nec_2018 -l 27-41 %}. It was indeed the threshold of a strange and terrifying nocturnal world, dominated by a no less fearsome being. Hercules got there by sailing from the coast in the golden cup, the chariot used by the Sun, a metaphor for its daily course on its way back to its starting point {% cite bader_les_1992 -l 36 %}. Following his landing, and before being able to seize the cattle, which was the ultimate goal of this epic story, the hero needed to get rid of Eurytion the herdsman, and Orthrus ("dawn") the two-headed dog, which he killed with his club. At this, Geryon took up his arms. Whether the monster’s name refers, depending on the authors, to a single three-parted creature, or to the three sons of Chrysaor {% cite diodore_de_sicile_bibliotheque_nodate -L IV, 5, 182 %} united by a very close and sacred bond to form a single symbolic body, it is important to note the powerful contrast, which is constant in battle iconography, between Hercules rendered invincible by his sole lion's pelt and the creature of the western island, vainly armed and clad in armour. Geryon has been represented thus since at least the second half of the 7th century BC, as shown by the votive bronze cuirass of the Heraion of Samos (the Archaeological Museum in Vathy) {% cite knapp_cambridge_2014 jourdain-annequin_heracles_1998 -L none -l p. 286, Pl. XXXIV, 2 -L none -l fig. 16-7 %}.

The representation of the three-headed monster in the relief in Toulouse stems from a desire to update the myth in Roman military history by means of the uniform of a general of the High Empire, characterised by the muscle cuirass (*lorica musculata*) of Greek origin, made of metal or even boiled leather. A large general's cloak (*paludamentum*) covers the shoulders. Sewn to the *subarmalis* (a protective leather or canvas undergarment worn under the cuirass), the *pteruges* (lappets), made of fabric rather than leather that hang down from the pelvis, protect the thighs, while others hand down from the shoulders to protect the upper arms; they testified to the high status of the soldier who wore them. Below the cuirass was a double row of metal *pteruges* (found only on the cuirasses of tribunes then mostly imperial armour), decorated with the heads of wolves, griffins, lions and Medusa. Géryon tries to defend himself with a sword, unfortunately broken, held in his right hand, and to protect himself by means of a shield, the *manipule* (handle) held in his left hand, the ends of which are shaped like anchors. As for the bevelled profile of the shield, it seems to be shaped like a hexagon, which could be either Germanic or Roman. Finally, it should be noted that although Geryon's military garb is indeed in keeping with prestigious Roman armament, his headgear, which is a Phrygian bonnet, belongs to a completely different context. It is indeed quintessentially symbolic of the wild, barbaric world. An allusion to foreigners, and Oriental foreigners in particular.

So why was so much attention paid to this particular event when the sculptures for this cycle were created? Two complementary considerations have been put forward: firstly, the geographical context of the *villa*, close to the Pyrenees and the various roads leading to the Iberian Peninsula, could have lent more weight to the episode {% cite cazes_musee_1999 %}. What's more, at the end of the 3rd century, that is to say probably at the time of the creation of the cycle, the army of Emperor Maximian likely crossed the territory in order to get to Hispania, then Africa {% cite balty_les_2008 -l 129-132 %}. Seeing as Maximian himself was beginning to be seen as a Herculean figure by this time, the entire cycle of Labours was perhaps seen as an opportunity to celebrate the imperial victories in an allegorical manner... all the more so in the challenge with Geryon, which refers directly to a Spanish expedition.

In 289, the eulogy delivered by Maximian's official panegyrist, the Gallic rhetorician Mamertinus, celebrated the Emperor's victory over the rural populations of Gaul, who had rebelled against power only to be subjugated by Maximian. The text refers to them as "bi-form monsters" and likens them to the giants of mythology fought by Zeus/Jupiter. At the same time, the reverse side of gold coin issues (*aurei*), struck for Diocletian, Maximian and Constantius Chlorus, show Jupiter raising a thunderbolt above a monster with a double fish tail {% cite gricourt_taranis_1991 -l 361 %}. In other words, an anguiped whose nature and attitude are strongly reminiscent of the sculpture of the Giant discovered in Silahtaraḡa (Turkey) {% cite chaisemartin_les_1984 -L none -l Pl. 100 %}. Could it not be said that Chiragan's Geryon shares the same composition set in a similar bellicose context? And is this battle between Hercules and Geryon, much like the Gigantomachies during the Greek period, not an allegory of barbarism that has been eliminated from the city, and thus from the civilised world? Of course it could also be seen as a "model for the acculturation of the Barbarians" {% cite bonnet_heracles_1992 -L none -l p. 312-313 et 394 %}. The relief discovered in *Villa* Chiragan is devoid of any oxen, and the only thing that appears to count is the battle between the two rivals symbolised by these two figures, the monster and the hero.

Mamertinus, continuing in the same courtesan and sycophantic vein - which is the very essence of a panegyric - makes Hercules an essential figure in the battle against the enemies of the Olympians, a reference to the epithet 'Augustus', which referred to the son of Zeus/Jupiter {% cite mamertin_panegyrique_nodate -L none -l X (2), 4, 2 %}. Furthermore, the tribute indirectly evokes the battles against the usurper Carausius, who had seceded in the province of Britannia (now Great Britain), the location of his territory in the western part of the Empire being sufficient reason for this traitor to be compared to Geryon {% cite eppinger_hercules_2015 -l 187 %}. Just as the imperial praise-singer had compared Diocletian to Jupiter, the ruler of the heavens, Maximian must surely have been equated to Hercules, the peacemaker of the universe, all the more so following his victories at sea and in Gaul {% cite mamertin_panegyrique_nodate -L none -l 11, 6 %}. The laudatory text in honour of the Tetrarch also emphasises the extent to which the victory over the "hideous monster" surpasses that of Hercules over the "coarse three-headed herdsman" {% cite mamertin_panegyrique_nodate -L none -l 2, 1 %}. Could this metaphor refer to the revolt of the rural bagaudae subsequently repressed by Maximian?

P. Capus
