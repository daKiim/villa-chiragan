---
title: Hercules and the Augian stables
id_inventaire: Ra 28 j
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 3<sup>rd</sup> century
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: "138"
largeur: "85"
longueur:
profondeur: "20"
epaisseur:
id_wikidata: Q24662037
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 80
priority: 3
id_notice: ra-28-j
image_principale: ra-28-j-1
vues: 1
image_imprime:
redirect_from:
- ra-28-j
- ark:/87276/a_ra_28_j
id_ark: a_ra_28_j
precisions_biblio:
- clef: bergmann_chiragan_1999
  note: ''
- clef: cazes_musee_1999-1
  note: p. 90-91
- clef: du_mege_description_1835
  note: n° 173, 178
- clef: du_mege_notice_1828
  note: n° 83, 86
- clef: esperandieu_recueil_1908
  note: n° 899
- clef: joulin_les_1901
  note: n° 101
- clef: massendari_haute-garonne_2006-1
  note: p. 251, fig. 129
- clef: mesple_raccords_1948
  note: p. 157
- clef: rachou_catalogue_1912
  note: n° 28 j
- clef: roschach_catalogue_1865
  note: n° 28 d
- clef: sylloge_nummorum_nodate
  note: ''
- clef: ziegle_hercule_2002
  note: p. 13

---

The fifth of twelve Labours, the episode of the stables takes place in the kingdom of Elis, west of the Peloponnese. King Augias, son of Helios (the Sun) owned huge strings of horses, butut he neglected their stables to such an extent that the accumulating manure caused the surrounding land to become sterile. Eurystheus intended to humiliate Heracles/Hercules by entrusting him with this task of cleaning the stables, a chore usually only performed by slaves. To do so, the hero diverted the two rivers Alpheios and Peneios. By combining their currents, he generated enough force to clean the enclosure.

On the relief unearthed in Chiragan, only the hero is represented, his right foot resting on an overturned, unused basket. He stands watching the water, as it does this humiliating job for him. Yet again, his attitude is reminiscent of that of certain Greek gods, as portrayed at least since the sculptor Lysippus in the 4th century BC {% cite rolley_sculpture_1999 -l 333-334 %}, and subsequently reproduced in Roman art. In the meantime, this pose was reiterated during the Hellenistic period in the form of statues and on coins, in reproductions of gods, guardians and founders. Also worth remembering is the famous series of tetradrachms on which Demetrius Poliorcetes, king of Macedonia, had himself depicted, probably based on a portrait statue of Alexander the Great {% cite noauthor_sng_nodate %}. In the Roman world, this same composition, combining nudity, a foot resting on a rock, or some warlike or allegorical symbol, together with the forward-leaning body, was adapted to the political context of the end of the civil wars, and to the world of portraiture by Caesar's heir, Octavian, who was thus portrayed to suggest his new position as master of the world {% cite sutherland_roman_1984 giard_bibliotheque_1976 -l 256 -L none -l Pl. 13-14 %}. This posture was also adopted by personalities who, although they did not enjoy such a military or political aura, nevertheless received homage in the form of a portrait. Such is the case of the <a href="/images/comp-ra-28-j-1.jpg" class="comparaison"><span class="img-comparaison"><img src="/images/comp-ra-28-j-1.jpg" alt="Caius Cartilius Poplicola, Ostia Museum, Sailko / Wikimedia Commons CC BY"/></span>statue of the _duumvir_ Cartilius Poplicola</a>, discovered by the stairway to the temple of Hercules at Ostia, erected during the triumvirate in the years 40-30 BC.

As in the medium-sized statue, also from *Villa* Chiragan, depicting Hercules leaning on his club (inv. Ra 115), the hero's hand is held behind his back, a sign that he is resting after the action, in this case after having diverted the rivers Alpheios and Peneios. Finally, the change in Hercules's physiognomy is clearly visible; unlike the hairless face in the early works, here he has a full beard to prove his maturity. As his success grows, the son of Jupiter acquires wisdom, and as our hero moves closer to deification, his appearance changes to resemble those of Greco-Roman gods Jupiter, Pluto and Asclepius.

P. Capus
