---
title: Juno (?)
id_inventaire: Ra 51bis
donnees_biographiques1:
donnees_biographiques2:
type_oeuvre:
date_creation: End of the 3<sup>rd</sup> century
materiau: Saint-Béat marble (Haute-Garonne)
hauteur: '36,5'
largeur: "33"
longueur:
profondeur: "24"
epaisseur:
id_wikidata: Q27926279
bibliographie: oui
layout: notice
type: notice
partie: 4
sous_groupe: 3
order: 260
priority: 3
id_notice: ra-51-bis
image_principale: ra-51-bis-1
vues: 8
image_imprime:
redirect_from:
- ra-51-bis
- ark:/87276/a_ra_51_bis
id_ark: a_ra_51_bis
precisions_biblio:
- clef: du_mege_description_1835
  note: n° 139
- clef: du_mege_notice_1813
  note: p. 55, n° 127
- clef: du_mege_notice_1818
  note: p. 71, n° 21
- clef: du_mege_notice_1828
  note: n° 59
- clef: esperandieu_recueil_1908
  note: p. 40, n° 901
- clef: joulin_les_1901
  note: fig. 72 B
- clef: rachou_catalogue_1912
  note: n° 51 bis
- clef: roschach_catalogue_1892
  note: n° 51

---

This head could be that of Juno (the Greek Hera), wife of Jupiter (Zeus). As a protector of women, she symbolised marriage when represented wearing a veil on her head, as she is here. Her tiara is not smooth but richly decorated with acanthus scrolls, in reference, no doubt, to fertility and renewal. These leafy spirals echo those on the large pilasters that punctuated the vast space devoted to this decor from Late Antiquity. For this severed head of Juno must undoubtedly have been part of the series of medallions of the gods, a collection that probably hung from the highest parts of the walls overlooking the reliefs of the Labours of Hercules.

The particularly unsightly holes that dot the nose and lips are the legacy of 19<sup>th</sup> century restorers, who did not hesitate to drill certain parts of the works to make it easier to attach their plaster parts (generally a nose, ear or chin). These additions were subsequently removed during the last century, revealing hollows and dowel holes that detract from the aesthetic quality of the sculpture.

P. Capus
