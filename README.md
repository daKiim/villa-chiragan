# Sources du catalogue Les sculptures de la villa romaine de Chiragan
Ce dépôt rassemble les sources du catalogue _Les sculptures de la villa romaine de Chiragan_, un livre numérique et imprimé édité par [le musée Saint-Raymond, musée d'Archéologie de Toulouse](https://saintraymond.toulouse.fr/).

## Livre numérique : site web
Le livre numérique, sous forme de site web, est disponible en ligne : [https://villachiragan.saintraymond.toulouse.fr](https://villachiragan.saintraymond.toulouse.fr/)

## Livre imprimé
La version imprimée sera publiée début 2020 (date à confirmer).

## Chaîne éditoriale et système de publication
Les deux versions du site sont produites à partir d'une même source et par le biais de programmes et logiciels inspirés du domaine du développement web.
Le système de publication modulaire est fortement inspiré par [Quire](https://gettypubs.github.io/quire/), la chaîne de publication numérique de Getty Publications.

Ce catalogue est la concrétisation des principes présentés dans le mémoire de Master d'Antoine Fauchié, [Vers un système de publication modulaire, éditer avec le numérique](https://memoire.quaternum.net).

## Équipe
Liste des différentes personnes qui interviennent sur ce projet éditorial (présentation par ordre alphabétique) :

- Laure Barthet : directrice de la publication, conservateur du Patrimoine, directrice du musée Saint-Raymond, musée d’Archéologie de Toulouse ;
- [Julie Blanc](https://julie-blanc.fr/) : design graphique des versions numérique et imprimée, gestion de projet ;
- Pascal Capus : responsable des collections de sculptures romaines et numismatiques, musée Saint-Raymond, musée d’Archéologie de Toulouse ;
- [Antoine Fauchié](https://www.quaternum.net/) : développement du système de publication, gestion de projet ;
- [Louis-Olivier Brassard](https://www.loupbrun.ca/) : développement du système de publication pour l’internationalisation (i18n) du catalogue ;
- Christelle Molinié : responsable des ressources documentaires, musée Saint-Raymond, musée d’Archéologie de Toulouse ;
- équipe du musée Saint-Raymond, musée d'Archéologie de Toulouse.

## Briques logicielles
Ce catalogue peut être produit grâce à plusieurs logiciels ou programmes, chacun dispose de sa propre licence d'utilisation :

- [Jekyll](https://jekyllrb.com/) : générateur de site statique ;
- [Jekyll microtypo](https://github.com/borisschapira/jekyll-microtypo/) : plugin Jekyll pour améliorer la microtypographie ;
- [Jekyll scholar](https://github.com/inukshuk/jekyll-scholar) : plugin Jekyll pour gérer l'affichage des citations bibliographiques et des bibliographies ;
- [Zotero](https://www.zotero.org/) : logiciel et service en ligne pour gérer les références bibliographiques ;
- [Better BibTeX](https://retorque.re/zotero-better-bibtex/) : plugin Zotero pour générer les exports des bibliographies ;
- [Forestry.io](https://forestry.io/) : système de gestion de contenu pour rédiger et organiser les contenus ;
- [Netlify](https://www.netlify.com/) : service en ligne de déploiement et d'hébergement de site statique ;
- [GitLab](https://gitlab.com/) : plateforme de dépôt de code et service en ligne de développement et d'intégration continus ;
- [Sass](https://sass-lang.com/) : préprocesseur CSS pour la génération de feuilles de style en cascade ;
- [paged.js](https://www.pagedmedia.org/paged-js/) : bibliothèque open source pour paginer des pages web, autrement appelé _polyfill_.


## Signalement de problèmes avec le site web
Deux options pour signaler un problème :

- ouvrir une _issue_ sur GitLab, pour cela un compte GitLab est nécessaire : [ouvrir une nouvelle issue](https://gitlab.com/musee-saint-raymond/villa-chiragan/issues/new) ;
- écrire à [Antoine Fauchié](mailto:antoine@quaternum.net).
